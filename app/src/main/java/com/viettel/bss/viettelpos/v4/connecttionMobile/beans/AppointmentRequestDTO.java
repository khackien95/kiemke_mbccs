package com.viettel.bss.viettelpos.v4.connecttionMobile.beans;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Created by leekien on 2/8/2018.
 */
@Root(name = "AppointmentRequestDTO", strict = false)
public class AppointmentRequestDTO implements Serializable {
    @Element(name = "isdn", required = false)
    public String isdn;
    @Element(name = "reqNote", required = false)
    public String reqNote;

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getReqNote() {
        return reqNote;
    }

    public void setReqNote(String reqNote) {
        this.reqNote = reqNote;
    }

    public String getReqDateTime() {
        return reqDateTime;
    }

    public void setReqDateTime(String reqDateTime) {
        this.reqDateTime = reqDateTime;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustOrderDetailId() {
        return custOrderDetailId;
    }

    public void setCustOrderDetailId(String custOrderDetailId) {
        this.custOrderDetailId = custOrderDetailId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Element(name = "reqDatetime", required = false)
    public String reqDateTime;
    @Element(name = "reasonName", required = false)
    public String reasonName;
    @Element(name = "taskId", required = false)
    public String taskId;
    @Element(name = "id", required = false)
    public String id;
    @Element(name = "custOrderDetailId", required = false)
    public String custOrderDetailId;
    @Element(name = "status", required = false)
    public String status;

}
