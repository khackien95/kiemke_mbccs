package com.viettel.bss.viettelpos.v4.plan.screen;

import android.app.Dialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity;
import com.viettel.bss.viettelpos.v4.base.BaseFragment;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.dialog.LoginDialog;
import com.viettel.bss.viettelpos.v4.plan.adapter.TabSaleResultAdapter;
import com.viettel.bss.viettelpos.v4.plan.adapter.TabSaleWarningAdapter;
import com.viettel.bss.viettelpos.v4.plan.asynctask.GetSalePlansResultAsyncTask;
import com.viettel.bss.viettelpos.v4.plan.event.PlanEvent;
import com.viettel.bss.viettelpos.v4.plan.model.SalePlansResultModel;
import com.viettel.bss.viettelpos.v4.plan.model.SalePointInfo;
import com.viettel.bss.viettelpos.v4.plan.model.SaleResultCtv;
import com.viettel.bss.viettelpos.v4.plan.model.WsResponseSaleCheckinCTV;
import com.viettel.bss.viettelpos.v4.plan.model.WsResponseSaleResultCTV;
import com.viettel.bss.viettelpos.v4.utils.FileUtils;
import com.viettel.bss.viettelpos.v4.utils.RecyclerUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by BaVV on 1/05/18.
 */
public class TabSaleResultFragment extends BaseFragment implements TabSaleResultAdapter.SaleResultClickListener {

    @BindView(R.id.tvPlanDate)
    TextView tvPlanDate;

    @BindView(R.id.tvCheckinNum)
    TextView tvCheckinNum;

    @BindView(R.id.tvRemainNum)
    TextView tvRemainNum;

    @BindView(R.id.tvPlanNum)
    TextView tvPlanNum;

    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;

    private TabSaleResultAdapter saleResultAdapter;

    List<SalePointInfo> salePointInfoList = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_tab_sale_result;
    }

    @Override
    protected void initData() {
        RecyclerUtils.setupVerticalRecyclerView(getContext(), recyclerView.getRecyclerView());
        saleResultAdapter = new TabSaleResultAdapter(getContext(), salePointInfoList, this);
        recyclerView.setAdapter(saleResultAdapter);
    }

    @Override
    protected boolean shouldListenEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PlanEvent event) {
        PlanEvent.Action action = event.getAction();
        switch (action) {
            case RELOAD_SALE_RESULT:
                SalePlansResultModel salePlansResultModel = (SalePlansResultModel) event.getData();
                if(null != salePlansResultModel) {
                    WsResponseSaleCheckinCTV wsResponseSaleCheckinCTV = salePlansResultModel.getWsResponseSaleCheckinCTV();
                    loadData(wsResponseSaleCheckinCTV);
                }

                break;
        }
    }

    private void loadData(WsResponseSaleCheckinCTV wsResponseSaleCheckinCTV) {
        if (null != wsResponseSaleCheckinCTV) {
            tvPlanDate.setText(DateTimeUtils.convertDateTimeToString(
                    DateTimeUtils.convertStringToTime(wsResponseSaleCheckinCTV.getPlanToDate(), "yyyy-MM-dd"), "dd/MM/yyyy"));
            tvCheckinNum.setText(wsResponseSaleCheckinCTV.getCheckinNum() + "");
            tvRemainNum.setText(wsResponseSaleCheckinCTV.getRemainNum() + "");
            tvPlanNum.setText(wsResponseSaleCheckinCTV.getPlanNum() + "");

            if(null != wsResponseSaleCheckinCTV.getListSalePoint()) {
                salePointInfoList.addAll(wsResponseSaleCheckinCTV.getListSalePoint());
                saleResultAdapter.notifyDataSetChanged();
            }
        }
    }

    private void clearData() {
        if (null == salePointInfoList) {
            salePointInfoList = new ArrayList<>();
        } else {
            salePointInfoList.clear();
        }
        saleResultAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(SalePointInfo salePointInfo) {

    }
}
