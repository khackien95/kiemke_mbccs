package com.viettel.bss.viettelpos.v4.plan.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "return", strict = false)
public class SalePlansResultModel {

    @Element(name = "errorCode", required = false)
    private String errorCode;

    @Element(name = "success", required = false)
    private boolean success;

    @Element(name = "wsResponseSaleCheckinCTV", required = false)
    private WsResponseSaleCheckinCTV wsResponseSaleCheckinCTV;

    @Element(name = "wsResponseSaleResultCTV", required = false)
    private WsResponseSaleResultCTV wsResponseSaleResultCTV;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public WsResponseSaleCheckinCTV getWsResponseSaleCheckinCTV() {
        return wsResponseSaleCheckinCTV;
    }

    public void setWsResponseSaleCheckinCTV(WsResponseSaleCheckinCTV wsResponseSaleCheckinCTV) {
        this.wsResponseSaleCheckinCTV = wsResponseSaleCheckinCTV;
    }

    public WsResponseSaleResultCTV getWsResponseSaleResultCTV() {
        return wsResponseSaleResultCTV;
    }

    public void setWsResponseSaleResultCTV(WsResponseSaleResultCTV wsResponseSaleResultCTV) {
        this.wsResponseSaleResultCTV = wsResponseSaleResultCTV;
    }
}
