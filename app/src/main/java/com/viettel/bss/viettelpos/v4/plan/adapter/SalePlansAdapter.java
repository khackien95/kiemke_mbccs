package com.viettel.bss.viettelpos.v4.plan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.StringUtils;
import com.viettel.bss.viettelpos.v4.plan.model.SalePlansModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by root on 21/11/2017.
 */

public class SalePlansAdapter extends RecyclerView.Adapter<SalePlansAdapter.SalePlansHolder> {

    private Context context;
    private List<SalePlansModel> mListSubcribers;
    private SalePlansClickListener clickListener;

    public SalePlansAdapter(Context context, List<SalePlansModel> mListSubcribers, SalePlansClickListener listener) {
        this.context = context;
        this.mListSubcribers = mListSubcribers;
        this.clickListener = listener;
    }

    @Override
    public SalePlansHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SalePlansHolder(LayoutInflater.from(context).inflate(R.layout.adapter_item_sale_plan_view, parent, false));
    }

    @Override
    public void onBindViewHolder(SalePlansHolder holder, int position) {
        final SalePlansModel salePlansModel = mListSubcribers.get(position);
        if (null == salePlansModel) return;

        String objectName = "";
        int objectType = salePlansModel.getObjectType();
        String date = salePlansModel.getPlanDate();
        String address = salePlansModel.getFullAddress();

        switch (objectType) {
            case 1:
                objectName = "Trạm";
                break;
            case 2:
                objectName = "Điểm bán";
                break;
            case 3:
                objectName = "Node";
                break;
        }

        holder.tvAddress.setText(address);
        holder.tvObjectName.setText((position + 1) + ". " + objectName + " " + salePlansModel.getObjectCode());
        holder.tvObjectcode.setText(salePlansModel.getObjectCode());
        holder.tvDate.setText(StringUtils.getDateFromDateTime(date));
        holder.tvDate.setVisibility(null == date ? View.GONE : View.VISIBLE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onClick(salePlansModel);
            }
        });

        holder.imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.deletePlan(salePlansModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListSubcribers.size();
    }

    public class SalePlansHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvObjectName)
        TextView tvObjectName;

        @BindView(R.id.tvAddress)
        TextView tvAddress;

        @BindView(R.id.tvDate)
        TextView tvDate;

        @BindView(R.id.tvObjectcode)
        TextView tvObjectcode;

        @BindView(R.id.imvDelete)
        ImageView imvDelete;

        private SalePlansHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface SalePlansClickListener {
        void onClick(SalePlansModel salePlansModel);

        void editPlan(SalePlansModel salePlansModel);

        void deletePlan(SalePlansModel salePlansModel);
    }
}
