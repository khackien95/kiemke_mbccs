package com.viettel.bss.viettelpos.v4.sale.object;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Created by thinhhq1 on 4/3/2018.
 */

@Root(name = "StockOrderStaffDTO", strict = false)
public class StockOrderStaffDTO implements Serializable {
    @Element(name = "approveDate", required = false)
    private String approveDate;
    @Element(name = "approveStaffId", required = false)
    private Long approveStaffId;
    @Element(name = "cancelDate", required = false)
    private String cancelDate;
    @Element(name = "createDate", required = false)
    private String createDate;
    @Element(name = "fromOwnerId", required = false)
    private Long fromOwnerId;
    @Element(name = "fromOwnerType", required = false)
    private Long fromOwnerType;
    @Element(name = "refuseDate", required = false)
    private String refuseDate;
    @Element(name = "refuseStaffId", required = false)
    private Long refuseStaffId;
    @Element(name = "staffCode", required = false)
    private String staffCode;
    @Element(name = "staffName", required = false)
    private String staffName;
    @Element(name = "status", required = false)
    private String status;
    @Element(name = "stockOrderCode", required = false)
    private String stockOrderCode;
    @Element(name = "stockOrderId", required = false)
    private Long stockOrderId;
    @Element(name = "toOwnerId", required = false)
    private Long toOwnerId;
    @Element(name = "toOwnerType", required = false)
    private Long toOwnerType;
    @Element(name = "toStaffCode", required = false)
    private String toStaffCode;
    @Element(name = "toStaffName", required = false)
    private String toStaffName;
    @Element(name = "fromStaffCode", required = false)
    private String fromStaffCode;
    @Element(name = "fromStaffName", required = false)
    private String fromStaffName;

    public String getToStaffCode() {
        return toStaffCode;
    }

    public void setToStaffCode(String toStaffCode) {
        this.toStaffCode = toStaffCode;
    }

    public String getToStaffName() {
        return toStaffName;
    }

    public void setToStaffName(String toStaffName) {
        this.toStaffName = toStaffName;
    }

    public String getFromStaffCode() {
        return fromStaffCode;
    }

    public void setFromStaffCode(String fromStaffCode) {
        this.fromStaffCode = fromStaffCode;
    }

    public String getFromStaffName() {
        return fromStaffName;
    }

    public void setFromStaffName(String fromStaffName) {
        this.fromStaffName = fromStaffName;
    }

    public String getApproveDate() {
        return approveDate;
    }

    public void setApproveDate(String approveDate) {
        this.approveDate = approveDate;
    }

    public Long getApproveStaffId() {
        return approveStaffId;
    }

    public void setApproveStaffId(Long approveStaffId) {
        this.approveStaffId = approveStaffId;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Long getFromOwnerId() {
        return fromOwnerId;
    }

    public void setFromOwnerId(Long fromOwnerId) {
        this.fromOwnerId = fromOwnerId;
    }

    public Long getFromOwnerType() {
        return fromOwnerType;
    }

    public void setFromOwnerType(Long fromOwnerType) {
        this.fromOwnerType = fromOwnerType;
    }

    public String getRefuseDate() {
        return refuseDate;
    }

    public void setRefuseDate(String refuseDate) {
        this.refuseDate = refuseDate;
    }

    public Long getRefuseStaffId() {
        return refuseStaffId;
    }

    public void setRefuseStaffId(Long refuseStaffId) {
        this.refuseStaffId = refuseStaffId;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStockOrderCode() {
        return stockOrderCode;
    }

    public void setStockOrderCode(String stockOrderCode) {
        this.stockOrderCode = stockOrderCode;
    }

    public Long getStockOrderId() {
        return stockOrderId;
    }

    public void setStockOrderId(Long stockOrderId) {
        this.stockOrderId = stockOrderId;
    }

    public Long getToOwnerId() {
        return toOwnerId;
    }

    public void setToOwnerId(Long toOwnerId) {
        this.toOwnerId = toOwnerId;
    }

    public Long getToOwnerType() {
        return toOwnerType;
    }

    public void setToOwnerType(Long toOwnerType) {
        this.toOwnerType = toOwnerType;
    }
}
