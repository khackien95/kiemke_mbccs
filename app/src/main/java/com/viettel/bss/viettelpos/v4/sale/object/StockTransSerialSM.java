package com.viettel.bss.viettelpos.v4.sale.object;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Created by thinhhq1 on 4/3/2018.
 */
@Root(name = "StockTransSerialSM", strict = false)
public class StockTransSerialSM implements Serializable {
    @Element(name = "fromSerial", required = false)
    private String fromSerial;
    @Element(name = "prodOfferId", required = false)
    private Long prodOfferId;
    @Element(name = "stateId", required = false)
    private Long stateId;
    @Element(name = "toSerial", required = false)
    private String toSerial;

    public String getFromSerial() {
        return fromSerial;
    }

    public void setFromSerial(String fromSerial) {
        this.fromSerial = fromSerial;
    }

    public Long getProdOfferId() {
        return prodOfferId;
    }

    public void setProdOfferId(Long prodOfferId) {
        this.prodOfferId = prodOfferId;
    }

    public Long getStateId() {
        return stateId;
    }

    public void setStateId(Long stateId) {
        this.stateId = stateId;
    }

    public String getToSerial() {
        return toSerial;
    }

    public void setToSerial(String toSerial) {
        this.toSerial = toSerial;
    }
}
