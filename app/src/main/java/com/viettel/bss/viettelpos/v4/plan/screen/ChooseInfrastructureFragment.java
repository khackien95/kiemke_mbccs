package com.viettel.bss.viettelpos.v4.plan.screen;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity;
import com.viettel.bss.viettelpos.v4.base.BaseFragment;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DBOpenHelper;
import com.viettel.bss.viettelpos.v4.commons.DataUtils;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.ReplaceFragment;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.dialog.LoginDialog;
import com.viettel.bss.viettelpos.v4.object.Location;
import com.viettel.bss.viettelpos.v4.plan.adapter.ExpandableListAdapter;
import com.viettel.bss.viettelpos.v4.plan.asynctask.GetListInfrastureAsyncTask;
import com.viettel.bss.viettelpos.v4.plan.event.PlanEvent;
import com.viettel.bss.viettelpos.v4.plan.model.GetListInfrastureModel;
import com.viettel.bss.viettelpos.v4.plan.model.InfrastureModel;
import com.viettel.bss.viettelpos.v4.sale.dal.StaffDAL;
import com.viettel.bss.viettelpos.v4.sale.object.Staff;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;

import static com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity.menuCreateOption;

/**
 * Created by root on 05/01/2018.
 */

public class ChooseInfrastructureFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.listBts)
    ExpandableListView listBts;

    @BindView(R.id.float_save)
    FloatingActionButton mFlooatSave;

    private ExpandableListAdapter expandableListAdapter;
    private ArrayList<String> headers = new ArrayList<>();
    private HashMap<String, ArrayList<InfrastureModel>> childs = new HashMap<>();
    String villageCode = "";
    InfrastureModel infrastureModelSelected;
    int typeView = Constant.typeLapKeHoach;

    public static ChooseInfrastructureFragment getInstance(String villageCode, int typeView) {
        ChooseInfrastructureFragment chooseInfrastructureFragment = new ChooseInfrastructureFragment();
        Bundle bundle = new Bundle();
        bundle.putString("villageCode", villageCode);
        bundle.putInt("typeView", typeView);
        chooseInfrastructureFragment.setArguments(bundle);
        return chooseInfrastructureFragment;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.choose_infrastructure_fragment;
    }

    @Override
    protected void initUI() {
        super.initUI();
        if (getArguments() != null) {
            this.villageCode = getArguments().getString("villageCode", "");
            this.typeView = getArguments().getInt("typeView", Constant.typeLapKeHoach);
        }

        MainActivity.getInstance().setTitleActionBar(R.string.title_choose_infrastructure);
        MainActivity.getInstance().disableMenu();

        setHeader();

        headers.add(Constant.headerTram);
        headers.add(Constant.headerNode);
        headers.add(Constant.headerDiemBan);

        mFlooatSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EventBus.getDefault().post(new PlanEvent(PlanEvent.Action.CHOOSE_INFRASTRUCTURE, infrastureModelSelected));
                getActivity().onBackPressed();

//                ReplaceFragment.replaceFragmentFromMain(getActivity(),
//                        CreatePlanFragment.getInstance(infrastureModelSelected, typeView), false);
            }
        });

        childs.put(Constant.headerTram, new ArrayList<InfrastureModel>());
        childs.put(Constant.headerNode, new ArrayList<InfrastureModel>());
        childs.put(Constant.headerDiemBan, new ArrayList<InfrastureModel>());

        expandableListAdapter = new ExpandableListAdapter(getActivity(), headers, childs,
                new ExpandableListAdapter.ChooseItemListener() {
                    @Override
                    public void onChoose(View view, int groupPosition, int chilPosition, boolean checked) {
                        infrastureModelSelected = childs.get(headers.get(groupPosition))
                                .get(chilPosition);
                        childs.get(headers.get(groupPosition)).get(chilPosition).setChecked(true);
//                        if(checked) {
                        for (int i = 0; i < childs.size(); i++) {
                            if (childs.get(headers.get(i)) != null) {
                                for (int j = 0; j < childs.get(headers.get(i)).size(); j++) {
                                    if (i != groupPosition || chilPosition != j) {
                                        childs.get(headers.get(i)).get(j).setChecked(false);
                                    }
                                }
                            }

                        }
//                        }
                        expandableListAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onGroupClick(View view, int groupPosition) {
                    }
                });
        // setting list adapter
        listBts.setAdapter(expandableListAdapter);
        listBts.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (0 == groupPosition && (childs.get(Constant.headerTram) == null ||
                        childs.get(Constant.headerTram).isEmpty())) {
                    getListInfrasture("1");
                }
                if (1 == groupPosition && (childs.get(Constant.headerNode) == null ||
                        childs.get(Constant.headerNode).isEmpty())) {
                    getListInfrasture("3");
                }
                if (2 == groupPosition && (childs.get(Constant.headerDiemBan) == null ||
                        childs.get(Constant.headerDiemBan).isEmpty())) {
                    getListInfrasture("2");
                }
                return false;
            }
        });
    }

    void setHeader() {
        MenuItem menuItemView = menuCreateOption.findItem(R.id.btnListOrGridView);
        menuItemView.setVisible(false);

        MenuItem menuItemHome = menuCreateOption.findItem(R.id.btnHome);
        menuItemHome.setVisible(false);
    }

    @Override
    protected void initData() {
        super.initData();
    }

    void getListInfrasture(final String objectType) {
        DBOpenHelper dbOpenHelper;
        SQLiteDatabase database;
        try {
            dbOpenHelper = new DBOpenHelper(getActivity());
            database = dbOpenHelper.getReadableDatabase();
            StaffDAL staffDAL = new StaffDAL(database);
            Staff staff = staffDAL.getStaffByStaffCode(Session.userName);
            new GetListInfrastureAsyncTask(getActivity(), new OnPostExecuteListener<GetListInfrastureModel>() {
                @Override
                public void onPostExecute(GetListInfrastureModel result, String errorCode, String description) {

                    if (null != result && null != result.getResults()) {
                        String x = null, y = null;
                        Location location = CommonActivity.findMyLocation(getContext());
                        if (null != location) {
                            x = location.getX();
                            y = location.getY();
                        }

                        ArrayList<InfrastureModel> childList = new ArrayList<>();
                        if (typeView == Constant.typeCheckIn) {
                            for (InfrastureModel model : result.getResults()) {
                                if (DataUtils.distanceValid(x, y, model.getLat(), model.getLng()))
                                    childList.add(model);
                            }
                        } else {
                            childList.addAll(result.getResults());
                        }

                        if (CommonActivity.isNullOrEmpty(childList)) {
                            if("1".equals(objectType) || "2".equals(objectType)) {
                                CommonActivity.createAlertDialog(getContext(),
                                        "Không tìm thấy hạ tầng theo xã của nhân viên",
                                        getString(R.string.app_name)).show();
                            } else {
                                CommonActivity.createAlertDialog(getContext(),
                                        "Không tìm thấy hạ tầng theo nhân viên",
                                        getString(R.string.app_name)).show();
                            }

                            return;
                        }

                        if ("1".equals(objectType)) {
                            childs.put(Constant.headerTram, childList);
                        } else if ("2".equals(objectType)) {
                            childs.put(Constant.headerDiemBan, childList);
                        } else {
                            childs.put(Constant.headerNode, childList);
                        }
                        expandableListAdapter.notifyDataSetChanged();
                    } else {
                        CommonActivity.createAlertDialog(getContext(),
                                "Không có dữ liệu trả về",
                                getString(R.string.app_name)).show();
                    }

                }
            }, moveLogInAct).execute(String.valueOf(staff.getStaffId()), villageCode, objectType);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected final View.OnClickListener moveLogInAct = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LoginDialog dialog = new LoginDialog(getActivity(), "");
            dialog.show();
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relaBackHome:
                getActivity().onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().post(new PlanEvent(PlanEvent.Action.BACK));
    }
}
