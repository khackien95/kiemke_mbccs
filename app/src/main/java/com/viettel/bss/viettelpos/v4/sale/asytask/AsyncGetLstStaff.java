package com.viettel.bss.viettelpos.v4.sale.asytask;

import android.app.Activity;
import android.util.Log;
import android.view.View.OnClickListener;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.asyntask.AsyncTaskCommonSupper;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.sale.object.Staff;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

public class AsyncGetLstStaff extends
        AsyncTaskCommonSupper<Void, Void, ArrayList<Staff>> {

    public AsyncGetLstStaff(Activity context,
                            OnPostExecuteListener<ArrayList<Staff>> listener,
                            OnClickListener moveLogInAct) {

        super(context, listener, moveLogInAct);
    }

    @Override
    protected ArrayList<Staff> doInBackground(Void... arg0) {
        return getListStaff();
    }

    private ArrayList<Staff> getListStaff() {
        String original = "";
        ArrayList<Staff> lstStaff = new ArrayList<>();
        try {
            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode",
                    "mbccs_getStaffListFromShopId");
            String rawData = "<ws:getStaffListFromShopId>" +
                    "<input>" +
                    "<token>" + Session.getToken() +
                    "</token>" +
                    "</input>" +
                    "</ws:getStaffListFromShopId>";

            String envelope = input.buildInputGatewayWithRawData(rawData);
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope,
                    Constant.BCCS_GW_URL, mActivity,
                    "mbccs_getStaffListFromShopId");
            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);

            original = output.getOriginal();
            Log.d("originalllllllll", original);

            // parse xml
            Document doc = parse.getDomElement(original);
            NodeList nodechild = null;
            NodeList nl = doc.getElementsByTagName("return");
            for (int i = 0; i < nl.getLength(); i++) {
                Element e2 = (Element) nl.item(i);
                errorCode = parse.getValue(e2, "errorCode");
                Log.d("errorCode", errorCode);
                description = parse.getValue(e2, "description");
                Log.d("description", description);
                nodechild = doc.getElementsByTagName("lstStaff");
                for (int j = 0; j < nodechild.getLength(); j++) {
                    Element e1 = (Element) nodechild.item(j);
                    Staff staff = new Staff();
                    String name = parse.getValue(e1, "name");
                    staff.setName(name);
                    String staffCode = parse.getValue(e1, "staffCode");
                    staff.setStaffCode(staffCode);
                    String staffId = parse.getValue(e1, "staffId");
                    if (!CommonActivity.isNullOrEmpty(staffId)) {
                        staff.setStaffId(Long.parseLong(staffId));
                    }
                    String shopId = parse.getValue(e1, "shopId");
                    if (!CommonActivity.isNullOrEmpty(shopId)) {
                        staff.setShopId(Long.parseLong(shopId));
                    }
                    lstStaff.add(staff);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lstStaff;
    }

}
