package com.viettel.bss.viettelpos.v4.connecttionMobile.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.connecttionMobile.beans.RecordBean;
import com.viettel.bss.viettelpos.v4.customview.obj.FileObj;
import com.viettel.bss.viettelpos.v4.customview.obj.RecordPrepaid;
import com.viettel.bss.viettelpos.v4.fragment.UpdateProfileFragment;
import com.viettel.bss.viettelpos.v4.ui.image.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UpdateModifyProfileAdapter extends ArrayAdapter<ArrayList<RecordBean>> implements OnClickListener {

	private Context context;
	private View.OnClickListener onClickDownload;
	private View.OnClickListener onClickSelectImage;
	private List<ArrayList<RecordBean>> listRecordArray = new ArrayList<>();
	private HashMap<String, ArrayList<FileObj>> hashmapFileObj = new HashMap<>();

	public UpdateModifyProfileAdapter(Context context,
									  List<ArrayList<RecordBean>> listRecordArray,
									  HashMap<String, ArrayList<FileObj>> hashmapFileObj) {

		super(context, R.layout.layout_modify_profile_update, listRecordArray);

		this.context = context;
		this.listRecordArray = listRecordArray;
		this.hashmapFileObj = hashmapFileObj;
	}

	@Override
	public View getView(int position, View v, ViewGroup parent) {

		// TODO Auto-generated method stub
		final ViewHolder holder;
		if (v == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			v = inflater.inflate(R.layout.layout_modify_profile_update, parent,false);
			holder = new ViewHolder();
			holder.spnRecord = (Spinner) v.findViewById(R.id.spnRecord);
			holder.tvDownloadImage = (TextView) v.findViewById(R.id.tvDownloadImage);
			holder.ibUploadImage = (ImageButton) v.findViewById(R.id.ibUploadImage);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		holder.tvDownloadImage.setOnClickListener(this);
		holder.ibUploadImage.setOnClickListener(this);
		final ArrayList<RecordBean> recordBeans = listRecordArray.get(position);
		final int imageId = recordBeans.get(0).getSourceId().intValue();
		Utils.setDataSpinner(context, recordBeans, holder.spnRecord);

		holder.ibUploadImage.setImageResource(R.drawable.logo_vt);
		holder.spnRecord.setId(imageId);
		holder.ibUploadImage.setId(imageId);
		holder.tvDownloadImage.setTag(position);
		holder.ibUploadImage.setTag(position);
		holder.spnRecord.setSelection(getPositionSelected(recordBeans));

		holder.spnRecord.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				RecordBean record = (RecordBean) holder.spnRecord.getSelectedItem();

				if (record.isSelected() && hashmapFileObj.containsKey(record.getSourceId().toString())) {
					Picasso.with(context)
							.load(hashmapFileObj.get(record.getSourceId().toString()).get(0).getFile())
							.centerCrop().resize(100, 100)
							.into(holder.ibUploadImage);
				} else {
					holder.ibUploadImage.setImageResource(R.drawable.logo_vt);
					removeFileObjs(record.getSourceId().toString());
				}

				for (RecordBean recordBean : recordBeans) {
					if (record.getRecordCode().equals(recordBean.getRecordCode())) {
						recordBean.setSelected(true);
						if (CommonActivity.isNullOrEmpty(recordBean.getRecordNameScan())) {
							holder.tvDownloadImage.setText(context.getString(R.string.profile_non_scan));
						} else {
							holder.tvDownloadImage.setText(recordBean.getRecordNameScan());
						}
					} else {
						recordBean.setSelected(false);
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		return v;
	}

	private int getPositionSelected(ArrayList<RecordBean> recordBeans) {
		for (int index = 0; index < recordBeans.size(); index++) {
			if(recordBeans.get(index).isSelected()) {
				return index;
			}
		}
		return 0;
	}

	public void removeFileObjs(String key) {
		if (hashmapFileObj.containsKey(key)) {
			hashmapFileObj.remove(key);
		}
	}

	public void setOnClickDownload(View.OnClickListener onClickDownload) {
		this.onClickDownload = onClickDownload;
	}

	public void setOnClickSelectImage(View.OnClickListener onClickSelectImage) {
		this.onClickSelectImage = onClickSelectImage;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listRecordArray.size();
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public class ViewHolder {
		public Spinner spnRecord;
		public TextView tvDownloadImage;
		public ImageButton ibUploadImage;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.tvDownloadImage:
			if (onClickDownload != null) {
				onClickDownload.onClick(v);
			}
			break;

		default:
			if (onClickSelectImage != null) {
				onClickSelectImage.onClick(v);
			}
			break;
		}
	}
}