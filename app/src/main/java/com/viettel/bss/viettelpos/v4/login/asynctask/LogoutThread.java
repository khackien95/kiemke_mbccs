package com.viettel.bss.viettelpos.v4.login.asynctask;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

/**
 * Created by hungtv64 on 2/7/2018.
 */

public class LogoutThread extends Thread {

    private Activity activity;

    public LogoutThread(Activity activity) {
        super("LogoutThread");
        this.activity = activity;
    }

    @Override
    public void run() {
        super.run();

        if ("0".equals(logoutMBCCS())) {
            try{
                Toast.makeText(activity, "MBCCS logout success!", Toast.LENGTH_SHORT);
            }catch (Exception ex){
                Log.d("logoutMBCCS", ex.getMessage(), ex);
            }

        }
    }

    private String logoutMBCCS() {

        String original = "";
        ParseOuput out = null;
        String func = "logout";
        try {
            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_" + func);
            StringBuilder rawData = new StringBuilder();
            rawData.append("<ws:" + func + ">");
            rawData.append("<input>");

            rawData.append("<token>" + Session.getToken() + "</token>");

            rawData.append("</input>");
            rawData.append("</ws:" + func + ">");
            Log.i("RowData", rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData.toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope, Constant.BCCS_GW_URL,
                    activity, "mbccs_" + func);
            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i(" Original", response);

            // parser
            Serializer serializer = new Persister();
            out = serializer.read(ParseOuput.class, original);
        } catch (Exception e) {
            Log.e(Constant.TAG, "blockSubForTerminate", e);
        }
        if (CommonActivity.isNullOrEmpty(out)) {
            return "1";
        }
        return out.getErrorCode();
    }
}
