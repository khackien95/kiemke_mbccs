package com.viettel.bss.viettelpos.v4.plan.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by bavv on 1/9/18.
 */

@Root(name = "results", strict = false)
public class SaleResultCtv {
    @Element(name = "kpiCode", required = false)
    protected String kpiCode;

    @Element(name = "kpiName", required = false)
    protected String kpiName;

    @Element(name = "percentComplete", required = false)
    protected Double percentComplete;

    @Element(name = "remainPaid", required = false)
    protected Double remainPaid;

    @Element(name = "totalAchieved", required = false)
    protected Double totalAchieved;

    @Element(name = "totalPlanned", required = false)
    protected Double totalPlanned;

    public String getKpiCode() {
        return kpiCode;
    }

    public void setKpiCode(String kpiCode) {
        this.kpiCode = kpiCode;
    }

    public String getKpiName() {
        return kpiName;
    }

    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    public Double getPercentComplete() {
        return percentComplete;
    }

    public void setPercentComplete(Double percentComplete) {
        this.percentComplete = percentComplete;
    }

    public Double getRemainPaid() {
        return remainPaid;
    }

    public void setRemainPaid(Double remainPaid) {
        this.remainPaid = remainPaid;
    }

    public Double getTotalAchieved() {
        return totalAchieved;
    }

    public void setTotalAchieved(Double totalAchieved) {
        this.totalAchieved = totalAchieved;
    }

    public Double getTotalPlanned() {
        return totalPlanned;
    }

    public void setTotalPlanned(Double totalPlanned) {
        this.totalPlanned = totalPlanned;
    }
}
