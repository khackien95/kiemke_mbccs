package com.viettel.bss.viettelpos.v4.stockinpect.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.FragmentCommon;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.stockinpect.asyntask.AsyncTaskDoStockInpect;
import com.viettel.bss.viettelpos.v4.stockinpect.asyntask.AsynctaskBeforeStockInspect;
import com.viettel.bss.viettelpos.v4.stockinpect.bo.StockInspectCheckDTO;
import com.viettel.bss.viettelpos.v4.stockinpect.bo.StockInspectDTO;
import com.viettel.bss.viettelpos.v4.stockinpect.control.adapter.RecycleViewQuantityAdapter;
import com.viettel.bss.viettelpos.v4.stockinpect.control.adapter.RecycleViewSerialAdapter;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StockInpectWithQuantityActivity extends FragmentCommon {
    @BindView(R.id.recListSerialNumber)
    RecyclerView recListSerialNumber;
    @BindView(R.id.confirm_inspect_serial)
    CheckBox confirm_inspect_serial;
    @BindView(R.id.btnInspect)
    Button btnInspect;
    ArrayList<StockInspectCheckDTO> stockInspectCheckDTOS; // mat hang duoc them

    RecycleViewSerialAdapter recycleViewSerialAdapter;

    StockInspectDTO checkStockInspectDTO; //Chứa các thông tin Kiểm kê lấy từ giao diện
    StockInspectDTO stockInspectSaveDTO; //đối tượng được trả về từ hàm 1.2.9

    @Override
    public void onResume() {
        super.onResume();
        setTitleActionBar(getActivity().getString(R.string.manage_stock_quantity));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        idLayout = R.layout.inspect_stock_with_quantity;
        ButterKnife.bind(getActivity());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void unit(View v) {
        stockInspectCheckDTOS = new ArrayList<>();
//        checkStockInspectDTO = new StockInspectDTO();
//        checkStockInspectDTO.setStateId((long) 1);
//        checkStockInspectDTO.setProdOfferTypeId((long) 8);
//        checkStockInspectDTO.setInspectStatus((long) 0);
//        stockInspectSaveDTO = new StockInspectDTO();
//        stockInspectSaveDTO.setInspectStatus((long) 2);
//        stockInspectSaveDTO.setOwnerId((long) 268);
//        stockInspectSaveDTO.setOwnerType((long) 2);
//        stockInspectSaveDTO.setProdOfferId((long) 7);
//        stockInspectSaveDTO.setProdOfferTypeId((long) 8);
//        stockInspectSaveDTO.setStateId((long) 1);
//
//
//        stockInspectCheckDTOS.add(new StockInspectCheckDTO("a", Long.parseLong("52345"), (long) 114352345, false,
//                "2313", (long) 1, (long) 1, "a", (long) 1, (long) 1, (long) 1, "a", "dt",
//                (long) 1, (long) 1, (long) 1, (long) 1, (long) 1, "no", null, (long) 1, "state", (long) 1, "567"));
//        stockInspectCheckDTOS.add(new StockInspectCheckDTO("a", Long.parseLong("52345"), (long) 114352345, false,
//                "2313", (long) 1, (long) 1, "a", (long) 1, (long) 1, (long) 1, "a", "dt",
//                (long) 1, (long) 1, (long) 1, (long) 1, (long) 1, "no", null, (long) 1, "state", (long) 1, "567"));
//        stockInspectCheckDTOS.add(new StockInspectCheckDTO("a", Long.parseLong("52345"), (long) 114352345, false,
//                "2313", (long) 1, (long) 1, "a", (long) 1, (long) 1, (long) 1, "a", "dt",
//                (long) 1, (long) 1, (long) 1, (long) 1, (long) 1, "no", null, (long) 1, "state", (long) 1, "567"));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recListSerialNumber.setLayoutManager(layoutManager);
//laydata
        ArrayList<StockInspectCheckDTO> stockInspectCheckDTOS = new ArrayList<>();
        RecycleViewQuantityAdapter recycleViewSerialAdapter = new RecycleViewQuantityAdapter(stockInspectCheckDTOS, getActivity());
        recListSerialNumber.setAdapter(recycleViewSerialAdapter);
    }


    @Override
    public void onClick(View arg0) {
        int id = arg0.getId();
        switch (id) {
            case R.id.btnInspect: {
                if (confirm_inspect_serial.isChecked()) {
                    AsynctaskBeforeStockInspect asynctaskBeforeStockInspect = new AsynctaskBeforeStockInspect(getActivity(),
                            new OnPostExecuteListener<ParseOuput>() {
                                @Override
                                public void onPostExecute(ParseOuput result, String errorCode, String description) {
                                    boolean checkOK = result.getStockInspectMessage().isCheckOk();
                                    if ("0".equals(errorCode)) {
                                        if (checkOK) {
                                            doStockInspect(true);
                                        } else {
                                            //hien thong bao
                                            CommonActivity.createDialog(getActivity(),
                                                    result.getStockInspectMessage().getWarnMessage(),
                                                    getString(R.string.app_name),
                                                    getString(R.string.cancel), getString(R.string.ok),
                                                    null, new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            doStockInspect(true);
                                                        }
                                                    }).show();
                                        }
                                    } else {
                                        CommonActivity.showConfirmValidate(getActivity(), R.string.no_data);
                                    }
                                }
                            }, moveLogInAct, checkStockInspectDTO, stockInspectCheckDTOS, 1 + "", true);
                    asynctaskBeforeStockInspect.execute();
                    Toast.makeText(getActivity(), R.string.stocking_status, Toast.LENGTH_SHORT).show();
                } else {
                    //warn
                    AsynctaskBeforeStockInspect asynctaskBeforeStockInspect = new AsynctaskBeforeStockInspect(getActivity(), new OnPostExecuteListener<ParseOuput>() {
                        @Override
                        public void onPostExecute(ParseOuput result, String errorCode, String description) {
                            if ("0".equals(errorCode)) {
                                boolean checkOK = result.getStockInspectMessage().isCheckOk();
                                if (checkOK) {
                                    //kiem ke
                                    doStockInspect(false);
                                } else {
                                    //hien thong bao

                                    CommonActivity.createDialog(getActivity(),
                                            result.getStockInspectMessage().getWarnMessage(),
                                            getString(R.string.app_name),
                                            getString(R.string.cancel), getString(R.string.ok),
                                            null,
                                            new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    doStockInspect(false);
                                                }
                                            }).show();


                                }
                            } else {
                                CommonActivity.showConfirmValidate(getActivity(), R.string.no_data);
                            }
                        }
                    }, moveLogInAct, checkStockInspectDTO, stockInspectCheckDTOS, 1 + "", false);
                    asynctaskBeforeStockInspect.execute();
                    //test ws
//                    AsyncTaskDoStockInpect asyncTaskDoStockInpect =
//                            new AsyncTaskDoStockInpect(getActivity(), new OnPostExecuteListener<ParseOuput>() {
//                                @Override
//                                public void onPostExecute(ParseOuput result, String errorCode, String description) {
//                                    Toast.makeText(getActivity(), result.getStockInspectMessage().getMessageSuccess(), Toast.LENGTH_SHORT).show();
//                                    btnInspect.setEnabled(false);
//                                }
//                            }, moveLogInAct, checkStockInspectDTO, stockInspectSaveDTO, stockInspectCheckDTOS, 1 + "", false);
//                    asyncTaskDoStockInpect.execute();
//                    Toast.makeText(getActivity(), R.string.stocking_status, Toast.LENGTH_SHORT).show();
                }

                break;
            }
        }
    }

    private void doStockInspect(boolean confirm) {
        AsyncTaskDoStockInpect asyncTaskDoStockInpect =
                new AsyncTaskDoStockInpect(getActivity(), new OnPostExecuteListener<ParseOuput>() {
                    @Override
                    public void onPostExecute(ParseOuput result, String errorCode, String description) {
                        if ("0".equals(errorCode)) {
                            if (result.isSuccess()) {
                                Toast.makeText(getActivity(), R.string.inspect_success, Toast.LENGTH_SHORT).show();
                                btnInspect.setEnabled(false);
                            } else {
                                Toast.makeText(getActivity(), R.string.inspect_fail, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            CommonActivity.showConfirmValidate(getActivity(), R.string.no_data);
                        }
                    }
                }, moveLogInAct, checkStockInspectDTO, stockInspectSaveDTO, stockInspectCheckDTOS, 1 + "", confirm);
        asyncTaskDoStockInpect.execute();
    }

    @Override
    protected void setPermission() {

    }
}
