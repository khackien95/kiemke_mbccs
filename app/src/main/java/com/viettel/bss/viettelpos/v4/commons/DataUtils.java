package com.viettel.bss.viettelpos.v4.commons;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;

import com.viettel.bss.viettelpos.v4.BuildConfig;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.connecttionService.beans.AreaBean;
import com.viettel.bss.viettelpos.v4.connecttionService.dal.GetAreaDal;
import com.viettel.bss.viettelpos.v4.customer.manage.RegisterNewFragment;
import com.viettel.bss.viettelpos.v4.customer.object.Spin;
import com.viettel.bss.viettelpos.v4.dal.ApParamDAL;
import com.viettel.bss.viettelpos.v4.omichanel.dao.ConnectionOrder;
import com.viettel.bss.viettelpos.v4.plan.model.InfrastureModel;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.viettel.bss.viettelpos.v4.commons.Constant.ACTION_SCAN;

public class DataUtils extends Activity {
	private static final List<Spin> lstStatusCSKH = new ArrayList<>();

	/**
	 * getLstStatusCSKH
	 * 
	 * @param context
	 * @return
	 */
	private static List<Spin> getLstStatusCSKH(Context context) {
		if (CommonActivity.isNullOrEmptyArray(lstStatusCSKH)) {
			List<Spin> lstTmp = initLstStatusCSKH(context,
					Constant.PAR_NAME.STATUS_CSKHLN);
			if (!CommonActivity.isNullOrEmptyArray(lstTmp)) {
				lstStatusCSKH.addAll(lstTmp);
			}
		}
		return lstStatusCSKH;
	}

	/**
	 * 
	 * @param context
	 * @param parName
	 * @return
	 */
	private static List<Spin> initLstStatusCSKH(Context context, String parName) {
		ApParamDAL apDal = null;
		try {
			apDal = new ApParamDAL(context);
			apDal.open();

            return apDal.getAppParam(parName, true);
		} catch (Exception ex) {
			Log.d("Error: ", "Error when initSpinner: " + ex);
			return null;
		} finally {
			if (apDal != null) {
				try {
					apDal.close();
				} catch (Exception ignored) {

				}
			}
		}
	}

	/**
	 * getStatusNameCSKH
	 * 
	 * @param context
	 * @param statusId
	 * @return
	 */
	public static String getStatusNameCSKH(Context context, String statusId) {
		getLstStatusCSKH(context);

		for (Spin spin : lstStatusCSKH) {
			if (spin.getId().equals(statusId)) {
				return spin.getValue();
			}
		}
		return context.getResources().getString(R.string.tvOtherToolShop, "");
	}

    public static ArrayList<AreaBean> getProvince(Activity activity){
        GetAreaDal dal = null;
        try {
            dal = new GetAreaDal(activity);
            dal.open();
            return dal.getLstProvince();
        } catch (Exception ex) {
            Log.e("initProvince", ex.toString());
            return null;
        } finally {
            if(dal != null){
                dal.close();
            }
        }
    }

	public static ArrayList<AreaBean> getProvince(Context mContext){
		GetAreaDal dal = null;
		try {
			dal = new GetAreaDal(mContext);
			dal.open();
			return dal.getLstProvince();
		} catch (Exception ex) {
			Log.e("initProvince", ex.toString());
			return null;
		} finally {
			if(dal != null){
				dal.close();
			}
		}
	}


	public static ArrayList<AreaBean> initDistrict(Activity activity,
			String province) {
		GetAreaDal dal = null;
		try {
			dal = new GetAreaDal(activity);
			dal.open();
			return dal.getLstDistrict(province);
		} catch (Exception ex) {
			Log.e("initDistrict", ex.toString());
			return null;
		} finally {
			if (dal != null) {
				dal.close();
			}
		}
	}

	public static ArrayList<AreaBean> initPrecinct(Activity activity,
			String province, String district) {
		GetAreaDal dal = null;
		try {
			dal = new GetAreaDal(activity);
			dal.open();
			return dal.getLstPrecinct(province, district);
		} catch (Exception ex) {
			Log.e("initPrecinct", ex.toString());
			return null;
		} finally {
			if (dal != null) {
				dal.close();
			}
		}
	}

	public static Long safeToLong(String value){
		if(value == null){
			return 0L;
		}

		try{
			if(value.contains(".")){
				value = value.replaceAll("\\.", "");
			}

			if(value.contains(",")){
				value = value.replaceAll(",", "");
			}
			return Long.valueOf(value.trim());
		} catch (Exception ex){
			return 0L;
		}
	}

	public static Long safeToLong(Long value){
		if(value == null){
			return 0L;
		}

		return value;
	}

	public static String safeToString(String value){
		if(CommonActivity.isNullOrEmpty(value)){
			return "";
		}
		return value.trim();
	}

	public static int safeToInteger(String value){
		if(CommonActivity.isNullOrEmpty(value)){
			return 0;
		}

		try{
			return Integer.valueOf(value);
		}catch (Exception ex){
			return 0;
		}
	}

	public static void scanQRCode(final Activity activity) {
		try {
			Intent intent = new Intent(ACTION_SCAN);
			intent.putExtra("SCAN_FORMATS",
					"CODE_39,CODE_93,CODE_128,DATA_MATRIX,ITF,CODABAR,EAN_13,EAN_8,UPC_A,QR_CODE");
			activity.startActivityForResult(intent, Constant.SCAN_QRCODE_REQUEST);

		} catch (Exception anfe) {

			View.OnClickListener click = new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					UpdateApkAsyn updateApkAsyn = new UpdateApkAsyn(activity);
					updateApkAsyn.execute();

				}
			};
			CommonActivity.createDialog(activity,
					activity.getString(R.string.confirmdlapk),
					activity.getString(R.string.app_name),
					activity.getString(R.string.cancel),
					activity.getString(R.string.ok), null,
					click).show();
		}
	}

	public static class UpdateApkAsyn extends AsyncTask<String, Void, String> {
		String urlinstall = "";
		ProgressDialog progress;
		private Context context = null;
		public UpdateApkAsyn(Context context) {
			this.context = context;
			this.progress = new ProgressDialog(this.context);
			// check font
			this.progress.setMessage(context.getResources().getString(
					R.string.processingdl));
			if (!this.progress.isShowing()) {
				this.progress.show();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			return UpdateVersion(Session.getToken());
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equalsIgnoreCase(Constant.SUCCESS_CODE)) {
				progress.dismiss();
				Uri uriApp;
				Intent intent;
				if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
					uriApp = Uri.fromFile(new File(urlinstall));
					intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(uriApp, "application/vnd.android.package-archive");
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				} else {
					uriApp = FileProvider.getUriForFile(context,
							context.getApplicationContext().getPackageName()
							+ ".fileprovider", new File(urlinstall));
					intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
					intent.setData(uriApp);
					intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
				}
				context.startActivity(intent);
			} else {
				progress.dismiss();
				Dialog dialog = CommonActivity.createAlertDialog((Activity) context,
						context.getResources().getString(R.string.downloadfail), context
								.getResources().getString(R.string.app_name));
				dialog.show();
			}
		}

		public String UpdateVersion(String token) {
			String result = "";
			String url = Constant.PATH_BARCODE + token;
			try {
				URL urlcontrol = new URL(url);
				Log.e("URL getVersion:", url);
				Log.i("Bo nho con trong", Double.toString(SdCardHelper
						.getAvailableInternalMemorySize()));
				double availAbleMemory = SdCardHelper
						.getAvailableInternalMemorySize();
				if (availAbleMemory > 50) {

					InputStream input = new BufferedInputStream(
							urlcontrol.openStream());
					OutputStream output;
					File file = new File(Constant.MBCCS_TEMP_FOLDER);
					if (!file.exists()) {
						file.mkdirs();
					}
					output = new FileOutputStream(Constant.MBCCS_TEMP_FOLDER
							+ "barcode.apk");
					urlinstall = Constant.MBCCS_TEMP_FOLDER + "barcode.apk";
					Log.d("urlinstall", urlinstall);
					byte data[] = new byte[1024];
					int count = 0;
					while ((count = input.read(data)) != -1) {
						output.write(data, 0, count);
					}
					output.flush();
					output.close();
					input.close();
					Log.e("FilePath", urlinstall);
					Log.e("UPDATE VERSION", "End Download >>>>>>>>>>>>>>>> ");
					result = Constant.SUCCESS_CODE;
				} else {
					result = Constant.ERROR_CODE;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}
	}

	public static boolean isFuncValid(Context context, ConnectionOrder connectionOrder){

		String parName = Constant.HSDT_NOT_ALLOW;
		if(connectionOrder.getTarget() == null) {
			parName = Constant.OMNI_NOT_ALLOW;
		} else if(Constant.HSDT.equals(connectionOrder.getTarget())) {
			parName = Constant.HSDT_NOT_ALLOW;
		} else if(Constant.HOTLINE.equals(connectionOrder.getTarget())) {
			parName = Constant.HOTLINE_NOT_ALLOW;
		}

		String parValue = new ApParamDAL(context).getValue(parName , connectionOrder.getOrderType());
		if(!CommonActivity.isNullOrEmpty(parValue) && "1".equals(parValue)){
			return false;
		}
		return true;
	}
	
	public static boolean distanceValid(String myLat, String mLng, String infLat, String infLng) {

		if(null == myLat || null == mLng || null == infLat || null == infLng) return false;
		try {
			android.location.Location loc1 = new android.location.Location("");
			loc1.setLatitude(Double.parseDouble(myLat));
			loc1.setLongitude(Double.parseDouble(mLng));

			android.location.Location loc2 = new android.location.Location("");
			loc2.setLatitude(Double.parseDouble(infLat));
			loc2.setLongitude(Double.parseDouble(infLng));

			float distanceInMeters = loc1.distanceTo(loc2);
			Log.e("@@@@@@", "distanceInMeters ==> " + distanceInMeters);

			if((int) distanceInMeters < 200) return true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

}
