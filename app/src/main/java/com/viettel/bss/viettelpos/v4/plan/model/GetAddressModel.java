package com.viettel.bss.viettelpos.v4.plan.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 05/01/2018.
 */

@Root(name = "return", strict = false)
public class GetAddressModel {

    @Element(name = "errorCode", required = false)
    private String errorCode;

    @Element(name = "success", required = false)
    private boolean success;

    @ElementList(name = "lstDStaffLocations", entry = "lstDStaffLocations", required = false, inline = true)
    private List<StaffLocationModel> staffLocationModelList = new ArrayList<>();

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<StaffLocationModel> getStaffLocationModelList() {
        return staffLocationModelList;
    }

    public void setStaffLocationModelList(List<StaffLocationModel> staffLocationModelList) {
        this.staffLocationModelList = staffLocationModelList;
    }
}
