package com.viettel.bss.viettelpos.v4.connecttionMobile.asynctask;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncTaskCommon;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Created by thuandq on 3/6/2018.
 */

public class AsynUpdatePromotion extends AsyncTaskCommon<String, Void, String> {
    public AsynUpdatePromotion(Activity context, OnPostExecuteListener<String> listener, View.OnClickListener moveLogInAct) {
        super(context, listener, moveLogInAct);
    }

    @Override
    protected String doInBackground(String... strings) {
        return updatePromotion(strings[0],strings[1],strings[2],strings[3],strings[4],strings[5]);
    }

    private String updatePromotion(String account, String serviceType,
                                   String promotionCodeNew, String prepaidCode, String reasonId, String effectTime) {
        String original = "";

        try {
            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_updatePromotion");
            StringBuilder rawData = new StringBuilder();
            rawData.append("<ws:updatePromotion>");
            rawData.append("<input>");
            rawData.append("<token>").append(Session.getToken()).append("</token>");
            rawData.append("<account>").append(account).append("</account>");

            rawData.append("<serviceType>").append(serviceType).append("</serviceType>");

            rawData.append("<promotionCodeNew>").append(promotionCodeNew).append("</promotionCodeNew>");
            if (!CommonActivity.isNullOrEmpty(prepaidCode)) {
                rawData.append("<prepaidCode>").append(prepaidCode).append("</prepaidCode>");
            } else {
                rawData.append("<prepaidCode>" + "" + "</prepaidCode>");
            }

            if (!CommonActivity.isNullOrEmpty(effectTime)) {
                rawData.append("<effectTime>").append(effectTime).append("</effectTime>");
            }


            rawData.append("<reasonId>").append(reasonId).append("</reasonId>");
            rawData.append("</input>");
            rawData.append("</ws:updatePromotion>");

            Log.i("LOG", "raw data" + rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData
                    .toString());
            Log.d("LOG", "Send evelop" + envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope,
                    Constant.BCCS_GW_URL, mActivity,
                    "mbccs_updatePromotion");
            Log.i("LOG", "Respone:  " + response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i("LOG", "Responseeeeeeeeee Original  " + original);

            // parser
            Document doc = parse.getDomElement(original);
            NodeList nl = doc.getElementsByTagName("return");
            for (int i = 0; i < nl.getLength(); i++) {
                Element e2 = (Element) nl.item(i);
                errorCode = parse.getValue(e2, "errorCode");
                description = parse.getValue(e2, "description");
                Log.d("errorCode", errorCode);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return errorCode;

    }
}
