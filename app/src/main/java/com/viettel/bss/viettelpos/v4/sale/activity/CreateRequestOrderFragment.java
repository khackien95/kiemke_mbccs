package com.viettel.bss.viettelpos.v4.sale.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.Define;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.ReplaceFragment;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.connecttionService.activity.SearchStaffActivity;
import com.viettel.bss.viettelpos.v4.dialog.LoginDialog;
import com.viettel.bss.viettelpos.v4.omichanel.dialog.ListStaffDialog;
import com.viettel.bss.viettelpos.v4.sale.adapter.StockReturnAdapter;
import com.viettel.bss.viettelpos.v4.sale.adapter.StockReturnAdapter.OnChangeQuantity;
import com.viettel.bss.viettelpos.v4.sale.adapter.StockReturnAdapter.OncancelStockModel;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncApproveOrderStaff;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncCancelOrderStaff;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncGetLstStaff;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncGetLstStockOrderStaffDetail;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncGetLstStockOrderStaffSerial;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncRefuseOrderStaff;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncRequestOrderStaff;
import com.viettel.bss.viettelpos.v4.sale.object.InventoryOutput;
import com.viettel.bss.viettelpos.v4.sale.object.ProductOfferingDTO;
import com.viettel.bss.viettelpos.v4.sale.object.Shop;
import com.viettel.bss.viettelpos.v4.sale.object.Staff;
import com.viettel.bss.viettelpos.v4.sale.object.StockOrderStaffDTO;
import com.viettel.bss.viettelpos.v4.sale.object.StockOrderStaffDetailDTO;
import com.viettel.bss.viettelpos.v4.sale.object.StockTransSerialDTO;
import com.viettel.bss.viettelpos.v4.sale.object.StockTransSerialSM;
import com.viettel.bss.viettelpos.v4.synchronizationdata.XmlDomParse;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class CreateRequestOrderFragment extends Fragment
        implements OncancelStockModel, OnClickListener, OnChangeQuantity {

    private View mView;
    private EditText edtSearch;
    private ListView lvStockModel;
    private Button btnSale;
    private Button btnViewStockModel;
    private TextView tvStaff, tvQuantitySaling;
    private LinearLayout lnSelectStaff, lnButton;
    private StockReturnAdapter stockReturnModelAdapter;
    private Activity mActivity;
    private LinearLayout btnSearch;
    private ProductOfferingDTO curentStockModel;
    private ArrayList<Staff> arrStaff;
    private Staff staff = null;
    private String type_check;
    private StockOrderStaffDTO stockOrderStaffDTO = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDataBundle();
        if (mView == null) {
            mView = inflater.inflate(R.layout.layout_sale_trans_good, container, false);
            lvStockModel = (ListView) mView.findViewById(R.id.lvStockModel);
            btnSale = (Button) mView.findViewById(R.id.btnOk);
            btnViewStockModel = (Button) mView.findViewById(R.id.btnViewStockModel);
            edtSearch = (EditText) mView.findViewById(R.id.edtSearch);
            btnSearch = (LinearLayout) mView.findViewById(R.id.btn_search);
            tvStaff = (TextView) mView.findViewById(R.id.tvStaff);
            lnSelectStaff = (LinearLayout) mView.findViewById(R.id.lnSelectStaff);
            tvQuantitySaling = (TextView) mView.findViewById(R.id.tvQuantitySaling);
            tvStaff.setOnClickListener(this);
            lnButton = (LinearLayout) mView.findViewById(R.id.lnButton);
            loadListStockModel();
        }

        btnViewStockModel.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnSale.setOnClickListener(this);

        lvStockModel.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Log", "click stock item position: " + position);

            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                onSearchStockModel();

            }
        });

        return mView;
    }

    private void getDataBundle() {
        type_check = getArguments().getString("TYPE", "");
        stockOrderStaffDTO = (StockOrderStaffDTO) getArguments().getSerializable("stockOrderStaffDTO");

    }

    private void loadListStockModel() {
        // truong hop tao moi
        if (!CommonActivity.isNullOrEmpty(type_check) && Define.STATUS_CREATE_NEW.equals(type_check)) {
            lnSelectStaff.setVisibility(View.VISIBLE);
            btnViewStockModel.setText(getActivity().getString(R.string.choosed));
            btnSale.setText(getActivity().getString(R.string.transgoodstr));
            tvQuantitySaling.setText(getActivity().getString(R.string.chuyen));

            if (CommonActivity.isNetworkConnected(mActivity)) {
                AsynctaskGetListStockModel asyntaskGetlistStockModel = new AsynctaskGetListStockModel(mActivity);
                asyntaskGetlistStockModel.execute();
            } else {
                CommonActivity.createAlertDialog(mActivity, getString(R.string.errorNetwork), getString(R.string.app_name))
                        .show();
            }
            // lay danh sach staff
            AsyncGetLstStaff asyncGetLstStaff = new AsyncGetLstStaff(getActivity(), onPostGetStaff, moveLogInAct);
            asyncGetLstStaff.execute();
        } else {
            lnSelectStaff.setVisibility(View.GONE);
            // neu truong hop khong phai tao moi ma do nguoi dung tao
            if (Session.userName.equalsIgnoreCase(stockOrderStaffDTO.getFromStaffCode()) && "0".equalsIgnoreCase(stockOrderStaffDTO.getStatus())) {
                tvQuantitySaling.setText(getActivity().getString(R.string.chuyen));
                btnViewStockModel.setVisibility(View.GONE);
                btnSale.setText(getActivity().getString(R.string.huyboyeucau));
            } else {
                btnViewStockModel.setText(getActivity().getString(R.string.refuse));
                btnSale.setText(getActivity().getString(R.string.btn_approve));
                if (Session.userName.equalsIgnoreCase(stockOrderStaffDTO.getFromStaffCode())) {
                    tvQuantitySaling.setText(getActivity().getString(R.string.chuyen));
                } else {
                    tvQuantitySaling.setText(getActivity().getString(R.string.nhan));
                }
                if (!CommonActivity.isNullOrEmpty(stockOrderStaffDTO) && !"0".equalsIgnoreCase(stockOrderStaffDTO.getStatus())) {
                    lnButton.setVisibility(View.GONE);
                } else {
                    lnButton.setVisibility(View.VISIBLE);
                }
            }
            // truong hop tu choi va phe duyet
            AsyncGetLstStockOrderStaffDetail asyncGetLstStockOrderStaffDetail = new AsyncGetLstStockOrderStaffDetail(getActivity(), onPostGetStockOrderStaffDetail, moveLogInAct);
            asyncGetLstStockOrderStaffDetail.execute(stockOrderStaffDTO.getStockOrderId() + "");
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        MainActivity.getInstance().setTitleActionBar(R.string.transgoodstr);
    }

    @Override
    public void onChangeQuantity(ProductOfferingDTO stockModel) {
        curentStockModel = stockModel;
        if (!CommonActivity.isNullOrEmpty(type_check) && Define.STATUS_CREATE_NEW.equals(type_check)) {
            Bundle mBundle = new Bundle();
            if (stockModel != null) {
                mBundle.putSerializable("stockModel", stockModel);
            }
            if (stockModel.getmListSerialSelection() != null && stockModel.getmListSerialSelection().size() > 0) {
                ArrayList<StockTransSerialDTO> listSerialPut = new ArrayList<>();
                listSerialPut.addAll(stockModel.getmListSerialSelection());
                mBundle.putSerializable("listSerial", listSerialPut);
            }
            FragmentChooseSerialReturnTheGood fragmentChooseSerial = new FragmentChooseSerialReturnTheGood();
            fragmentChooseSerial.setArguments(mBundle);
            fragmentChooseSerial.setTargetFragment(CreateRequestOrderFragment.this, 100);
            ReplaceFragment.replaceFragment(mActivity, fragmentChooseSerial, true);
        }else{
            AsyncGetLstStockOrderStaffSerial asyncGetLstStockOrderStaffSerial = new AsyncGetLstStockOrderStaffSerial(getActivity() , onPostGetListSerial, moveLogInAct);
            asyncGetLstStockOrderStaffSerial.execute(stockOrderStaffDTO.getStockOrderId() + "", stockModel.getStockDetailId() + "");
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == 100) {
                if (curentStockModel.getCheckSerial() == 0) {
                    curentStockModel.setQuantitySaling(
                            Long.parseLong(data.getExtras().getString("strNumberSerial").toString()));
                } else {

                    ArrayList<StockTransSerialDTO> listSerialReceiver = (ArrayList<StockTransSerialDTO>) data
                            .getExtras().getSerializable("listSerial");
                    long countSerial = 0L;
                    for (StockTransSerialDTO serial : listSerialReceiver) {
                        countSerial += serial.getNumber();
                    }
                    curentStockModel.setQuantitySaling(countSerial);
                    curentStockModel.setmListSerialSelection(listSerialReceiver);

                }

            }

            if (requestCode == 101) {
                staff = (Staff) data.getExtras().getSerializable("StaffBeans");
                if (staff != null
                        && !CommonActivity.isNullOrEmpty(staff.toString())) {
                    tvStaff.setText(staff.toString());
                } else {
                    tvStaff.setText(getActivity().getString(
                            R.string.select_employee));
                }
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relaBackHome:
                getActivity().onBackPressed();
                break;
            case R.id.btn_search:
                onSearchStockModel();
                break;

            case R.id.btnViewStockModel:
                if (!CommonActivity.isNullOrEmpty(type_check) && Define.STATUS_CREATE_NEW.equalsIgnoreCase(type_check)) {
                    onFilterViewStock(v);
                } else {
                    if (CommonActivity.isNetworkConnected(mActivity)) {
                        CommonActivity.createDialog(mActivity, getString(R.string.confirmrefuse),
                                getString(R.string.app_name), getString(R.string.say_ko), getString(R.string.say_co), null,
                                returnRefuseCallBack).show();
                    } else {
                        CommonActivity.createAlertDialog(mActivity, getString(R.string.errorNetwork),
                                getString(R.string.app_name)).show();
                    }
                }
                break;
            case R.id.tvStaff:
                if (arrStaff.size() > 0) {
                    Intent intent = new Intent(getActivity(),
                            SearchStaffActivity.class);
                    intent.putExtra("arrStaff", arrStaff);
                    startActivityForResult(intent, 101);
                }
                break;
            case R.id.btnOk:

                if (!CommonActivity.isNullOrEmpty(type_check) && Define.STATUS_CREATE_NEW.equalsIgnoreCase(type_check)) {
                    if (staff == null) {
                        CommonActivity
                                .createAlertDialog(mActivity, getString(R.string.checkstaffacept), getString(R.string.app_name))
                                .show();
                        return;

                    }
                    boolean isReturnTheGood = false;
                    for (ProductOfferingDTO stockModel : stockReturnModelAdapter.getLstData()) {
                        if (stockModel.getQuantitySaling() > 0) {
                            isReturnTheGood = true;
                            break;
                        }
                    }
                    if (isReturnTheGood) {
                        if (CommonActivity.isNetworkConnected(mActivity)) {
                            CommonActivity.createDialog(mActivity, getString(R.string.message_confirm_trans_the_good),
                                    getString(R.string.app_name), getString(R.string.say_ko), getString(R.string.say_co), null,
                                    returnTheGoodConfirmCallBack).show();
                        } else {
                            CommonActivity.createAlertDialog(mActivity, getString(R.string.errorNetwork),
                                    getString(R.string.app_name)).show();
                        }
                    } else {
                        CommonActivity.createAlertDialog(mActivity, getString(R.string.message_please_sellect_return_the_good),
                                getString(R.string.app_name)).show();
                    }
                } else {
                    if (CommonActivity.isNetworkConnected(mActivity)) {
                        View.OnClickListener onClickListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AsyncCancelOrderStaff asyncCancelOrderStaff = new AsyncCancelOrderStaff(getActivity(), onPostCancelOrderStaff, moveLogInAct);
                                asyncCancelOrderStaff.execute(stockOrderStaffDTO.getStockOrderCode());
                            }
                        };
                        if (Session.userName.equalsIgnoreCase(stockOrderStaffDTO.getFromStaffCode()) && "0".equalsIgnoreCase(stockOrderStaffDTO.getStatus())) {
                            CommonActivity.createDialog(getActivity(), getActivity().getString(R.string.cancel_order_confirm), getActivity().getString(R.string.app_name),
                                    getActivity().getString(R.string.cancel)
                                    , getActivity().getString(R.string.ok),
                                    null, onClickListener).show();
                        } else {
                            CommonActivity.createDialog(mActivity, getString(R.string.message_confirm_approve),
                                    getString(R.string.app_name), getString(R.string.say_ko), getString(R.string.say_co), null,
                                    returnApproveCallBack).show();
                        }
                    } else {
                        CommonActivity.createAlertDialog(mActivity, getString(R.string.errorNetwork),
                                getString(R.string.app_name)).show();
                    }
                }
                break;
            default:
                break;
        }
    }

    // move login
    private final OnClickListener moveLogInAct = new OnClickListener() {

        @Override
        public void onClick(View v) {
            LoginDialog dialog = new LoginDialog(getActivity(),
                    Constant.VSAMenu.MENU_RETURN_GOOD);
            dialog.show();
        }
    };

    // confirm update
    private final OnClickListener returnTheGoodConfirmCallBack = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (!CommonActivity.isNetworkConnected(mActivity)) {
                CommonActivity
                        .createAlertDialog(mActivity, getString(R.string.errorNetwork), getString(R.string.app_name))
                        .show();
                return;
            }

            ArrayList<ProductOfferingDTO> listStockReturn = new ArrayList<>();
            for (ProductOfferingDTO stockModel : stockReturnModelAdapter.getLstData()) {
                if (stockModel.getQuantitySaling() > 0) {
                    listStockReturn.add(stockModel);
                }
            }
            AsyncRequestOrderStaff asyncRequestOrderStaff = new AsyncRequestOrderStaff(getActivity(), onPostCreateRequestTransOrder, listStockReturn, moveLogInAct);
            asyncRequestOrderStaff.execute(staff.getStaffId() + "");
        }
    };

    private OnClickListener returnApproveCallBack = new OnClickListener() {
        @Override
        public void onClick(View view) {
            AsyncApproveOrderStaff asyncRefuseOrderStaff = new AsyncApproveOrderStaff(getActivity(), onPostApproveOrderStaff, moveLogInAct);
            asyncRefuseOrderStaff.execute(stockOrderStaffDTO.getStockOrderCode());
        }
    };

    private OnClickListener returnRefuseCallBack = new OnClickListener() {
        @Override
        public void onClick(View view) {
            AsyncRefuseOrderStaff asyncRefuseOrderStaff = new AsyncRefuseOrderStaff(getActivity(), onPostRefuseOrderStaff, moveLogInAct);
            asyncRefuseOrderStaff.execute(stockOrderStaffDTO.getStockOrderCode());
        }
    };


    private void onFilterViewStock(View v) {
        if (!CommonActivity.isNullOrEmpty(stockReturnModelAdapter)) {
            if (v.getTag().equals("2")) {
                boolean isReturnTheGood = false;
                if (!CommonActivity.isNullOrEmpty(stockReturnModelAdapter.getLstData())) {
                    for (ProductOfferingDTO stockModel : stockReturnModelAdapter.getLstData()) {
                        if (stockModel.getQuantitySaling() > 0) {
                            isReturnTheGood = true;
                            break;
                        }
                    }
                }
                if (!isReturnTheGood) {
                    CommonActivity.createAlertDialog(mActivity, getString(R.string.message_not_select_trans_the_good),
                            getString(R.string.app_name)).show();
                } else {
                    stockReturnModelAdapter.filter(true);
                    v.setTag("3");
                    ((Button) v).setText(mActivity.getResources().getString(R.string.view_comback));
                }
            } else {
                stockReturnModelAdapter.filter(false);
                v.setTag("2");
                ((Button) v).setText(mActivity.getResources().getString(R.string.choosed));
            }
        }

    }

    @SuppressWarnings("unused")
    private void onSearchStockModel() {
        if (stockReturnModelAdapter != null) {
            String strKeySearch = edtSearch.getText().toString();
            stockReturnModelAdapter.SearchInput(strKeySearch);
        }
    }

    @Override
    public void onCancelStockModelListener(ProductOfferingDTO stockModel) {
    }

    @SuppressWarnings("unused")
    private class AsynctaskGetListStockModel extends AsyncTask<Void, Void, InventoryOutput> {

        private Activity mActivity = null;
        XmlDomParse parse = new XmlDomParse();
        final String errorCode = "";
        final String description = "";
        final ProgressDialog progress;

        public AsynctaskGetListStockModel(Activity mActivity) {
            this.mActivity = mActivity;
            this.progress = new ProgressDialog(mActivity);
            this.progress.setCancelable(false);
            this.progress.setMessage(mActivity.getResources().getString(R.string.getdataing));
            if (!this.progress.isShowing()) {
                this.progress.show();
            }
        }

        @Override
        protected InventoryOutput doInBackground(Void... params) {
            return getListStockModel();
        }

        @Override
        protected void onPostExecute(InventoryOutput result) {
            super.onPostExecute(result);
            progress.dismiss();
            if ("0".equals(result.getErrorCode())) {

                ArrayList<ProductOfferingDTO> mListStockModel = result.getLstProductOfferingDTO();
                if (mListStockModel != null && mListStockModel.size() > 0) {
                    stockReturnModelAdapter = new StockReturnAdapter(getActivity(), mListStockModel,
                            CreateRequestOrderFragment.this, CreateRequestOrderFragment.this, CreateRequestOrderFragment.this,
                            false);
                    lvStockModel.setAdapter(stockReturnModelAdapter);
                } else {
                    CommonActivity.createAlertDialog(MainActivity.getInstance(), getActivity().getString(R.string.ko_co_dl),
                            getString(R.string.app_name)).show();// ,getString(R.string.thu_lai)
                }
            } else {

                if (errorCode.equals(Constant.INVALID_TOKEN2)) {
                    Dialog dialog = CommonActivity.createAlertDialog(mActivity, description,
                            mActivity.getResources().getString(R.string.app_name), moveLogInAct);
                    dialog.show();

                } else {
                    String description = result.getDescription();
                    if (CommonActivity.isNullOrEmpty(description)) {
                        description = getActivity().getString(R.string.fails_not_description,
                                getString(R.string.getListDeptByObject));
                    }
                    CommonActivity.createErrorDialog(MainActivity.getInstance(), description, result.getErrorCode())
                            .show();
                }

            }
        }

        private InventoryOutput getListStockModel() {
            ArrayList<ProductOfferingDTO> listStockModel = new ArrayList<>();
            String original = "";
            try {
                BCCSGateway input = new BCCSGateway();
                input.addValidateGateway("username", Constant.BCCSGW_USER);
                input.addValidateGateway("password", Constant.BCCSGW_PASS);
                input.addValidateGateway("wscode", "mbccs_getStockStaffDetailBccs2");
                StringBuilder rawData = new StringBuilder();
                rawData.append("<ws:getStockStaffDetail>");
                rawData.append("<input>");
                rawData.append("<token>").append(Session.getToken()).append("</token>");
                rawData.append("</input>");
                rawData.append("</ws:getStockStaffDetail>");
                Log.i("RowData", rawData.toString());
                String envelope = input.buildInputGatewayWithRawData(rawData.toString());
                Log.d("Send evelop", envelope);
                Log.i("LOG", Constant.BCCS_GW_URL);
                String response = input.sendRequest(envelope, Constant.BCCS_GW_URL, getActivity(),
                        "mbccs_getStockStaffDetailBccs2");
                Log.i("Responseeeeeeeeee", response);
                CommonOutput output = input.parseGWResponse(response);
                original = output.getOriginal();
                Log.i("Responseeeeeeeeee Original", original);

                Serializer serializer = new Persister();
                InventoryOutput inventoryOtuput = serializer.read(InventoryOutput.class, original);
                if (inventoryOtuput == null) {
                    inventoryOtuput = new InventoryOutput();
                    inventoryOtuput.setDescription(getString(R.string.no_return_from_system));
                    inventoryOtuput.setErrorCode(Constant.ERROR_CODE);
                    return inventoryOtuput;
                } else {
                    return inventoryOtuput;
                }
            } catch (Exception e) {
                Log.e("mbccs_getStockStaffDetailBccs2", e.toString() + "description error", e);
                InventoryOutput inventoryOtuput = new InventoryOutput();
                inventoryOtuput.setDescription(getString(R.string.no_return_from_system));
                inventoryOtuput.setErrorCode(Constant.ERROR_CODE);
                return inventoryOtuput;
            }
        }
    }


    // ham lay danh sach nhan vien cung shop
    private OnPostExecuteListener<ArrayList<Staff>> onPostGetStaff = new OnPostExecuteListener<ArrayList<Staff>>() {
        @Override
        public void onPostExecute(ArrayList<Staff> result, String errorCode, String description) {
            arrStaff = result;
            for (Staff item : arrStaff) {
                if (Session.userName.equalsIgnoreCase(item.getStaffCode())) {
                    arrStaff.remove(item);
                    break;
                }
            }
        }
    };

    // ham tao yeu cau
    private OnPostExecuteListener<ParseOuput> onPostCreateRequestTransOrder = new OnPostExecuteListener<ParseOuput>() {
        @Override
        public void onPostExecute(ParseOuput result, String errorCode, String description) {
            if ("0".equals(result.getErrorCode())) {
                String des = result.getDescription();
                if (CommonActivity.isNullOrEmpty(des)) {
                    des = mActivity.getString(R.string.message_transgood_success);
                }
//				OnClickListener onClickListener = new OnClickListener() {
//					@Override
//					public void onClick(View view) {
//						loadListStockModel();
//					}
//				};
                CommonActivity.createAlertDialog(mActivity, des,
                        getString(R.string.app_name), onClickListenerBack).show();

            } else {
                Log.d("Log", "description error update" + description);
                if (errorCode.equals(Constant.INVALID_TOKEN2)) {
                    Dialog dialog = CommonActivity.createAlertDialog(mActivity, description,
                            mActivity.getResources().getString(R.string.app_name), moveLogInAct);
                    dialog.show();
                } else {
                    if (description == null || description.isEmpty()) {
                        description = mActivity.getString(R.string.checkdes);
                    }
                    Dialog dialog = CommonActivity.createAlertDialog(getActivity(), description,
                            getResources().getString(R.string.app_name));
                    dialog.show();
                }
            }
        }
    };
    // ham lay chi tiet don hang doi voi truong hop nhan vien nhan hang
    private OnPostExecuteListener<ParseOuput> onPostGetStockOrderStaffDetail = new OnPostExecuteListener<ParseOuput>() {
        @Override
        public void onPostExecute(ParseOuput result, String errorCode, String description) {
            if (result != null && !CommonActivity.isNullOrEmpty(result.getListStockOrderStaffDetailDTOs())) {
                ArrayList<ProductOfferingDTO> mListStockModel = convertStockOrderToProductOfId(result.getListStockOrderStaffDetailDTOs());
                if (mListStockModel != null && mListStockModel.size() > 0) {
                    stockReturnModelAdapter = new StockReturnAdapter(getActivity(), mListStockModel,
                            CreateRequestOrderFragment.this, CreateRequestOrderFragment.this, CreateRequestOrderFragment.this,
                            false, true);
                    lvStockModel.setAdapter(stockReturnModelAdapter);
                } else {
                    stockReturnModelAdapter = new StockReturnAdapter(getActivity(), new ArrayList<ProductOfferingDTO>(),
                            CreateRequestOrderFragment.this, CreateRequestOrderFragment.this, CreateRequestOrderFragment.this,
                            false);
                    lvStockModel.setAdapter(stockReturnModelAdapter);
                    CommonActivity.createAlertDialog(MainActivity.getInstance(), getActivity().getString(R.string.no_data),
                            getString(R.string.app_name)).show();
                }
            } else {
                stockReturnModelAdapter = new StockReturnAdapter(getActivity(), new ArrayList<ProductOfferingDTO>(),
                        CreateRequestOrderFragment.this, CreateRequestOrderFragment.this, CreateRequestOrderFragment.this,
                        false);
                lvStockModel.setAdapter(stockReturnModelAdapter);
                CommonActivity.createAlertDialog(MainActivity.getInstance(), getActivity().getString(R.string.no_data),
                        getString(R.string.app_name)).show();
            }
        }
    };

    // convert object hang hoa
    private ArrayList<ProductOfferingDTO> convertStockOrderToProductOfId(ArrayList<StockOrderStaffDetailDTO> arrStockOrderStaffDetailDTOS) {
        ArrayList<ProductOfferingDTO> lstReturn = new ArrayList<>();
        for (StockOrderStaffDetailDTO item : arrStockOrderStaffDetailDTOS) {
            ProductOfferingDTO productOfferingDTO = new ProductOfferingDTO();
            productOfferingDTO.setStateIdName(item.getStateName());
            productOfferingDTO.setStateId(item.getStateId());
            productOfferingDTO.setCode(item.getProdOfferCode());
            productOfferingDTO.setName(item.getProdOfferName());
            productOfferingDTO.setProductOfferingId(item.getProdOfferId());
            productOfferingDTO.setQuantity(item.getQuantity());
            productOfferingDTO.setQuantitySaling(item.getQuantity());
            productOfferingDTO.setStockDetailId(item.getStockOrderDetailId());
            productOfferingDTO.setCheckSerial(item.getCheckSerial());
            lstReturn.add(productOfferingDTO);
        }
        return lstReturn;
    }


    private ArrayList<StockTransSerialDTO> convertStockSerial(ArrayList<StockTransSerialSM> listStockTransSerialSMs) {

        ArrayList<StockTransSerialDTO> lstRturn = new ArrayList<>();
        for (StockTransSerialSM item : listStockTransSerialSMs) {
            StockTransSerialDTO stockTransSerialDTO = new StockTransSerialDTO();
            stockTransSerialDTO.setFromSerial(item.getFromSerial());
            stockTransSerialDTO.setToSerial(item.getToSerial());
            lstRturn.add(stockTransSerialDTO);
        }
        return lstRturn;
    }

    OnClickListener onClickListenerBack = new OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
            getActivity().onBackPressed();
        }
    };
    // ham tao yeu cau
    private OnPostExecuteListener<ParseOuput> onPostRefuseOrderStaff = new OnPostExecuteListener<ParseOuput>() {
        @Override
        public void onPostExecute(ParseOuput result, String errorCode, String description) {
            String des = result.getDescription();
            if ("0".equals(result.getErrorCode())) {

                CommonActivity.createAlertDialog(mActivity, getString(R.string.refuseOrderSucess),
                        getString(R.string.app_name), onClickListenerBack).show();


            } else {
                Log.d("Log", "description error update" + description);
                if (errorCode.equals(Constant.INVALID_TOKEN2)) {
                    Dialog dialog = CommonActivity.createAlertDialog(mActivity, description,
                            mActivity.getResources().getString(R.string.app_name), moveLogInAct);
                    dialog.show();
                } else {
                    if (des == null || des.isEmpty()) {
                        des = mActivity.getString(R.string.checkdes);
                    }
                    Dialog dialog = CommonActivity.createAlertDialog(getActivity(), des,
                            getResources().getString(R.string.app_name));
                    dialog.show();
                }
            }
        }
    };

    // ham tao yeu cau
    private OnPostExecuteListener<ParseOuput> onPostApproveOrderStaff = new OnPostExecuteListener<ParseOuput>() {
        @Override
        public void onPostExecute(ParseOuput result, String errorCode, String description) {
            String des = result.getDescription();
            if ("0".equals(result.getErrorCode())) {
                CommonActivity.createAlertDialog(mActivity, getString(R.string.message_acceptitem_success),
                        getString(R.string.app_name), onClickListenerBack).show();
            } else {
                Log.d("Log", "description error update" + description);
                if (errorCode.equals(Constant.INVALID_TOKEN2)) {
                    Dialog dialog = CommonActivity.createAlertDialog(mActivity, description,
                            mActivity.getResources().getString(R.string.app_name), moveLogInAct);
                    dialog.show();
                } else {
                    if (des == null || des.isEmpty()) {
                        des = mActivity.getString(R.string.checkdes);
                    }
                    Dialog dialog = CommonActivity.createAlertDialog(getActivity(), des,
                            getResources().getString(R.string.app_name));
                    dialog.show();
                }
            }
        }
    };
    private OnPostExecuteListener<ParseOuput> onPostGetListSerial = new OnPostExecuteListener<ParseOuput>() {
        @Override
        public void onPostExecute(ParseOuput result, String errorCode, String description) {
            if (result != null && !CommonActivity.isNullOrEmpty(result.getListStockTransSerialSMs())) {
                ArrayList<StockTransSerialDTO> mListStockModel = convertStockSerial(result.getListStockTransSerialSMs());
                if (mListStockModel != null && mListStockModel.size() > 0) {
//                    if (stockModel.getmListSerialSelection() != null && stockModel.getmListSerialSelection().size() > 0) {
//                        ArrayList<StockTransSerialDTO> listSerialPut = new ArrayList<>();
//                        listSerialPut.addAll(stockModel.getmListSerialSelection());
//                        mBundle.putSerializable("listSerial", listSerialPut);
//                    }
                    Bundle mBundle = new Bundle();
                    if (curentStockModel != null) {
                        mBundle.putSerializable("stockModel", curentStockModel);
                    }
                    mBundle.putSerializable("listSerial", mListStockModel);
                    FragmentChooseSerialReturnTheGood fragmentChooseSerial = new FragmentChooseSerialReturnTheGood();
                    fragmentChooseSerial.setArguments(mBundle);
                    fragmentChooseSerial.setTargetFragment(CreateRequestOrderFragment.this, 100);
                    ReplaceFragment.replaceFragment(mActivity, fragmentChooseSerial, true);
                } else {

                    CommonActivity.createAlertDialog(MainActivity.getInstance(), getActivity().getString(R.string.no_data),
                            getString(R.string.app_name)).show();
                }
            } else {

                CommonActivity.createAlertDialog(MainActivity.getInstance(), getActivity().getString(R.string.no_data),
                        getString(R.string.app_name)).show();
            }
        }
    };


    // ham yeu cau
    private OnPostExecuteListener<ParseOuput> onPostCancelOrderStaff = new OnPostExecuteListener<ParseOuput>() {
        @Override
        public void onPostExecute(ParseOuput result, String errorCode, String description) {
            if ("0".equals(result.getErrorCode())) {
                CommonActivity.createAlertDialog(getActivity(), getActivity().getString(R.string.cancelReSucess),
                        getActivity().getString(R.string.app_name), onClickListenerBack).show();
            } else {
                android.util.Log.d("Log", "description error update" + description);
                if (errorCode.equals(Constant.INVALID_TOKEN2)) {
                    Dialog dialog = CommonActivity.createAlertDialog(getActivity(), description,
                            getActivity().getResources().getString(R.string.app_name), moveLogInAct);
                    dialog.show();
                } else {
                    if (description == null || description.isEmpty()) {
                        description = getActivity().getString(R.string.checkdes);
                    }
                    Dialog dialog = CommonActivity.createAlertDialog(getActivity(), description,
                            getActivity().getResources().getString(R.string.app_name));
                    dialog.show();
                }
            }
        }
    };


}
