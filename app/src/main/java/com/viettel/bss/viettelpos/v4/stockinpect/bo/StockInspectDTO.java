package com.viettel.bss.viettelpos.v4.stockinpect.bo;

import com.viettel.bss.viettelpos.v4.commons.CommonActivity;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Created by leekien on 5/11/2018.
 */
@Root(name = "stockInspectDTO", strict = false)
public class StockInspectDTO extends BaseDTO implements Serializable{
//    @Element(name = "accountingModelCode", required = false)
//    protected String accountingModelCode;
//    protected String approveNote;
//    protected Long approveStatus;
//    protected String approveUser;
//    protected Long approveUserId;
//    protected String approveUserName;
//    protected String createUser;
//    protected Long createUserId;
//    protected String createUserName;
//    @Element(name = "inspectStatus", required = false)
//    protected Long inspectStatus;
//    protected Long isFinished;
//    protected Long isFinishedAll;
//    protected Long isScan;
//    protected String note;
//    @Element(name = "ownerId", required = false)
//    protected Long ownerId;
//    @Element(name = "ownerType", required = false)
//    protected Long ownerType;
//    @Element(name = "prodOfferId", required = false)
//    protected Long prodOfferId;
//    @Element(name = "prodOfferTypeId", required = false)
//    protected Long prodOfferTypeId;
//    protected Long quantityFinance;
//    protected Long quantityPoor;
//    protected Long quantityReal;
//    protected Long quantitySys;
//    protected String shopCode;
//    protected Long shopId;
//    protected String shopName;
//    @Element(name = "stateId", required = false)
//    protected Long stateId;
//    protected Long stockInspectId;
@Element(name = "approveDate", required = false)
protected XMLGregorianCalendar approveDate;
    @Element(name = "approveNote", required = false)
    protected String approveNote;
    @Element(name = "approveStatus", required = false)
    protected Long approveStatus;
    @Element(name = "approveUser", required = false)
    protected String approveUser;
    @Element(name = "approveUserId", required = false)
    protected Long approveUserId;
    @Element(name = "approveUserName", required = false)
    protected String approveUserName;
    @Element(name = "createDate", required = false)
    protected XMLGregorianCalendar createDate;
    @Element(name = "createUser", required = false)
    protected String createUser;
    @Element(name = "createUserId", required = false)
    protected Long createUserId;
    @Element(name = "createUserName", required = false)
    protected String createUserName;
    @Element(name = "inspectStatus", required = false)
    protected Long inspectStatus;
    @Element(name = "isFinished", required = false)
    protected Long isFinished;
    @Element(name = "isFinishedAll", required = false)
    protected Long isFinishedAll;
    @Element(name = "isScan", required = false)
    protected Long isScan;
    @Element(name = "note", required = false)
    protected String note;
    @Element(name = "ownerId", required = false)
    protected Long ownerId;
    @Element(name = "ownerType", required = false)
    protected Long ownerType;
    @Element(name = "prodOfferId", required = false)
    protected Long prodOfferId;
    @Element(name = "prodOfferTypeId", required = false)
    protected Long prodOfferTypeId;
    @Element(name = "quantityFinance", required = false)
    protected Long quantityFinance;
    @Element(name = "quantityPoor", required = false)
    protected Long quantityPoor;
    @Element(name = "quantityReal", required = false)
    protected Long quantityReal;
    @Element(name = "quantitySys", required = false)
    protected Long quantitySys;
    @Element(name = "shopCode", required = false)
    protected String shopCode;
    @Element(name = "shopId", required = false)
    protected Long shopId;
    @Element(name = "shopName", required = false)
    protected String shopName;
    @Element(name = "stateId", required = false)
    protected Long stateId;
    @Element(name = "stockInspectId", required = false)
    protected Long stockInspectId;
    @ElementList(name = "stockInspectRealDTOs", entry = "stockInspectRealDTOs", required = false)
    protected List<StockInspectRealDTO> stockInspectRealDTOs;
    @Element(name = "updateDate", required = false)
    protected XMLGregorianCalendar updateDate;

    public Long getInspectStatus() {
        return inspectStatus;
    }

    public void setInspectStatus(Long inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    public Long getProdOfferId() {
        return prodOfferId;
    }

    public void setProdOfferId(Long prodOfferId) {
        this.prodOfferId = prodOfferId;
    }

    public Long getProdOfferTypeId() {
        return prodOfferTypeId;
    }

    public void setProdOfferTypeId(Long prodOfferTypeId) {
        this.prodOfferTypeId = prodOfferTypeId;
    }

    public Long getStateId() {
        return stateId;
    }

    public void setStateId(Long stateId) {
        this.stateId = stateId;
    }



    public String toXML(String rootTag) {
        StringBuilder rawData = new StringBuilder();
        if (CommonActivity.isNullOrEmpty(rootTag)) {
            rootTag = "checkStockInspectDTO";
        }
        rawData.append("<" + rootTag + ">");

        if (!CommonActivity.isNullOrEmpty(getProdOfferId())) {
            rawData.append("<prodOfferId>" + String.valueOf(getProdOfferId()) + "</prodOfferId>");
        }
        if (!CommonActivity.isNullOrEmpty(getProdOfferTypeId())) {
            rawData.append("<prodOfferTypeId>" + String.valueOf(getProdOfferTypeId()) + "</prodOfferTypeId>");
        }
        if (!CommonActivity.isNullOrEmpty(getInspectStatus())) {
            rawData.append("<inspectStatus>" + String.valueOf(getInspectStatus()) + "</inspectStatus>");
        }
        if (!CommonActivity.isNullOrEmpty(getStateId())) {
            rawData.append("<stateId>" + String.valueOf(getStateId()) + "</stateId>");


        }
        rawData.append("</" + rootTag + ">");
        return rawData.toString();
    }

}
