package com.viettel.bss.viettelpos.v4.connecttionMobile.asynctask;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.customer.object.Spin;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncTaskCommon;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thinhhq1 on 4/25/2017.
 */

public class AsyncFindListBankPlusInfo extends AsyncTaskCommon<String, Void, ParseOuput> {
    private Activity context;
    private List<String> lstIsdn;
    public AsyncFindListBankPlusInfo(Activity context, OnPostExecuteListener<ParseOuput> listener,
                                     View.OnClickListener moveLogInAct) {
        super(context, listener, moveLogInAct);
        this.context = context;
    }

    public AsyncFindListBankPlusInfo(Activity context, OnPostExecuteListener<ParseOuput> listener,
                                     View.OnClickListener moveLogInAct , List<String> lstStr) {
        super(context, listener, moveLogInAct);
        this.context = context;
        this.lstIsdn = lstStr;
    }


        @Override
    protected ParseOuput doInBackground(String... params) {
        return getOptionSetValue(params[0]);
    }

    private ParseOuput getOptionSetValue(String actionCode) {
        ParseOuput out = new ParseOuput();
        String original = "";
        try {

            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_findListBankPlusInfo");
            StringBuilder rawData = new StringBuilder();
            rawData.append("<ws:findListBankPlusInfo>");
            rawData.append("<input>");
            rawData.append("<token>" + Session.getToken() + "</token>");
            //code "IDTYPE_FIELD_USAGE"
            rawData.append("<actionCode>" + actionCode + "</actionCode>");
//            lstString
            if(!CommonActivity.isNullOrEmpty(lstIsdn)){
                for (String isdn: lstIsdn) {
                    rawData.append("<lstString>" + isdn + "</lstString>");
                }
            }
            rawData.append("</input>");
            rawData.append("</ws:findListBankPlusInfo>");

            Log.i("LOG", "raw data" + rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData.toString());
            Log.d("LOG", "Send evelop" + envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope, Constant.BCCS_GW_URL, context,
                    "mbccs_findListBankPlusInfo");
            Log.i("LOG", "Respone:  " + response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i("LOG", "Responseeeeeeeeee Original  " + response);

            // parser
            // parser
            Serializer serializer = new Persister();
            out = serializer.read(ParseOuput.class, original);
            if (out == null) {
                errorCode = Constant.ERROR_CODE;
                return null;
            } else {
                description = out.getDescription();
                errorCode = out.getErrorCode();
            }


        } catch (Exception e) {
            e.printStackTrace();

            description = e.getMessage();
            errorCode = Constant.ERROR_CODE;
            return null;
        }
        return out;
    }

}
