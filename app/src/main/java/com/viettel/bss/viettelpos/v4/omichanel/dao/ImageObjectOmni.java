package com.viettel.bss.viettelpos.v4.omichanel.dao;

import android.graphics.Bitmap;

/**
 * Created by hungtv64 on 3/16/2018.
 */

public class ImageObjectOmni {

    private String fileExt;
    private String base64String;
    private Bitmap bitmap;

    public ImageObjectOmni(String fileExt, Bitmap bitmap, String base64String) {
        this.fileExt = fileExt;
        this.base64String = base64String;
        this.bitmap = bitmap;
    }

    public String getFileExt() {
        return fileExt;
    }

    public void setFileExt(String fileExt) {
        this.fileExt = fileExt;
    }

    public String getBase64String() {
        return base64String;
    }

    public void setBase64String(String base64String) {
        this.base64String = base64String;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
