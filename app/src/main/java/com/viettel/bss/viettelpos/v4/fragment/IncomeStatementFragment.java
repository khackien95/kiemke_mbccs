package com.viettel.bss.viettelpos.v4.fragment;


import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.FragmentCommon;
import com.viettel.bss.viettelpos.v4.adapter.IncomeStatementAdapter;
import com.viettel.bss.viettelpos.v4.adapter.ReportRegisterDetailAdapter;
import com.viettel.bss.viettelpos.v4.bo.IncomeDTO;
import com.viettel.bss.viettelpos.v4.bo.IncomeModel;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DateTimeDialogWrapper;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.ReplaceFragment;
import com.viettel.bss.viettelpos.v4.commons.StringUtils;
import com.viettel.gem.asynctask.GetIncomeStatementAsyncTask;
import com.viettel.gem.utils.RecyclerUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class IncomeStatementFragment extends FragmentCommon implements IncomeStatementAdapter.OnIncomeClickListener {

    @BindView(R.id.edtDate)
    EditText edtDate;

    @BindView(R.id.btnXemLuong)
    Button btnXemLuong;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    List<IncomeDTO> incomeDTOList = new ArrayList<>();

    IncomeStatementAdapter incomeStatementAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idLayout = R.layout.fragment_income_statement;
    }

    @Override
    protected void unit(View v) {
        new DateTimeDialogWrapper(edtDate, getActivity()).setHideDay(true).setTitle(getString(R.string.title_select_ky_luong));
        edtDate.setText(DateTimeUtils.getFirstDateOfMonthWithoutDay());

        RecyclerUtils.setupVerticalRecyclerView(getActivity(), recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));
    }

    @Override
    protected void setPermission() {

    }

    @Override
    public void onResume() {
        super.onResume();
        setTitleActionBar(R.string.txt_bao_cao_thu_nhap);
    }

    @OnClick(R.id.btnXemLuong)
    public void onClickXemLuong() {
        if(!validateDate()) return;
        String salaryPeriod = edtDate.getText().toString();
        getIncomeStatement(salaryPeriod);
    }

    private boolean validateDate() {
        Calendar calDate = Calendar.getInstance();
        calDate.setTime(DateTimeUtils.convertStringToTime(
                edtDate.getText().toString(), "MM/yyyy"));

        Calendar curDate = Calendar.getInstance();
        if (calDate.getTime().after(curDate.getTime())) {
            CommonActivity.createAlertDialog(getActivity(),
                    getString(R.string.error_date_now),
                    getString(R.string.app_name)).show();
            return false;
        }

        return true;
    }

    private void getIncomeStatement(String salaryPeriod) {

        new GetIncomeStatementAsyncTask(getActivity(), new OnPostExecuteListener<IncomeModel>() {
            @Override
            public void onPostExecute(IncomeModel result, String errorCode, String description) {
                if (!CommonActivity.isNullOrEmpty(result) && !CommonActivity.isNullOrEmpty(result.getIncomeDTOList())) {
                    if(!CommonActivity.isNullOrEmpty(incomeDTOList)) {
                        incomeDTOList.clear();
                    } else {
                        incomeDTOList = new ArrayList<>();
                    }
                    incomeDTOList.addAll(result.getIncomeDTOList());
                    incomeStatementAdapter = new IncomeStatementAdapter(getActivity(), incomeDTOList, IncomeStatementFragment.this);
                    incomeStatementAdapter.setTemporary(true);
                    incomeStatementAdapter.setShowSalaryTypeName(true);
                    recyclerView.setAdapter(incomeStatementAdapter);
                } else {
                    if(!CommonActivity.isNullOrEmpty(incomeDTOList)) {
                        incomeDTOList.clear();
                    } else {
                        incomeDTOList = new ArrayList<>();
                    }
                    incomeStatementAdapter = new IncomeStatementAdapter(getActivity(), incomeDTOList, IncomeStatementFragment.this);
                    recyclerView.setAdapter(incomeStatementAdapter);
                    if(!CommonActivity.isNullOrEmpty(result)) description = result.getDescription();
                    if (errorCode.equals(Constant.INVALID_TOKEN2)) {
                        Dialog dialog = CommonActivity.createAlertDialog(getActivity(), description,
                                getResources().getString(R.string.app_name), moveLogInAct);
                        dialog.show();
                    } else {
                        if(CommonActivity.isNullOrEmpty(description))  {
                            description = getResources().getString(R.string.no_data);
                        }
                        Dialog dialog = CommonActivity.createAlertDialog(getActivity(), description,
                                getResources().getString(R.string.app_name));
                        dialog.show();
                    }
                }
            }
        }, moveLogInAct).execute(salaryPeriod);
    }

    @Override
    public void onItemClick(IncomeDTO incomeDTO) {
        if(CommonActivity.isNullOrEmpty(incomeDTO) || CommonActivity.isNullOrEmpty(incomeDTO.getFormula())) return;
        IncomeStatementDetailsFragment incomeStatementDetailsFragment = new IncomeStatementDetailsFragment();
        Bundle b = new Bundle();
        b.putSerializable("income", incomeDTO);
        incomeStatementDetailsFragment.setArguments(b);
        ReplaceFragment.replaceFragment(getActivity(), incomeStatementDetailsFragment, true);
    }
}
