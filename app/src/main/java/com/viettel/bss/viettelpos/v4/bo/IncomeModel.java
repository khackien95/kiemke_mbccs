package com.viettel.bss.viettelpos.v4.bo;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

/**
 * Created by BaVV on 02/06/2018.
 */
@Root(name = "return", strict = false)
public class IncomeModel implements Serializable {
    @Element(name = "errorCode", required = false)
    private String errorCode;
    @Element(name = "description", required = false)
    private String description;
    @ElementList(name = "salaryBeans", entry = "salaryBeans", required = false, inline = true)
    private List<IncomeDTO> incomeDTOList;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<IncomeDTO> getIncomeDTOList() {
        return incomeDTOList;
    }

    public void setIncomeDTOList(List<IncomeDTO> incomeDTOList) {
        this.incomeDTOList = incomeDTOList;
    }
}
