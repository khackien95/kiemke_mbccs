package com.viettel.bss.viettelpos.v4.listener;

import com.viettel.bss.viettelpos.v4.object.ProfileBO;

/**
 * Interface finish dialog select file
 * Created by toancx on 5/23/2017.
 */
public interface OnFinishDSFListener {
    public void onFinish(ProfileBO output);
}
