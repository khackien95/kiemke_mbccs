package com.viettel.bss.viettelpos.v4.sale.asytask;

import android.app.Activity;
import android.util.Log;
import android.view.View.OnClickListener;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.asyntask.AsyncTaskCommonSupper;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

public class AsyncGetLstStockOrderStaffDetail extends
        AsyncTaskCommonSupper<String, Void, ParseOuput> {

    public AsyncGetLstStockOrderStaffDetail(Activity context,
                                            OnPostExecuteListener<ParseOuput> listener,
                                            OnClickListener moveLogInAct) {

        super(context, listener, moveLogInAct);
    }

    @Override
    protected ParseOuput doInBackground(String... arg0) {
        return getLstStockOrderStaffDetail(arg0[0]);
    }

    private ParseOuput getLstStockOrderStaffDetail(String stockOrderId) {
        String original = "";
        ParseOuput parseOuput = new ParseOuput();
        try {
            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_getLstStockOrderStaffDetail");
            StringBuilder rawData = new StringBuilder();
            rawData.append("<ws:getLstStockOrderStaffDetail>");
            rawData.append("<input>");
            rawData.append("<token>").append(Session.getToken()).append("</token>");
            rawData.append("<stockOrderId>").append(stockOrderId).append("</stockOrderId>");
            rawData.append("</input>");
            rawData.append("</ws:getLstStockOrderStaffDetail>");
            Log.i("RowData", rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData
                    .toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope, Constant.BCCS_GW_URL,
                    mActivity, "mbccs_getLstStockOrderStaffDetail");
            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i("Responseeeeeeeeee Original", original);


            Serializer serializer = new Persister();
            parseOuput = serializer.read(ParseOuput.class, original);
            if(parseOuput == null){
                parseOuput = new ParseOuput();
                parseOuput.setErrorCode(Constant.ERROR_CODE);
                parseOuput.setDescription(mActivity.getString(R.string.no_data));
            }
            return parseOuput;
        } catch (Exception e) {
            Log.d("mbccs_getLstStockOrderStaffDetail", e.toString()
                    + "description error", e);
            parseOuput = new ParseOuput();
            parseOuput.setErrorCode(Constant.ERROR_CODE);
            parseOuput.setDescription(mActivity.getString(R.string.no_data));
        }
        return parseOuput;

    }

}
