package com.viettel.bss.viettelpos.v4.connecttionService.beans;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

@Root(name = "return", strict = false)
public class PakageChargeBeans  implements Serializable{
	@Element(name = "createDate", required = false)
	private String createDate;
	@Element(name = "groupProductId", required = false)
	private String groupProductId;
	@Element(name = "offerCode", required = false)
	private String offerCode;
	@Element(name = "offerId", required = false)
	private String offerId;
	@Element(name = "offerName", required = false)
	private String offerName;
	@Element(name = "offerTypeId", required = false)
	private String offerTypeId;
	@Element(name = "productId", required = false)
	private String productId;
	@Element(name = "startDate", required = false)
	private String startDate;
	@Element(name = "status", required = false)
	private String status;
	@Element(name = "productCode", required = false)
	private String productCode;
	@Element(name = "msType", required = false)
	private String msType;
	@Element(name = "isFTTB", required = false)
	private String isFTTB;
	@Element(name = "description", required = false)
	private String description;
	@Element(name = "mstNoRf", required = false)
	private String mstNoRf;
	
	
	public String getMstNoRf() {
		return mstNoRf;
	}
	public void setMstNoRf(String mstNoRf) {
		this.mstNoRf = mstNoRf;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIsFTTB() {
		return isFTTB;
	}
	public void setIsFTTB(String isFTTB) {
		this.isFTTB = isFTTB;
	}
	public String getMsType() {
		return msType;
	}
	public void setMsType(String msType) {
		this.msType = msType;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getGroupProductId() {
		return groupProductId;
	}
	public void setGroupProductId(String groupProductId) {
		this.groupProductId = groupProductId;
	}
	public String getOfferCode() {
		return offerCode;
	}
	public void setOfferCode(String offerCode) {
		this.offerCode = offerCode;
	}
	public String getOfferId() {
		return offerId;
	}
	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}
	public String getOfferName() {
		return offerName;
	}
	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}
	public String getOfferTypeId() {
		return offerTypeId;
	}
	public void setOfferTypeId(String offerTypeId) {
		this.offerTypeId = offerTypeId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
