package com.viettel.bss.viettelpos.v4.connecttionService.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.FragmentCommon;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.connecttionMobile.beans.AppointmentRequestDTO;
import com.viettel.bss.viettelpos.v4.connecttionService.asyn.ApprovedManageRequestAsyn;
import com.viettel.bss.viettelpos.v4.ui.DateTime;
import com.viettel.bss.viettelpos.v4.ui.DateTimePicker;
import com.viettel.bss.viettelpos.v4.ui.SimpleDateTimePicker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;

/**
 * Created by leekien on 2/9/2018.
 */

public class FragmentApprovedManageRequest extends FragmentCommon {
    @BindView(R.id.edtIsdn)
    EditText edtIsdn;

    @BindView(R.id.edtReqNote)
    EditText edtReqNote;

    @BindView(R.id.dpkReqDateTime)
    EditText dpkReqDateTime;

//    @BindView(R.id.edtHour)
//    EditText edtHour;

    @BindView(R.id.spnActionType)
    Spinner spnActionType;

    @BindView(R.id.edtContent)
    EditText edtContent;

    @BindView(R.id.btnConfirm)
    Button btnConfirm;

    AppointmentRequestDTO appointmentRequestDTO;

    private ArrayList<String> actionTypeList = new ArrayList<>();
    private Context context;
    private View.OnClickListener moveLoginAct;

    private Boolean choseAgain = false;
    private String hour = "00";
    private String min = "00";
    private DatePickerDialog.OnDateSetListener toDateCallBack = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if (!view.isShown()) {
                return;
            }
            Calendar cal = Calendar.getInstance();
            cal.set(year, monthOfYear, dayOfMonth);
//            int hour = cal.get(Calendar.HOUR_OF_DAY);
//            int min = cal.get(Calendar.MINUTE);

            dpkReqDateTime.setText(DateTimeUtils.convertDateTimeToString(cal.getTime(), "dd/MM/yyyy"));
        }
    };
    private OnPostExecuteListener onPostApproved = new OnPostExecuteListener() {
        @Override
        public void onPostExecute(Object result, String errorCode, String description) {
            if ("0".equals(errorCode)) {
                Dialog dialog = CommonActivity.createAlertDialog(getActivity(), getString(R.string.confirmOk),
                        getResources().getString(R.string.app_name));
                dialog.show();
                btnConfirm.setVisibility(View.GONE);
                return;
            } else {
                if (CommonActivity.isNullOrEmpty(description)) {
                    description = getString(R.string.confirmNOk);
                }
                Dialog dialog = CommonActivity.createAlertDialog(getActivity(), description,
                        getResources().getString(R.string.app_name));
                dialog.show();
                return;
            }
        }
    };

    public FragmentApprovedManageRequest(Context context, View.OnClickListener moveLoginAct) {
        this.context = context;
        this.moveLoginAct = moveLoginAct;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        idLayout = R.layout.layout_approved_manage_request;
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void unit(View v) {
        Bundle bundle = getArguments();
        appointmentRequestDTO = (AppointmentRequestDTO) bundle.getSerializable("appointmentRequestDTO");

        setTitleActionBar("Chi tiết yêu cầu: " + appointmentRequestDTO.getCustOrderDetailId());

        edtIsdn.setText(appointmentRequestDTO.getIsdn());
        edtIsdn.setFocusable(false);
        edtReqNote.setText(appointmentRequestDTO.getReqNote());
        edtReqNote.setFocusable(false);
        dpkReqDateTime.setText(DateTimeUtils.convertDate(appointmentRequestDTO.getReqDateTime()));
        dpkReqDateTime.setText(DateTimeUtils.convertDateHour(appointmentRequestDTO.getReqDateTime()));
        actionTypeList.add(getString(R.string.ok));
        actionTypeList.add(getString(R.string.n_ok));
        com.viettel.bss.viettelpos.v4.ui.image.utils.Utils.setDataSpinner(context, actionTypeList, spnActionType);

        dpkReqDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choseAgain = true;
//                CommonActivity.showDatePickerDialog(getActivity(), DateTimeUtils.convertStringToTime(dpkReqDateTime.getText().toString(), "dd/MM/yyyy HH:mm"), toDateCallBack);

                SimpleDateTimePicker.make(getString(R.string.txt_dat_lich),
                        new Date(), new DateTimePicker.OnDateTimeSetListener() {

                            @Override
                            public void DateTimeSet(Date date) {
                                // TODO Auto-generated method stub
                                DateTime mDateTime = new DateTime(date);
                                String dateReq = mDateTime
                                        .getDateString("dd/MM/yyyy HH:mm");
                                dpkReqDateTime.setText(dateReq);
                            }
                        }, getFragmentManager()).show();


            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String isdn = appointmentRequestDTO.getIsdn();
                String reqNote = appointmentRequestDTO.getReqNote();
                if (!validate()) {
                    return;
                }
                String resultDatetime = DateTimeUtils.formatDateInsert(DateTimeUtils.convertDate(appointmentRequestDTO.getReqDateTime()));
//                if (CommonActivity.isNullOrEmpty(dpkReqDateTime.getText().toString())) {
//                    CommonActivity.showConfirmValidate(getActivity(), R.string.null_reqDatetime);
//                    return;
//                }



                String dateNow = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(Calendar.getInstance().getTime());
                boolean checkTime = DateTimeUtils.convertStringToTime(dateNow, "dd/MM/yyyy HH:mm").after(DateTimeUtils.convertStringToTime(dpkReqDateTime.getText().toString(), "dd/MM/yyyy HH:mm"));

                if (checkTime) {
                    CommonActivity.showConfirmValidate(getActivity(), R.string.confirm_warn_date_now);
                    return;
                } else {
                    String[] date = dpkReqDateTime.getText().toString().split(" ")[0].split("/");
                    String hour = dpkReqDateTime.getText().toString().split(" ")[1] + ":00";
                    if(date.length>=2){
                        resultDatetime = date[2] + "-" +date[1]+"-" +date[0] + " " + hour;
                    }else {
                    resultDatetime = "0000-00-00" + " " + hour;
                }
                }



                String taskId = appointmentRequestDTO.getTaskId();
                String id = appointmentRequestDTO.getId();
                String custOrderDetailId = appointmentRequestDTO.getCustOrderDetailId();
                String resultNote = edtContent.getText().toString();
                String reasonName = appointmentRequestDTO.getReasonName();

                String reqDateTime = DateTimeUtils.formatDateInsert(DateTimeUtils.convertDate(appointmentRequestDTO.getReqDateTime()));
                String actionType = "";

                String actionText = spnActionType.getSelectedItem().toString();
                if (getString(R.string.ok).equals(actionText)) {
                    actionType = "1";
                } else {
                    actionType = "0";
                }


                ApprovedManageRequestAsyn approvedManageRequestAsyn = new ApprovedManageRequestAsyn((Activity) context, onPostApproved, moveLogInAct);
                approvedManageRequestAsyn.execute(actionType, isdn, taskId, reqNote, reqDateTime, reasonName, resultNote, resultDatetime, id, custOrderDetailId);

            }
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void setPermission() {

    }

    private Boolean validate() {
//        if (choseAgain && (CommonActivity.isNullOrEmpty(edtHour.getText().toString().trim()))) {
//            CommonActivity.showConfirmValidate(getActivity(), R.string.nul_detail_time);
//            return false;
//        }
        if (CommonActivity.isNullOrEmpty(edtContent.getText().toString())) {
            CommonActivity.showConfirmValidate(getActivity(), R.string.contentisempty);
            return false;
        }
        return true;
    }

}
