package com.viettel.bss.viettelpos.v4.login.object;
import java.io.Serializable;

/**
 * Created by hungtv64 on 1/25/2018.
 */

public class DataOutputLogin implements Serializable {

    private String description;
    private String errorCode;
    private String forceUpgrade;
    private String version;
    private String checkSyn;
    private String daysBetweenExpried;
    private String lstMenuString;
    private String serialSim;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getForceUpgrade() {
        return forceUpgrade;
    }

    public void setForceUpgrade(String forceUpgrade) {
        this.forceUpgrade = forceUpgrade;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCheckSyn() {
        return checkSyn;
    }

    public void setCheckSyn(String checkSyn) {
        this.checkSyn = checkSyn;
    }

    public String getDaysBetweenExpried() {
        return daysBetweenExpried;
    }

    public void setDaysBetweenExpried(String daysBetweenExpried) {
        this.daysBetweenExpried = daysBetweenExpried;
    }

    public String getLstMenuString() {
        return lstMenuString;
    }

    public void setLstMenuString(String lstMenuString) {
        this.lstMenuString = lstMenuString;
    }

    public String getSerialSim() {
        return serialSim;
    }

    public void setSerialSim(String serialSim) {
        this.serialSim = serialSim;
    }
}
