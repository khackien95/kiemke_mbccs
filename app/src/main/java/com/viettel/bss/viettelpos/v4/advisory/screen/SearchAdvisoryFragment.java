package com.viettel.bss.viettelpos.v4.advisory.screen;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.FragmentCommon;
import com.viettel.bss.viettelpos.v4.cc.object.CCOutput;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DataUtils;
import com.viettel.bss.viettelpos.v4.commons.Define;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.ReplaceFragment;
import com.viettel.bss.viettelpos.v4.commons.StringUtils;
import com.viettel.bss.viettelpos.v4.customer.asynctask.QRCodeAsyncTask;
import com.viettel.bss.viettelpos.v4.customer.manage.RegisterInfoFragment;
import com.viettel.bss.viettelpos.v4.customer.object.QRCodeModel;
import com.viettel.bss.viettelpos.v4.customer.object.QRInfoBean;
import com.viettel.bss.viettelpos.v4.task.AsynTaskGetSubscriberInfoTVBH;

import butterknife.BindView;

import static android.view.View.GONE;

/**
 * Created by hantt47 on 7/10/2017.
 */

public class SearchAdvisoryFragment extends FragmentCommon {

    @BindView(R.id.searchButton)
    Button searchButton;
    @BindView(R.id.edtNumberValue)
    EditText edtNumberValue;

    @Override
    public void onResume() {
        super.onResume();
        setTitleActionBar(getActivity().getString(R.string.advisory_customers));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.idLayout = R.layout.search_advisory_layout;
    }

    @Override
    protected void unit(View v) {

        //this.edtNumberValue.setText("973674067");

        this.searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //ReplaceFragment.replaceFragment(getActivity(), new MainAdvisoryFragment(new CCOutput()), false);

                doSearch();
            }
        });

        //[BaVV] Add QRCode Start
        ImageView mBtnQRCode = (ImageView) v.findViewById(R.id.btnScanQRCode);

        SharedPreferences preferences = getActivity().getSharedPreferences(
                Define.PRE_NAME, Activity.MODE_PRIVATE);
        String name = preferences.getString(Define.KEY_MENU_NAME, "");
        if (!name.contains("menu.app.qr_code_myvt")) {
            mBtnQRCode.setVisibility(GONE);
        }

        mBtnQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataUtils.scanQRCode(getActivity());
            }
        });
        //[BaVV] Add QRCode Start
    }

    @Override
    protected void setPermission() {

    }

    //[BaVV] Add QRCode Start
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constant.SCAN_QRCODE_REQUEST:
                    String qrCode = data.getStringExtra("SCAN_RESULT");
                    if(!CommonActivity.isNullOrEmpty(qrCode)) {
                        getInfoByQrcode(qrCode);
                    }
                    break;
            }
        }
    }

    private void getInfoByQrcode(String qrCode) {
        new QRCodeAsyncTask(getActivity(), new OnPostExecuteListener<QRInfoBean>() {
            @Override
            public void onPostExecute(QRInfoBean result, String errorCode, String description) {
                if ("0".equals(errorCode)) {
                    if(null != result) {
                        edtNumberValue.setText(CommonActivity.isNullOrEmpty(result.getQrCodeModel().getMsisdn()) ? "" : result.getQrCodeModel().getMsisdn());
                        if(!CommonActivity.isNullOrEmpty(edtNumberValue.getText().toString())) {
                            doSearch();
                        }

                        String idNo = result.getQrCodeModel().getIdNo();
                        String misdn = result.getQrCodeModel().getMsisdn();
                        if (CommonActivity.isNullOrEmpty(misdn)) {
                            CommonActivity.createAlertDialog(getActivity(),
                                    R.string.msg_qrcode_miss_isdn, R.string.app_name).show();
                        }
                    }
                } else if ("TOKEN_INVALID".equals(errorCode)) {
                    Dialog dialog = CommonActivity
                            .createAlertDialog(getActivity(),
                                    R.string.token_invalid, R.string.app_name,
                                    moveLogInAct);
                    dialog.show();
                } else {
                    CommonActivity.createAlertDialog(getActivity(), description, R.string.app_name).show();
                }
            }
        }, moveLogInAct).execute(qrCode);
    }

    private void doSearch() {
        new AsynTaskGetSubscriberInfoTVBH(getActivity(), new OnPostExecuteListener<CCOutput>() {
            @Override
            public void onPostExecute(CCOutput result, String errorCode, String description) {
                if(!CommonActivity.isNullOrEmpty(result)){
                    ReplaceFragment.replaceFragment(getActivity(), new MainAdvisoryFragment(result), false);
                } else {
                    CommonActivity.toast(getActivity(), R.string.no_data);
                }
            }
        }, moveLogInAct).execute(StringUtils.formatIsdn(edtNumberValue.getText().toString()));
    }
    //[BaVV] Add QRCode End
}