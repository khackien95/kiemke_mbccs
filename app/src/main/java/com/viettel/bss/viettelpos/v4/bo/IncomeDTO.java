package com.viettel.bss.viettelpos.v4.bo;

import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.DataUtils;
import com.viettel.bss.viettelpos.v4.commons.StringUtils;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

/**
 * Created by BaVV on 02/06/2018.
 */
@Root(name = "return", strict = false)
public class IncomeDTO implements Serializable {
    @Element(name = "columnName", required = false)
    private String columnName;
    @Element(name = "title", required = false)
    private String title;
    @Element(name = "value", required = false)
    private String value;
    @Element(name = "isTemporary", required = false)
    private String isTemporary;
    @Element(name = "formula", required = false)
    private String formula;
    @Element(name = "salaryTypeName", required = false)
    private String salaryTypeName;
    @Element(name = "strDependentColumn", required = false)
    private String strDependentColumn;
    @ElementList(name = "dependentColumns", entry = "dependentColumns", required = false, inline = true)
    private List<IncomeDTO> incomeDTOList;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        if(!CommonActivity.isNullOrEmpty(value) && value.contains(".")) {
            return value;
        }
        return StringUtils.formatMoney(DataUtils.safeToString(value));
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getIsTemporary() {
        return isTemporary;
    }

    public void setIsTemporary(String isTemporary) {
        this.isTemporary = isTemporary;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public String getSalaryTypeName() {
        return salaryTypeName;
    }

    public void setSalaryTypeName(String salaryTypeName) {
        this.salaryTypeName = salaryTypeName;
    }

    public String getStrDependentColumn() {
        return strDependentColumn;
    }

    public void setStrDependentColumn(String strDependentColumn) {
        this.strDependentColumn = strDependentColumn;
    }

    public List<IncomeDTO> getIncomeDTOList() {
        return incomeDTOList;
    }

    public void setIncomeDTOList(List<IncomeDTO> incomeDTOList) {
        this.incomeDTOList = incomeDTOList;
    }
}
