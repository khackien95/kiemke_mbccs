package com.viettel.bss.viettelpos.v4.plan.screen;

import android.app.Dialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity;
import com.viettel.bss.viettelpos.v4.base.BaseFragment;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.dialog.LoginDialog;
import com.viettel.bss.viettelpos.v4.plan.adapter.TabSaleWarningAdapter;
import com.viettel.bss.viettelpos.v4.plan.asynctask.GetSalePlansResultAsyncTask;
import com.viettel.bss.viettelpos.v4.plan.event.PlanEvent;
import com.viettel.bss.viettelpos.v4.plan.model.SalePlansResultModel;
import com.viettel.bss.viettelpos.v4.plan.model.SaleResultCtv;
import com.viettel.bss.viettelpos.v4.plan.model.WsResponseSaleCheckinCTV;
import com.viettel.bss.viettelpos.v4.plan.model.WsResponseSaleResultCTV;
import com.viettel.bss.viettelpos.v4.utils.FileUtils;
import com.viettel.bss.viettelpos.v4.utils.RecyclerUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by BaVV on 1/05/18.
 */
public class TabSaleWarningFragment extends BaseFragment implements TabSaleWarningAdapter.SaleWarningClickListener {

    @BindView(R.id.tvPlanDate)
    TextView tvPlanDate;

    @BindView(R.id.tvTotalRow)
    TextView tvTotalRow;

    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;

    private TabSaleWarningAdapter saleWarningAdapter;

    List<SaleResultCtv> saleResultCtvList = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_tab_sale_warning;
    }

    @Override
    protected void initData() {
        RecyclerUtils.setupVerticalRecyclerView(getContext(), recyclerView.getRecyclerView());
        saleWarningAdapter = new TabSaleWarningAdapter(getContext(), saleResultCtvList, this);
        recyclerView.setAdapter(saleWarningAdapter);
    }

    @Override
    protected boolean shouldListenEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PlanEvent event) {
        PlanEvent.Action action = event.getAction();
        switch (action) {
            case RELOAD_SALE_WARNING:
                SalePlansResultModel salePlansResultModel = (SalePlansResultModel) event.getData();
                if (null != salePlansResultModel) {
                    WsResponseSaleResultCTV wsResponseSaleResultCTV = salePlansResultModel.getWsResponseSaleResultCTV();
                    loadData(wsResponseSaleResultCTV);
                }

                break;
        }
    }

    private void loadData(WsResponseSaleResultCTV wsResponseSaleResultCTV) {
        if (null != wsResponseSaleResultCTV) {

            tvPlanDate.setText(DateTimeUtils.convertDateTimeToString(
                    DateTimeUtils.convertStringToTime(wsResponseSaleResultCTV.getPlanDate(), "yyyy-MM-dd"), "dd/MM/yyyy"));

            if (null != wsResponseSaleResultCTV.getResults()) {
                saleResultCtvList.addAll(wsResponseSaleResultCTV.getResults());
                saleWarningAdapter.notifyDataSetChanged();
            }

            tvTotalRow.setText(null == wsResponseSaleResultCTV.getResults() ? "0" : wsResponseSaleResultCTV.getResults().size() + "");
        }
    }

    private void clearData() {
        if (null == saleResultCtvList) {
            saleResultCtvList = new ArrayList<>();
        } else {
            saleResultCtvList.clear();
        }
        saleWarningAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(SaleResultCtv resultCtv) {

    }

}
