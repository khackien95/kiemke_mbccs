package com.viettel.bss.viettelpos.v4.login.asynctask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.util.Log;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.Define;
import com.viettel.bss.viettelpos.v4.commons.EncryptKeystore;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.SecurityUtil;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.commons.StringUtils;
import com.viettel.bss.viettelpos.v4.handler.VSAMenuHandler;
import com.viettel.bss.viettelpos.v4.login.object.DataOutputLogin;
import com.viettel.bss.viettelpos.v4.object.KeyPairs;
import com.viettel.bss.viettelpos.v4.sale.business.CacheData;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;
import com.viettel.savelog.SaveLog;

import org.apache.http.conn.ConnectTimeoutException;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Date;

import static com.viettel.bss.viettelpos.v4.commons.Session.userName;

/**
 * Created by hungtv64 on 1/25/2018.
 */

public class LoginAsyncTask extends AsyncTask<String, String, DataOutputLogin> {

    // feilds
    private ProgressDialog progressDialog;
    private Activity activity;
    private CountDownTimer countDownTimer;
    private SharedPreferences preferences;
    private DataOutputLogin dataOutputLogin;

    private final OnPostExecuteListener<DataOutputLogin> listener;

    private float count;
    private int maxProgess;

    public LoginAsyncTask(Activity activity,
                          OnPostExecuteListener<DataOutputLogin> listener,
                          SharedPreferences preferences) {

        this.activity = activity;
        this.preferences = preferences;
        this.progressDialog = new ProgressDialog(activity);
        this.dataOutputLogin = new DataOutputLogin();
        this.listener = listener;
        this.count = 0;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (progressDialog != null && !progressDialog.isShowing()) {
            progressDialog.setMessage(activity.getResources().getString(R.string.logining));
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.setProgress(0);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            maxProgess = Constant.LOGIN_TIME_OUT_RESPONE / 100;
            countDownTimer = new CountDownTimer(Constant.LOGIN_TIME_OUT_RESPONE, 100) {
                @Override
                public void onTick(long millisUntilFinished) {
                    count++;
                    if (progressDialog != null && progressDialog.isShowing()) {
                        publishProgress("" + count / 10);
                        progressDialog.setProgress(100 - (int) (count * 100 / maxProgess));
                    }
                }
                @Override
                public void onFinish() {
                    Log.v(this.getClass().getSimpleName(), "onFinish "
                            + count);
                    if (progressDialog != null && progressDialog.isShowing()) {
                        publishProgress(""
                                + Constant.LOGIN_TIME_OUT_RESPONE / 1000);
                        progressDialog.setProgress(100);
                    } else {
                        Log.v(this.getClass().getSimpleName(), "onFinish progress NULL or Not Showing");
                    }
                }
            };
            countDownTimer.start();
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        if (progressDialog != null) {
            String message = activity.getResources().getString(R.string.otp_processing);
            if (values != null && values.length > 0) {
                message += " " + values[0] + " s";
            }
            progressDialog.setMessage(message);
        }
    }

    @Override
    protected void onPostExecute(DataOutputLogin result) {
        super.onPostExecute(result);

        if (!activity.isFinishing() && progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        listener.onPostExecute(result, result.getErrorCode(), result.getDescription());
    }

    @Override
    protected DataOutputLogin doInBackground(String... params) {

        String user = params[0];
        String pass = params[1];
        String addInfo = params[2];
        String serialSim = params[3];

        if (user.trim().isEmpty()) {
            dataOutputLogin.setErrorCode(Constant.ERROR_USER_MISSING);
            return dataOutputLogin;
        }
        if (user.trim().isEmpty()) {
            dataOutputLogin.setErrorCode(Constant.ERROR_PASSWORD_MISSING);
            return dataOutputLogin;
        }
        if (!CommonActivity.isInternetReachable()) {
            dataOutputLogin.setErrorCode(Constant.ERROR_PING_SERVER);
            return dataOutputLogin;
        }

        // thientv7 them phan bo sung thong tin user kpilog
        String userNameContain = user;
        if ((Constant.addInfo)) {
            if (userNameContain.contains(";,;")) {
                String[] parts = userNameContain.split(";,;");
                String part1 = parts[0];
                String part2 = parts[1];
                Session.userName = part2;
                userNameContain = part1;
                addInfo = part2;
            } else {
                Session.userName = userNameContain;
            }
        } else {
            userName = userNameContain;
        }
        return loginBccs2(userNameContain, pass, addInfo, serialSim);
    }

    private DataOutputLogin loginBccs2(String user, String pass, String addInfo, String serialSim) {
        boolean isException = false;
        Exception exception = null;
        String original = "";

        ParseOuput parseOuput;
        CommonOutput commonOutput;
        String func = "mbccs_loginBccs2";
        try {
            BCCSGateway bccsGateway = new BCCSGateway();
            bccsGateway.addValidateGateway("username", Constant.BCCSGW_USER);
            bccsGateway.addValidateGateway("password", Constant.BCCSGW_PASS);
            bccsGateway.addValidateGateway("wscode", func);

            // KpiLog
            String envelope = bccsGateway.buildInputGatewayWithRawData(
                    buildRawData(user, pass, addInfo, serialSim), 0, true);
            Log.e("envlop Login", envelope);

            String response = bccsGateway.sendRequest(envelope,
                    Constant.BCCS_GW_URL, activity, func,
                    Constant.LOGIN_TIME_OUT_CONECT,
                    Constant.LOGIN_TIME_OUT_RESPONE);
            Log.e("response Login", response);

            commonOutput = bccsGateway.parseGWResponse(response);
            if (commonOutput == null) {
                dataOutputLogin.setDescription(activity.getResources().getString(R.string.exception));
                dataOutputLogin.setErrorCode(Constant.ERROR_CODE);
                return dataOutputLogin;
            }

            original = commonOutput.getOriginal();
            if (original != null) {
                Log.d("original", original);
            }

            // parser output
            Serializer serializer = new Persister();
            parseOuput = serializer.read(ParseOuput.class, original);
            if (!parseOuput.getErrorCode().equals("0")) {
                dataOutputLogin.setDescription(parseOuput.getDescription());
                dataOutputLogin.setErrorCode(Constant.ERROR_CODE);
                return dataOutputLogin;
            }

            VSAMenuHandler handler = (VSAMenuHandler) CommonActivity.parseXMLHandler(new VSAMenuHandler(), original);
            commonOutput = handler.getItem();
            if (commonOutput == null) {
                dataOutputLogin.setDescription(activity.getResources().getString(R.string.exception));
                dataOutputLogin.setErrorCode(Constant.ERROR_CODE);
                return dataOutputLogin;
            }

            dataOutputLogin.setDescription(handler.getUpgradeDescription());
            dataOutputLogin.setForceUpgrade(handler.getForceUpgrade());
            dataOutputLogin.setVersion(handler.getVersion());
            dataOutputLogin.setCheckSyn(handler.getCheckSyn());

            StringBuilder menuListString = new StringBuilder(";");
            if (handler.getLstVSAMenu() != null) {
                for (int i = 0; i < handler.getLstVSAMenu().size(); i++) {
                    menuListString.append(handler.getLstVSAMenu().get(i).getObjectName()).append(";");
                }
            }
            dataOutputLogin.setLstMenuString(menuListString.toString());
            dataOutputLogin.setDaysBetweenExpried(handler.getDaysBetweenExpried());

            Session.setFixErrorVersion(handler.getDebugMsg());
            CacheData.getInstanse().setVersion(handler.getVersion());

            String lastLogin = preferences.getString(Define.KEY_LOGIN_NAME, "");
            SharedPreferences.Editor editor = preferences.edit();
            if (!user.equalsIgnoreCase(lastLogin)) {
                activity.deleteDatabase(Define.DB_NAME);
                editor.putString(Constant.KEY_FINGER_USER, "0");
                editor.putString(Constant.KEY_PASS, "");
                editor.putString(Constant.KEY_IV, "");
            }
            editor.putString(Define.KEY_MENU_NAME, dataOutputLogin.getLstMenuString());
            editor.putString(Define.KEY_LOGIN_NAME, user.toUpperCase());
            editor.putString(Define.KEY_TRACKING, handler.getIsTracking());

            KeyPairs keyPairs = new KeyPairs();
            keyPairs.setClientPrivateKey(handler.getPrivateKey());
            keyPairs.setViettelPublicKey(handler.getPublicKey());
            Session.setPublicKey(handler.getPublicKey());
            Session.setPrivateKey(handler.getPrivateKey());

            // Mac dinh
            String token = StringUtils.encryptIt(commonOutput.getToken(), activity);
            editor.putString(Define.KEY_TEMP, token);
            editor.putString(Define.KEY_INVALID_TOKEN, "0");

            if (!CommonActivity.isNullOrEmpty(addInfo)) {
                userName = addInfo.toUpperCase();
            } else {
                userName = user.toUpperCase();
            }
            Session.passWord = pass;
            String useFinger = preferences.getString(Constant.KEY_FINGER_USER, "");
            if ("1".equals(useFinger)) {
                String[] x = EncryptKeystore.encrypt(pass);
                if (x != null) {
                    editor.putString(Constant.KEY_PASS, x[0]);
                    editor.putString(Constant.KEY_IV, x[1]);
                }
            }
            editor.commit();

            // neu yeu cau OTP
            if (CommonActivity.isNullOrEmpty(commonOutput.getToken())) {
                dataOutputLogin.setErrorCode(Constant.OTP_REQIRE);
                return dataOutputLogin;
            }
            Session.setToken(commonOutput.getToken());

            dataOutputLogin.setErrorCode(Constant.SUCCESS_CODE);
            return dataOutputLogin;
        } catch (UnknownHostException ex) {
            isException = true;
            exception = ex;
            Log.e("UnknownHostException", ex.toString(), ex);
            dataOutputLogin.setDescription(activity.getResources()
                    .getString(R.string.unknown_host_exception));
            dataOutputLogin.setErrorCode(Constant.ERROR_CODE);
            return dataOutputLogin;
        } catch (ConnectTimeoutException ex) {
            Log.e("ConnectTimeoutException", ex.getMessage(), ex);
            isException = true;
            exception = ex;
            dataOutputLogin.setDescription(activity.getResources()
                    .getString(R.string.connect_timeout_exception));
            dataOutputLogin.setErrorCode(Constant.ERROR_CODE);
            return dataOutputLogin;
        } catch (SocketTimeoutException ex) {
            Log.e("SocketTimeoutException", ex.getMessage(), ex);
            isException = true;
            exception = ex;
            dataOutputLogin.setDescription(activity.getResources()
                    .getString(R.string.socket_timeout_exception));
            dataOutputLogin.setErrorCode(Constant.ERROR_CODE);
            return dataOutputLogin;
        } catch (SocketException ex) {
            Log.e("SocketException", ex.getMessage(), ex);
            isException = true;
            exception = ex;
            dataOutputLogin.setDescription(activity.getResources()
                    .getString(R.string.socket_close_exception));
            dataOutputLogin.setErrorCode(Constant.ERROR_CODE);
            return dataOutputLogin;
        } catch (IllegalStateException ex) {
            Log.e("IllegalStateException", ex.getMessage(), ex);
            isException = true;
            exception = ex;
            dataOutputLogin.setDescription(activity.getResources()
                    .getString(R.string.illegal_state_exception));
            dataOutputLogin.setErrorCode(Constant.ERROR_CODE);
            return dataOutputLogin;
        } catch (IllegalArgumentException ex) {
            isException = true;
            exception = ex;
            Log.e("IllegalArgumentException", ex.getMessage(), ex);
            dataOutputLogin.setDescription(activity.getResources()
                    .getString(R.string.illegal_state_exception) + ex.toString());
            dataOutputLogin.setErrorCode(Constant.ERROR_CODE);
            return dataOutputLogin;
        } catch (IOException ex) {
            Log.e("IOException", ex.getMessage(), ex);
            isException = true;
            exception = ex;
            dataOutputLogin.setDescription(activity.getResources()
                    .getString(R.string.socket_io_exception));
            dataOutputLogin.setErrorCode(Constant.ERROR_CODE);
            return dataOutputLogin;
        } catch (Exception ex) {
            Log.e("Exception", ex.getMessage(), ex);
            isException = true;
            exception = ex;
            dataOutputLogin.setDescription(activity.getResources()
                    .getString(R.string.exception));
            dataOutputLogin.setErrorCode(Constant.ERROR_CODE);
            return dataOutputLogin;
        } finally {
            try {
                if (isException) {
                    SaveLog saveLog = new SaveLog(activity,
                            Constant.SYSTEM_NAME, userName, "loginBccs2_exception",
                            CommonActivity.findMyLocation(activity).getX(),
                            CommonActivity.findMyLocation(activity).getY());

                    saveLog.saveLogException(exception, new Date(), new Date(), userName
                            + "_" + CommonActivity.getDeviceId(activity)
                            + "_" + System.currentTimeMillis());
                }
            } catch (Exception e) {
                Log.e("Login", e.getMessage());
            }
        }
    }

    private String buildRawData(String user, String pass, String addInfo, String serialSim) throws Exception {

        StringBuilder result = new StringBuilder();
        result.append("<ws:login>");

        result.append("<userName>");
        result.append(new SecurityUtil().encrypt(user));
        result.append("</userName>");

        result.append("<passWord>");
        result.append(new SecurityUtil().encrypt(pass));
        result.append("</passWord>");

        result.append("<addInfo>");
        result.append(new SecurityUtil().encrypt(addInfo));
        result.append("</addInfo>");

        result.append("<clientTime>");
        result.append(System.currentTimeMillis());
        result.append("</clientTime>");

        result.append("<version>");
        result.append(CommonActivity.getversionclient(activity));
        result.append("</version>");

        result.append("<serialSim>");
        if (serialSim == null || serialSim.trim().isEmpty()) {
            serialSim = user.toUpperCase();
        }
        result.append(serialSim);
        result.append("</serialSim>");

        result.append("<osType>").append(Constant.OS_TYPE).append("</osType>");
        result.append("<networkType>").append(Constant.versionType == Constant.PUBLIC_INTERNET_VERSION ?
                Constant.NETWORK_TYPE.PUBLIC : Constant.NETWORK_TYPE.PRIVATE).append("</networkType>");

        result.append("</ws:login>");
        return result.toString();
    }
}