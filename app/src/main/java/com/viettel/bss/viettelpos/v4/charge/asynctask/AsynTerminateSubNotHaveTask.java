package com.viettel.bss.viettelpos.v4.charge.asynctask;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostSuccessExecute;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.sale.object.SubscriberDTO;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

/**
 * Created by thuandq on 6/27/2017.
 */

// thanh ly
public class AsynTerminateSubNotHaveTask extends AsyncTask<String, Void, ParseOuput> {
    private Activity activity;
    private ProgressDialog progress;
    private String errorCode;
    private String description;
    private String subId;
    private SubscriberDTO subscriberDTO;
    private String reasonId;
    private OnPostSuccessExecute<ParseOuput> onPostExecute;
    protected View.OnClickListener moveLogInAct;

    public AsynTerminateSubNotHaveTask(Activity activity, String subId, String reasonId, SubscriberDTO subscriberDTO, OnPostSuccessExecute<ParseOuput> onPostExecute,
                                       View.OnClickListener moveLogInAct) {
        this.activity = activity;
        this.subId = subId;
        this.moveLogInAct = moveLogInAct;
        this.subscriberDTO = subscriberDTO;
        this.reasonId = reasonId;
        this.onPostExecute = onPostExecute;
        this.moveLogInAct = moveLogInAct;
        this.progress = new ProgressDialog(activity);
        this.progress.setCancelable(false);
        this.progress
                .setMessage(activity.getString(R.string.processing));
        this.progress.show();
    }

    @Override
    protected ParseOuput doInBackground(String... params) {
        return block();
    }

    @Override
    protected void onPostExecute(ParseOuput result) {
        super.onPostExecute(result);
        this.progress.dismiss();
        onPostExecute.onPostSuccess(result);
        description = result.getDescription();
        if ("0".equals(result.getErrorCode())) {

            if (CommonActivity.isNullOrEmpty(description)) {
                description = "Thực hiện thành công!";
            }
            Dialog dialog = CommonActivity.createAlertDialog(activity, description,
                    activity.getString(R.string.app_name));
            dialog.show();
        } else {
            if (Constant.INVALID_TOKEN2.equals(errorCode)) {
                Dialog dialog = CommonActivity.createAlertDialog(activity,
                        description,
                        activity.getResources().getString(R.string.app_name),
                        moveLogInAct);
                dialog.show();
            } else {
                if (description == null || description.isEmpty()) {
                    description = activity.getString(R.string.checkdes);
                }
                Dialog dialog = CommonActivity.createAlertDialog(activity,
                        description,
                        activity.getResources().getString(R.string.app_name));
                dialog.show();
            }
        }

    }

    private ParseOuput block() {
        String original = "";
        ParseOuput out = null;
        String func = "terminateSubNotHaveTask";
        try {

            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_" + func);
            StringBuilder rawData = new StringBuilder();
            rawData.append("<ws:" + func + ">");
            rawData.append("<input>");
            rawData.append("<token>" + Session.getToken() + "</token>");
            if (!CommonActivity.isNullOrEmpty(subId))
                rawData.append("<subId>").append(subId).append("</subId>");
            if (!CommonActivity.isNullOrEmpty(reasonId))
                rawData.append("<reasonId>").append(reasonId).append("</reasonId>");
            rawData.append("</input>");
            rawData.append("</ws:" + func + ">");
            Log.i("RowData", rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData
                    .toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope, Constant.BCCS_GW_URL,
                    activity, "mbccs_" + func);
            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i(" Original", response);

            // parser
            Serializer serializer = new Persister();
            out = serializer.read(ParseOuput.class, original);
            if (out == null) {
                description = activity
                        .getString(R.string.no_return_from_system);
                errorCode = Constant.ERROR_CODE;
                out.setErrorCode(Constant.ERROR_CODE);
                out.setDescription(description);
            }
        } catch (Exception e) {
            Log.e(Constant.TAG, "blockSubForTerminate", e);
            out.setErrorCode(Constant.ERROR_CODE);
            out.setDescription(description);
        }

        return out;
    }

    private Boolean validate() {
        if (CommonActivity.isNullOrEmpty(subId)) {
            Dialog dialog = CommonActivity.createAlertDialog(activity, activity.getString(R.string.checkSubscriber),
                    activity.getString(R.string.app_name));
            dialog.show();
            return false;
        }
        if (CommonActivity.isNullOrEmpty(reasonId)) {
            Dialog dialog = CommonActivity.createAlertDialog(activity, activity.getString(R.string.checklydothanhly),
                    activity.getString(R.string.app_name));
            dialog.show();
            return false;
        }

        return true;
    }
}
