package com.viettel.bss.viettelpos.v4.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.FragmentCommon;
import com.viettel.bss.viettelpos.v4.adapter.IncomeStatementAdapter;
import com.viettel.bss.viettelpos.v4.bo.IncomeDTO;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.DataUtils;
import com.viettel.bss.viettelpos.v4.commons.DateTimeDialogWrapper;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.commons.ReplaceFragment;
import com.viettel.bss.viettelpos.v4.commons.StringUtils;
import com.viettel.gem.utils.RecyclerUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class IncomeStatementDetailsFragment extends FragmentCommon implements IncomeStatementAdapter.OnIncomeClickListener {

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvValue)
    TextView tvValue;
    @BindView(R.id.tvFormula)
    TextView tvFormula;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    IncomeDTO mIncomeDTO;

    List<IncomeDTO> incomeDTOList = new ArrayList<>();

    IncomeStatementAdapter incomeStatementAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idLayout = R.layout.fragment_income_statement_details;
    }

    @Override
    protected void unit(View v) {

        Bundle bundle = getArguments();
        mIncomeDTO = (IncomeDTO) bundle.get("income");

        RecyclerUtils.setupVerticalRecyclerView(getActivity(), recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        if(CommonActivity.isNullOrEmpty(mIncomeDTO)) return;

        setTitleActionBar(mIncomeDTO.getTitle());

        String value = DataUtils.safeToString(mIncomeDTO.getValue());
        if(CommonActivity.isNullOrEmpty(value)) value = "0";
        tvValue.setText(value/* + " VNĐ"*/);

        tvTitle.setText(DataUtils.safeToString(mIncomeDTO.getTitle()));
        tvFormula.setText("(= " + DataUtils.safeToString(mIncomeDTO.getFormula()) + ")");

        incomeDTOList = mIncomeDTO.getIncomeDTOList();
        if(!CommonActivity.isNullOrEmpty(incomeDTOList)) {
            incomeStatementAdapter = new IncomeStatementAdapter(getContext(), incomeDTOList, this);
            recyclerView.setAdapter(incomeStatementAdapter);
        }
    }

    @Override
    protected void setPermission() {

    }

    @Override
    public void onResume() {
        super.onResume();

        if(null != mIncomeDTO) setTitleActionBar(mIncomeDTO.getTitle());
    }

    @Override
    public void onItemClick(IncomeDTO incomeDTO) {
        if(CommonActivity.isNullOrEmpty(incomeDTO) || CommonActivity.isNullOrEmpty(incomeDTO.getFormula())) return;

        IncomeStatementDetailsFragment incomeStatementDetailsFragment = new IncomeStatementDetailsFragment();
        Bundle b = new Bundle();
        b.putSerializable("income", incomeDTO);
        incomeStatementDetailsFragment.setArguments(b);
        ReplaceFragment.replaceFragment(getActivity(), incomeStatementDetailsFragment, true);
    }
}
