package com.viettel.bss.viettelpos.v4.stockinpect.bo;

/**
 * Created by leekien on 5/9/2018.
 */

public class BaseDTO {
    protected String keySet;

    public String getKeySet() {
        return keySet;
    }

    public void setKeySet(String keySet) {
        this.keySet = keySet;
    }
}
