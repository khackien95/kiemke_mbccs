package com.viettel.bss.viettelpos.v4.stockinpect.view;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.FragmentCommon;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.SdCardHelper;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.dialog.LoginDialog;
import com.viettel.bss.viettelpos.v4.stockinpect.asyntask.AsycntaskDoAddSerialFormBCCS;
import com.viettel.bss.viettelpos.v4.stockinpect.asyntask.AsyncTaskDoStockInpect;
import com.viettel.bss.viettelpos.v4.stockinpect.asyntask.AsynctaskBeforeStockInspect;
import com.viettel.bss.viettelpos.v4.stockinpect.bo.StockInspectCheckDTO;
import com.viettel.bss.viettelpos.v4.stockinpect.bo.StockInspectDTO;
import com.viettel.bss.viettelpos.v4.stockinpect.bo.StockInspectRealDTO;
import com.viettel.bss.viettelpos.v4.stockinpect.control.adapter.RecycleViewSerialAdapter;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;
import com.viettel.gem.utils.RecyclerUtils;
import com.viettel.savelog.SaveLog;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StockInpectWithSerialActivity extends FragmentCommon {
    private static final int TO_SERIAL_CODE = 5683;
    private static final int FROM_SERIAL_CODE = 5684;
    private static final int SERIAL_CODE = 5685;
    //    @BindView(R.id.rdGroupSerial)
//    RadioGroup rdGroupSerial;
//    @BindView(R.id.rdSerial)
//    RadioButton rdSerial;
//    @BindView(R.id.rdListSerial)
//    RadioButton rdListSerial;
    @BindView(R.id.lnNumSerial)
    LinearLayout lnNumSerial;
    @BindView(R.id.txtserial)
    EditText txtserial;
    @BindView(R.id.btnbar)
    Button btnbar;
    @BindView(R.id.lnFromSerial)
    LinearLayout lnFromSerial;
    @BindView(R.id.txtSerialFrom)
    EditText txtSerialFrom;
    @BindView(R.id.btnbarFrom)
    Button btnbarFrom;
    @BindView(R.id.lnToSerial)
    LinearLayout lnToSerial;
    @BindView(R.id.txtSerialTo)
    EditText txtSerialTo;
    @BindView(R.id.btnbarTo)
    Button btnbarTo;
    @BindView(R.id.btnAddSerial)
    Button btnAddSerial;
    @BindView(R.id.quantity)
    TextView quantiy;
    @BindView(R.id.recListSerial)
    RecyclerView recListSerial;
    @BindView(R.id.confirm_inspect_serial)
    CheckBox confirm_inspect_serial;
    @BindView(R.id.btnInspect)
    Button btnInspect;

    int inputMethod = 1;

    List<StockInspectCheckDTO> stockInspectCheckDTOS; // mat hang duoc them

    RecycleViewSerialAdapter recycleViewSerialAdapter;

    StockInspectDTO checkStockInspectDTO; //Chứa các thông tin Kiểm kê lấy từ giao diện
    StockInspectDTO stockInspectSaveDTO; //đối tượng được trả về từ hàm 1.2.9
    String staticNote;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        idLayout = R.layout.inspect_manager_serial;
        ButterKnife.bind(getActivity());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void unit(View v) {
        stockInspectCheckDTOS = new ArrayList<>();
        Bundle bundle = getArguments();
        if (bundle != null && bundle.getSerializable("value") != null) {
            checkStockInspectDTO = (StockInspectDTO) bundle.getSerializable("stockInspectDTO");
            stockInspectSaveDTO=(StockInspectDTO) bundle.getSerializable("stockInspectSaveDTO");
            stockInspectCheckDTOS = (List<StockInspectCheckDTO>) bundle.getSerializable("listProductCheck");
            staticNote = bundle.getString("staticNote");
        }
        quantiy.setText(staticNote);

//        checkStockInspectDTO.setInspectStatus((long) 2);
//        checkStockInspectDTO.setOwnerId((long) 268);
//        checkStockInspectDTO.setStateId((long) 1);
//        checkStockInspectDTO.setProdOfferId((long) 7);
//        checkStockInspectDTO.setProdOfferTypeId((long) 8);
//        checkStockInspectDTO.setInspectStatus((long) 0);
//
//        stockInspectSaveDTO = new StockInspectDTO();
//        stockInspectSaveDTO.setInspectStatus((long) 2);
//        stockInspectSaveDTO.setOwnerId((long) 268);
//        stockInspectSaveDTO.setOwnerType((long) 2);
//        stockInspectSaveDTO.setProdOfferId((long) 7);
//        stockInspectSaveDTO.setProdOfferTypeId((long) 8);
//        stockInspectSaveDTO.setStateId((long) 1);
//
//
//        stockInspectCheckDTOS.add(new StockInspectCheckDTO("a", Long.parseLong("52345"), (long) 114352345, false,
//                "2313", (long) 1, (long) 1, "a", (long) 1, (long) 1, (long) 1, "a", "dt",
//                (long) 1, (long) 1, (long) 1, (long) 1, (long) 1, "no", null, (long) 1, "state", (long) 1, "567"));
//        stockInspectCheckDTOS.add(new StockInspectCheckDTO("a", Long.parseLong("52345"), (long) 114352345, false,
//                "2313", (long) 1, (long) 1, "a", (long) 1, (long) 1, (long) 1, "a", "dt",
//                (long) 1, (long) 1, (long) 1, (long) 1, (long) 1, "no", null, (long) 1, "state", (long) 1, "567"));
//        stockInspectCheckDTOS.add(new StockInspectCheckDTO("a", Long.parseLong("52345"), (long) 114352345, false,
//                "2313", (long) 1, (long) 1, "a", (long) 1, (long) 1, (long) 1, "a", "dt",
//                (long) 1, (long) 1, (long) 1, (long) 1, (long) 1, "no", null, (long) 1, "state", (long) 1, "567"));
//        stockInspectCheckDTOS.add(new StockInspectCheckDTO("a", Long.parseLong("52345"), (long) 114352345, false,
//                "2313", (long) 1, (long) 1, "a", (long) 1, (long) 1, (long) 1, "a", "dt",
//                (long) 1, (long) 1, (long) 1, (long) 1, (long) 1, "no", null, (long) 1, "state", (long) 1, "567"));
//        stockInspectCheckDTOS.add(new StockInspectCheckDTO("a", Long.parseLong("52345"), (long) 114352345, false,
//                "2313", (long) 1, (long) 1, "a", (long) 1, (long) 1, (long) 1, "a", "dt",
//                (long) 1, (long) 1, (long) 1, (long) 1, (long) 1, "no", null, (long) 1, "state", (long) 1, "567"));
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
//        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        recListSerial.setLayoutManager(layoutManager);
        recycleViewSerialAdapter = new RecycleViewSerialAdapter(stockInspectCheckDTOS);
//        recListSerial.setAdapter(recycleViewSerialAdapter);

        RecyclerUtils.setupVerticalRecyclerView(getContext(), recListSerial);
        recListSerial.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recListSerial.setAdapter(recycleViewSerialAdapter);

        btnAddSerial.setOnClickListener(this);
        btnbar.setOnClickListener(this);
        btnbarFrom.setOnClickListener(this);
        btnbarTo.setOnClickListener(this);
        btnInspect.setOnClickListener(this);

        txtserial.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    txtSerialFrom.setEnabled(true);
                    txtSerialTo.setEnabled(true);
                } else {
                    txtSerialFrom.setEnabled(false);
                    txtSerialTo.setEnabled(false);
                }
            }
        });

        txtSerialTo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    txtserial.setEnabled(true);
                } else {
                    txtserial.setEnabled(false);
                }
            }
        });

        txtSerialFrom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    txtserial.setEnabled(true);
                } else {
                    txtserial.setEnabled(false);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitleActionBar(getActivity().getString(R.string.manage_stock_serial));
    }

    @Override
    public void onClick(View arg0) {
        int id = arg0.getId();
        switch (id) {
            case R.id.btnInspect: {
                if (confirm_inspect_serial.isChecked()) {
                    AsynctaskBeforeStockInspect asynctaskBeforeStockInspect = new AsynctaskBeforeStockInspect(getActivity(),
                            new OnPostExecuteListener<ParseOuput>() {
                                @Override
                                public void onPostExecute(ParseOuput result, String errorCode, String description) {
                                    boolean checkOK = result.getStockInspectMessage().isCheckOk();
                                    if ("0".equals(errorCode)) {
                                        if (checkOK) {
                                            doStockInspect(true);
                                        } else {
                                            //hien thong bao
                                            CommonActivity.createDialog(getActivity(),
                                                    result.getStockInspectMessage().getWarnMessage(),
                                                    getString(R.string.app_name),
                                                    getString(R.string.cancel), getString(R.string.ok),
                                                    null, new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            doStockInspect(true);
                                                        }
                                                    }).show();
                                        }
                                    } else {
                                        CommonActivity.showConfirmValidate(getActivity(), R.string.no_data);
                                    }
                                }
                            }, moveLogInAct, checkStockInspectDTO, stockInspectCheckDTOS, 1 + "", true);
                    asynctaskBeforeStockInspect.execute();
                    Toast.makeText(getActivity(), R.string.stocking_status, Toast.LENGTH_SHORT).show();
                } else {
                    //warn
                    AsynctaskBeforeStockInspect asynctaskBeforeStockInspect = new AsynctaskBeforeStockInspect(getActivity(), new OnPostExecuteListener<ParseOuput>() {
                        @Override
                        public void onPostExecute(ParseOuput result, String errorCode, String description) {
                            if ("0".equals(errorCode)) {
                                boolean checkOK = result.getStockInspectMessage().isCheckOk();
                                if (checkOK) {
                                    //kiem ke
                                    doStockInspect(false);
                                } else {
                                    //hien thong bao

                                    CommonActivity.createDialog(getActivity(),
                                            result.getStockInspectMessage().getWarnMessage(),
                                            getString(R.string.app_name),
                                            getString(R.string.cancel), getString(R.string.ok),
                                            null,
                                            new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    doStockInspect(false);
                                                }
                                            }).show();


                                }
                            } else {
                                CommonActivity.showConfirmValidate(getActivity(), R.string.no_data);
                            }
                        }
                    }, moveLogInAct, checkStockInspectDTO, stockInspectCheckDTOS, 1 + "", false);
                    asynctaskBeforeStockInspect.execute();
                    //test ws
//                    AsyncTaskDoStockInpect asyncTaskDoStockInpect =
//                            new AsyncTaskDoStockInpect(getActivity(), new OnPostExecuteListener<ParseOuput>() {
//                                @Override
//                                public void onPostExecute(ParseOuput result, String errorCode, String description) {
//                                    Toast.makeText(getActivity(), result.getStockInspectMessage().getMessageSuccess(), Toast.LENGTH_SHORT).show();
//                                    btnInspect.setEnabled(false);
//                                }
//                            }, moveLogInAct, checkStockInspectDTO, stockInspectSaveDTO, stockInspectCheckDTOS, 1 + "", false);
//                    asyncTaskDoStockInpect.execute();
//                    Toast.makeText(getActivity(), R.string.stocking_status, Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.btnAddSerial: {
                String _serial = txtserial.getText().toString();
                String _serial_to = txtSerialTo.getText().toString();
                String _serial_from = txtSerialFrom.getText().toString();

                //do
                if (isValidateSerial(_serial) == false) {
                    if (isValidateSerial(_serial_from) == false || isValidateSerial(_serial_to) == false) {
                        CommonActivity.createAlertDialog(getActivity(), getActivity().getString(R.string.chua_nhap_serial),
                                getActivity().getString(R.string.app_name)).show();
                    } else {
                        StockInspectRealDTO stockInspectRealDTO = new StockInspectRealDTO();
                        stockInspectRealDTO.setSingleSerial(_serial);
                        stockInspectRealDTO.setToSerial(_serial_to);
                        stockInspectRealDTO.setFromSerial(_serial_from);
                        AsycntaskDoAddSerialFormBCCS asycntaskDoAddSerialFormBCCS =
                                new AsycntaskDoAddSerialFormBCCS(getActivity(),
                                        new OnPostExecuteListener<ParseOuput>() {
                                            @Override
                                            public void onPostExecute(ParseOuput result, String errorCode, String description) {
                                                if ("0".equals(errorCode)) {
                                                    if (result.getStockInspectMessage().isAddSuccessSerial() == true) {
                                                        if (!CommonActivity.isNullOrEmpty(result.getStockInspectMessage().getListProductCheck())) {
                                                            List<StockInspectCheckDTO> stockInspectCheckDTOS1 = result.getStockInspectMessage().getListProductCheck();
                                                               stockInspectCheckDTOS.clear();
                                                            for (int i = 0; i < stockInspectCheckDTOS1.size(); i++)
                                                                stockInspectCheckDTOS.add(stockInspectCheckDTOS1.get(i));
                                                            recycleViewSerialAdapter.notifyDataSetChanged();
                                                        }
                                                    } else {
                                                        Dialog dialog = CommonActivity
                                                                .createAlertDialog(getContext(), result.getStockInspectMessage().getMessageWarnAddSerial(), getContext()
                                                                        .getResources().getString(R.string.app_name));
                                                        dialog.show();
                                                    }
                                                } else {
                                                    CommonActivity.showConfirmValidate(getActivity(), R.string.no_data);
                                                }
                                            }
                                        }, moveLogInAct, stockInspectRealDTO, inputMethod, stockInspectSaveDTO, checkStockInspectDTO, stockInspectCheckDTOS);
                        asycntaskDoAddSerialFormBCCS.execute();
                    }

                } else {
                    StockInspectRealDTO stockInspectRealDTO = new StockInspectRealDTO();
                    stockInspectRealDTO.setSingleSerial(_serial);
                    stockInspectRealDTO.setToSerial(_serial_to);
                    stockInspectRealDTO.setFromSerial(_serial_from);
                    AsycntaskDoAddSerialFormBCCS asycntaskDoAddSerialFormBCCS =
                            new AsycntaskDoAddSerialFormBCCS(getActivity(),
                                    new OnPostExecuteListener<ParseOuput>() {
                                        @Override
                                        public void onPostExecute(ParseOuput result, String errorCode, String description) {
                                            if ("0".equals(errorCode)) {
                                                if (result.getStockInspectMessage().isAddSuccessSerial() == true) {
                                                    if (!CommonActivity.isNullOrEmpty(result.getStockInspectMessage().getListProductCheck())) {
                                                        List<StockInspectCheckDTO> stockInspectCheckDTOS1 = result.getStockInspectMessage().getListProductCheck();
                                                        for (int i = 0; i < stockInspectCheckDTOS1.size(); i++)
                                                            stockInspectCheckDTOS.add(stockInspectCheckDTOS1.get(i));

                                                        recycleViewSerialAdapter.notifyDataSetChanged();
                                                    }
                                                } else {
                                                    Dialog dialog = CommonActivity
                                                            .createAlertDialog(getContext(), result.getStockInspectMessage().getMessageWarnAddSerial(), getContext()
                                                                    .getResources().getString(R.string.app_name));
                                                    dialog.show();
                                                }
                                            } else {
                                                CommonActivity.showConfirmValidate(getActivity(), R.string.no_data);
                                            }
                                        }
                                    }, moveLogInAct, stockInspectRealDTO, inputMethod, stockInspectSaveDTO, checkStockInspectDTO, stockInspectCheckDTOS);
                    asycntaskDoAddSerialFormBCCS.execute();
                }
                break;
            }
            case R.id.btnbarFrom: {
                try {
                    Intent intent = new Intent(Constant.ACTION_SCAN);
                    intent.putExtra("SCAN_FORMATS",
                            "CODE_39,CODE_93,CODE_128,DATA_MATRIX,ITF,CODABAR,EAN_13,EAN_8,UPC_A,QR_CODE");
                    startActivityForResult(intent, FROM_SERIAL_CODE);

                } catch (ActivityNotFoundException anfe) {

                    View.OnClickListener click = new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            UpdateApkAsyn updateApkAsyn = new UpdateApkAsyn(getActivity());
                            updateApkAsyn.execute();

                        }
                    };
                    CommonActivity.createDialog(getActivity(),
                            getActivity().getString(R.string.confirmdlapk),
                            getActivity().getString(R.string.app_name),
                            getActivity().getString(R.string.cancel),
                            getActivity().getString(R.string.ok), null,
                            click).show();
                }
                break;
            }
            case R.id.btnbar: {
                inputMethod = 2;
                try {
                    Intent intent = new Intent(Constant.ACTION_SCAN);
                    intent.putExtra("SCAN_FORMATS",
                            "CODE_39,CODE_93,CODE_128,DATA_MATRIX,ITF,CODABAR,EAN_13,EAN_8,UPC_A,QR_CODE");
                    startActivityForResult(intent, SERIAL_CODE);

                } catch (ActivityNotFoundException anfe) {

                    View.OnClickListener click = new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            UpdateApkAsyn updateApkAsyn = new UpdateApkAsyn(getActivity());
                            updateApkAsyn.execute();

                        }
                    };
                    CommonActivity.createDialog(getActivity(),
                            getActivity().getString(R.string.confirmdlapk),
                            getActivity().getString(R.string.app_name),
                            getActivity().getString(R.string.cancel),
                            getActivity().getString(R.string.ok), null,
                            click).show();
                }
                break;
            }
            case R.id.btnbarTo: {
                inputMethod = 2;
                try {
                    Intent intent = new Intent(Constant.ACTION_SCAN);
                    intent.putExtra("SCAN_FORMATS",
                            "CODE_39,CODE_93,CODE_128,DATA_MATRIX,ITF,CODABAR,EAN_13,EAN_8,UPC_A,QR_CODE");
                    startActivityForResult(intent, TO_SERIAL_CODE);

                } catch (ActivityNotFoundException anfe) {

                    View.OnClickListener click = new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            UpdateApkAsyn updateApkAsyn = new UpdateApkAsyn(getActivity());
                            updateApkAsyn.execute();

                        }
                    };
                    CommonActivity.createDialog(getActivity(),
                            getActivity().getString(R.string.confirmdlapk),
                            getActivity().getString(R.string.app_name),
                            getActivity().getString(R.string.cancel),
                            getActivity().getString(R.string.ok), null,
                            click).show();
                }
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TAG 9", "FragmentConnectionMobileNew onActivityResult requestCode : " + requestCode);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case FROM_SERIAL_CODE: {
                    String contents = data.getStringExtra("SCAN_RESULT");
                    txtSerialFrom.setText(contents);
                    txtserial.requestFocus();

                    try {
                        SaveLog saveLog = new SaveLog(getActivity(),
                                Constant.SYSTEM_NAME, Session.userName,
                                "connectmobile_barcode", CommonActivity.findMyLocation(
                                getActivity()).getX(), CommonActivity
                                .findMyLocation(getActivity()).getY());
                        saveLog.saveLogRequest(
                                " barcode scan serial : " + contents,

                                new Date(), new Date(), Session.userName + "_"
                                        + CommonActivity.getDeviceId(getActivity())
                                        + "_" + System.currentTimeMillis());
                    } catch (Exception e) {
                        Log.d("saveLog", e.toString());
                    }
                    break;
                }
                case TO_SERIAL_CODE: {
                    String contents = data.getStringExtra("SCAN_RESULT");
                    txtSerialTo.setText(contents);
                    txtserial.requestFocus();

                    try {
                        SaveLog saveLog = new SaveLog(getActivity(),
                                Constant.SYSTEM_NAME, Session.userName,
                                "connectmobile_barcode", CommonActivity.findMyLocation(
                                getActivity()).getX(), CommonActivity
                                .findMyLocation(getActivity()).getY());
                        saveLog.saveLogRequest(
                                " barcode scan serial : " + contents,

                                new Date(), new Date(), Session.userName + "_"
                                        + CommonActivity.getDeviceId(getActivity())
                                        + "_" + System.currentTimeMillis());
                    } catch (Exception e) {
                        Log.d("saveLog", e.toString());
                    }
                    break;
                }
                case SERIAL_CODE: {
                    String contents = data.getStringExtra("SCAN_RESULT");
                    txtserial.setText(contents);
                    txtserial.requestFocus();

                    try {
                        SaveLog saveLog = new SaveLog(getActivity(),
                                Constant.SYSTEM_NAME, Session.userName,
                                "connectmobile_barcode", CommonActivity.findMyLocation(
                                getActivity()).getX(), CommonActivity
                                .findMyLocation(getActivity()).getY());
                        saveLog.saveLogRequest(
                                " barcode scan serial : " + contents,

                                new Date(), new Date(), Session.userName + "_"
                                        + CommonActivity.getDeviceId(getActivity())
                                        + "_" + System.currentTimeMillis());
                    } catch (Exception e) {
                        Log.d("saveLog", e.toString());
                    }
                    break;
                }

            }
        }
    }

    @Override
    protected void setPermission() {

    }

    public class UpdateApkAsyn extends AsyncTask<String, Void, String> {

        String urlinstall = "";

        ProgressDialog progress;
        private Context context = null;

        public UpdateApkAsyn(Context context) {
            this.context = context;
            this.progress = new ProgressDialog(this.context);
            // check font

            this.progress.setMessage(context.getResources().getString(
                    R.string.processingdl));
            if (!this.progress.isShowing()) {
                this.progress.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            return UpdateVersion(Session.getToken());
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase(Constant.SUCCESS_CODE)) {
                progress.dismiss();
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setDataAndType(Uri.fromFile(new File(urlinstall)),
//                        "application/vnd.android.package-archive");
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(intent);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(new File(urlinstall)),
                            "application/vnd.android.package-archive");
//			intent.setDataAndType(FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".fileprovider", new File(urlinstall)),
//					"application/vnd.android.package-archive");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {
                    Log.d(Constant.TAG, "fileApk path = " + urlinstall);
                    File fileApk = new File(urlinstall);
                    Uri apkUri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".fileprovider", fileApk);
                    Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                    intent.setDataAndType(apkUri,
                            "application/vnd.android.package-archive");
                    ;
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }

            } else {
                progress.dismiss();
                Dialog dialog = CommonActivity
                        .createAlertDialog((Activity) context, context
                                .getResources()
                                .getString(R.string.downloadfail), context
                                .getResources().getString(R.string.app_name));
                dialog.show();

            }
        }

        public String UpdateVersion(String token) {
            String result = "";
            String url = Constant.PATH_BARCODE + token;
            try {
                URL urlcontrol = new URL(url);
                Log.e("URL getVersion:", url);

                Log.i("Bo nho con trong", Double.toString(SdCardHelper
                        .getAvailableInternalMemorySize()));

                double availAbleMemory = SdCardHelper
                        .getAvailableInternalMemorySize();

                if (availAbleMemory > 50) {

                    InputStream input = new BufferedInputStream(
                            urlcontrol.openStream());
                    OutputStream output;

                    File file = new File(Constant.MBCCS_TEMP_FOLDER);
                    if (!file.exists()) {
                        file.mkdirs();
                    }

                    output = new FileOutputStream(Constant.MBCCS_TEMP_FOLDER
                            + "barcode.apk");
                    urlinstall = Constant.MBCCS_TEMP_FOLDER + "barcode.apk";
                    Log.d("urlinstall", urlinstall);
                    byte data[] = new byte[1024];
                    int count = 0;

                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }

                    output.flush();
                    output.close();
                    input.close();

                    Log.e("FilePath", urlinstall);

                    Log.e("UPDATE VERSION", "End Download >>>>>>>>>>>>>>>> ");
                    result = Constant.SUCCESS_CODE;
                } else {
                    result = Constant.ERROR_CODE;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

    }

    private boolean isValidateSerial(String s) {

        if (CommonActivity.isNullOrEmpty(s.trim())) {
            return false;  // sai
        }
        return true;
    }

    protected final View.OnClickListener moveLogInAct = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LoginDialog dialog = new LoginDialog(getActivity(), permission);
            dialog.show();
        }
    };

    private final View.OnClickListener onclickConfirmUpdate = new View.OnClickListener() {

        @Override
        public void onClick(View arg0) {
            AsyncTaskDoStockInpect asyncTaskDoStockInpect =
                    new AsyncTaskDoStockInpect(getActivity(), new OnPostExecuteListener<ParseOuput>() {
                        @Override
                        public void onPostExecute(ParseOuput result, String errorCode, String description) {
                            Toast.makeText(getActivity(), result.getStockInspectMessage().getMessageSuccess(), Toast.LENGTH_SHORT).show();
                            btnInspect.setEnabled(false);
                        }
                    }, moveLogInAct, checkStockInspectDTO, stockInspectSaveDTO, stockInspectCheckDTOS, 1 + "", true);
            asyncTaskDoStockInpect.execute();
        }
    };

    private void doStockInspect(boolean confirm) {
        AsyncTaskDoStockInpect asyncTaskDoStockInpect =
                new AsyncTaskDoStockInpect(getActivity(), new OnPostExecuteListener<ParseOuput>() {
                    @Override
                    public void onPostExecute(ParseOuput result, String errorCode, String description) {
                        if ("0".equals(errorCode)) {
                            if (result.isSuccess()) {
                                Toast.makeText(getActivity(), R.string.inspect_success, Toast.LENGTH_SHORT).show();
                                btnInspect.setEnabled(false);
                            } else {
                                Toast.makeText(getActivity(), R.string.inspect_fail, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            CommonActivity.showConfirmValidate(getActivity(), R.string.no_data);
                        }
                    }
                }, moveLogInAct, checkStockInspectDTO, stockInspectSaveDTO, stockInspectCheckDTOS, 1 + "", confirm);
        asyncTaskDoStockInpect.execute();
    }

}
