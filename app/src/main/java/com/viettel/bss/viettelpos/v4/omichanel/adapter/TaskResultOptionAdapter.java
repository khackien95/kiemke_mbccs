package com.viettel.bss.viettelpos.v4.omichanel.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.StringUtils;
import com.viettel.bss.viettelpos.v4.omichanel.dao.PoCatalogOutsideDTO;
import com.viettel.bss.viettelpos.v4.omichanel.dao.TaskResultOption;
import com.viettel.bss.viettelpos.v4.omichanel.dialog.UpdateJobDialog;
import com.viettel.bss.viettelpos.v4.omichanel.fragment.EditVasPlusFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hungtv64 on 3/9/2018.
 */

public class TaskResultOptionAdapter extends
        RecyclerView.Adapter<TaskResultOptionAdapter.ViewHolder> {

    private Context context;
    private UpdateJobDialog updateJobDialog;
    private List<TaskResultOption> taskResultOptions;

    public TaskResultOptionAdapter(
            Context context,
            UpdateJobDialog updateJobDialog,
            List<TaskResultOption> taskResultOptions) {

        this.context = context;
        this.updateJobDialog = updateJobDialog;
        this.taskResultOptions = taskResultOptions;
    }

    @Override
    public TaskResultOptionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_select_ctv_kpi, parent, false);
        return new TaskResultOptionAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TaskResultOptionAdapter.ViewHolder holder, final int position) {

        final TaskResultOption taskResultOption = taskResultOptions.get(position);

        if (taskResultOption.isSelected()) {
            holder.rbOptionSelect.setChecked(true);
        } else {
            holder.rbOptionSelect.setChecked(false);
        }

        holder.rbOptionSelect.setText(taskResultOption.getName());

        holder.rbOptionSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateJobDialog.processSelectItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return taskResultOptions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.llContent)
        LinearLayout llContent;
        @BindView(R.id.rbOptionSelect)
        RadioButton rbOptionSelect;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}