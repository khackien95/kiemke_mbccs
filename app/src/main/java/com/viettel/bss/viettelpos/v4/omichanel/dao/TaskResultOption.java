package com.viettel.bss.viettelpos.v4.omichanel.dao;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Created by hungtv64 on 3/9/2018.
 */
@Root(name = "taskResultOption", strict = false)
public class TaskResultOption implements Serializable {

    @Element(name = "createDate", required = false)
    protected String createDate;
    @Element(name = "createUser", required = false)
    protected String createUser;
    @Element(name = "id", required = false)
    protected Long id;
    @Element(name = "name", required = false)
    protected String name;
    @Element(name = "resultCode", required = false)
    protected String resultCode;
    @Element(name = "resultToken", required = false)
    protected String resultToken;
    @Element(name = "status", required = false)
    protected Long status;
    @Element(name = "taskCode", required = false)
    protected String taskCode;

    @Element(name = "isSelected", required = false)
    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultToken() {
        return resultToken;
    }

    public void setResultToken(String resultToken) {
        this.resultToken = resultToken;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode;
    }
}
