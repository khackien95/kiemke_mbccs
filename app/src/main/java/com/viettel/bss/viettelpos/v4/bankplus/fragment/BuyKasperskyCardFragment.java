package com.viettel.bss.viettelpos.v4.bankplus.fragment;

import android.os.Bundle;
import android.view.View;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.FragmentCommon;

/**
 * Created by hantt47 on 7/25/2017.
 */

public class BuyKasperskyCardFragment extends FragmentCommon {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        idLayout = R.layout.bankplus_buy_kaspersky_card_fragment;
    }

    @Override
    public void unit(View v) {
        
    }

    @Override
    public void setPermission() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        setTitleActionBar(getString(R.string.bankplus_menu_buy_kaspersky_card));
    }
}