package com.viettel.bss.viettelpos.v4.stockinpect.bo;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

/**
 * Created by leekien on 5/11/2018.
 */
@Root(name = "stockInspectMessage", strict = false)
public class StockInspectMessage extends BaseMessage implements Serializable{
    @Element(name = "description", required = false)
    protected String description;
    @Element(name = "errorCode", required = false)
    protected String errorCode;
    @ElementList(name = "paramsMsg", entry = "paramsMsg", required = false, inline = true)
    protected List<String> paramsMsg;
    protected boolean success;
    @Element(name = "addSuccessSerial", required = false)
    protected boolean addSuccessSerial;
    @Element(name = "checkEndStockInpect", required = false)
    protected boolean checkEndStockInpect;
    @Element(name = "checkNedToDelete", required = false)
    protected boolean checkNedToDelete;
    @Element(name = "checkOk", required = false)
    protected boolean checkOk;
    @ElementList(name = "listNeedToDelete", entry = "listNeedToDelete", required = false, inline = true)
    protected List<StockInspectCheckDTO> listNeedToDelete;
    @ElementList(name = "listProductCheck", entry = "listProductCheck", required = false, inline = true)
    protected List<StockInspectCheckDTO> listProductCheck;
    @ElementList(name = "messageNotice", entry = "messageNotice", required = false)
    protected List<String> messageNotice;
    @Element(name = "messageSuccess", required = false)
    protected String messageSuccess;
    @Element(name = "messageWarnAddSerial", required = false)
    protected String messageWarnAddSerial;
    @Element(name = "staticNotice", required = false)
    protected String staticNotice;
    @Element(name = "stockInspectSaveDTO", required = false)
    protected StockInspectDTO stockInspectSaveDTO;
    @Element(name = "warnMessage", required = false)
    protected String warnMessage;

    public boolean isAddSuccessSerial() {
        return addSuccessSerial;
    }

    public void setAddSuccessSerial(boolean addSuccessSerial) {
        this.addSuccessSerial = addSuccessSerial;
    }

    public boolean isCheckEndStockInpect() {
        return checkEndStockInpect;
    }

    public void setCheckEndStockInpect(boolean checkEndStockInpect) {
        this.checkEndStockInpect = checkEndStockInpect;
    }

    public boolean isCheckNedToDelete() {
        return checkNedToDelete;
    }

    public void setCheckNedToDelete(boolean checkNedToDelete) {
        this.checkNedToDelete = checkNedToDelete;
    }

    public boolean isCheckOk() {
        return checkOk;
    }

    public void setCheckOk(boolean checkOk) {
        this.checkOk = checkOk;
    }

    public List<StockInspectCheckDTO> getListNeedToDelete() {
        return listNeedToDelete;
    }

    public void setListNeedToDelete(List<StockInspectCheckDTO> listNeedToDelete) {
        this.listNeedToDelete = listNeedToDelete;
    }

    public List<StockInspectCheckDTO> getListProductCheck() {
        return listProductCheck;
    }

    public void setListProductCheck(List<StockInspectCheckDTO> listProductCheck) {
        this.listProductCheck = listProductCheck;
    }

    public List<String> getMessageNotice() {
        return messageNotice;
    }

    public void setMessageNotice(List<String> messageNotice) {
        this.messageNotice = messageNotice;
    }

    public String getMessageSuccess() {
        return messageSuccess;
    }

    public void setMessageSuccess(String messageSuccess) {
        this.messageSuccess = messageSuccess;
    }

    public String getMessageWarnAddSerial() {
        return messageWarnAddSerial;
    }

    public void setMessageWarnAddSerial(String messageWarnAddSerial) {
        this.messageWarnAddSerial = messageWarnAddSerial;
    }

    public String getStaticNotice() {
        return staticNotice;
    }

    public void setStaticNotice(String staticNotice) {
        this.staticNotice = staticNotice;
    }

    public StockInspectDTO getStockInspectSaveDTO() {
        return stockInspectSaveDTO;
    }

    public void setStockInspectSaveDTO(StockInspectDTO stockInspectSaveDTO) {
        this.stockInspectSaveDTO = stockInspectSaveDTO;
    }

    public String getWarnMessage() {
        return warnMessage;
    }

    public void setWarnMessage(String warnMessage) {
        this.warnMessage = warnMessage;
    }
}
