package com.viettel.bss.viettelpos.v4.plan.screen;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.util.DialogUtils;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity;
import com.viettel.bss.viettelpos.v4.base.BaseFragment;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DataUtils;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.dialog.LoginDialog;
import com.viettel.bss.viettelpos.v4.object.Location;
import com.viettel.bss.viettelpos.v4.plan.adapter.PlanListAdapter;
import com.viettel.bss.viettelpos.v4.plan.asynctask.CreatePlansAsyncTask;
import com.viettel.bss.viettelpos.v4.plan.asynctask.InsertSaleCheckinAsyncTask;
import com.viettel.bss.viettelpos.v4.plan.event.PlanEvent;
import com.viettel.bss.viettelpos.v4.plan.model.SalePlansModel;
import com.viettel.bss.viettelpos.v4.utils.RecyclerUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by BaVV on 1/05/18.
 */
public class PlanListFragment extends BaseFragment implements PlanListAdapter.PlanListClickListener, View.OnClickListener {

    @BindView(R.id.listPlan)
    SuperRecyclerView rcvListSalePlan;

    private PlanListAdapter salePlansAdapter;

    List<SalePlansModel> salePlansModelList = new ArrayList<>();

    private int typeView = Constant.typeLapKeHoach;

    public static boolean allowBack = false;

    public static PlanListFragment getInstance(int typeView) {
        PlanListFragment planListFragment = new PlanListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("typeView", typeView);
        planListFragment.setArguments(bundle);
        return planListFragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_plan_list;
    }

    @Override
    protected void initUI() {
        super.initUI();
        if (getArguments() != null) {
            this.typeView = getArguments().getInt("typeView", Constant.typeLapKeHoach);
        }

        replaceCreatePlanFragment(CreatePlanFragment.getInstance(Constant.typeLapKeHoach));
    }

    @Override
    protected void initData() {
        RecyclerUtils.setupVerticalRecyclerView(getContext(), rcvListSalePlan.getRecyclerView());
//        rcvListSalePlan.setupMoreListener(this, 1);
//        rcvListSalePlan.hideMoreProgress();
//        rcvListSalePlan.removeMoreListener();

        salePlansAdapter = new PlanListAdapter(getContext(), salePlansModelList, PlanListFragment.this);
        rcvListSalePlan.setAdapter(salePlansAdapter);

    }

    @Override
    public void onResume() {
        setupTitle();
        super.onResume();
    }

    private void setupTitle() {
        MainActivity.getInstance().setTitleActionBar(R.string.title_plan_list);
        MainActivity.getInstance().disableMenu();
        MainActivity.getInstance().enableMenuPlan();
    }

    @Override
    protected boolean shouldListenEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PlanEvent event) {
        PlanEvent.Action action = event.getAction();
        switch (action) {
            case MENU_CREATE_PLAN:
                MainActivity.getInstance().disableMenuPlan();
                replaceCreatePlanFragment(CreatePlanFragment.getInstance(Constant.typeLapKeHoach));
                break;
            case BACK_FROM_CREATE_PLAN:
                setupTitle();
                break;
            case CHOOSE_NEW_PLAN:
                Object data = event.getData();
                if (data instanceof SalePlansModel) {
                    if (null == salePlansModelList) {
                        salePlansModelList = new ArrayList<>();
                    }
                    salePlansModelList.add((SalePlansModel) data);
                    salePlansAdapter.notifyDataSetChanged();
                }

                break;
            case SAVE_PLAN:
                if (validateSavePlan()) {
                    showConfirmCreatePlan();
                } else {
                    allowBack = true;
                    getActivity().onBackPressed();
                }
                break;
        }
    }

    private boolean validateSavePlan() {
        return !CommonActivity.isNullOrEmpty(salePlansModelList)/* && salePlansModelList.size() >= 5*/;
    }

    private void showConfirmCreatePlan() {
        try {
            String messg = getString(R.string.title_skip_create_plan);
            CommonActivity.createDialog(getActivity(), messg,
                    getString(R.string.app_name),
                    getString(R.string.say_ko), getString(R.string.say_co), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            allowBack = true;
                            getActivity().onBackPressed();
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View arg0) {
                            if (!CommonActivity.isNullOrEmpty(salePlansModelList) && salePlansModelList.size() >= 5) {
                                new CreatePlansAsyncTask(getActivity(), new OnPostExecuteListener<String>() {
                                    @Override
                                    public void onPostExecute(String result, String errorCode, String description) {
                                        if ("0".equals(errorCode)) {
                                            showDialogMessage("Lưu kế hoạch thành công");
                                            allowBack = true;
                                            getActivity().onBackPressed();
                                        } else if ("TOKEN_INVALID".equals(errorCode)) {
                                            Dialog dialog = CommonActivity
                                                    .createAlertDialog(getActivity(),
                                                            R.string.token_invalid, R.string.app_name,
                                                            moveLogInAct);
                                            dialog.show();
                                        } else {
                                            showDialogMessage(description);
                                        }
                                    }
                                }, moveLogInAct, salePlansModelList).execute();
                            } else {
                                showDialogMessage("Bạn phải lập kế hoạch 5 ngày liên tiếp");
                            }

                        }

                    }).show();
        } catch (Exception e) {
            Log.d("@@@", "Error showConfirmCreatePlan", e);
        }
    }

    private void replaceCreatePlanFragment(Fragment createPlanFragment) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        String tag = CreatePlanFragment.class.getName();
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            fragment = createPlanFragment;
            ft.add(R.id.frame_container, fragment, tag);
            ft.addToBackStack(tag);
        } else {
            ft.show(fragment);
        }
        Fragment newsfeedFragment =
                getActivity().getSupportFragmentManager().findFragmentByTag(PlanListFragment.class.getName());
        if (null != newsfeedFragment) {
            ft.hide(newsfeedFragment);
        }

        ft.commit();
    }

    @OnClick(R.id.btnSave)
    void save() {
        createPlan();
    }

    void createPlan() {

        if (!CommonActivity.isNullOrEmpty(salePlansModelList) && salePlansModelList.size() >= 5) {
            CommonActivity.createDialog(MainActivity.getInstance(),
                    MainActivity.getInstance().getResources().getString(R.string.msg_save_plan),
                    MainActivity.getInstance().getString(R.string.app_name),
                    MainActivity.getInstance().getString(R.string.cancel),
                    MainActivity.getInstance().getString(R.string.ok), null, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            new CreatePlansAsyncTask(getActivity(), new OnPostExecuteListener<String>() {
                                @Override
                                public void onPostExecute(String result, String errorCode, String description) {
                                    if ("0".equals(errorCode)) {
                                        showDialogMessage("Lưu kế hoạch thành công");
                                        allowBack = true;
                                        getActivity().onBackPressed();
                                    } else if ("TOKEN_INVALID".equals(errorCode)) {
                                        Dialog dialog = CommonActivity
                                                .createAlertDialog(getActivity(),
                                                        R.string.token_invalid, R.string.app_name,
                                                        moveLogInAct);
                                        dialog.show();
                                    } else {
                                        showDialogMessage(description);
                                    }
                                }
                            }, moveLogInAct, salePlansModelList).execute();
                        }
                    }).show();
        } else {
            showDialogMessage("Bạn phải lập kế hoạch 5 ngày liên tiếp");
        }

    }

    protected final View.OnClickListener moveLogInAct = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LoginDialog dialog = new LoginDialog(getActivity(), "");
            dialog.show();
        }
    };


    private void clearData() {
        if (null == salePlansModelList) {
            salePlansModelList = new ArrayList<>();
        } else {
            salePlansModelList.clear();
        }
        salePlansAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(final SalePlansModel salePlansModel) {

    }

    @Override
    public void deletePlan(final SalePlansModel salePlansModel) {
        if (null == salePlansModel || CommonActivity.isNullOrEmpty(salePlansModelList)) return;

        Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
                "Bạn có chắc muốn xóa kế hoạch này?", getString(R.string.app_name), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        salePlansModelList.remove(salePlansModel);
                        salePlansAdapter.notifyDataSetChanged();
                    }
                });
        dialog.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relaBackHome:
                getActivity().onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        allowBack = false;
        MainActivity.getInstance().disableMenuPlan();
        EventBus.getDefault().post(new PlanEvent(PlanEvent.Action.BACK_FROM_PLAN_LIST));
    }
}
