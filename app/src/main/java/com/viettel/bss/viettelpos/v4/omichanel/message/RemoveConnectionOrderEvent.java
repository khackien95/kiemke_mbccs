package com.viettel.bss.viettelpos.v4.omichanel.message;

import com.viettel.bss.viettelpos.v4.omichanel.dao.ConnectionOrder;

/**
 * Created by hungtv64 on 3/12/2018.
 */

public class RemoveConnectionOrderEvent {
    private String processId;
    public RemoveConnectionOrderEvent(String processId){
        this.processId = processId;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }
}
