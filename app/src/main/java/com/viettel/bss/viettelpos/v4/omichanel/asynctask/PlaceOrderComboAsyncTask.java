package com.viettel.bss.viettelpos.v4.omichanel.asynctask;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.viettel.bss.viettelpos.v3.connecttionService.model.OrderIdCombo;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.omichanel.dao.ProfileRecord;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncTaskCommon;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by knight on 18/01/2018.
 */

public class PlaceOrderComboAsyncTask extends AsyncTaskCommon<String, Void, ArrayList<OrderIdCombo>> {
    private String target = "";
    private Map<Long, List<ProfileRecord>> mapProfileRecordCombo;


    public PlaceOrderComboAsyncTask(Activity context,
                               OnPostExecuteListener<ArrayList<OrderIdCombo>> listener,
                               View.OnClickListener moveLogInAct,
                               String target,
                               Map<Long, List<ProfileRecord>> mapProfileRecordCombo) {

        super(context, listener, moveLogInAct);
        this.target = target;
        this.mapProfileRecordCombo = mapProfileRecordCombo;
    }

    @Override
    protected ArrayList<OrderIdCombo> doInBackground(String... params) {
        return placeOtherTask(params[0], params[1]);
    }

    private ArrayList<OrderIdCombo> placeOtherTask(String nameCus, String orderType) {
        String original = "";
        ParseOuput out = null;
        String func = "placeOrderCombo";
        try {

            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_" + func);
            StringBuilder rawData = new StringBuilder();
            rawData.append("<ws:" + func + ">");
            rawData.append("<input>");
            rawData.append("<token>" + Session.getToken() + "</token>");

            if(!CommonActivity.isNullOrEmpty(orderType)) {
                rawData.append("<orderType>").append(orderType).append("</orderType>");
            }
            rawData.append("<name>").append(nameCus).append("</name>");

            if(!CommonActivity.isNullOrEmpty(target)){
                rawData.append("<target>").append(target).append("</target>");
            }


            if(!CommonActivity.isNullOrEmptyArray(mapProfileRecordCombo)) {
                for (Map.Entry<Long, List<ProfileRecord>> entry : mapProfileRecordCombo.entrySet()) {
                    rawData.append("<profileRecordCombos>");

                    rawData.append("<subId>" + entry.getKey());
                    rawData.append("</subId>");

                    for (ProfileRecord item : entry.getValue()) {
                        rawData.append("<profileRecords>" + item.toXml());
                        rawData.append("</profileRecords>");
                    }
                    rawData.append("</profileRecordCombos>");
                }
            }
            rawData.append("</input>");
            rawData.append("</ws:" + func + ">");
            Log.i("RowData", rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData
                    .toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope, Constant.BCCS_GW_URL,
                    mActivity, "mbccs_" + func);
            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i(" Original", response);

            // parser
            Serializer serializer = new Persister();
            out = serializer.read(ParseOuput.class, original);

        } catch (Exception e) {
            Log.e(Constant.TAG, "blockSubForTerminate", e);
            description = e.getMessage();
            errorCode = Constant.ERROR_CODE;
        }
        if (CommonActivity.isNullOrEmpty(out)) {
            description = mActivity
                    .getString(R.string.no_return_from_system);
            errorCode = Constant.ERROR_CODE;
            return null;
        } else {
            description = out.getDescription();
            errorCode = out.getErrorCode();
        }
        return out.getLstOrderIdCombo();
    }
}
