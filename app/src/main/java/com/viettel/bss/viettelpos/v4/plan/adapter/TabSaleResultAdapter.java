package com.viettel.bss.viettelpos.v4.plan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.plan.model.SalePointInfo;
import com.viettel.bss.viettelpos.v4.plan.model.SaleResultCtv;
import com.viettel.bss.viettelpos.v4.work.object.SalePoint;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by BaVV on 9/1/2018.
 */

public class TabSaleResultAdapter extends RecyclerView.Adapter<TabSaleResultAdapter.SaleResultHolder> {

    private Context context;
    private List<SalePointInfo> salePointInfoList;
    private SaleResultClickListener clickListener;

    public TabSaleResultAdapter(Context context, List<SalePointInfo> salePointInfoList, SaleResultClickListener listener) {
        this.context = context;
        this.salePointInfoList = salePointInfoList;
        this.clickListener = listener;
    }

    @Override
    public SaleResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SaleResultHolder(LayoutInflater.from(context).inflate(R.layout.adapter_item_tab_sale_result_view, parent, false));
    }

    @Override
    public void onBindViewHolder(SaleResultHolder holder, int position) {
        final SalePointInfo resultCtv = salePointInfoList.get(position);
        if (null == resultCtv) return;

        holder.tvIndex.setText((position + 1) + ".");
        holder.tvAddress.setText(resultCtv.getAddress());
        holder.tvPlanDate.setText(resultCtv.getPlanDate());
        holder.tvPoinCode.setText(resultCtv.getSalePointCode());
        holder.tvPoinName.setText(resultCtv.getSalePointName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != clickListener) clickListener.onClick(resultCtv);
            }
        });
    }

    @Override
    public int getItemCount() {
        return null == salePointInfoList ? 0 : salePointInfoList.size();
    }

    public class SaleResultHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvIndex)
        TextView tvIndex;

        @BindView(R.id.tvAddress)
        TextView tvAddress;

        @BindView(R.id.tvPlanDate)
        TextView tvPlanDate;

        @BindView(R.id.tvPoinCode)
        TextView tvPoinCode;

        @BindView(R.id.tvPoinName)
        TextView tvPoinName;

        private SaleResultHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface SaleResultClickListener {
        void onClick(SalePointInfo salePointInfo);
    }
}
