package com.viettel.bss.viettelpos.v4.omichanel.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.omichanel.dao.PoCatalogOutsideDTO;

/**
 * Created by hungtv64 on 1/13/2018.
 */

public class BundleDetailDialog extends Dialog {

    private TextView tvBundleCode;
    private TextView tvBundleName;
    private TextView tvBundleDesc;
    private Button btnOk;

    private Context context;
    private PoCatalogOutsideDTO poCatalogOutsideDTO;

    public BundleDetailDialog(Context context, PoCatalogOutsideDTO poCatalogOutsideDTO) {
        super(context);
        this.context = context;
        this.poCatalogOutsideDTO = poCatalogOutsideDTO;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.omni_vas_plus_detail_dialog);

        this.tvBundleCode = (TextView) findViewById(R.id.tvVasCode);
        this.tvBundleName = (TextView) findViewById(R.id.tvVasName);
        this.tvBundleDesc = (TextView) findViewById(R.id.tvVasDetail);
        this.btnOk = (Button) findViewById(R.id.btnOk);

        this.tvBundleCode.setText(poCatalogOutsideDTO.getCode());
        this.tvBundleName.setText(poCatalogOutsideDTO.getName());
        this.tvBundleDesc.setText(poCatalogOutsideDTO.getDescription());

        this.tvBundleName.setVisibility(View.GONE);

        this.btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });
    }
}