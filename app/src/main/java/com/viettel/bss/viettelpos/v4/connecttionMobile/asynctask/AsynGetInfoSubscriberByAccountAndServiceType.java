package com.viettel.bss.viettelpos.v4.connecttionMobile.asynctask;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.connecttionService.beans.InfoSub;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncTaskCommon;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Created by thuandq on 3/6/2018.
 */

public class AsynGetInfoSubscriberByAccountAndServiceType extends AsyncTaskCommon<String, Void, InfoSub> {
    public AsynGetInfoSubscriberByAccountAndServiceType(Activity context, OnPostExecuteListener<InfoSub> listener, View.OnClickListener moveLogInAct) {
        super(context, listener, moveLogInAct);
    }

    @Override
    protected InfoSub doInBackground(String... strings) {
        return getInfoSubscriberByAccountAndServiceType(strings[0],strings[1]);
    }
    private InfoSub getInfoSubscriberByAccountAndServiceType(String account,String serviceType) {

        // Spin serviceItem = (Spin) spnService.getSelectedItem();
        InfoSub infoSub = new InfoSub();
        String original = "";
        try {
            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode",
                    "mbccs_getInfoSubscriberByAccountAndServiceType");
            StringBuilder rawData = new StringBuilder();
            rawData.append("<ws:getInfoSubscriberByAccountAndServiceType>");
            rawData.append("<input>");
            rawData.append("<token>").append(Session.getToken()).append("</token>");
            rawData.append("<account>").append(account).append("</account>");
            // if (serviceItem != null) {
            rawData.append("<serviceType>").append(serviceType).append("</serviceType>");
            rawData.append("<isGetCust>" + true + "</isGetCust>");
            // }
            rawData.append("</input>");
            rawData.append("</ws:getInfoSubscriberByAccountAndServiceType>");

            Log.i("LOG", "raw data" + rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData
                    .toString());
            Log.d("LOG", "Send evelop" + envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope,
                    Constant.BCCS_GW_URL, mActivity,
                    "mbccs_getInfoSubscriberByAccountAndServiceType");
            Log.i("LOG", "Respone:  " + response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i("LOG", "Responseeeeeeeeee Original  " + original);

            // parser
            Document doc = parse.getDomElement(original);
            NodeList nl = doc.getElementsByTagName("return");
            NodeList nodechild = null;
            NodeList nodeCustomer = null;
            for (int i = 0; i < nl.getLength(); i++) {
                Element e2 = (Element) nl.item(i);
                errorCode = parse.getValue(e2, "errorCode");
                description = parse.getValue(e2, "description");
                Log.d("errorCode", errorCode);

                // lay ra thong tin thue bao
                nodechild = doc.getElementsByTagName("infoSub");
                for (int j = 0; j < nodechild.getLength(); j++) {
                    Element e1 = (Element) nodechild.item(j);
                    String account1 = parse.getValue(e1, "account");
                    infoSub.setAccount(account1);
                    String precinct = parse.getValue(e1, "precinct");
                    infoSub.setPrecinct(precinct);
                    String prepaidCode = parse.getValue(e1, "prepaidCode");
                    infoSub.setPrepaidCode(prepaidCode);

                    String productCode = parse.getValue(e1, "productCode");
                    infoSub.setProductCode(productCode);
                    String promotionCode = parse.getValue(e1,
                            "promotionCode");
                    infoSub.setPromotionCode(promotionCode);
                    String province = parse.getValue(e1, "province");
                    infoSub.setProvince(province);

                    String district = parse.getValue(e1, "district");
                    infoSub.setDistrict(district);

                    String deployAddress = parse.getValue(e1,
                            "deployAddress");
                    infoSub.setDeployAddress(deployAddress);

                    nodeCustomer = e1.getElementsByTagName("customer");
                    for (int k = 0; k < nodeCustomer.getLength(); k++) {
                        Element e3 = (Element) nodeCustomer.item(k);

                        String customerName = parse.getValue(e3, "name");
                        infoSub.getCustommerByIdNoBean().setNameCustomer(
                                customerName);

                        String idNo = parse.getValue(e3, "idNo");
                        infoSub.getCustommerByIdNoBean().setIdNo(idNo);

                        String busPermitNo = parse.getValue(e3,
                                "busPermitNo");
                        infoSub.getCustommerByIdNoBean().setBusPermitNo(
                                busPermitNo);

                        String tin = parse.getValue(e3, "tin");
                        infoSub.getCustommerByIdNoBean().setTin(tin);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return infoSub;
    }

}
