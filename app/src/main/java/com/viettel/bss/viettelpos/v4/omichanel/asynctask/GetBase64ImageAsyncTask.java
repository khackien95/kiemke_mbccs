package com.viettel.bss.viettelpos.v4.omichanel.asynctask;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;

import com.viettel.bss.viettelpos.v4.commons.FileUtils;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.omichanel.dao.ImageObjectOmni;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hungtv64 on 3/16/2018.
 */

public class GetBase64ImageAsyncTask extends AsyncTask<String, Void, HashMap<String, ImageObjectOmni>> {

    OnPostExecuteListener<HashMap<String, ImageObjectOmni>> listener;
    private HashMap<String, ImageObjectOmni> hashMapImage;

    public GetBase64ImageAsyncTask(
            HashMap<String, ImageObjectOmni> hashMapImage,
            OnPostExecuteListener<HashMap<String, ImageObjectOmni>> listener) {

        this.hashMapImage = hashMapImage;
        this.listener = listener;
    }

    @Override
    protected HashMap<String, ImageObjectOmni> doInBackground(String... params) {
        return loadImageFromWebOperations();
    }

    @Override
    protected void onPostExecute(HashMap<String, ImageObjectOmni> hashMapImage) {
        super.onPostExecute(hashMapImage);
        listener.onPostExecute(hashMapImage, null, null);
    }

    public HashMap<String, ImageObjectOmni> loadImageFromWebOperations() {

        for (Map.Entry<String, ImageObjectOmni> entry : hashMapImage.entrySet()) {
            ImageObjectOmni imageObjectOmni = entry.getValue();
            imageObjectOmni.setBase64String(FileUtils.getBase64String(
                    imageObjectOmni.getBitmap(), imageObjectOmni.getFileExt()));
        }

        return hashMapImage;
    }
}