package com.viettel.bss.viettelpos.v4.sale.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.FragmentCommon;
import com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.commons.Define;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.ReplaceFragment;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.customer.object.Spin;
import com.viettel.bss.viettelpos.v4.dialog.FixedHoloDatePickerDialog;
import com.viettel.bss.viettelpos.v4.dialog.LoginDialog;
import com.viettel.bss.viettelpos.v4.sale.activity.CreateRequestOrderFragment;
import com.viettel.bss.viettelpos.v4.sale.adapter.OrderTransAdapter;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncCancelOrderStaff;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncGetLstStockOrderStaff;
import com.viettel.bss.viettelpos.v4.sale.object.StockOrderStaffDTO;
import com.viettel.bss.viettelpos.v4.ui.image.utils.Utils;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by thinhhq1 on 4/3/2018.
 */

public class SearchOrderTransFragment extends FragmentCommon implements OrderTransAdapter.OnCancelOrder {

    private RecyclerView lvOrder;
    private FloatingActionButton imgCreateOrder;
    private TextView txtTitle;
    private EditText edtFromDate;
    private EditText edtToDate;
    private Button btnSearch;
    private Spinner spnMyOrder,spnStatus;
    private OrderTransAdapter orderTransAdapter;
    private ArrayList<StockOrderStaffDTO> listStockOrderStaffDTO;
    private StockOrderStaffDTO stockOrderStaffDTOMain;
    private Spin spinStatus;
    private Spin spinMyOrder;
    public  static SearchOrderTransFragment intance = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        intance = this;
        idLayout = R.layout.search_request_transitem_fragment;
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void unit(View v) {


        Calendar cal = Calendar.getInstance();
        int fromYear = cal.get(Calendar.YEAR);
        int fromMonth = cal.get(Calendar.MONTH);
        int fromDay = cal.get(Calendar.DAY_OF_MONTH);


        StringBuilder strFromDate = new StringBuilder();
        StringBuilder strToDate = new StringBuilder();
        if (fromDay < 10) {
            strToDate.append("0");
        }
        strToDate.append(fromDay).append("/");
        strFromDate.append("01").append("/");
        if (fromMonth < 9) {
            strToDate.append("0");
            strFromDate.append("0");
        }
        strFromDate.append(fromMonth + 1).append("/");
        strToDate.append(fromMonth + 1).append("/");
        strFromDate.append(fromYear);
        strToDate.append(fromYear);




        txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        txtTitle.setVisibility(View.GONE);
        lvOrder = (RecyclerView) v.findViewById(R.id.lvOrder);
        lvOrder.setHasFixedSize(true);
        lvOrder.setNestedScrollingEnabled(false);
        lvOrder.setLayoutManager(new LinearLayoutManager(getActivity()));
        imgCreateOrder = (FloatingActionButton) v.findViewById(R.id.imgCreateOrder);
        imgCreateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateRequestOrderFragment fragment = new CreateRequestOrderFragment();
                Bundle bundle = new Bundle();
                bundle.putString("TYPE", Define.STATUS_CREATE_NEW);
                fragment.setArguments(bundle);
                fragment.setTargetFragment(SearchOrderTransFragment.this, 100);
                ReplaceFragment.replaceFragment(getActivity(), fragment, true);
            }
        });

        spnMyOrder = (Spinner) v.findViewById(R.id.spnMyOrder);
        spnMyOrder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spinMyOrder = (Spin) adapterView.getItemAtPosition(i);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        spnStatus  = (Spinner) v.findViewById(R.id.spnStatus);
        spnStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spinStatus = (Spin) adapterView.getItemAtPosition(i);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        btnSearch = (Button) v.findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);
        edtFromDate = (EditText) v.findViewById(R.id.edtFromDate);

        edtFromDate.setText(strFromDate);

        edtFromDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean isFocus) {
                // TODO Auto-generated method stub
                if (!isFocus) {
                    if (!CommonActivity.isNullOrEmpty(edtFromDate.getText()
                            .toString().trim())) {
                        if (edtFromDate.getText().toString().trim().length() != 10
                                || CommonActivity.isNullOrEmpty(DateTimeUtils
                                .convertStringToTime(edtFromDate
                                                .getText().toString().trim(),
                                        "dd/MM/yyyy"))) {
                            CommonActivity
                                    .createAlertDialog(
                                            getActivity(),
                                            getString(R.string.txt_format_date_invalid),
                                            getString(R.string.app_name))
                                    .show();
                        }
                    }
                }
            }
        });

        LinearLayout lnFromDate = (LinearLayout) v.findViewById(R.id.lnFromDate);
        lnFromDate.setOnClickListener(this);

        edtToDate = (EditText) v.findViewById(R.id.edtToDate);
        edtToDate.setText(strToDate);
        edtToDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean isFocus) {
                // TODO Auto-generated method stub
                if (!isFocus) {
                    if (!CommonActivity.isNullOrEmpty(edtToDate.getText()
                            .toString().trim())) {
                        if (edtToDate.getText().toString().trim().length() != 10
                                || CommonActivity.isNullOrEmpty(DateTimeUtils
                                .convertStringToTime(edtToDate
                                                .getText().toString().trim(),
                                        "dd/MM/yyyy"))) {
                            CommonActivity
                                    .createAlertDialog(
                                            getActivity(),
                                            getString(R.string.txt_format_date_invalid),
                                            getString(R.string.app_name))
                                    .show();
                        }
                    }
                }
            }
        });

        LinearLayout lnToDate = (LinearLayout) v.findViewById(R.id.lnToDate);
        lnToDate.setOnClickListener(this);

        // khoi tao status
        initStatus();
        // khoi tao trang thai loai yeu cau có the nhan
        initSpnStatusOrder();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 100) {
                if(validateDate()){
                    AsyncGetLstStockOrderStaff asyncGetLstStockOrderStaff = new AsyncGetLstStockOrderStaff(getActivity(), onPostGetLstStockOrderStaff, moveLogInAct);
                    asyncGetLstStockOrderStaff.execute(edtFromDate.getText().toString().trim(),edtToDate.getText().toString().trim());
                }
            }
        }
    }

    private void initStatus(){
        // init status
        ArrayList<Spin> lstSpinStatus = new ArrayList<>();
//        lstSpinStatus.add(new Spin("", getString(R.string.all)));
        lstSpinStatus.add(new Spin("0", getString(R.string.moitao)));
        lstSpinStatus.add(new Spin("1", getString(R.string.nhanhangdanhan)));
        lstSpinStatus.add(new Spin("2", getString(R.string.nvdhhuy)));
        lstSpinStatus.add(new Spin("3", getString(R.string.nvdhtc)));
        Utils.setDataSpinner(getActivity(), lstSpinStatus, spnStatus);
        spnStatus.setSelection(0);
    }
    private void initSpnStatusOrder() {
        ArrayList<Spin> lstSpinMyOrder = new ArrayList<>();
//        lstSpinMyOrder.add(new Spin("", getString(R.string.all)));
        lstSpinMyOrder.add(new Spin("1", getString(R.string.trans_of_me)));
        lstSpinMyOrder.add(new Spin("2", getString(R.string.trans_reciver_able)));
        Utils.setDataSpinner(getActivity(), lstSpinMyOrder, spnMyOrder);
        spnMyOrder.setSelection(1);
    }
    @Override
    protected void setPermission() {
    }
    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.lnFromDate:
                showDialogFromDate();
                break;
            case R.id.lnToDate:
                showDialogToDate();
                break;
            case R.id.btnSearch:
                if(validateDate()){
                    AsyncGetLstStockOrderStaff asyncGetLstStockOrderStaff = new AsyncGetLstStockOrderStaff(getActivity(), onPostGetLstStockOrderStaff, moveLogInAct);
                    asyncGetLstStockOrderStaff.execute(edtFromDate.getText().toString().trim(),edtToDate.getText().toString().trim());
                }
                break;
            default:
                break;
        }
    }
    private int day;
    private int month;
    private int year;

    private void showDialogFromDate() {
        String date = edtFromDate.getText().toString();
        if (CommonActivity.isNullOrEmpty(date)) {
            Calendar cal = Calendar.getInstance();
            day = cal.get(Calendar.DAY_OF_MONTH);
            month = cal.get(Calendar.MONTH);
            year = cal.get(Calendar.YEAR);
        } else {
            String[] params = date.split("/");
            if (params.length == 3) {
                day = Integer.valueOf(params[0]);
                month = Integer.valueOf(params[1]) - 1;
                year = Integer.valueOf(params[2]);
            }

        }

        DatePickerDialog fromDateDialog = new FixedHoloDatePickerDialog(getActivity(),
                AlertDialog.THEME_HOLO_LIGHT, fromDatePickerListener, year, month, day);
        fromDateDialog.show();
    }

    private void showDialogToDate() {
        String date = edtToDate.getText().toString();
        if (CommonActivity.isNullOrEmpty(date)) {
            Calendar cal = Calendar.getInstance();
            day = cal.get(Calendar.DAY_OF_MONTH);
            month = cal.get(Calendar.MONTH);
            year = cal.get(Calendar.YEAR);
        } else {
            String[] params = date.split("/");
            day = Integer.valueOf(params[0]);
            month = Integer.valueOf(params[1]) - 1;
            year = Integer.valueOf(params[2]);
        }

        DatePickerDialog fromDateDialog = new FixedHoloDatePickerDialog(getActivity(),
                AlertDialog.THEME_HOLO_LIGHT, toDatePickerListener, year, month, day);
        fromDateDialog.show();
    }

    private final DatePickerDialog.OnDateSetListener fromDatePickerListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            StringBuilder strDate = new StringBuilder();
            if (selectedDay < 10) {
                strDate.append("0");
            }
            strDate.append(selectedDay).append("/");
            if (selectedMonth < 9) {
                strDate.append("0");
            }
            strDate.append(selectedMonth + 1).append("/");
            strDate.append(selectedYear);

            edtFromDate.setText(strDate);
        }
    };

    private final DatePickerDialog.OnDateSetListener toDatePickerListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            StringBuilder strDate = new StringBuilder();
            if (selectedDay < 10) {
                strDate.append("0");
            }
            strDate.append(selectedDay).append("/");
            if (selectedMonth < 9) {
                strDate.append("0");
            }
            strDate.append(selectedMonth + 1).append("/");
            strDate.append(selectedYear);

            edtToDate.setText(strDate);
        }
    };


    private boolean validateDate() {
        if (CommonActivity.isNullOrEmpty(edtFromDate.getText().toString())) {
            CommonActivity.createAlertDialog(getActivity(),
                    getString(R.string.notstartdate),
                    getString(R.string.app_name)).show();
            return false;
        }

        if (CommonActivity.isNullOrEmpty(edtToDate.getText().toString())) {
            CommonActivity
                    .createAlertDialog(getActivity(),
                            getString(R.string.notendate),
                            getString(R.string.app_name)).show();
            return false;
        }

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date from = sdf.parse(edtFromDate.getText().toString());
            Date to = sdf.parse(edtToDate.getText().toString());
            if (from.after(to)) {
                CommonActivity.createAlertDialog(getActivity(),
                        getString(R.string.checktimeupdatejob),
                        getString(R.string.app_name)).show();
                return false;
            }

            long diff = to.getTime() - from.getTime();
            long dayDiff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            if (dayDiff > 30) {
                CommonActivity.createAlertDialog(getActivity(),
                        getString(R.string.modify_profile_search_err01),
                        getString(R.string.app_name)).show();
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }


    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getInstance().setTitleActionBar(R.string.transgoodstr);
    }

    private OnPostExecuteListener<ParseOuput> onPostGetLstStockOrderStaff = new OnPostExecuteListener<ParseOuput>() {
        @Override
        public void onPostExecute(ParseOuput result, String errorCode, String description) {
            listStockOrderStaffDTO = new ArrayList<>();
            if (result != null && !CommonActivity.isNullOrEmpty(result.getListStockOrderStaffDTO())) {
                txtTitle.setVisibility(View.VISIBLE);

                for (StockOrderStaffDTO item:result.getListStockOrderStaffDTO()) {
                    /// yeu cau duoc phep nhan
                    if("2".equals(spinMyOrder.getId())){
                        if(!Session.userName.equalsIgnoreCase(item.getFromStaffCode()) && spinStatus.getId().equalsIgnoreCase(item.getStatus())){
                            listStockOrderStaffDTO.add(item);
                        }
                    }else{
                        if(Session.userName.equalsIgnoreCase(item.getFromStaffCode()) && spinStatus.getId().equalsIgnoreCase(item.getStatus())){
                            listStockOrderStaffDTO.add(item);
                        }
                    }

                }

//                listStockOrderStaffDTO = result.getListStockOrderStaffDTO();
                orderTransAdapter = new OrderTransAdapter(getActivity(), listStockOrderStaffDTO, SearchOrderTransFragment.this);
                lvOrder.setAdapter(orderTransAdapter);
            } else {
                txtTitle.setVisibility(View.GONE);
                orderTransAdapter = new OrderTransAdapter(getActivity(), listStockOrderStaffDTO, SearchOrderTransFragment.this);
                lvOrder.setAdapter(orderTransAdapter);
            }
        }
    };


    // ham yeu cau
    private OnPostExecuteListener<ParseOuput> onPostCancelOrderStaff = new OnPostExecuteListener<ParseOuput>() {
        @Override
        public void onPostExecute(ParseOuput result, String errorCode, String description) {
            if ("0".equals(result.getErrorCode())) {

                View.OnClickListener onClickListenerRemove = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(!CommonActivity.isNullOrEmpty(listStockOrderStaffDTO) && !CommonActivity.isNullOrEmpty(stockOrderStaffDTOMain)){
                            for (StockOrderStaffDTO stockOrderStaffDTO:
                                 listStockOrderStaffDTO) {
                                if(stockOrderStaffDTO.getStockOrderCode().equalsIgnoreCase(stockOrderStaffDTOMain.getStockOrderCode())){
                                    listStockOrderStaffDTO.remove(stockOrderStaffDTO);
                                    break;
                                }
                            }
                            if(orderTransAdapter != null){
                                orderTransAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                };

                CommonActivity.createAlertDialog(getActivity(), getActivity().getString(R.string.cancelReSucess),
                        getActivity().getString(R.string.app_name),onClickListenerRemove).show();

            } else {
                android.util.Log.d("Log", "description error update" + description);
                if (errorCode.equals(Constant.INVALID_TOKEN2)) {
                    Dialog dialog = CommonActivity.createAlertDialog(getActivity(), description,
                            getActivity().getResources().getString(R.string.app_name), moveLogInAct);
                    dialog.show();
                } else {
                    if (description == null || description.isEmpty()) {
                        description = getActivity().getString(R.string.checkdes);
                    }
                    Dialog dialog = CommonActivity.createAlertDialog(getActivity(), description,
                            getActivity().getResources().getString(R.string.app_name));
                    dialog.show();
                }
            }
        }
    };
    private final View.OnClickListener moveLogInAct = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            LoginDialog dialog = new LoginDialog(getActivity(),
                    "");
            dialog.show();
        }
    };

    @Override
    public void onCancelOrderListenner( StockOrderStaffDTO stockOrderStaffDTO) {
        stockOrderStaffDTOMain = stockOrderStaffDTO;
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AsyncCancelOrderStaff asyncCancelOrderStaff = new AsyncCancelOrderStaff(getActivity(), onPostCancelOrderStaff, moveLogInAct);
                asyncCancelOrderStaff.execute(stockOrderStaffDTOMain.getStockOrderCode());
            }
        };
        CommonActivity.createDialog(getActivity(), getActivity().getString(R.string.cancel_order_confirm), getActivity().getString(R.string.app_name),
                getActivity().getString(R.string.cancel)
                , getActivity().getString(R.string.ok),
                null, onClickListener).show();
    }
}
