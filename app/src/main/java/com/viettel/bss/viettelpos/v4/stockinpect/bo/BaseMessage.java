package com.viettel.bss.viettelpos.v4.stockinpect.bo;

import java.util.List;

/**
 * Created by leekien on 5/11/2018.
 */

public class BaseMessage {
    protected String description;
    protected String errorCode;
    protected String keyMsg;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getKeyMsg() {
        return keyMsg;
    }

    public void setKeyMsg(String keyMsg) {
        this.keyMsg = keyMsg;
    }

    public List<String> getParamsMsg() {
        return paramsMsg;
    }

    public void setParamsMsg(List<String> paramsMsg) {
        this.paramsMsg = paramsMsg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    protected List<String> paramsMsg;
    protected boolean success;

}
