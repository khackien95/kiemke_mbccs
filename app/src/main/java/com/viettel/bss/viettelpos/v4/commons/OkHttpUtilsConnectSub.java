package com.viettel.bss.viettelpos.v4.commons;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class OkHttpUtilsConnectSub {
    private static final String TAG = "OkHttpUtils";
    private static final MediaType XML
            = MediaType.parse("text/xml;charset=UTF-8");
    private static OkHttpClient client;

    static {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[0];
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext ssContext = SSLContext.getInstance("TLS");
            ssContext.init(null, trustAllCerts,
                    new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = ssContext
                    .getSocketFactory();
//            HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
//            if (BuildConfig.DEBUG)
//                logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            client = builder .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(false)
                    .sslSocketFactory(sslSocketFactory)
                    //.addNetworkInterceptor(loggingInterceptor)
                    .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
                    .build();
//            if(Constant..BUILD_DEV) {
//                logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//                builder.addInterceptor(logInterceptor);
//            }

        } catch (Exception e) {
            Log.d(TAG, "ERROR INIT OkHttpClient", e);
        }

    }


    public static String callViaHttpClient(String url, String envelope) {
        String result = "";
        try {
            Log.d(TAG, "callViaHttpClient url = " + url + ", envelope = " + envelope);
            RequestBody body = RequestBody.create(XML, envelope);
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            result = response.body().string();
        } catch (Exception e) {
            Log.d(TAG, "callViaHttpClient", e);
        }
        return result;
    }

    public static String callViaHttpClientWriteToFile(String url, String envelope, final String fileName) {
        String result = "";
        try {
            RequestBody body = RequestBody.create(XML, envelope);
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            result =  writeToFile(response.body().string(), fileName);
        } catch (Exception e) {
            Log.d(TAG, "callViaHttpClient", e);
        }
        return result;
    }

    private static String writeToFile(String data, String filename) {
        String result = "";
        String path = Environment.getExternalStorageDirectory() + File.separator  + "parseFolder";
        // Create the folder.
        File folder = new File(path);
        folder.mkdirs();

        // Create the file.
        File file = new File(folder, filename);
        try {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
            result = file.getPath();
            return  result;
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
        return  result;
    }


    public static InputStream callViaHttpClientRest(String url, String token , String path) {
        try {
//            Log.d(TAG, "callViaHttpClient url = " + url + ", envelope = " + envelope);
//            RequestBody body = RequestBody.create(XML, envelope);
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("token",token)
                    .build();
            Response response = client.newCall(request).execute();
//            if(response != null)
            return response.body().byteStream();
//            result = writeToFileRest(response.body().string(), path);
        } catch (Exception e) {
            Log.d(TAG, "callViaHttpClientRest", e);
        }
        return null;
    }

    private static String writeToFileRest(String data, String path) {
        String result = "";
//        String path = Environment.getExternalStorageDirectory() + File.separator  + "parseFolder";
        // Create the folder.
//        File folder = new File(path);
//        folder.mkdirs();

        // Create the file.
        File file = new File(path);
        try {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
            result = file.getPath();
            return  result;
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
        return  result;
    }




    public static OkHttpClient getClient(){
        return client;
    }
}
