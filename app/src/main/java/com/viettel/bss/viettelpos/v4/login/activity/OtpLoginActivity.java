package com.viettel.bss.viettelpos.v4.login.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DatabaseUtils;
import com.viettel.bss.viettelpos.v4.commons.Define;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.infrastrucure.dal.InfrastrucureDB;
import com.viettel.bss.viettelpos.v4.login.asynctask.ResendOTPAsyncTask;
import com.viettel.bss.viettelpos.v4.login.asynctask.SyncAsyncTask;
import com.viettel.bss.viettelpos.v4.login.asynctask.VerifyOTPAsyncTask;
import com.viettel.bss.viettelpos.v4.login.object.DataOutputLogin;
import com.viettel.bss.viettelpos.v4.sale.business.StaffBusiness;
import com.viettel.bss.viettelpos.v4.synchronizationdata.UpdateVersionAsyn;
import java.util.ArrayList;

import static com.viettel.bss.viettelpos.v4.commons.Session.userName;

public class OtpLoginActivity extends AppCompatActivity {

    private ImageButton btnRefreshOTP;
    private EditText edtOtp1;
    private EditText edtOtp2;
    private EditText edtOtp3;
    private EditText edtOtp4;
    private EditText edtOtp5;
    private EditText edtOtp6;

    private DataOutputLogin dataOutputLogin;
    private ArrayList<EditText> editTexts;
    private int refreshCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        dataOutputLogin = (DataOutputLogin) getIntent().getSerializableExtra("DataOutputLogin");

        btnRefreshOTP = (ImageButton) findViewById(R.id.btnRefreshOTP);
        edtOtp1 = (EditText) findViewById(R.id.edtOtp1);
        edtOtp2 = (EditText) findViewById(R.id.edtOtp2);
        edtOtp3 = (EditText) findViewById(R.id.edtOtp3);
        edtOtp4 = (EditText) findViewById(R.id.edtOtp4);
        edtOtp5 = (EditText) findViewById(R.id.edtOtp5);
        edtOtp6 = (EditText) findViewById(R.id.edtOtp6);

        editTexts = new ArrayList<>();
        editTexts.add(edtOtp1);
        editTexts.add(edtOtp2);
        editTexts.add(edtOtp3);
        editTexts.add(edtOtp4);
        editTexts.add(edtOtp5);
        editTexts.add(edtOtp6);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnRefreshOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (refreshCount > 3) {
                    CommonActivity.createAlertDialog(OtpLoginActivity.this,
                            "Bạn đã gửi mã OTP 3 lần, nếu không nhận được mã OTP vui lòng liên hệ với quản trị hệ thống để kiểm tra!",
                            getString(R.string.app_name)).show();
                    return;
                }
                refreshCount++;

                ResendOTPAsyncTask resendOTPAsyncTask = new ResendOTPAsyncTask(OtpLoginActivity.this, new OnPostExecuteListener<String>() {
                    @Override
                    public void onPostExecute(String result, String errorCode, String description) {
                        if ("0".equals(result)) {
                            Toast.makeText(OtpLoginActivity.this,
                                    "Gửi mà OTP thành công!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OtpLoginActivity.this,
                                    description, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, null);
                resendOTPAsyncTask.execute(Session.userName, dataOutputLogin.getSerialSim());
            }
        });

        edtOtp1.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals("")) {
                    edtOtp2.requestFocus();
                    edtOtp2.setSelectAllOnFocus(true);
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void afterTextChanged(Editable s) {}
        });

        edtOtp2.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals("")) {
                    edtOtp3.requestFocus();
                    edtOtp3.setSelectAllOnFocus(true);
                } else {
                    edtOtp1.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void afterTextChanged(Editable s) {}
        });

        edtOtp3.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals("")) {
                    edtOtp4.requestFocus();
                    edtOtp4.setSelectAllOnFocus(true);
                } else {
                    edtOtp2.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void afterTextChanged(Editable s) {}
        });

        edtOtp4.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals("")) {
                    edtOtp5.requestFocus();
                    edtOtp5.setSelectAllOnFocus(true);
                } else {
                    edtOtp3.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void afterTextChanged(Editable s) {}
        });

        edtOtp5.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals("")) {
                    edtOtp6.requestFocus();
                    edtOtp6.setSelectAllOnFocus(true);
                } else {
                    edtOtp4.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void afterTextChanged(Editable s) {}
        });

        edtOtp6.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals("")) {
                    validateInput();
                } else {
                    edtOtp5.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void afterTextChanged(Editable s) {}
        });
    }

    private void validateInput() {
        if (CommonActivity.isNullOrEmpty(edtOtp1.getText().toString().trim())
                || CommonActivity.isNullOrEmpty(edtOtp1.getText().toString().trim())
                || CommonActivity.isNullOrEmpty(edtOtp1.getText().toString().trim())
                || CommonActivity.isNullOrEmpty(edtOtp1.getText().toString().trim())
                || CommonActivity.isNullOrEmpty(edtOtp1.getText().toString().trim())
                || CommonActivity.isNullOrEmpty(edtOtp1.getText().toString().trim())) {
            Toast.makeText(this,
                    "Bạn phải nhập đủ mã OTP để đăng nhập!", Toast.LENGTH_SHORT).show();
            return;
        }
        String otp = edtOtp1.getText().toString().trim()
                + edtOtp2.getText().toString().trim()
                + edtOtp3.getText().toString().trim()
                + edtOtp4.getText().toString().trim()
                + edtOtp5.getText().toString().trim()
                + edtOtp6.getText().toString().trim();

        processVerifyOTP(otp);

        return;
    }

    public void upDateOTP(final String otp) {
        final String[] arrayString = otp.split("");
        if (arrayString.length != 7) {
            return;
        }
        final Handler handler = new Handler();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (int index = 0; index < 6; index++) {
                    final int i = index;
                    handler.post(new Runnable() {
                        public void run() {
                            editTexts.get(i).setText(arrayString[i+1]);
                        }
                    });
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Constant.REGISTER_RECEIVER));
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String msg = intent.getStringExtra("msg_body");
                Log.d("msg", "msg :" + msg);
                String[] resultOTPLogin = msg.split(":");
                if (resultOTPLogin.length > 1) {
                    final String otp = resultOTPLogin[1].trim();
                    upDateOTP(otp);
                }
            } catch (Exception e) {
                Log.e("BroadcastReceiver OTP", e.getMessage());
            }
        }
    };

    @Override
    protected void onPause() {
        CommonActivity.hideSoftKeyboard(this);
        unregisterReceiver(receiver);
        super.onPause();
    }

    private void processVerifyOTP(String otp) {
        VerifyOTPAsyncTask verifyOTPAsyncTask = new VerifyOTPAsyncTask(this, new OnPostExecuteListener<String>() {
            @Override
            public void onPostExecute(String result, String errorCode, String description) {
                if ("0".equals(errorCode)) {
                    Session.setToken(result);
                    Crashlytics.setUserIdentifier(Session.token); // set token cho log user
                    View.OnClickListener okListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View arg0) {
                            successLogin();
                        }
                    };
                    if (dataOutputLogin.getDaysBetweenExpried() != null && !dataOutputLogin.getDaysBetweenExpried().isEmpty()) {
                        String msg = getApplication().getString(R.string.password_will_expire, dataOutputLogin.getDaysBetweenExpried());
                        if ("0".equals(dataOutputLogin.getDaysBetweenExpried())) {
                            msg = getApplication().getString(R.string.password_expire_today, dataOutputLogin.getDaysBetweenExpried());
                        }
                        CommonActivity.createDialog(OtpLoginActivity.this, msg,
                                getApplication().getString(R.string.app_name),
                                "",
                                getApplication().getString(R.string.ok),
                                null, okListener).show();
                    } else {
                        successLogin();
                    }
                }
            }
        }, null);
        verifyOTPAsyncTask.execute(userName, otp, dataOutputLogin.getSerialSim());
    }

    public void successLogin() {
        //===========run check version==============
        if (dataOutputLogin.getForceUpgrade().equalsIgnoreCase("0")
                && !dataOutputLogin.getVersion().equals(CommonActivity.getversionclient(this))) {
            // ========== khong bat buoc cap nhat version===== show
            // aletdialog
            Dialog dialog = CommonActivity.createDialog(
                    this, getResources().getString(R.string.isversion),
                    getResources().getString(R.string.updateversion),
                    getResources().getString(R.string.cancel),
                    getResources().getString(R.string.ok),
                    cancelUpdateVer, onclickCheckUpdate);
            dialog.show();

        } else if (dataOutputLogin.getForceUpgrade().equalsIgnoreCase("1")
                && !dataOutputLogin.getVersion().equals(CommonActivity.getversionclient(this))) {
            // ========== bat buoc cap nhat ======================
            Log.d("versionnnnnserrver", dataOutputLogin.getVersion());
            UpdateVersionAsyn updateVersionAsyn = new UpdateVersionAsyn(
                    this, Constant.PATH_UPDATE_VERSION + Session.token);
            updateVersionAsyn.execute();
        } else {
            // ==========chay dong bo du lieu ==========================
            boolean isDatabaseExists = DatabaseUtils.doesDatabaseExist(this, Define.DB_NAME);
            Log.e("DatabaseExists", "" + isDatabaseExists);
            // Dang nhap thanh cong, kiem tra ton tai database
            if (isDatabaseExists) {
                if ("1".equals(dataOutputLogin.getCheckSyn())) {
                    SharedPreferences preferences = getSharedPreferences(Define.PRE_NAME, MODE_PRIVATE);
                    String lastVersion = preferences.getString(Define.KEY_LAST_VERSION, "");
                    PackageInfo packageInfo;
                    try {
                        packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        String currentVersion = packageInfo.versionName;
                        if (lastVersion == null || !lastVersion.equals(currentVersion)) {
                            deleteDatabase(Define.DB_NAME);

                            SyncAsyncTask sync = new SyncAsyncTask(
                                    this, onPostSyncListener);
                            sync.execute(Session.getToken(), userName);
                        } else {
                            try {
                                InfrastrucureDB mInfrastrucureDB = new InfrastrucureDB(this);
                                String shop[] = mInfrastrucureDB.getProvince();
                                Session.province = shop[0];
                                Session.district = shop[1];
                                mInfrastrucureDB.close();
                                Session.loginUser = StaffBusiness.getStaffByStaffCode(this, userName);
                            } catch (Exception ignored) {
                                Log.e("successLogin", ignored.getMessage());
                            }
                            // Neu database da ton tai, login vao man
                            // hinh chinh
                            Intent i = new Intent(this, MainActivity.class);
                            clearOmniStaff();
                            startActivity(i);
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else {
                    try {
                        InfrastrucureDB mInfrastrucureDB = new InfrastrucureDB(this);
                        String shop[] = mInfrastrucureDB.getProvince();
                        Session.province = shop[0];
                        Session.district = shop[1];
                        mInfrastrucureDB.close();
                        Session.loginUser = StaffBusiness.getStaffByStaffCode(this, userName);
                    } catch (Exception ignored) {
                    }
                    // Neu database da ton tai, login vao man hinh chinh
                    Intent i = new Intent(this, MainActivity.class);
                    clearOmniStaff();
                    startActivity(i);
                }
            } else {
                SyncAsyncTask sync = new SyncAsyncTask(
                        this, onPostSyncListener);
                sync.execute(Session.getToken(), userName);
            }
        }
    }

    private final OnPostExecuteListener<String> onPostSyncListener = new OnPostExecuteListener<String>() {
        @Override
        public void onPostExecute(String result, String errorCode, String description) {
            if (Constant.SUCCESS_CODE.equals(result)) {
                // Dong bo du lieu thanh cong,
                // luu bien staff vao session,
                // vao man hinh chinh chinh
                InfrastrucureDB mInfrastrucureDB = new InfrastrucureDB(OtpLoginActivity.this);
                String shop[] = mInfrastrucureDB.getProvince();
                mInfrastrucureDB.close();
                Session.loginUser = StaffBusiness.getStaffByStaffCode(OtpLoginActivity.this, userName);
                Session.province = shop[0];
                Session.district = shop[1];
                Intent i = new Intent(OtpLoginActivity.this, MainActivity.class);
                clearOmniStaff();
                startActivity(i);
                finish();
            } else if (Constant.ERROR_CODE.equals(result)) {
                String message = getString(R.string.sync_error);
                String title = getString(R.string.app_name);
                Dialog dialog = CommonActivity.createAlertDialog(OtpLoginActivity.this, message, title);
                dialog.show();
            } else {
                String title = getString(R.string.app_name);
                String message = getResources().getString(R.string.exception);
                Dialog dialog = CommonActivity.createAlertDialog(OtpLoginActivity.this, message, title);
                dialog.show();
            }
        }
    };

    private void clearOmniStaff() {
        SharedPreferences sharedPreferences = getSharedPreferences(
                Constant.SHARE_PREFERENCES_FILE, MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();

        edit.putString(Constant.OMNI_STAFF_NAME_SAVE, Constant.OMNI_STAFF_INVALID);
        edit.putString(Constant.SIGNATURE_STAFF_SAVE, "");
        edit.putString(Constant.SIGNATURE_STAFF_EXISTS, "");

        edit.commit();
    }

    // check updateDateVersion
    private final View.OnClickListener onclickCheckUpdate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            UpdateVersionAsyn updateVersionAsyn = new UpdateVersionAsyn(
                    OtpLoginActivity.this, Constant.PATH_UPDATE_VERSION
                    + Session.token);
            updateVersionAsyn.execute();
        }
    };

    // cacel updateVersion
    private final View.OnClickListener cancelUpdateVer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            boolean isDatabaseExists = DatabaseUtils.doesDatabaseExist(OtpLoginActivity.this, Define.DB_NAME);
            Log.e("DatabaseExists", "" + isDatabaseExists);
            // Dang nhap thanh cong, kiem tra
            // ton tai database
            if (isDatabaseExists) {
                if (!CommonActivity.isNullOrEmpty(dataOutputLogin.getCheckSyn())
                        && "1".equals(dataOutputLogin.getCheckSyn())) {

                    SharedPreferences preferences = getSharedPreferences(
                            Define.PRE_NAME, MODE_PRIVATE);
                    String lastVersion = preferences.getString(
                            Define.KEY_LAST_VERSION, "");
                    PackageInfo packageInfo;

                    try {
                        packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        String currentVersion = packageInfo.versionName;
                        if (lastVersion == null || !lastVersion.equals(currentVersion)) {
                            deleteDatabase(Define.DB_NAME);
                            SyncAsyncTask sync = new SyncAsyncTask(
                                    OtpLoginActivity.this, onPostSyncListener);
                            sync.execute(Session.getToken(), userName);
                        } else {
                            try {
                                InfrastrucureDB mInfrastrucureDB = new InfrastrucureDB(OtpLoginActivity.this);
                                String shop[] = mInfrastrucureDB.getProvince();
                                Session.province = shop[0];
                                Session.district = shop[1];
                                mInfrastrucureDB.close();
                                Session.loginUser = StaffBusiness.getStaffByStaffCode(
                                        OtpLoginActivity.this, userName);
                            } catch (Exception ignored) {
                            }
                            // Neu database da ton tai, login vao man hinh chinh
                            Intent i = new Intent(OtpLoginActivity.this,
                                    MainActivity.class);
                            clearOmniStaff();
                            startActivity(i);
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else {
                    try {
                        InfrastrucureDB mInfrastrucureDB = new InfrastrucureDB(OtpLoginActivity.this);
                        String shop[] = mInfrastrucureDB.getProvince();
                        Session.province = shop[0];
                        Session.district = shop[1];
                        mInfrastrucureDB.close();
                        Session.loginUser = StaffBusiness.getStaffByStaffCode(
                                OtpLoginActivity.this, userName);
                    } catch (Exception ignored) {
                    }
                    // Neu database da ton tai, login vao man hinh chinh
                    Intent i = new Intent(OtpLoginActivity.this,
                            MainActivity.class);
                    clearOmniStaff();
                    startActivity(i);
                }
            } else {
                SyncAsyncTask sync = new SyncAsyncTask(
                        OtpLoginActivity.this, onPostSyncListener);
                sync.execute(Session.getToken(), userName);
            }
        }
    };
}
