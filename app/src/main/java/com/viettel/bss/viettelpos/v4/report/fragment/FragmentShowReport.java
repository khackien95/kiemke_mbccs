package com.viettel.bss.viettelpos.v4.report.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.github.mikephil.charting.charts.BarChart;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.FragmentCommon;
import com.viettel.bss.viettelpos.v4.commons.chart.MPAndroidChartHelper;
import com.viettel.bss.viettelpos.v4.report.object.ItemBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentShowReport extends FragmentCommon {

    @BindView(R.id.barChart)
    BarChart barChart;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idLayout = R.layout.fragment_show_report_chart;

    }

    @Override
    public void unit(View v) {

    }

    @Override
    public void setPermission() {

    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessage(List<ItemBean> lstItemBean){
        MPAndroidChartHelper.drawBarChart(barChart, lstItemBean, getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
