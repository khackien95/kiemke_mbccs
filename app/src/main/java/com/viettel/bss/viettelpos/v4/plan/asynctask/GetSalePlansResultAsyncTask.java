package com.viettel.bss.viettelpos.v4.plan.asynctask;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.asyntask.AsyncTaskCommonSupper;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.plan.model.SalePlansResultModel;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

/**
 * Created by BaVV on 1/05/2018.
 */

public class GetSalePlansResultAsyncTask extends AsyncTaskCommonSupper<String, Void, SalePlansResultModel> {

    public GetSalePlansResultAsyncTask(
            Activity context,
            OnPostExecuteListener<SalePlansResultModel> listener,
            View.OnClickListener moveLogInAct) {

        super(context, listener, moveLogInAct);
    }

    @Override
    protected SalePlansResultModel doInBackground(String... params) {
        return getSalePlansResult(params[0]);
    }

    //    isdn: h004_ftth_thaott6 idNo: 1232342343 token: f3b53334b42b43bdbe5dd74420b09ca3
    private SalePlansResultModel getSalePlansResult(String planDate) {

        String original = "";
        SalePlansResultModel salePlansResultModel = null;
        String func = "getSalePlansResult";

        try {
            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_" + func);
            StringBuilder rawData = new StringBuilder();

            rawData.append("<ws:" + func + ">");
            rawData.append("<input>");
            rawData.append("<token>" + Session.getToken() + "</token>");
            rawData.append("<planDate>" + planDate + "</planDate>");

            rawData.append("</input>");
            rawData.append("</ws:" + func + ">");

            Log.i("RowData", rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData.toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope, Constant.BCCS_GW_URL,
                    mActivity, "mbccs_" + func);
            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i("Original", response);

            Serializer serializer = new Persister();
            salePlansResultModel = serializer.read(SalePlansResultModel.class, original);
        } catch (Exception e) {
            Log.e(Constant.TAG, "blockSubForTerminate", e);
            description = e.getMessage();
            errorCode = Constant.ERROR_CODE;
        }

        if (CommonActivity.isNullOrEmpty(salePlansResultModel)) {
            description = mActivity.getString(R.string.no_return_from_system);
            errorCode = Constant.ERROR_CODE;
            return null;
        } else {
//            description = subscriberModel.getDescription();
            errorCode = salePlansResultModel.getErrorCode();

            if (description != null && description.contains("java.lang.String.length()")) {
                description = mActivity.getString(R.string.server_time_out);
            }
        }
        return salePlansResultModel;
    }
}