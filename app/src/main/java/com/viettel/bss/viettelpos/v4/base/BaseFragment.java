package com.viettel.bss.viettelpos.v4.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by BaVV on 1/05/18.
 */
public abstract class BaseFragment
        extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);

        if (shouldListenEventBus()) {
            if (!EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().register(this);
            }
        }

        ButterKnife.bind(this, view);

        initUI();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initData();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    protected abstract int getLayoutId();

    protected boolean shouldListenEventBus() {
        return false;
    }

    protected void initUI() {

    }

    protected void initData() {

    }

    protected void onBackPressed() {
        try {
            Activity activity = getActivity();
            if (null != activity) {
                activity.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void showDialogMessage(String message) {
//        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
//        CommonActivity.createAlertDialogInfo(getActivity(), message, "Thông báo", "Đóng").show();
        CommonActivity.createAlertDialog(getContext(),
                message, getString(R.string.app_name)).show();
    }
}
