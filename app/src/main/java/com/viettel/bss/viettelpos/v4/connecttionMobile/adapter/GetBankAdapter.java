package com.viettel.bss.viettelpos.v4.connecttionMobile.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v3.connecttionService.model.ProductCatalogDTO;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.customer.object.Spin;

import java.util.ArrayList;

public class GetBankAdapter extends BaseAdapter{
	private Context mContext;
	private ArrayList<Spin> arrPakageVasBeans = new ArrayList<Spin>();


	public OnChangeCheckQuantityService onChangeCheckQuantityService;


	public interface OnChangeCheckQuantityService{

		public void onChangeCheckQuantityService(Spin productCatalogDTO);

	}


	public GetBankAdapter(ArrayList<Spin> arr , Context context  , OnChangeCheckQuantityService mOnChangeCheckQuantityService){

		this.mContext = context;
		this.arrPakageVasBeans = arr;
		this.onChangeCheckQuantityService = mOnChangeCheckQuantityService;
	}

	@Override
	public int getCount() {
		return arrPakageVasBeans.size();
	}

	@Override
	public Object getItem(int arg0) {
		return arrPakageVasBeans.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	private class ViewHolder{
		private TextView textName;
		private CheckBox checkBox;
		private TextView tvQuantitySaling;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		final ViewHolder holder;
		View mView = arg1;
		if(mView == null){
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			mView = inflater.inflate(R.layout.item_grid_service, arg2,
					false);
			holder = new ViewHolder();
			holder.textName = (TextView) mView.findViewById(R.id.textnamePakage);
			holder.checkBox = (CheckBox) mView.findViewById(R.id.checkPakage);
			holder.tvQuantitySaling = (TextView) mView.findViewById(R.id.tvQuantitySaling);
			mView.setTag(holder);
		}else{
			holder = (ViewHolder) mView.getTag();
		}
		final Spin pakageVasBean = arrPakageVasBeans.get(arg0);
		if(!CommonActivity.isNullOrEmpty(pakageVasBean.getCode())){
			holder.textName.setText(pakageVasBean.getCode());
		}


		holder.checkBox.setChecked(pakageVasBean.isCheck());
		holder.checkBox.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				onChangeCheckQuantityService.onChangeCheckQuantityService(pakageVasBean);
			}
		});
		


		
		
		return mView;
	}

}
