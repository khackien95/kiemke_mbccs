package com.viettel.bss.viettelpos.v4.stockinpect.control.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.stockinpect.bo.StockInspectCheckDTO;

import java.util.List;

public class RecycleViewSerialAdapter extends RecyclerView.Adapter<RecycleViewSerialAdapter.ViewHolder>{
    List<StockInspectCheckDTO> stockInspectCheckDTOS;


    public RecycleViewSerialAdapter(List<StockInspectCheckDTO> stockInspectCheckDTOS) {
        this.stockInspectCheckDTOS = stockInspectCheckDTOS;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        View view = mInflater.inflate(R.layout.item_list_stock_inpect_check, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final StockInspectCheckDTO stockInspectCheckDTO = stockInspectCheckDTOS.get(position);
        holder.stt.setText(position + 1 + "");
        holder.name.setText(stockInspectCheckDTO.getProductName());
        holder.status.setText(stockInspectCheckDTO.getStateName());
        holder.serial.setText(stockInspectCheckDTO.getFromSerial() + " - " + stockInspectCheckDTO.getToSerial());
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stockInspectCheckDTOS.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, stockInspectCheckDTOS.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        return stockInspectCheckDTOS.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView stt, name, status, serial;
        ImageView delete;

        public ViewHolder(View itemView) {
            super(itemView);
            stt = (TextView) itemView.findViewById(R.id.stt);
            name = (TextView) itemView.findViewById(R.id.name);
            status = (TextView) itemView.findViewById(R.id.status);
            serial = (TextView) itemView.findViewById(R.id.serial);
            delete = (ImageView) itemView.findViewById(R.id.delete);
        }


    }


}
