package com.viettel.bss.viettelpos.v4.sale.asytask;

import android.app.Activity;
import android.util.Log;
import android.view.View.OnClickListener;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.asyntask.AsyncTaskCommonSupper;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.sale.object.ProductOfferingDTO;
import com.viettel.bss.viettelpos.v4.sale.object.StockTransSerialDTO;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.ArrayList;

public class AsyncRequestOrderStaff extends
        AsyncTaskCommonSupper<String, Void, ParseOuput> {
    private ArrayList<ProductOfferingDTO> mlistStockModel = new ArrayList<>();
    public AsyncRequestOrderStaff(Activity context,
                                  OnPostExecuteListener<ParseOuput> listener,ArrayList<ProductOfferingDTO> listStockModel,
                                  OnClickListener moveLogInAct) {
        super(context, listener, moveLogInAct);
        this.mlistStockModel = listStockModel;
    }

    @Override
    protected ParseOuput doInBackground(String... arg0) {
        return requestOrderStaff(arg0[0]);
    }

    private ParseOuput requestOrderStaff(String staffId) {
        String original = "";
        ParseOuput parseOuput = new ParseOuput();
        try {
            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_requestOrderStaff");
            StringBuilder rawData = new StringBuilder();
            rawData.append("<ws:requestOrderStaff>");
            rawData.append("<input>");
            rawData.append("<token>").append(Session.getToken()).append("</token>");
            rawData.append("<staffId>").append(staffId).append("</staffId>");

            for (ProductOfferingDTO stockModel2 : mlistStockModel) {
                rawData.append("<listStockOrderStaffDetailDTOs>");
                rawData.append("<prodOfferId>").append(stockModel2.getProductOfferingId()).append("</prodOfferId>");
                rawData.append("<stateId>").append(stockModel2.getStateId()).append("</stateId>");
                rawData.append("<quantity>").append(stockModel2.getQuantitySaling()).append("</quantity>");
                if (stockModel2.getCheckSerial() == 1) {
                    for (int i = 0; i < stockModel2.getmListSerialSelection().size(); i++) {
                        StockTransSerialDTO serial = stockModel2.getmListSerialSelection().get(i);
                        String strFromSerial = "";
                        String strToSerial = "";
                        strFromSerial = serial.getFromSerial();
                        strToSerial = serial.getToSerial();
                        rawData.append("<listStockTransSerialDTOs>");
                        rawData.append("<fromSerial>").append(strFromSerial).append("</fromSerial>");
                        rawData.append("<toSerial>").append(strToSerial).append("</toSerial>");
                        rawData.append("</listStockTransSerialDTOs>");
                    }
                }
                rawData.append("</listStockOrderStaffDetailDTOs>");
            }
            rawData.append("</input>");
            rawData.append("</ws:requestOrderStaff>");
            Log.i("RowData", rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData
                    .toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope, Constant.BCCS_GW_URL,
                    mActivity, "mbccs_requestOrderStaff");
            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i("Responseeeeeeeeee Original", original);


            Serializer serializer = new Persister();
            parseOuput = serializer.read(ParseOuput.class, original);
            if(parseOuput == null){
                parseOuput = new ParseOuput();
                parseOuput.setErrorCode(Constant.ERROR_CODE);
                parseOuput.setDescription(mActivity.getString(R.string.no_data));
            }
            return parseOuput;
        } catch (Exception e) {
            Log.d("mbccs_requestOrderStaff", e.toString()
                    + "description error", e);
            parseOuput = new ParseOuput();
            parseOuput.setErrorCode(Constant.ERROR_CODE);
            parseOuput.setDescription(mActivity.getString(R.string.no_data));
        }
        return parseOuput;

    }

}
