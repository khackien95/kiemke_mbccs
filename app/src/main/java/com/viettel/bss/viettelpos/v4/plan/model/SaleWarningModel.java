package com.viettel.bss.viettelpos.v4.plan.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "return", strict = false)
public class SaleWarningModel {

    @Element(name = "errorCode", required = false)
    private String errorCode;

    @Element(name = "success", required = false)
    private boolean success;

    @Element(name = "notification", required = false)
    private String notification;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }
}
