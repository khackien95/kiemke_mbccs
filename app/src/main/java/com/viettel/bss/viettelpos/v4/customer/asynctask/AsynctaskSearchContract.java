package com.viettel.bss.viettelpos.v4.customer.asynctask;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.viettel.bss.viettelpos.v4.charge.object.ChargeContractItem;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncTaskCommon;
import com.viettel.bss.viettelpos.v4.synchronizationdata.XmlDomParse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

/**
 * Created by nainn on 2018-03-13.
 */

public class AsynctaskSearchContract extends
        AsyncTaskCommon<String, Void, ArrayList<ChargeContractItem>> {

    final XmlDomParse parse = new XmlDomParse();

    public AsynctaskSearchContract(Activity context, OnPostExecuteListener<ArrayList<ChargeContractItem>> listener, View.OnClickListener moveLogInAc) {
        super(context, listener, moveLogInAc);
    }

    @Override
    protected ArrayList<ChargeContractItem> doInBackground(String... params) {

        return searchContract(params[0]);
    }


    private ArrayList<ChargeContractItem> searchContract(String isdn) {
        ArrayList<ChargeContractItem> listChargeContractItem = new ArrayList<>();
        String original = "";
        try {
            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_searchContract");
            StringBuilder rawData = new StringBuilder();
            rawData.append("<ws:searchContract>");
            rawData.append("<input>");
            rawData.append("<token>").append(Session.getToken()).append("</token>");

            rawData.append("<isdn>").append(isdn).append("</isdn>");

            rawData.append("</input>");
            rawData.append("</ws:searchContract>");
            Log.i("RowData", rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData
                    .toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope,
                    Constant.BCCS_GW_URL, mActivity,
                    "mbccs_searchContract");
            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i("Responseeeeeeeeee Original", response);

            // parser
            Document doc = parse.getDomElement(original);
            NodeList nl = doc.getElementsByTagName("return");
            NodeList nodechild = null;
            for (int i = 0; i < nl.getLength(); i++) {
                Element e2 = (Element) nl.item(i);
                errorCode = parse.getValue(e2, "errorCode");
                description = parse.getValue(e2, "description");
                Log.d("errorCode", errorCode);
                nodechild = doc.getElementsByTagName("lstMContractBean");
                for (int j = 0; j < nodechild.getLength(); j++) {
                    Element e1 = (Element) nodechild.item(j);
                    ChargeContractItem chargeItemObject = new ChargeContractItem();
                    chargeItemObject.setAddress(parse.getValue(e1,
                            "address"));
                    chargeItemObject.setContractFormMngt(parse.getValue(e1,
                            "contractFormMngt"));
                    chargeItemObject.setContractFormMngtName(parse
                            .getValue(e1, "contractFormMngtName"));
                    chargeItemObject.setContractId(parse.getValue(e1,
                            "contractId"));
                    chargeItemObject.setContractNo(parse.getValue(e1,
                            "contractNo"));
                    chargeItemObject.setDebit(parse.getValue(e1, "debit"));
                    chargeItemObject.setHotCharge(parse.getValue(e1,
                            "hotCharge"));
                    chargeItemObject.setIsdn(parse.getValue(e1, "isdn"));
                    chargeItemObject.setPayMethodCode(parse.getValue(e1,
                            "payMethodCode"));
                    chargeItemObject.setPayMethodName(parse.getValue(e1,
                            "payMethodName"));
                    chargeItemObject.setPayer(parse.getValue(e1, "payer"));

                    chargeItemObject.setVerifyStatus(parse.getValue(e1,
                            "verifyStatus"));
                    chargeItemObject.setxPos(parse.getValue(e1, "xPos"));
                    chargeItemObject.setyPos(parse.getValue(e1, "yPos"));

                    String s = parse.getValue(e1, "paymentStatus");
                    if (s == null || s.isEmpty()) {
                        s = "0";
                    }
                    chargeItemObject.setPaymentStatus(s);

                    chargeItemObject.setPriorDebit(parse.getValue(e1,
                            "priorDebit"));
                    chargeItemObject
                            .setTelFax(parse.getValue(e1, "telFax"));
                    chargeItemObject.setTotCharge(parse.getValue(e1,
                            "totCharge"));
                    listChargeContractItem.add(chargeItemObject);
                }
            }
        } catch (Exception e) {
            Log.e(Constant.TAG,
                    "FragmentContractVerifySearch searchContract", e);
        }
        return listChargeContractItem;
    }
}
