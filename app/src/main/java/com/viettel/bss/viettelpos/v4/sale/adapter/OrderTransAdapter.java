package com.viettel.bss.viettelpos.v4.sale.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.activity.FragmentSearchSubChangeSim;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DBOpenHelper;
import com.viettel.bss.viettelpos.v4.commons.DataUtils;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.commons.Define;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.ReplaceFragment;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.commons.StringUtils;
import com.viettel.bss.viettelpos.v4.customer.manage.RegisterInfoFragment;
import com.viettel.bss.viettelpos.v4.dialog.LoginDialog;
import com.viettel.bss.viettelpos.v4.hsdt.asynctask.CancelOrderAsyn;
import com.viettel.bss.viettelpos.v4.hsdt.fragments.ProfileInfoOmniFragment;
import com.viettel.bss.viettelpos.v4.omichanel.asynctask.CheckInAsyncTask;
import com.viettel.bss.viettelpos.v4.omichanel.asynctask.ClaimAsyncTask;
import com.viettel.bss.viettelpos.v4.omichanel.asynctask.ClaimForReceptionistAsyncTask;
import com.viettel.bss.viettelpos.v4.omichanel.asynctask.CompleteCollReceiveOrderAsyncTask;
import com.viettel.bss.viettelpos.v4.omichanel.asynctask.TransferOrderAsyncTask;
import com.viettel.bss.viettelpos.v4.omichanel.asynctask.UnclaimForReceptionistAsyncTask;
import com.viettel.bss.viettelpos.v4.omichanel.dao.ConnectionOrder;
import com.viettel.bss.viettelpos.v4.omichanel.dao.Order;
import com.viettel.bss.viettelpos.v4.omichanel.dao.OrderState;
import com.viettel.bss.viettelpos.v4.omichanel.dao.ProfileRecord;
import com.viettel.bss.viettelpos.v4.omichanel.fragment.DetailOrderOmniFragment;
import com.viettel.bss.viettelpos.v4.omichanel.fragment.SearchOrderOmniFragment;
import com.viettel.bss.viettelpos.v4.sale.activity.CreateRequestOrderFragment;
import com.viettel.bss.viettelpos.v4.sale.business.StaffBusiness;
import com.viettel.bss.viettelpos.v4.sale.dal.StaffDAL;
import com.viettel.bss.viettelpos.v4.sale.fragment.SearchOrderTransFragment;
import com.viettel.bss.viettelpos.v4.sale.object.Staff;
import com.viettel.bss.viettelpos.v4.sale.object.StockOrderStaffDTO;
import com.viettel.bss.viettelpos.v4.utils.Log;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hantt47 on 9/1/2017.
 */

public class OrderTransAdapter extends RecyclerView.Adapter<OrderTransAdapter.ViewHolder> {

    private Activity activity;
    private ArrayList<StockOrderStaffDTO> orderList;
    private OnCancelOrder onCancelOrder;
    public interface OnCancelOrder {
        void onCancelOrderListenner(
                StockOrderStaffDTO stockOrderStaffDTO);
    }
    public OrderTransAdapter(Activity activity, ArrayList<StockOrderStaffDTO> orderList , OnCancelOrder onCancelOrder) {
        this.activity = activity;
        this.orderList = orderList;
        this.onCancelOrder = onCancelOrder;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_trans_row, parent, false);
        return new ViewHolder(view);
    }
     boolean isCheckStaff = false;
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final StockOrderStaffDTO stockOrderStaffDTO = orderList.get(position);
        if(stockOrderStaffDTO != null){
            holder.tvOrderCode.setText(stockOrderStaffDTO.getStockOrderCode());
            holder.tvCreateDate.setText(StringUtils.convertDate(stockOrderStaffDTO.getCreateDate()));
            holder.tvCreateName.setText(stockOrderStaffDTO.getFromStaffCode() + "-" + stockOrderStaffDTO.getFromStaffName());
            holder.tvAcceptName.setText(stockOrderStaffDTO.getToStaffCode() + "-" + stockOrderStaffDTO.getToStaffName());
            if (Session.userName.toLowerCase().equalsIgnoreCase(stockOrderStaffDTO.getFromStaffCode()) && "0".equalsIgnoreCase(stockOrderStaffDTO.getStatus())) {
                holder.tvDestroy.setVisibility(View.VISIBLE);
                isCheckStaff = true;
            } else {
                holder.tvDestroy.setVisibility(View.GONE);
            }
            holder.tvDestroy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onCancelOrder.onCancelOrderListenner(stockOrderStaffDTO);
                }
            });
            String[] arrStatus = activity.getResources().getStringArray(R.array.order_trans_status);
            holder.tvStatus.setText(arrStatus[Integer.parseInt(stockOrderStaffDTO.getStatus())]);
            // chuyen sang man hinh chi tiet don hang
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        CreateRequestOrderFragment fragment = new CreateRequestOrderFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("stockOrderStaffDTO",stockOrderStaffDTO);
                        fragment.setArguments(bundle);
                        fragment.setTargetFragment(SearchOrderTransFragment.intance, 100);
                        ReplaceFragment.replaceFragment(activity, fragment, true);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cardView)
        CardView cardView;
        @BindView(R.id.tvOrderCode)
        TextView tvOrderCode;
        @BindView(R.id.tvStatus)
        TextView tvStatus;
        @BindView(R.id.tvCreateDate)
        TextView tvCreateDate;
        @BindView(R.id.tvCreateName)
        TextView tvCreateName;
        @BindView(R.id.tvAcceptName)
        TextView tvAcceptName;
        @BindView(R.id.tvDestroy)
        TextView tvDestroy;
        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }



}
