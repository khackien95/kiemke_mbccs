package com.viettel.bss.viettelpos.v4.login.asynctask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.Define;
import com.viettel.bss.viettelpos.v4.commons.OkHttpUtils;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.SdCardHelper;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.commons.ZipUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by hungtv64 on 1/26/2018.
 */

public class SyncAsyncTask extends AsyncTask<String, String, String> {

    private final OnPostExecuteListener<String> listener;
    private final Activity activity;
    private ProgressDialog progress;
    private String errorCode = "";
    private String description = "";

    public SyncAsyncTask(Activity activity, OnPostExecuteListener<String> listener) {
        this.activity = activity;
        this.listener = listener;
        showProgressDialog();
    }

    private void dismissProgressDialog() {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
        }
    }

    private void showProgressDialog() {
        if (progress == null) {
            progress = new ProgressDialog(activity);
            progress.setCancelable(false);
            progress.setMessage(activity.getResources().getString(R.string.waitingsyn));
        }
        if (!progress.isShowing()) {
            progress.show();
            progress.setCancelable(false);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Session.isSyncRunning = true;
        return sync(params[0], params[1]);
    }

    @Override
    protected void onPreExecute() {
        Long start = System.currentTimeMillis();
        while (Session.isSyncRunning) {
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (System.currentTimeMillis() - start > 300000L) {
                break;
            }
        }
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        dismissProgressDialog();
        Session.isSyncRunning = false;
        listener.onPostExecute(result, errorCode, description);
    }

    public String sync(String token, String userName) {
        Log.e("downloadAndSyn", "Start Download >>>>>>>>>>>>>>>> ");
        String result = "";
        String url = Constant.PATH_DOWNLOAD;
        try {
            Log.i("Bo nho con trong", Double.toString(SdCardHelper.getAvailableInternalMemorySize()));
            double availAbleMemory = SdCardHelper.getAvailableInternalMemorySize();
            if (availAbleMemory > Constant.checkmemoryPhone) {
                File databaseFolder = new File(Define.PATH_DATABASE);
                if (!databaseFolder.exists()) {
                    databaseFolder.mkdirs();
                }
                String databaseFilePath =
                        Define.PATH_DATABASE + userName + ".zip";
                InputStream inputStream =
                        OkHttpUtils.callViaHttpClientRest(url,token,databaseFilePath);
                if(inputStream == null){
                    return  Constant.ERROR_CODE;
                }
                OutputStream output = new FileOutputStream(databaseFilePath);
                byte data[] = new byte[1024];
                int count = 0;
                BufferedInputStream input = new BufferedInputStream(inputStream);
                while ((count = input.read(data)) != -1) {
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
                File zipFile = new File(databaseFilePath);
                Log.d("zipFile.length" ,zipFile.length() + "");
                File desFolder = new File(Define.PATH_DATABASE);
                ZipUtils.unzip(zipFile, desFolder);
                String pathData = Define.PATH_DATABASE
                        + userName.toUpperCase();
                File file2 = new File(pathData);
                File newFileName = new File(Define.PATH_DATABASE + "smartphone");
                file2.renameTo(newFileName);
                zipFile.delete();
                Log.e("FilePath", Define.PATH_DATABASE);
                result = Constant.SUCCESS_CODE;
            } else {
                result = Constant.ERROR_CODE;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}