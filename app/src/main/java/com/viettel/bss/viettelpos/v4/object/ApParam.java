package com.viettel.bss.viettelpos.v4.object;

import java.io.Serializable;

/**
 * Created by thinhhq1 on 2/22/2018.
 */

public class ApParam implements Serializable{

    private String parName;
    private String parType;
    private String parValue;
    private String description;
    private Long status;
//    private Long required;


    public String getParName() {
        return parName;
    }

    public void setParName(String parName) {
        this.parName = parName;
    }

    public String getParType() {
        return parType;
    }

    public void setParType(String parType) {
        this.parType = parType;
    }

    public String getParValue() {
        return parValue;
    }

    public void setParValue(String parValue) {
        this.parValue = parValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

//    public Long getRequired() {
//        return required;
//    }
//
//    public void setRequired(Long required) {
//        this.required = required;
//    }
}
