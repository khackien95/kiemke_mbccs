package com.viettel.bss.viettelpos.v4.stockinpect.bo;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.beans.ShopDTO;

import org.simpleframework.xml.Element;

import java.util.List;

/**
 * Created by leekien on 5/9/2018.
 */

public class ProductOfferingTotalDTO extends BaseDTO {
    @Element(name = "accountingModelCode", required = false)
    protected String accountingModelCode;
    @Element(name = "accountingModelName", required = false)
    protected String accountingModelName;
    @Element(name = "availableQuantity", required = false)
    protected Long availableQuantity;
    @Element(name = "checkSerial", required = false)
    protected String checkSerial;
    @Element(name = "code", required = false)
    protected String code;
    @Element(name = "currentQuantity", required = false)
    protected Long currentQuantity;
    @Element(name = "hangQuantity", required = false)
    protected Long hangQuantity;
    @Element(name = "lstPriceDeposit", required = false)
    protected List<Long> lstPriceDeposit;
    @Element(name = "name", required = false)
    protected String name;
    @Element(name = "ownerId", required = false)
    protected Long ownerId;
    @Element(name = "ownerType", required = false)
    protected String ownerType;
    @Element(name = "priceAmount", required = false)
    protected Long priceAmount;
    @Element(name = "priceDeposit", required = false)
    protected Long priceDeposit;
    @Element(name = "productOfferTypeId", required = false)
    protected Long productOfferTypeId;
    @Element(name = "productOfferTypeName", required = false)
    protected String productOfferTypeName;
    @Element(name = "productOfferingId", required = false)
    protected Long productOfferingId;
    @Element(name = "quantityBccs", required = false)
    protected Long quantityBccs;
    @Element(name = "quantityFinance", required = false)
    protected Long quantityFinance;
    @Element(name = "quantityInspect", required = false)
    protected Long quantityInspect;
    @Element(name = "requestQuantity", required = false)
    protected Long requestQuantity;
    @Element(name = "requestQuantityInput", required = false)
    protected Long requestQuantityInput;
    @Element(name = "sourcePrice", required = false)
    protected Long sourcePrice;
    @Element(name = "stateId", required = false)
    protected Long stateId;
    @Element(name = "stateName", required = false)
    protected String stateName;
    @Element(name = "tablePk", required = false)
    protected String tablePk;
    @Element(name = "telecomServiceId", required = false)
    protected Long telecomServiceId;
    @Element(name = "unit", required = false)
    protected String unit;
    @Element(name = "unitName", required = false)
    protected String unitName;
}
