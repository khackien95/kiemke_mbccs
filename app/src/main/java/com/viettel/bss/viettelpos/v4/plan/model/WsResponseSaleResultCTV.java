package com.viettel.bss.viettelpos.v4.plan.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by bavv on 1/9/18.
 */

@Root(name = "wsResponseSaleResultCTV", strict = false)
public class WsResponseSaleResultCTV {

    @Element(name = "planDate", required = false)
    protected String planDate;

    @ElementList(name = "results", entry = "results", required = false, inline = true)
    protected List<SaleResultCtv> results;

    @Element(name = "staffCode", required = false)
    protected String staffCode;

    public String getPlanDate() {
        return planDate;
    }

    public void setPlanDate(String planDate) {
        this.planDate = planDate;
    }

    public List<SaleResultCtv> getResults() {
        return results;
    }

    public void setResults(List<SaleResultCtv> results) {
        this.results = results;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }
}
