package com.viettel.bss.viettelpos.v4.plan.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by bavv on 1/9/18.
 */

@Root(name = "listSalePoint", strict = false)
public class SalePointInfo {
    @Element(name = "address", required = false)
    protected String address;

    @Element(name = "planDate", required = false)
    protected String planDate;

    @Element(name = "salePointCode", required = false)
    protected String salePointCode;

    @Element(name = "salePointName", required = false)
    protected String salePointName;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPlanDate() {
        return planDate;
    }

    public void setPlanDate(String planDate) {
        this.planDate = planDate;
    }

    public String getSalePointCode() {
        return salePointCode;
    }

    public void setSalePointCode(String salePointCode) {
        this.salePointCode = salePointCode;
    }

    public String getSalePointName() {
        return salePointName;
    }

    public void setSalePointName(String salePointName) {
        this.salePointName = salePointName;
    }
}
