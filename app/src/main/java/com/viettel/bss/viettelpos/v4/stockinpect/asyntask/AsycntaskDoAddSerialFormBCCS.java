package com.viettel.bss.viettelpos.v4.stockinpect.asyntask;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncTaskCommon;

import com.viettel.bss.viettelpos.v4.stockinpect.bo.StockInspectCheckDTO;
import com.viettel.bss.viettelpos.v4.stockinpect.bo.StockInspectDTO;
import com.viettel.bss.viettelpos.v4.stockinpect.bo.StockInspectRealDTO;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.List;

public class AsycntaskDoAddSerialFormBCCS extends AsyncTaskCommon<Void, Void, ParseOuput> {
    StockInspectRealDTO realDTO;
    long inputMethod;
    StockInspectDTO stockInspectSaveDTO, checkStockInspectDTO;
    List<StockInspectCheckDTO> dtoList;

    public AsycntaskDoAddSerialFormBCCS(Activity context, OnPostExecuteListener<ParseOuput> listener,
                                        View.OnClickListener moveLogInAct, StockInspectRealDTO realDTO,
                                        long inputMethod, StockInspectDTO stockInspectSaveDTO,
                                        StockInspectDTO checkStockInspectDTO, List<StockInspectCheckDTO> dtoList) {
        super(context, listener, moveLogInAct);
        this.realDTO = realDTO;
        this.inputMethod = inputMethod;
        this.stockInspectSaveDTO = stockInspectSaveDTO;
        this.checkStockInspectDTO = checkStockInspectDTO;
        this.dtoList = dtoList;
    }

    @Override
    protected ParseOuput doInBackground(Void... voids) {
        String original = "";
        ParseOuput out = null;
        String func = "doAddSerialFormBCCS";
        try {
            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_" + func);
            StringBuilder rawData = new StringBuilder();
            rawData.append("<ws:" + func + ">");
            rawData.append("<input>");
            rawData.append("<token>" + Session.getToken() + "</token>");

            rawData.append("<stockInspectRealDTO>");
            if (!CommonActivity.isNullOrEmpty(realDTO.getSingleSerial()))
                rawData.append("<singleSerial>").append(realDTO.getSingleSerial()).append("</singleSerial>");
            if (!CommonActivity.isNullOrEmpty(realDTO.getFromSerial()))
                rawData.append("<fromSerial>").append(realDTO.getFromSerial()).append("</fromSerial>");
            if (!CommonActivity.isNullOrEmpty(realDTO.getToSerial()))
                rawData.append("<toSerial>").append(realDTO.getToSerial()).append("</toSerial>");
            if (!CommonActivity.isNullOrEmpty(realDTO.getReason()))
                rawData.append("<reason>").append(realDTO.getReason()).append("</reason>");
            rawData.append("</stockInspectRealDTO>");

            rawData.append("<inputMethodId>").append(inputMethod + "").append("</inputMethodId>");//test

            rawData.append("<stockInspectSaveDTO>");
            if (!CommonActivity.isNullOrEmpty(stockInspectSaveDTO.getInspectStatus()))
                rawData.append("<inspectStatus>").append(stockInspectSaveDTO.getInspectStatus()).append("</inspectStatus>");
//            if (!CommonActivity.isNullOrEmpty(stockInspectSaveDTO.getOwnerType()))
//                rawData.append("<ownerType>").append(stockInspectSaveDTO.getOwnerType()).append("</ownerType>");
//            if (!CommonActivity.isNullOrEmpty(stockInspectSaveDTO.getOwnerId()))
//                rawData.append("<ownerId>").append(stockInspectSaveDTO.getOwnerType()).append("</ownerId>");
            if (!CommonActivity.isNullOrEmpty(stockInspectSaveDTO.getStateId()))
                rawData.append("<stateId>").append(stockInspectSaveDTO.getStateId()).append("</stateId>");
            if (!CommonActivity.isNullOrEmpty(stockInspectSaveDTO.getProdOfferTypeId()))
                rawData.append("<prodOfferTypeId>").append(stockInspectSaveDTO.getProdOfferTypeId()).append("</prodOfferTypeId>");
            if (!CommonActivity.isNullOrEmpty(stockInspectSaveDTO.getProdOfferId()))
                rawData.append("<prodOfferId>").append(stockInspectSaveDTO.getProdOfferId()).append("</prodOfferId>");
            rawData.append("</stockInspectSaveDTO>");

            rawData.append("<checkStockInspectDTO>");
            //t
            if (!CommonActivity.isNullOrEmpty(checkStockInspectDTO.getInspectStatus()))
                rawData.append("<inspectStatus>").append(checkStockInspectDTO.getInspectStatus()).append("</inspectStatus>");
//            if (!CommonActivity.isNullOrEmpty(checkStockInspectDTO.getOwnerType()))
//                rawData.append("<ownerType>").append(checkStockInspectDTO.getOwnerType()).append("</ownerType>");
//            if (!CommonActivity.isNullOrEmpty(checkStockInspectDTO.getOwnerId()))
//                rawData.append("<ownerId>").append(checkStockInspectDTO.getOwnerType()).append("</ownerId>");
            if (!CommonActivity.isNullOrEmpty(checkStockInspectDTO.getStateId()))
                rawData.append("<stateId>").append(checkStockInspectDTO.getStateId()).append("</stateId>");
            if (!CommonActivity.isNullOrEmpty(checkStockInspectDTO.getProdOfferTypeId()))
                rawData.append("<prodOfferTypeId>").append(checkStockInspectDTO.getProdOfferTypeId()).append("</prodOfferTypeId>");
            if (!CommonActivity.isNullOrEmpty(checkStockInspectDTO.getProdOfferId()))
                rawData.append("<prodOfferId>").append(checkStockInspectDTO.getProdOfferId()).append("</prodOfferId>");
            rawData.append("</checkStockInspectDTO>");
//            for (StockInspectCheckDTO stockInspectCheckDTO : dtoList) {
            rawData.append("<listProductCheck>");
//                if (!CommonActivity.isNullOrEmpty(stockInspectCheckDTO.getInspectStatus()))
//                    rawData.append("<inspectStatus>").append(stockInspectCheckDTO.getInspectStatus()).append("</inspectStatus>");
//                if (!CommonActivity.isNullOrEmpty(stockInspectCheckDTO.getOwnerType()))
//                    rawData.append("<ownerType>").append(stockInspectCheckDTO.getOwnerType()).append("</ownerType>");
//                if (!CommonActivity.isNullOrEmpty(stockInspectCheckDTO.getOwnerId()))
//                    rawData.append("<ownerId>").append(stockInspectCheckDTO.getOwnerType()).append("</ownerId>");
//                if (!CommonActivity.isNullOrEmpty(stockInspectCheckDTO.getStateId()))
//                    rawData.append("<stateId>").append(stockInspectCheckDTO.getStateId()).append("</stateId>");
//                if (!CommonActivity.isNullOrEmpty(stockInspectCheckDTO.getProdOfferTypeId()))
//                    rawData.append("<prodOfferTypeId>").append(stockInspectCheckDTO.getProdOfferTypeId()).append("</prodOfferTypeId>");
//                if (!CommonActivity.isNullOrEmpty(stockInspectCheckDTO.getProdOfferId()))
//                    rawData.append("<prodOfferId>").append(stockInspectCheckDTO.getProdOfferId()).append("</prodOfferId>");
            rawData.append("</listProductCheck>");

//            }

            rawData.append("</input>");
            rawData.append("</ws:" + func + ">");
            Log.i("RowData", rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData.toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope, Constant.BCCS_GW_URL,
                    mActivity, "mbccs_" + func);
            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i(" Original", response);

            // parser
            Serializer serializer = new Persister();
            out = serializer.read(ParseOuput.class, original);
        } catch (Exception e) {
            Log.e(Constant.TAG, e.getMessage(), e);
            description = e.getMessage();
            errorCode = Constant.ERROR_CODE;
        }
        if (CommonActivity.isNullOrEmpty(out)) {
            description = mActivity.getString(R.string.no_return_from_system);
            errorCode = Constant.ERROR_CODE;
        } else {
            description = out.getDescription();
            errorCode = out.getErrorCode();
        }
        return out;

    }


}
