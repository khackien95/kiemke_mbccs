package com.viettel.bss.viettelpos.v4.plan.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Root(name = "return", strict = false)
public class SalePlansOfDayModel {

    @Element(name = "errorCode", required = false)
    private String errorCode;

    @Element(name = "success", required = false)
    private boolean success;

    @Element(name = "totalRow", required = false)
    private int totalRow;

    @ElementList(name = "lstSalePlansCtvs", entry = "lstSalePlansCtvs", required = false, inline = true)
    private List<SalePlansModel> salePlansModelList = new ArrayList<>();

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }

    public List<SalePlansModel> getSalePlansModelList() {
        return salePlansModelList;
    }

    public void setSalePlansModelList(List<SalePlansModel> salePlansModelList) {
        this.salePlansModelList = salePlansModelList;
    }
}
