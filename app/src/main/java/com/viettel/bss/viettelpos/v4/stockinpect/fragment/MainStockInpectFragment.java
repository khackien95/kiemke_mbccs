package com.viettel.bss.viettelpos.v4.stockinpect.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.beans.ProductOfferTypeDTO;
import com.viettel.bss.viettelpos.v3.connecttionService.model.OptionSetValueDTO;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.FragmentCommon;
import com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.ReplaceFragment;
import com.viettel.bss.viettelpos.v4.stockinpect.asyntask.AsynCheckSale;
import com.viettel.bss.viettelpos.v4.stockinpect.asyntask.AsynCheckStock;
import com.viettel.bss.viettelpos.v4.stockinpect.asyntask.AsynCheckStockInpect;
import com.viettel.bss.viettelpos.v4.stockinpect.asyntask.AsynDeleteStockInpect;
import com.viettel.bss.viettelpos.v4.stockinpect.asyntask.AsynGetListStock;
import com.viettel.bss.viettelpos.v4.stockinpect.asyntask.AsynGetStock;
import com.viettel.bss.viettelpos.v4.stockinpect.asyntask.AsynStateStock;
import com.viettel.bss.viettelpos.v4.stockinpect.asyntask.AsynStatusInpect;
import com.viettel.bss.viettelpos.v4.stockinpect.bo.ProductOfferingTotalDTO;
import com.viettel.bss.viettelpos.v4.stockinpect.bo.StockInspectCheckDTO;
import com.viettel.bss.viettelpos.v4.stockinpect.bo.StockInspectDTO;
import com.viettel.bss.viettelpos.v4.stockinpect.bo.StockInspectMessage;
import com.viettel.bss.viettelpos.v4.stockinpect.view.StockInpectWithQuantityActivity;
import com.viettel.bss.viettelpos.v4.stockinpect.view.StockInpectWithSerialActivity;
import com.viettel.bss.viettelpos.v4.ui.image.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leekien on 5/7/2018.
 */

public class MainStockInpectFragment extends FragmentCommon {
    Activity activity;
    String typeInspec = "1";
    @BindView(R.id.rbNumber)
    RadioButton rbNumber;
    @BindView(R.id.rbSerial)
    RadioButton rbSerial;
    @BindView(R.id.btnCheck)
    Button btnCheck;
    @BindView(R.id.spStatusInpect)
    Spinner spStatusInpect;
    @BindView(R.id.spStock)
    Spinner spStock;
    @BindView(R.id.spStateStock)
    Spinner spStateStock;
    @BindView(R.id.spTypeStock)
    Spinner spTypeStock;
    Long stateId;
    Long inspectStatus;
    String productOfferTypeId;
    StockInspectDTO stockInspectDTO = new StockInspectDTO();
    List<ProductOfferTypeDTO> lstProductOfferTypeDTO = new ArrayList<>();

    @Override
    public void onResume() {
        MainActivity.getInstance().setTitleActionBar("Kiểm kho hàng thực tế");
        MainActivity.getInstance().enableMenuListOrGridView(true);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        idLayout = R.layout.layout_main_inpect_stock;
        ButterKnife.bind(getActivity());
        activity = getActivity();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void unit(View v) {
        if (rbNumber.isChecked()) {
            typeInspec = "2";
        } else if (rbSerial.isChecked()) {
            typeInspec = "1";
            //
        }

        AsynGetListStock asynGetListStock = new AsynGetListStock(getActivity(), new OnPostExecuteListener<List<ProductOfferTypeDTO>>() {
            @Override
            public void onPostExecute(List<ProductOfferTypeDTO> result, String errorCode, String description) {
                if ("0".equals(errorCode) && !CommonActivity.isNullOrEmpty(result)) {
                    for (ProductOfferTypeDTO productOfferTypeDTO : result) {
                        lstProductOfferTypeDTO.add(productOfferTypeDTO);
                    }
                    List<String> list = new ArrayList<>();
                    for (int i = 0; i < result.size(); i++) {
                        list.add(result.get(i).getName());
                        Utils.setDataSpinner(getActivity(), list, spTypeStock);
                    }
                } else

                {
                    if (description == null || description.isEmpty()) {
                        description = getString(R.string.checkdes);
                    }
                    if (description != null && description.contains("java.lang.String.length()")) {
                        description = getString(R.string.server_time_out);
                    }
                    Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
                            description,
                            getResources().getString(R.string.app_name));
                    dialog.show();
                }
            }
        }, moveLogInAct);
        asynGetListStock.execute();
        spStateStock.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                OptionSetValueDTO optionSetValueDTO = (OptionSetValueDTO) spStateStock.getSelectedItem();
                stateId = optionSetValueDTO.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spStatusInpect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                OptionSetValueDTO optionSetValueDTO = (OptionSetValueDTO) spStatusInpect.getSelectedItem();
                inspectStatus = optionSetValueDTO.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spTypeStock.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                for (ProductOfferTypeDTO productOfferTypeDTO : lstProductOfferTypeDTO) {
                    if (productOfferTypeDTO.getName().equals(spTypeStock.getSelectedItem())) {
                        productOfferTypeId = productOfferTypeDTO.getProductOfferTypeId();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        AsynStatusInpect asynStatusInpect = new AsynStatusInpect(getActivity(), new OnPostExecuteListener<List<OptionSetValueDTO>>() {
            @Override
            public void onPostExecute(List<OptionSetValueDTO> result, String errorCode, String description) {
                if ("0".equals(errorCode) && !CommonActivity.isNullOrEmpty(result)) {
                    Utils.setDataSpinner(getActivity(), result, spStatusInpect);

                } else {
                    if (description == null || description.isEmpty()) {
                        description = getString(R.string.checkdes);
                    }
                    if (description != null && description.contains("java.lang.String.length()")) {
                        description = getString(R.string.server_time_out);
                    }
                    Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
                            description,
                            getResources().getString(R.string.app_name));
                    dialog.show();
                }
            }
        }, moveLogInAct);
        asynStatusInpect.execute();
        AsynStateStock asynStateStock = new AsynStateStock(getActivity(), new OnPostExecuteListener<List<OptionSetValueDTO>>() {
            @Override
            public void onPostExecute(List<OptionSetValueDTO> result, String errorCode, String description) {
                if ("0".equals(errorCode) && !CommonActivity.isNullOrEmpty(result)) {
                    Utils.setDataSpinner(getActivity(), result, spStateStock);


                } else {
                    if (description == null || description.isEmpty()) {
                        description = getString(R.string.checkdes);
                    }
                    if (description != null && description.contains("java.lang.String.length()")) {
                        description = getString(R.string.server_time_out);
                    }
                    Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
                            description,
                            getResources().getString(R.string.app_name));
                    dialog.show();
                }
            }
        }, moveLogInAct);
        asynStateStock.execute();
        spStateStock.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (CommonActivity.isNullOrEmpty(spStateStock.getSelectedItem().toString()) || CommonActivity.isNullOrEmpty(spStatusInpect.getSelectedItem().toString())) {
                    CommonActivity.showConfirmValidate(getActivity(), "Bạn phải chọn loại mặt hàng và tình trạng hàng");
                } else {
                    AsynGetStock asynGetStock = new AsynGetStock(getActivity(), new OnPostExecuteListener<List<ProductOfferingTotalDTO>>() {
                        @Override
                        public void onPostExecute(List<ProductOfferingTotalDTO> result, String errorCode, String description) {
                            if ("0".equals(errorCode) && !CommonActivity.isNullOrEmpty(result)) {
                                Utils.setDataSpinner(getActivity(), result, spStock);
                            } else {
                                if (description == null || description.isEmpty()) {
                                    description = getString(R.string.checkdes);
                                }
                                if (description != null && description.contains("java.lang.String.length()")) {
                                    description = getString(R.string.server_time_out);
                                }
                                Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
                                        description,
                                        getResources().getString(R.string.app_name));
                                dialog.show();
                            }
                        }
                    }, moveLogInAct, productOfferTypeId, String.valueOf(stateId), typeInspec);
                    asynGetStock.execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnCheck.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                for (ProductOfferTypeDTO productOfferTypeDTO : lstProductOfferTypeDTO) {
                    if (productOfferTypeDTO.getName().equals(spTypeStock.getSelectedItem())) {
                        productOfferTypeId = productOfferTypeDTO.getProductOfferTypeId();
                    }
                }
                OptionSetValueDTO optionSetValueDTO = (OptionSetValueDTO) spStateStock.getItemAtPosition(0);
                stateId = optionSetValueDTO.getId();
                OptionSetValueDTO optionSetValueDTO1 = (OptionSetValueDTO) spStatusInpect.getItemAtPosition(0);
                inspectStatus = optionSetValueDTO1.getId();

                if (!CommonActivity.isNullOrEmpty(productOfferTypeId))

                {
                    stockInspectDTO.setProdOfferTypeId(Long.valueOf(productOfferTypeId));
                }
                stockInspectDTO.setInspectStatus(inspectStatus);
                stockInspectDTO.setStateId(stateId);
                final AsynCheckStock asynCheckStock = new AsynCheckStock(getActivity(), new OnPostExecuteListener<StockInspectMessage>() {
                    @Override
                    public void onPostExecute(StockInspectMessage result, String errorCode, String description) {
                        if ("0".equals(errorCode) && !CommonActivity.isNullOrEmpty(result)) {
                            if (result.isCheckEndStockInpect() == true && typeInspec == "1") {
                                final Dialog dialog = CommonActivity.createDialog(getActivity(), result.getMessageNotice().get(0), "MBCCS", "Chấp nhận", "Bỏ qua", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        AsynCheckSale asynCheckSale = new AsynCheckSale(getActivity(), new OnPostExecuteListener<StockInspectMessage>() {
                                            @Override
                                            public void onPostExecute(StockInspectMessage result, String errorCode, String description) {
                                                if ("0".equals(errorCode) && !CommonActivity.isNullOrEmpty(result)) {


                                                } else {
                                                    if (description == null || description.isEmpty()) {
                                                        description = getString(R.string.checkdes);
                                                    }
                                                    if (description != null && description.contains("java.lang.String.length()")) {
                                                        description = getString(R.string.server_time_out);
                                                    }
                                                    Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
                                                            description,
                                                            getResources().getString(R.string.app_name));
                                                    dialog.show();
                                                }
                                            }
                                        }, moveLogInAct, stockInspectDTO, typeInspec);
                                        asynCheckSale.execute();
                                    }
                                }, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                });
                                dialog.setCancelable(false);
                                dialog.show();
                                return;
                            } else if (result.isCheckEndStockInpect() == false && typeInspec == "1") {
                                AsynCheckSale asynCheckSale = new AsynCheckSale(getActivity(), new OnPostExecuteListener<StockInspectMessage>() {
                                    @Override
                                    public void onPostExecute(StockInspectMessage result, String errorCode, String description) {
                                        if ("0".equals(errorCode) && !CommonActivity.isNullOrEmpty(result)) {
                                            final List<StockInspectCheckDTO> listNeedToDelete = result.getListNeedToDelete();
                                            if (result.isCheckNedToDelete() == true) {
                                                final Dialog dialog = CommonActivity.createDialog(getActivity(), result.getMessageNotice().get(0), "MBCCS", "Chấp nhận", "Bỏ qua", new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        AsynDeleteStockInpect asynDeleteStockInpect = new AsynDeleteStockInpect(getActivity(), new OnPostExecuteListener<StockInspectMessage>() {
                                                            @Override
                                                            public void onPostExecute(StockInspectMessage result, String errorCode, String description) {
                                                                if ("0".equals(errorCode) && !CommonActivity.isNullOrEmpty(result)) {

                                                                } else {
                                                                    if (description == null || description.isEmpty()) {
                                                                        description = getString(R.string.checkdes);
                                                                    }
                                                                    if (description != null && description.contains("java.lang.String.length()")) {
                                                                        description = getString(R.string.server_time_out);
                                                                    }
                                                                    Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
                                                                            description,
                                                                            getResources().getString(R.string.app_name));
                                                                    dialog.show();
                                                                }
                                                            }
                                                        }, moveLogInAct, listNeedToDelete, stockInspectDTO);
                                                        asynDeleteStockInpect.execute();
                                                    }
                                                }, new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {

                                                    }
                                                });
                                                dialog.setCancelable(false);
                                                dialog.show();
                                                return;
                                            } else {
                                                AsynCheckStockInpect asynCheckStockInpect = new AsynCheckStockInpect(getActivity(), new OnPostExecuteListener<StockInspectMessage>() {
                                                    @Override
                                                    public void onPostExecute(StockInspectMessage result, String errorCode, String description) {
                                                        if ("0".equals(errorCode) && !CommonActivity.isNullOrEmpty(result)) {
                                                            StockInspectDTO stockInspectSaveDTO = result.getStockInspectSaveDTO();
                                                            List<StockInspectCheckDTO> listProductCheck = new ArrayList<StockInspectCheckDTO>();
                                                            if (!CommonActivity.isNullOrEmpty(result.getListProductCheck())) {
                                                                listProductCheck = result.getListProductCheck();
                                                            }
                                                            StockInpectWithSerialActivity stockInpectWithSerialActivity = new StockInpectWithSerialActivity();
                                                            Bundle bundle = new Bundle();
                                                            bundle.putSerializable("stockInspectDTO", (Serializable) stockInspectDTO);
                                                            bundle.putSerializable("listProductCheck", (Serializable) listProductCheck);
                                                            bundle.putSerializable("stockInspectSaveDTO", stockInspectSaveDTO);
                                                            stockInpectWithSerialActivity.setArguments(bundle);
                                                            ReplaceFragment.replaceFragment(getActivity(), stockInpectWithSerialActivity, false);

                                                        } else {
                                                            if (description == null || description.isEmpty()) {
                                                                description = getString(R.string.checkdes);
                                                            }
                                                            if (description != null && description.contains("java.lang.String.length()")) {
                                                                description = getString(R.string.server_time_out);
                                                            }
                                                            Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
                                                                    description,
                                                                    getResources().getString(R.string.app_name));
                                                            dialog.show();
                                                        }
                                                    }
                                                }, moveLogInAct, stockInspectDTO, typeInspec);
                                                asynCheckStockInpect.execute();
                                            }


                                        } else {
                                            if (description == null || description.isEmpty()) {
                                                description = getString(R.string.checkdes);
                                            }
                                            if (description != null && description.contains("java.lang.String.length()")) {
                                                description = getString(R.string.server_time_out);
                                            }
                                            Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
                                                    description,
                                                    getResources().getString(R.string.app_name));
                                            dialog.show();
                                        }
                                    }
                                }, moveLogInAct, stockInspectDTO, typeInspec);
                                asynCheckSale.execute();
                            } else {
                                AsynCheckStockInpect asynCheckStockInpect = new AsynCheckStockInpect(getActivity(), new OnPostExecuteListener<StockInspectMessage>() {
                                    @Override
                                    public void onPostExecute(StockInspectMessage result, String errorCode, String description) {
                                        if ("0".equals(errorCode) && !CommonActivity.isNullOrEmpty(result)) {
                                            StockInspectDTO stockInspectSaveDTO = result.getStockInspectSaveDTO();
                                            String staticNote = "";
                                            if (!CommonActivity.isNullOrEmpty(result.getStaticNotice())) { staticNote = result.getStaticNotice();}
                                            List<StockInspectCheckDTO> listProductCheck = new ArrayList<StockInspectCheckDTO>();
                                            if (!CommonActivity.isNullOrEmpty(result.getListProductCheck())) {
                                                listProductCheck = result.getListProductCheck();
                                            }
                                            if (typeInspec == "1") {
                                                StockInpectWithSerialActivity stockInpectWithSerialActivity = new StockInpectWithSerialActivity();
                                                Bundle bundle = new Bundle();
                                                bundle.putSerializable("stockInspectDTO", (Serializable) stockInspectDTO);
                                                bundle.putSerializable("listProductCheck", (Serializable) listProductCheck);
                                                bundle.putSerializable("stockInspectSaveDTO", stockInspectSaveDTO);
                                                bundle.putString("staticNote", staticNote);
                                                stockInpectWithSerialActivity.setArguments(bundle);
                                                ReplaceFragment.replaceFragment(getActivity(), stockInpectWithSerialActivity, false);
                                            }
                                            if (typeInspec == "2") {
                                                StockInpectWithQuantityActivity stockInpectWithQuantityActivity = new StockInpectWithQuantityActivity();
                                                Bundle bundle = new Bundle();
                                                bundle.putSerializable("stockInspectDTO", (Serializable) stockInspectDTO);
                                                bundle.putSerializable("listProductCheck", (Serializable) listProductCheck);
                                                bundle.putSerializable("stockInspectSaveDTO", stockInspectSaveDTO);
                                                bundle.putString("staticNote", staticNote);
                                                stockInpectWithQuantityActivity.setArguments(bundle);
                                                ReplaceFragment.replaceFragment(getActivity(), stockInpectWithQuantityActivity, false);
                                            }


                                        } else {
                                            if (description == null || description.isEmpty()) {
                                                description = getString(R.string.checkdes);
                                            }
                                            if (description != null && description.contains("java.lang.String.length()")) {
                                                description = getString(R.string.server_time_out);
                                            }
                                            Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
                                                    description,
                                                    getResources().getString(R.string.app_name));
                                            dialog.show();
                                        }
                                    }
                                }, moveLogInAct, stockInspectDTO, typeInspec);
                                asynCheckStockInpect.execute();
                            }


                        } else {
                            if (description == null || description.isEmpty()) {
                                description = getString(R.string.checkdes);
                            }
                            if (description != null && description.contains("java.lang.String.length()")) {
                                description = getString(R.string.server_time_out);
                            }
                            Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
                                    description,
                                    getResources().getString(R.string.app_name));
                            dialog.show();
                        }
                    }
                }, moveLogInAct, stockInspectDTO, typeInspec);
                asynCheckStock.execute();
            }
        });

    }


    @Override
    protected void setPermission() {

    }
}
