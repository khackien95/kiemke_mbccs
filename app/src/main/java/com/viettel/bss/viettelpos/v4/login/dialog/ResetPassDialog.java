package com.viettel.bss.viettelpos.v4.login.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.login.asynctask.GetSecretKeyAsyncTask;
import com.viettel.bss.viettelpos.v4.login.asynctask.ResetPassAsyncTask;

/**
 * Created by hungtv64 on 1/26/2018.
 */

public class ResetPassDialog extends Dialog {

    private Activity activity;
    private String userName;

    private EditText edtUserNameReset;
    private EditText edtNewPasswordReset;
    private EditText edtConfirmPasswordReset;
    private EditText edtmabimat;

    private Button btnsinhmabimat;
    private Button btncresetpass;
    private Button btncanel;

    private final OnPostExecuteListener<String> onPostResetPasslistener;
    private final OnPostExecuteListener<String> onPostGetSecretKeyListener;

    public ResetPassDialog(
            Activity activity,
            String userName,
            OnPostExecuteListener<String> onPostResetPasslistener,
            OnPostExecuteListener<String> onPostGetSecretKeyListener) {

        super(activity);
        this.activity = activity;
        this.userName = userName;
        this.onPostResetPasslistener = onPostResetPasslistener;
        this.onPostGetSecretKeyListener = onPostGetSecretKeyListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_reset_pass);

        edtUserNameReset = (EditText) findViewById(R.id.edtUserNameReset);
        edtNewPasswordReset = (EditText) findViewById(R.id.edtNewPasswordReset);
        edtConfirmPasswordReset = (EditText) findViewById(R.id.edtConfirmPasswordReset);
        edtmabimat = (EditText) findViewById(R.id.edtmabimat);

        btnsinhmabimat = (Button) findViewById(R.id.btnsinhmabimat);
        btncresetpass = (Button) findViewById(R.id.btncresetpass);
        btncanel = (Button) findViewById(R.id.btncanel);

        if (!CommonActivity.isNullOrEmpty(userName)) {
            edtUserNameReset.setText(userName);
        }

        btnsinhmabimat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (edtUserNameReset.getText() != null && !edtUserNameReset.getText().toString().isEmpty()) {
                    if (CommonActivity.isNetworkConnected(activity)) {
                        GetSecretKeyAsyncTask getSecretKeyAsyn = new GetSecretKeyAsyncTask(
                                activity, onPostGetSecretKeyListener);
                        getSecretKeyAsyn.execute(edtUserNameReset.getText().toString().trim());
                    } else {
                        CommonActivity.createAlertDialog(activity,
                                activity.getString(R.string.errorNetwork),
                                activity.getString(R.string.app_name)).show();
                    }
                } else {
                    String message = activity.getString(R.string.userNameRequired);
                    String title = activity.getString(R.string.app_name);
                    Dialog dialog = CommonActivity.createAlertDialog(
                            activity, message, title);
                    dialog.show();
                    edtUserNameReset.requestFocus();
                }
            }
        });


        btncresetpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (edtUserNameReset.getText() != null && !edtUserNameReset.getText().toString().isEmpty()) {
                    if (edtNewPasswordReset.getText() != null && !edtNewPasswordReset.getText().toString().isEmpty()) {
                        if (edtConfirmPasswordReset.getText() != null && !edtConfirmPasswordReset.getText().toString().isEmpty()) {
                            if (edtmabimat.getText() != null && !edtmabimat.getText().toString().isEmpty()) {
                                if (edtNewPasswordReset.getText().toString()
                                        .equalsIgnoreCase(edtConfirmPasswordReset.getText().toString())) {

                                    if (CommonActivity.isNetworkConnected(activity)) {
                                        CommonActivity.createDialog(activity,
                                                activity.getString(R.string.confirmresetpassaction),
                                                activity.getString(R.string.app_name),
                                                activity.getString(R.string.buttonOk),
                                                activity.getString(R.string.buttonCancel),
                                                onclickconfireset, null).show();
                                    } else {
                                        CommonActivity.createAlertDialog(activity,
                                                activity.getString(R.string.errorNetwork),
                                                activity.getString(R.string.app_name)).show();
                                    }
                                } else {
                                    String message = activity.getString(R.string.password_not_same_repass);
                                    String title = activity.getString(R.string.app_name);
                                    Dialog dialog = CommonActivity.createAlertDialog(activity, message, title);
                                    dialog.show();
                                }
                            } else {
                                String message = activity.getString(R.string.checkmabimat);
                                String title = activity.getString(R.string.app_name);
                                Dialog dialog = CommonActivity.createAlertDialog(activity, message, title);
                                dialog.show();
                                edtmabimat.requestFocus();
                            }
                        } else {
                            String message = activity.getString(R.string.passwordConfirmRequired);
                            String title = activity.getString(R.string.app_name);
                            Dialog dialog = CommonActivity.createAlertDialog(
                                    activity, message, title);
                            dialog.show();
                            edtConfirmPasswordReset.requestFocus();
                        }
                    } else {
                        String message = activity.getString(R.string.passwordNewRequired);
                        String title = activity.getString(R.string.app_name);
                        Dialog dialog = CommonActivity.createAlertDialog(
                                activity, message, title);
                        dialog.show();
                        edtNewPasswordReset.requestFocus();
                    }
                } else {
                    String message = activity.getString(R.string.userNameRequired);
                    String title = activity.getString(R.string.app_name);
                    Dialog dialog = CommonActivity.createAlertDialog(
                            activity, message, title);
                    dialog.show();
                    edtUserNameReset.requestFocus();
                }
            }
        });

        btncanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                cancel();
            }
        });
    }

    private final View.OnClickListener onclickconfireset = new View.OnClickListener() {
        @Override
        public void onClick(View arg0) {
            ResetPassAsyncTask resetPassAsyncTask =
                    new ResetPassAsyncTask(activity, onPostResetPasslistener);
            resetPassAsyncTask.execute(edtUserNameReset.getText().toString().trim(),
                    edtNewPasswordReset.getText().toString().trim(),
                    edtConfirmPasswordReset.getText().toString().trim(),
                    edtmabimat.getText().toString().trim());
        }
    };

    public String getUserName() {
        return edtUserNameReset.getText().toString().trim();
    }

    public String getPassword() {
        return edtNewPasswordReset.getText().toString().trim();
    }
}