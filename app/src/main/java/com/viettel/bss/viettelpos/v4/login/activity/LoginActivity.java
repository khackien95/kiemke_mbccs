package com.viettel.bss.viettelpos.v4.login.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.GuideSettingPermissionDialog;
import com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity;
import com.viettel.bss.viettelpos.v4.business.ApParamBusiness;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DatabaseUtils;
import com.viettel.bss.viettelpos.v4.commons.Define;
import com.viettel.bss.viettelpos.v4.commons.EncryptKeystore;
import com.viettel.bss.viettelpos.v4.commons.FingerManager;
import com.viettel.bss.viettelpos.v4.commons.FingerManager.OnAuthenticationError;
import com.viettel.bss.viettelpos.v4.commons.FingerManager.OnAuthenticationHelp;
import com.viettel.bss.viettelpos.v4.commons.FingerManager.OnAuthenticationSucceed;
import com.viettel.bss.viettelpos.v4.commons.GPSTracker;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.SecurityUtil;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.infrastrucure.dal.InfrastrucureDB;
import com.viettel.bss.viettelpos.v4.plan.asynctask.InsertSaleCheckInHistoryThread;
import com.viettel.bss.viettelpos.v4.login.asynctask.LoginAsyncTask;
import com.viettel.bss.viettelpos.v4.login.asynctask.SyncAsyncTask;
import com.viettel.bss.viettelpos.v4.login.dialog.ChangePassDialog;
import com.viettel.bss.viettelpos.v4.login.dialog.ResetPassDialog;
import com.viettel.bss.viettelpos.v4.login.object.DataOutputLogin;
import com.viettel.bss.viettelpos.v4.sale.business.StaffBusiness;
import com.viettel.bss.viettelpos.v4.synchronizationdata.UpdateVersionAsyn;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import static com.viettel.bss.viettelpos.v4.commons.Session.userName;

public class LoginActivity extends GPSTracker implements OnClickListener {

    private static Activity act;

    private static final int REQUEST_CODE_ASK_PERMISSIONS = 12354;
    private static final int PERM_REQUEST_CODE_DRAW_OVERLAYS = 11111;
    private static final int APP_SETTING_RESULT_CODE = 333;
    private static final String LOG_TAG = LoginActivity.class.getName();
    private static final FingerManager fingerManager = new FingerManager();

    private TextInputEditText edtUserName;
    private EditText edtPassword;
    private Button btnLogin;
    private String serialSim = "";

    private ProgressDialog dialogSendSMS;
    private String description = "";
    private String forceUpgrade = "";
    private String version = "";
    private String checkSyn = "";
    private TelephonyManager tele;
    private Animation shake;
    private TextView tvFinger;
    private Activity context;

    private CountDownTimer countDownTimer;
    private ChangePassDialog dialogChangePass;
    private ResetPassDialog dialogResetpass;
    private TextView txtchangePass;
    private TextView txtresetpass;
    private GuideSettingPermissionDialog dialogGuideSetting;
    private final List<String> lstPermissionDenied = new ArrayList<>();

    // thientv7 bo sung addInfo
    String addInfo = "";
    private SharedPreferences preferences;

    public static Activity getInstance() {
        return act;
    }

    private static final String[] arrPermission = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.SEND_SMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_SMS,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @BindView(R.id.btnLoginPublic)
    Button btnLoginPublic;
    @BindView(R.id.imgSwitchNetwork)
    ImageView imgSwitchNetwork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        ButterKnife.bind(this);

        this.context = this;
        this.preferences = context.getSharedPreferences(Define.PRE_NAME, MODE_PRIVATE);

        edtUserName = (TextInputEditText) findViewById(R.id.edtUserName);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtPassword.setTransformationMethod(new PasswordTransformationMethod());
        edtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    FragmentLoginNotData.mView = null;
                    edtUserName.setText(edtUserName.getText().toString().trim());
                    edtPassword.setText(edtPassword.getText().toString().trim());
                    if (edtUserName.getText().toString().isEmpty()) {
                        String message = getString(R.string.userNameRequired);
                        String title = getString(R.string.app_name);
                        Dialog dialog = CommonActivity.createAlertDialog(LoginActivity.this,
                                message, title);
                        dialog.show();
                        edtUserName.requestFocus();
                        edtUserName.startAnimation(shake);
                    } else if (edtPassword.getText().toString().isEmpty()) {
                        String message = getString(R.string.passwordRequired);
                        String title = getString(R.string.app_name);
                        Dialog dialog = CommonActivity.createAlertDialog(LoginActivity.this,
                                message, title);
                        dialog.show();
                        edtPassword.requestFocus();
                    } else {
                        if (CommonActivity.isNetworkConnected(LoginActivity.this)) {
                            LoginAsyncTask loginAsyncTask = new LoginAsyncTask(
                                    context, onPostLoginListener, preferences);
                            loginAsyncTask.execute(edtUserName.getText().toString(),
                                    edtPassword.getText().toString(), addInfo, serialSim);
                        } else {
                            OnClickListener loginClick = new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogSendSMS = new ProgressDialog(
                                            LoginActivity.this);
                                    dialogSendSMS.setMessage(getResources()
                                            .getString(R.string.processing));
                                    dialogSendSMS.setCancelable(false);
                                    if (!dialogSendSMS.isShowing()) {
                                        dialogSendSMS.show();
                                    }
                                    String synTask = "0SI";

                                    try {
                                        CommonActivity.sendSMS(
                                                Constant.EXCHANGE_ADDRESS,
                                                buildRawData(true, edtUserName
                                                        .getText().toString()
                                                        .trim(), edtPassword
                                                        .getText().toString()
                                                        .trim(), addInfo),
                                                LoginActivity.this, dialogSendSMS,
                                                synTask);
                                    } catch (Exception e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }

                                    new CountDownTimer(Constant.TIMEOUT_SMS, 1000) {
                                        @Override
                                        public void onTick(long millisUntilFinished) {
                                        }
                                        @Override
                                        public void onFinish() {
                                            if (dialogSendSMS.isShowing()) {
                                                dialogSendSMS.dismiss();
                                                Toast.makeText(
                                                        LoginActivity.this,
                                                        getResources()
                                                                .getString(
                                                                        R.string.time_out_sms),
                                                        Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }.start();
                                }
                            };
                            CommonActivity.createDialog(
                                    LoginActivity.this,
                                    getResources().getString(
                                            R.string.no_network_message),
                                    getResources().getString(
                                            R.string.no_network_title),
                                    getResources().getString(R.string.cancel),
                                    getResources().getString(R.string.ok),
                                    null, loginClick).show();
                        }
                    }
                    return true;
                }
                return false;
            }
        });
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        tvFinger = (TextView) findViewById(R.id.tvFinger);
        TextView txtversion = (TextView) findViewById(R.id.txtversion);
        version = CommonActivity.getversionclient(context);
        if (version != null && !version.equals("")) {
            txtversion.setText(getString(R.string.text_version) + " " + version);
        } else {
            txtversion.setText("");
        }
        txtchangePass = (TextView) findViewById(R.id.txtchangePass);
        txtchangePass.setVisibility(View.VISIBLE);
        txtchangePass.setOnClickListener(this);
        txtchangePass.setOnClickListener(this);
        txtresetpass = (TextView) findViewById(R.id.txtresetpass);
        txtresetpass.setOnClickListener(this);
        act = this;
        // checkDualSim();
        String lastLogin = "";
        if (preferences != null) {
            lastLogin = preferences.getString(Define.KEY_LOGIN_NAME, "");
        }
        Session.KPI_REQUEST = true;
        edtUserName.setText(lastLogin);
        serialSim = System.currentTimeMillis() + "";
        shake = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.shake);
        try {
            CommonActivity.getMemory(LoginActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        CommonActivity.clearCache();
        Fabric.with(this, new Crashlytics());

        dialogGuideSetting = new GuideSettingPermissionDialog(
                this, onClickAcceptGuideSetting);
        if (!checkDonePermission()) {
            showDialogViewSetting();
        }

        if (CommonActivity.askPermission()) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                tele = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                serialSim = tele.getSimSerialNumber();
            }
        }
        if (CommonActivity.isNullOrEmpty(serialSim)) {
            serialSim = System.currentTimeMillis() + "";
        }

        if(Constant.versionType == Constant.PUBLIC_INTERNET_VERSION){
            imgSwitchNetwork.setVisibility(View.VISIBLE);
        } else {
            imgSwitchNetwork.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Constant.REGISTER_RECEIVER));
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        String useFinger = preferences.getString(Constant.KEY_FINGER_USER, "");
        // Neu cau hinh co dung van tay
        if (!FingerManager.checkFingerSupported(this)
                || !FingerManager.checkFingerEnable(this)) {
            // lnFingerfin
            findViewById(R.id.lnFinger).setVisibility(View.GONE);
        } else {
            findViewById(R.id.lnFinger).setVisibility(View.VISIBLE);
        }
        try {
            CommonActivity.getMemory(LoginActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if ("1".equals(useFinger) && FingerManager.checkFingerSupported(this)) {
            if (countDownTimer == null) {
                fingerManager.fingerListener(this, onSuccess, onError, onHelp, onFail);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initLogUser() {
        Crashlytics.setUserName(edtUserName.getText().toString());
        Crashlytics.setUserEmail(edtUserName.getText().toString());
        Crashlytics.setUserIdentifier(Session.token);
    }

    private String buildRawData(Boolean isSMS, String user, String pass, String addInfo) throws Exception {
        StringBuilder result = new StringBuilder();
        result.append("<ws:login>");
        result.append("<userName>");
        result.append(new SecurityUtil().encrypt(user));
        result.append("</userName>");
        result.append("<passWord>");
        result.append(new SecurityUtil().encrypt(pass));
        result.append("</passWord>");

        result.append("<addInfo>");
        result.append(new SecurityUtil().encrypt(addInfo));
        result.append("</addInfo>");

        result.append("<clientTime>");
        result.append(System.currentTimeMillis());
        result.append("</clientTime>");
        result.append("<version>");
        result.append(CommonActivity.getversionclient(context));
        result.append("</version>");
        result.append("<serialSim>");
        if (serialSim == null || serialSim.trim().isEmpty()) {
            serialSim = user.toUpperCase();
        }
        result.append(serialSim);
        result.append("</serialSim>");
        result.append("</ws:login>");
        return result.toString();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public void successLogin() {

        // CTV checkIn
        InsertSaleCheckInHistoryThread insertSaleCheckInHistoryThread =
                new InsertSaleCheckInHistoryThread(LoginActivity.this);
        insertSaleCheckInHistoryThread.start();

        //===========run check version==============
        if (forceUpgrade.equalsIgnoreCase("0")
                && !version.equals(CommonActivity.getversionclient(context))) {
            // ========== khong bat buoc cap nhat version===== show
            // aletdialog
            Dialog dialog = CommonActivity.createDialog(
                    LoginActivity.this,
                    description
                            + "\n"
                            + context.getResources()
                            .getString(R.string.isversion),
                    context.getResources().getString(R.string.updateversion),
                    context.getResources().getString(R.string.cancel),
                    context.getResources().getString(R.string.ok),
                    cancelUpdateVer, onclickCheckUpdate);
            dialog.show();

        } else if (forceUpgrade.equalsIgnoreCase("1")
                && !version.equals(CommonActivity.getversionclient(context))) {
            // ========== bat buoc cap nhat ======================
            Log.d("versionnnnnserrver", version);
            UpdateVersionAsyn updateVersionAsyn = new UpdateVersionAsyn(
                    LoginActivity.this, Constant.PATH_UPDATE_VERSION
                    + Session.token);
            updateVersionAsyn.execute();

        } else {
            // ==========chay dong bo du lieu ==========================
            boolean isDatabaseExists = DatabaseUtils.doesDatabaseExist(
                    LoginActivity.this, Define.DB_NAME);
            Log.e("DatabaseExists", "" + isDatabaseExists);
            // Dang nhap thanh cong, kiem tra ton tai database
            if (isDatabaseExists) {
                if (!CommonActivity.isNullOrEmpty(checkSyn) && "1".equals(checkSyn)) {
                    String lastVersion = preferences.getString(Define.KEY_LAST_VERSION, "");
                    PackageInfo packageInfo;
                    try {
                        packageInfo = getPackageManager().getPackageInfo(
                                getPackageName(), 0);
                        String currentVersion = packageInfo.versionName;
                        if (lastVersion == null || !lastVersion.equals(currentVersion)) {
                            deleteDatabase(Define.DB_NAME);
                            SyncAsyncTask sync = new SyncAsyncTask(
                                    LoginActivity.this, onPostSyncListener);
                            sync.execute(Session.getToken(), userName);
                        } else {
                            try {
                                InfrastrucureDB mInfrastrucureDB = new InfrastrucureDB(context);
                                String shop[] = mInfrastrucureDB.getProvince();
                                Session.province = shop[0];
                                Session.district = shop[1];
                                mInfrastrucureDB.close();
                                Session.loginUser = StaffBusiness.getStaffByStaffCode(context, userName);
                            } catch (Exception ignored) {
                                Log.e(LOG_TAG, ignored.getMessage());
                            }
                            // Neu database da ton tai, login vao man
                            // hinh chinh
                            Intent i = new Intent(LoginActivity.this, MainActivity.class);
                            clearOmniStaff();
                            startActivity(i);
                        }
                    } catch (NameNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else {
                    try {
                        InfrastrucureDB mInfrastrucureDB = new InfrastrucureDB(context);
                        String shop[] = mInfrastrucureDB.getProvince();
                        Session.province = shop[0];
                        Session.district = shop[1];
                        mInfrastrucureDB.close();
                        Session.loginUser = StaffBusiness.getStaffByStaffCode(LoginActivity.this, userName);
                    } catch (Exception ignored) {
                    }
                    // Neu database da ton tai, login vao man hinh chinh
                    Intent i = new Intent(LoginActivity.this,
                            MainActivity.class);
                    clearOmniStaff();
                    startActivity(i);
                }
                // finish();
            } else {
                SyncAsyncTask sync = new SyncAsyncTask(
                        LoginActivity.this, onPostSyncListener);
                sync.execute(Session.getToken(), userName);
            }
        }
    }

    private OnClickListener showAPNSetting = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            startActivityForResult(new Intent(Settings.ACTION_APN_SETTINGS), 0);
        }
    };

    private final OnPostExecuteListener<String> onPostSyncListener = new OnPostExecuteListener<String>() {
        @Override
        public void onPostExecute(String result, String errorCode, String description) {
            if (Constant.SUCCESS_CODE.equals(result)) {
                // Dong bo du lieu thanh cong,
                // luu bien staff vao session,
                // vao man hinh chinh chinh
                InfrastrucureDB mInfrastrucureDB = new InfrastrucureDB(context);
                String shop[] = mInfrastrucureDB.getProvince();
                mInfrastrucureDB.close();
                Session.loginUser = StaffBusiness.getStaffByStaffCode(LoginActivity.this, userName);
                Session.province = shop[0];
                Session.district = shop[1];
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                clearOmniStaff();
                startActivity(i);
//                finish();
            } else if (Constant.ERROR_CODE.equals(result)) {
                String message = getString(R.string.exception);
                String title = getString(R.string.app_name);
                Dialog dialog = CommonActivity.createAlertDialog(context, message, title);
                dialog.show();
            } else {
                String title = getString(R.string.app_name);
                String message = getResources().getString(R.string.exception);
                Dialog dialog = CommonActivity.createAlertDialog(context, message, title);
                dialog.show();
            }
        }
    };

    // check updateDateVersion
    private final OnClickListener onclickCheckUpdate = new OnClickListener() {

        @Override
        public void onClick(View v) {
            UpdateVersionAsyn updateVersionAsyn = new UpdateVersionAsyn(
                    LoginActivity.this, Constant.PATH_UPDATE_VERSION
                    + Session.token);
            updateVersionAsyn.execute();
        }
    };

    // cacel updateVersion
    private final OnClickListener cancelUpdateVer = new OnClickListener() {
        @Override
        public void onClick(View v) {
            boolean isDatabaseExists = DatabaseUtils.doesDatabaseExist(
                    LoginActivity.this, Define.DB_NAME);
            Log.e("DatabaseExists", "" + isDatabaseExists);
            // Dang nhap thanh cong, kiem tra
            // ton tai database
            if (isDatabaseExists) {
                if (!CommonActivity.isNullOrEmpty(checkSyn) && "1".equals(checkSyn)) {
                    String lastVersion = preferences.getString(
                            Define.KEY_LAST_VERSION, "");
                    PackageInfo packageInfo;
                    try {
                        packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        String currentVersion = packageInfo.versionName;
                        if (lastVersion == null || !lastVersion.equals(currentVersion)) {
                            deleteDatabase(Define.DB_NAME);
                            SyncAsyncTask sync = new SyncAsyncTask(
                                    LoginActivity.this, onPostSyncListener);
                            sync.execute(Session.getToken(), userName);
                        } else {
                            try {
                                InfrastrucureDB mInfrastrucureDB = new InfrastrucureDB(context);
                                String shop[] = mInfrastrucureDB.getProvince();
                                Session.province = shop[0];
                                Session.district = shop[1];
                                mInfrastrucureDB.close();
                                Session.loginUser = StaffBusiness
                                        .getStaffByStaffCode(
                                                LoginActivity.this,
                                                userName);
                            } catch (Exception ignored) {
                            }
                            // Neu database da ton tai, login vao man hinh chinh
                            Intent i = new Intent(LoginActivity.this,
                                    MainActivity.class);
                            clearOmniStaff();
                            startActivity(i);
                        }
                    } catch (NameNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else {
                    try {
                        InfrastrucureDB mInfrastrucureDB = new InfrastrucureDB(context);
                        String shop[] = mInfrastrucureDB.getProvince();
                        Session.province = shop[0];
                        Session.district = shop[1];
                        mInfrastrucureDB.close();
                        Session.loginUser = StaffBusiness.getStaffByStaffCode(
                                LoginActivity.this, userName);
                    } catch (Exception ignored) {
                    }
                    // Neu database da ton tai, login vao man hinh chinh
                    Intent i = new Intent(LoginActivity.this,
                            MainActivity.class);
                    clearOmniStaff();
                    startActivity(i);
                }
            } else {
                SyncAsyncTask sync = new SyncAsyncTask(
                        LoginActivity.this, onPostSyncListener);
                sync.execute(Session.getToken(), userName);
            }
        }
    };

    private final OnPostExecuteListener<DataOutputLogin> onPostLoginListener = new OnPostExecuteListener<DataOutputLogin>() {
        @Override
        public void onPostExecute(DataOutputLogin result, String errorCode, String description) {
            initLogUser();
            if (Constant.OTP_REQIRE.equals(errorCode)) {

                Session.isUserOTP = true;
                Toast.makeText(context, context.getString(
                        R.string.otp_send_success), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(LoginActivity.this, OtpLoginActivity.class);
                result.setSerialSim(serialSim);
                intent.putExtra("DataOutputLogin", result);
                clearOmniStaff();
                startActivity(intent);
            } else if (Constant.SUCCESS_CODE.equals(errorCode)) {

                LoginActivity.this.description = result.getDescription();
                LoginActivity.this.forceUpgrade = result.getForceUpgrade();
                LoginActivity.this.version = result.getVersion();
                LoginActivity.this.checkSyn = result.getCheckSyn();

                View.OnClickListener changePass = new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        showPopupChangePass();
                    }
                };
                View.OnClickListener later = new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        successLogin();
                    }
                };
                if (result.getDaysBetweenExpried() != null && !result.getDaysBetweenExpried().isEmpty()) {
                    String msg = context.getString(R.string.password_will_expire, result.getDaysBetweenExpried());
                    if ("0".equals(result.getDaysBetweenExpried())) {
                        msg = context.getString(R.string.password_expire_today, result.getDaysBetweenExpried());
                    }
                    CommonActivity.createDialog(LoginActivity.this, msg,
                            context.getString(R.string.app_name),
                            context.getString(R.string.later),
                            context.getString(R.string.change_pass_now),
                            later, changePass).show();
                } else {
                    successLogin();
                }
            } else {
                fingerManager.fingerListener(LoginActivity.this, onSuccess, onError, onHelp, onFail);
                if (Constant.ERROR_USER_MISSING.equals(errorCode)) {
                    String message = getString(R.string.userNameRequired);
                    String title = getString(R.string.app_name);
                    Dialog dialog = CommonActivity.createAlertDialog(context, message, title);
                    dialog.show();
                    edtUserName.requestFocus();
                } else if (Constant.ERROR_PASSWORD_MISSING.equals(errorCode)) {
                    String message = getString(R.string.passwordRequired);
                    String title = getString(R.string.app_name);
                    Dialog dialog = CommonActivity.createAlertDialog(context, message, title);
                    dialog.show();
                    edtPassword.requestFocus();
                } else if (Constant.ERROR_CODE.equals(errorCode)) {
                    String message = getString(R.string.exception);
                    if(!CommonActivity.isNullOrEmpty(description)){
                        message = description;
                    }
                    String title = getString(R.string.app_name);
                    Dialog dialog = CommonActivity.createAlertDialog(context, message, title);
                    dialog.show();
                } else if (Constant.ERROR_PING_SERVER.equals(errorCode)) {
                    String message = getString(R.string.login_error);
                    String title = getString(R.string.app_name);
                    if(Constant.versionType == 5){
                        message = context.getString(R.string.login_public_error);
                        title = context.getString(R.string.app_public_name);
                        CommonActivity.createAlertDialog(context,
                                message, title).show();
                    }else{
                        Dialog dialog = CommonActivity.createDialog(context,
                                message, title, getString(R.string.ok),
                                getString(R.string.check_apn), null, showAPNSetting);
                        dialog.show();
                    }

                }
            }
        }
    };

    @Override
    public void onClick(View v) {
        try {
            if (v.equals(txtchangePass)) {
                showPopupChangePass();
            }
            if (v.equals(txtresetpass)) {
                showPopupResetPass();
            }
            if (v.equals(btnLogin)) {
//                Constant.reloadConfig(Constant.TEST_VERSION);
                processBtnLoginClick();
            }

//            if(v.equals(btnLoginPublic)){
//                Constant.reloadConfig(Constant.PUBLIC_INTERNET_VERSION);
//                processBtnLoginClick();
//            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.toString(), e);
            CommonActivity.createAlertDialog(
                    this,
                    getResources().getString(R.string.exception) + " "
                            + e.toString(),
                    getResources().getString(R.string.app_name));
        }
    }

    private void processBtnLoginClick() {
        FragmentLoginNotData.mView = null;
        edtUserName.setText(edtUserName.getText().toString().trim());
        edtPassword.setText(edtPassword.getText().toString().trim());
        if (edtUserName.getText().toString().isEmpty()) {
            String message = getString(R.string.userNameRequired);
            String title = getString(R.string.app_name);
            Dialog dialog = CommonActivity.createAlertDialog(this,
                    message, title);
            dialog.show();
            edtUserName.requestFocus();
            edtUserName.startAnimation(shake);
        } else if (edtPassword.getText().toString().isEmpty()) {
            String message = getString(R.string.passwordRequired);
            String title = getString(R.string.app_name);
            Dialog dialog = CommonActivity.createAlertDialog(this,
                    message, title);
            dialog.show();
            edtPassword.requestFocus();
        } else {
            if (CommonActivity.isNetworkConnected(this)) {
                LoginAsyncTask loginAsyncTask = new LoginAsyncTask(
                        context, onPostLoginListener, preferences);
                loginAsyncTask.execute(edtUserName.getText().toString(),
                        edtPassword.getText().toString(), addInfo, serialSim);
            } else {
                OnClickListener loginClick = new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialogSendSMS = new ProgressDialog(
                                LoginActivity.this);
                        dialogSendSMS.setMessage(getResources()
                                .getString(R.string.processing));
                        dialogSendSMS.setCancelable(false);
                        if (!dialogSendSMS.isShowing()) {
                            dialogSendSMS.show();
                        }
                        String synTask = "0SI";

                        try {
                            CommonActivity.sendSMS(
                                    Constant.EXCHANGE_ADDRESS,
                                    buildRawData(true, edtUserName
                                            .getText().toString()
                                            .trim(), edtPassword
                                            .getText().toString()
                                            .trim(), addInfo),
                                    LoginActivity.this, dialogSendSMS,
                                    synTask);
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        new CountDownTimer(Constant.TIMEOUT_SMS, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                            }

                            @Override
                            public void onFinish() {
                                if (dialogSendSMS.isShowing()) {
                                    dialogSendSMS.dismiss();
                                    Toast.makeText(
                                            LoginActivity.this,
                                            getResources()
                                                    .getString(
                                                            R.string.time_out_sms),
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        }.start();
                    }
                };
                CommonActivity.createDialog(
                        LoginActivity.this,
                        getResources().getString(
                                R.string.no_network_message),
                        getResources().getString(
                                R.string.no_network_title),
                        getResources().getString(R.string.cancel),
                        getResources().getString(R.string.ok),
                        null, loginClick).show();
            }
        }
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {

        @SuppressLint("DefaultLocale")
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String msg = intent.getStringExtra("msg_body");
                Log.d("msg", "msg :" + msg);
                String[] result = msg.split(";", 7);
                if (result.length > 3) {
                    Session.setToken(result[0]);
                    String syntax = result[1];
                    Log.d("syntax", syntax);
                    String errorCode = result[3];
                    String message = getResources().getString(
                            R.string.login_fail);
                    if (syntax.equalsIgnoreCase(Constant.LOGIN_SYNTAX)) {
                        if (errorCode.equals("0")) {
                            // Dang nhap thanh cong,
                            // Kiem tra version
                            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(
                                            getPackageName(), 0);
                            String currentVersion = packageInfo.versionName;
                            String newVersion = result[5];
                            if (!currentVersion.equals(newVersion)
                                    && "1".equalsIgnoreCase(result[6])) {
                                // neu khac phien ban va bat buoc cap nhat,
                                // khong cho
                                // dang nhap
                                CommonActivity
                                        .createAlertDialog(
                                                LoginActivity.this,
                                                getResources()
                                                        .getString(
                                                                R.string.update_version_required),
                                                getResources().getString(
                                                        R.string.app_name),
                                                dismisDialog).show();
                                return;
                            }
                            // Kiem tra database
                            boolean isDatabaseExists = DatabaseUtils
                                    .doesDatabaseExist((Activity) context,
                                            Define.DB_NAME);
                            if (!isDatabaseExists) {
                                CommonActivity
                                        .createAlertDialog(
                                                LoginActivity.this,
                                                getResources()
                                                        .getString(
                                                                R.string.login_database_required),
                                                getResources().getString(
                                                        R.string.app_name),
                                                dismisDialog).show();
                                return;
                            }
                            String lastLogin = preferences.getString(Define.KEY_LOGIN_NAME, "");
                            if (!edtUserName.getText().toString().trim()
                                    .equalsIgnoreCase(lastLogin)) {
                                // deleteDatabase(Define.DB_NAME);
                                CommonActivity
                                        .createAlertDialog(
                                                LoginActivity.this,
                                                getResources()
                                                        .getString(
                                                                R.string.login_database_required),
                                                getResources().getString(
                                                        R.string.app_name),
                                                dismisDialog).show();
                                return;
                            }

                            // insert thong tin role quyen
                            String roles[] = result[4].split(",");
                            String role = ApParamBusiness.getListMenu(
                                    LoginActivity.this, roles);
                            SharedPreferences.Editor editor = preferences
                                    .edit();
                            editor.putString(Define.KEY_MENU_NAME, role);
                            editor.putString(Define.KEY_LOGIN_NAME, edtUserName
                                    .getText().toString().trim().toUpperCase());
                            editor.commit();
                            userName = edtUserName.getText().toString()
                                    .trim().toUpperCase();
                            InfrastrucureDB mInfrastrucureDB = new InfrastrucureDB(
                                    LoginActivity.this);
                            String shop[] = mInfrastrucureDB.getProvince();
                            Session.loginUser = StaffBusiness
                                    .getStaffByStaffCode(LoginActivity.this,
                                            userName);
                            Session.province = shop[0];
                            Session.district = shop[1];
                            Intent i = new Intent(LoginActivity.this,
                                    MainActivity.class);
                            clearOmniStaff();
                            startActivity(i);
                        } else {
                            if (result[2] != null && !result[2].isEmpty()) {
                                message = result[4];
                            }
                            CommonActivity
                                    .createAlertDialog(
                                            LoginActivity.this,
                                            message,
                                            getResources().getString(
                                                    R.string.app_name)).show();
                        }
                    }
                    if (dialogSendSMS != null && dialogSendSMS.isShowing()) {
                        dialogSendSMS.dismiss();
                    }
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, e.toString(), e);
                CommonActivity.createAlertDialog(
                        LoginActivity.this,
                        getResources().getString(R.string.exception) + " "
                                + e.toString(),
                        getResources().getString(R.string.app_name)).show();
                if (dialogSendSMS != null && dialogSendSMS.isShowing()) {
                    dialogSendSMS.dismiss();
                }
            }
        }
    };

    private final OnClickListener dismisDialog = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if (dialogSendSMS != null && dialogSendSMS.isShowing()) {
                dialogSendSMS.dismiss();
            }
        }
    };

    // change password popup
    private void showPopupChangePass() {
        dialogChangePass = new ChangePassDialog(this,
                edtUserName.getText().toString().trim(),
                onPostChangePasslistener);
        dialogChangePass.show();
    }

    // reset password popup
    private void showPopupResetPass() {
        dialogResetpass = new ResetPassDialog(LoginActivity.this,
                edtUserName.getText().toString().trim(),
                onPostResetPasslistener,
                onPostGetSecretKeyListener);
        dialogResetpass.show();
    }

    private final OnPostExecuteListener<String> onPostGetSecretKeyListener = new OnPostExecuteListener<String>() {
        @Override
        public void onPostExecute(String result, String errorCode, String description) {
            if ("0".equalsIgnoreCase(errorCode)) {
                CommonActivity.createAlertDialog(LoginActivity.this,
                        getString(R.string.sinhmathanhcong),
                        getString(R.string.app_name)).show();
            } else if (Constant.ERROR_PING_SERVER.equals(result)) {
                String message = getString(R.string.login_error);
                String title = getString(R.string.app_name);
                Dialog dialog = CommonActivity.createDialog(LoginActivity.this,
                        message, title, getString(R.string.ok),
                        getString(R.string.check_apn), null, showAPNSetting);
                dialog.show();
            } else {
                if (description == null || description.isEmpty()) {
                    description = getString(R.string.sinhmathatbai);
                }
                CommonActivity.createAlertDialog(LoginActivity.this,
                        description, getString(R.string.app_name)).show();
            }
        }
    };

    private final OnPostExecuteListener<String> onPostResetPasslistener = new OnPostExecuteListener<String>() {
        @Override
        public void onPostExecute(String result, String errorCode, String description) {
            if ("0".equalsIgnoreCase(errorCode)) {
                CommonActivity.createAlertDialog(LoginActivity.this,
                        getString(R.string.resetpasssucess),
                        getString(R.string.app_name), onclickresetPassSucess).show();
            } else if (Constant.ERROR_PING_SERVER.equals(result)) {
                String message = getString(R.string.login_error);
                String title = getString(R.string.app_name);
                Dialog dialog = CommonActivity.createDialog(LoginActivity.this,
                        message, title, getString(R.string.ok),
                        getString(R.string.check_apn), null, showAPNSetting);
                dialog.show();
            } else {
                if (description == null || description.isEmpty()) {
                    description = getString(R.string.resetpassfail);
                }
                CommonActivity.createAlertDialog(LoginActivity.this,
                        description, getString(R.string.app_name)).show();
            }
        }
    };

    private final OnPostExecuteListener<String> onPostChangePasslistener = new OnPostExecuteListener<String>() {
        @Override
        public void onPostExecute(String result, String errorCode, String description) {
            if ("0".equalsIgnoreCase(errorCode)) {
                CommonActivity.createAlertDialog(LoginActivity.this,
                        getString(R.string.cp_success),
                        getString(R.string.app_name),
                        onclickChangePassSucess).show();
            } else if (Constant.ERROR_PING_SERVER.equals(result)) {
                String message = getString(R.string.login_error);
                String title = getString(R.string.app_name);
                Dialog dialog = CommonActivity.createDialog(LoginActivity.this,
                        message, title, getString(R.string.ok),
                        getString(R.string.check_apn), null, showAPNSetting);
                dialog.show();
            } else {
                if (description == null || description.isEmpty()) {
                    description = getString(R.string.cp_fail);
                }
                CommonActivity.createAlertDialog(LoginActivity.this,
                        description, getString(R.string.app_name)).show();
            }
        }
    };

    private final OnClickListener onclickresetPassSucess = new OnClickListener() {
        @Override
        public void onClick(View arg0) {

            if (CommonActivity.isNetworkConnected(LoginActivity.this)) {
                if (dialogResetpass != null) {
                    dialogResetpass.cancel();
                }
                edtUserName.setText(dialogResetpass.getUserName());
                edtPassword.setText(dialogResetpass.getPassword());
                LoginAsyncTask loginAsyncTask = new LoginAsyncTask(
                        context, onPostLoginListener, preferences);
                loginAsyncTask.execute(edtUserName.getText().toString(),
                        edtPassword.getText().toString(), addInfo, serialSim);
            } else {
                CommonActivity.createAlertDialog(LoginActivity.this,
                        getString(R.string.errorNetwork),
                        getString(R.string.app_name)).show();
            }
        }
    };

    private final OnClickListener onclickChangePassSucess = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            if (CommonActivity.isNetworkConnected(LoginActivity.this)) {
                if (dialogChangePass != null) {
                    dialogChangePass.cancel();
                }
                edtUserName.setText(dialogChangePass.getUserName());
                edtPassword.setText(dialogChangePass.getPassword());
                LoginAsyncTask loginAsyncTask = new LoginAsyncTask(
                        context, onPostLoginListener, preferences);
                loginAsyncTask.execute(edtUserName.getText().toString(),
                        edtPassword.getText().toString(), addInfo, serialSim);
            } else {
                CommonActivity.createAlertDialog(LoginActivity.this,
                        getString(R.string.errorNetwork),
                        getString(R.string.app_name)).show();
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (String permission : permissions) {
                    if (permission.equals(Manifest.permission.READ_PHONE_STATE)) {
                        if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                            //TODO
                            try {
                                tele = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                                serialSim = tele.getSimSerialNumber();
                            } catch (SecurityException e) {
                                Log.e("REQUEST_CODE_ASK_PERMISSIONS", e.getMessage());
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    private final OnAuthenticationSucceed onSuccess = new OnAuthenticationSucceed() {
        @Override
        public void onAuthenticationSucceed() {
            String pass = "";
            try {
                pass = EncryptKeystore.decrypt(
                        preferences.getString(Constant.KEY_PASS, ""),
                        preferences.getString(Constant.KEY_IV, ""));
                edtPassword.setText(pass);
                String lastLogin = "";
                lastLogin = preferences.getString(Define.KEY_LOGIN_NAME, "");
                edtUserName.setText(lastLogin);
                LoginAsyncTask loginAsyncTask = new LoginAsyncTask(
                        context, onPostLoginListener, preferences);
                loginAsyncTask.execute(edtUserName.getText().toString(),
                        edtPassword.getText().toString(), addInfo, serialSim);
            } catch (Exception e) {
                Log.e("Exception", "ex", e);
            }
        }
    };

    private final OnAuthenticationHelp onHelp = new OnAuthenticationHelp() {
        @Override
        public void onAuthenticationHelp(String msg) {
            showErrorFinger(msg);
        }
    };

    private final OnAuthenticationError onError = new OnAuthenticationError() {
        @Override
        public void onAuthenticationError(String msg) {
            fingerManager.cancelFinger();
            countDownTimer = new CountDownTimer(30000, 1000) {
                @Override
                public void onTick(long l) {
                    tvFinger.setText(getString(R.string.finger_error, l / 1000));
                }
                @Override
                public void onFinish() {
                    tvFinger.setText(getString(R.string.finger_hint));
                    fingerManager.fingerListener(LoginActivity.this, onSuccess, onError,
                            onHelp, onFail);
                    countDownTimer = null;
                }
            };
            countDownTimer.start();
        }
    };
    private final FingerManager.OnAuthenticationFail onFail = new FingerManager.OnAuthenticationFail() {
        @Override
        public void onAuthenticationFail() {
            showErrorFinger(getString(R.string.finger_not_match));
        }
    };

    private void showErrorFinger(CharSequence text) {
        tvFinger.setText(text);
        tvFinger.removeCallbacks(mResetErrorTextRunnable);
        tvFinger.setTextColor(getColor(R.color.red_normal));
        tvFinger.postDelayed(mResetErrorTextRunnable, 2000);
    }

    private final Runnable mResetErrorTextRunnable = new Runnable() {
        @Override
        public void run() {
            tvFinger.setText(
                    getString(R.string.finger_hint));
            tvFinger.setTextColor(getColor(R.color.white));
        }
    };

    private void requestPermission() {
        if (CommonActivity.askPermission()) {
            List<String> lstPermissionNotGrant = new ArrayList<>();
            lstPermissionDenied.clear();

            for (String permission : arrPermission) {
                int havePermission = checkSelfPermission(permission);
                if (havePermission != PackageManager.PERMISSION_GRANTED) {
                    if (!shouldShowRequestPermissionRationale(permission)) {
                        lstPermissionDenied.add(permission);
                        Log.d("requestPermission lstPermissionDenied", permission);
                    } else {
                        lstPermissionNotGrant.add(permission);
                        Log.d("requestPermission lstPermissionNotGrant", permission);
                    }
                }
            }

            if (!lstPermissionDenied.isEmpty()) {
                onClickPermission.onClick(getCurrentFocus());
                return;
            }

            if (!lstPermissionNotGrant.isEmpty()) {
                requestPermissions(convertListToArray(lstPermissionNotGrant),
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
    }

    private final OnClickListener onClickPermission = new OnClickListener() {
        @Override
        public void onClick(View view) {
            requestPermissions(convertListToArray(lstPermissionDenied),
                    REQUEST_CODE_ASK_PERMISSIONS);
        }
    };

    private String[] convertListToArray(List<String> lstData) {
        String[] arrData = new String[lstData.size()];
        for (int i = 0; i < lstData.size(); i++) {
            arrData[i] = lstData.get(i);
        }
        return arrData;
    }

    public void checkDrawOverlayPermission() {
        if (CommonActivity.askPermission()) {   //Android M Or Over
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, PERM_REQUEST_CODE_DRAW_OVERLAYS);
            } else {
                requestPermission();
            }
        }
    }

    @Override
    @TargetApi(Build.VERSION_CODES.M)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PERM_REQUEST_CODE_DRAW_OVERLAYS) {
            if (Settings.canDrawOverlays(this)) {
                requestPermission();
            } else {
                checkDrawOverlayPermission();
            }
        } else if (requestCode == APP_SETTING_RESULT_CODE) {
            if (!checkDonePermission()) {
                showDialogViewSetting();
            }
        }
    }

    private boolean checkDonePermission() {
        if (!CommonActivity.askPermission()) {
            return true;
        }

        for (String permission : arrPermission) {
            int havePermission = checkSelfPermission(permission);
            if (havePermission != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    private final OnClickListener onClickAcceptGuideSetting = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            dialogGuideSetting.dismiss();
            startSettingApp();
        }
    };

    private void startSettingApp() {
        try {
            Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, APP_SETTING_RESULT_CODE);
        } catch (ActivityNotFoundException e) {
            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
            startActivityForResult(intent, APP_SETTING_RESULT_CODE);
        }
    }

    private final OnClickListener cancelViewSettingListener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            startSettingApp();
        }
    };

    private final OnClickListener okViewSettingListener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            dialogGuideSetting.show();
        }
    };

    private void showDialogViewSetting() {
        Dialog dialog = CommonActivity.createDialog(this, "Bạn chưa cấp đủ quyền cho ứng dụng, bạn có muốn xem hướng dẫn cấp quyền không?",
                getString(R.string.app_name),
                getString(R.string.cancel),
                getString(R.string.ok),
                cancelViewSettingListener, okViewSettingListener);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void clearOmniStaff() {
        SharedPreferences sharedPreferences = getSharedPreferences(
                Constant.SHARE_PREFERENCES_FILE, MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();

        edit.putString(Constant.OMNI_STAFF_NAME_SAVE, Constant.OMNI_STAFF_INVALID);
        edit.putString(Constant.SIGNATURE_STAFF_SAVE, "");
        edit.putString(Constant.SIGNATURE_STAFF_EXISTS, "");

        edit.commit();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.login_layout;
    }
}
