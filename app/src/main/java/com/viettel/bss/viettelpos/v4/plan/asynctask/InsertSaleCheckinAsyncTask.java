package com.viettel.bss.viettelpos.v4.plan.asynctask;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.asyntask.AsyncTaskCommonSupper;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.object.Location;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

/**
 * Created by BaVV on 1/05/2018.
 */

public class InsertSaleCheckinAsyncTask extends AsyncTaskCommonSupper<String, Void, String> {

    public InsertSaleCheckinAsyncTask(
            Activity context,
            OnPostExecuteListener<String> listener,
            View.OnClickListener moveLogInAct) {

        super(context, listener, moveLogInAct);
    }

    @Override
    protected String doInBackground(String... params) {
        return insertSaleCheckInHistory(params[0], params[1], params[2], params[3]);
    }

    //    isdn: h004_ftth_thaott6 idNo: 1232342343 token: f3b53334b42b43bdbe5dd74420b09ca3
    private String insertSaleCheckInHistory(
            String salePlansCtvId,
            String streetBlockCode,
            String objectCode,
            String objectType
    ) {

        String original = "";
        ParseOuput out = null;
        String func = "insertSaleCheckInHistory";
        String x = "";
        String y = "";

        try {

            Location location = CommonActivity.findMyLocation(mActivity);
            if(null != location) {
                x = location.getX();
                y = location.getY();
            }

            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_" + func);
            StringBuilder rawData = new StringBuilder();

            rawData.append("<ws:" + func + ">");
            rawData.append("<input>");
            rawData.append("<token>" + Session.getToken() + "</token>");

            if (!CommonActivity.isNullOrEmpty(salePlansCtvId))
                rawData.append("<salePlansCtvId>").append(salePlansCtvId).append("</salePlansCtvId>");

            if (!CommonActivity.isNullOrEmpty(streetBlockCode))
                rawData.append("<streetBlockCode>").append(streetBlockCode).append("</streetBlockCode>");

            if (!CommonActivity.isNullOrEmpty(objectCode))
                rawData.append("<objectCode>").append(objectCode).append("</objectCode>");

            if (!CommonActivity.isNullOrEmpty(objectType))
                rawData.append("<objectType>").append(objectType).append("</objectType>");

            if (!CommonActivity.isNullOrEmpty(x))
                rawData.append("<lat>").append(x).append("</lat>");

            if (!CommonActivity.isNullOrEmpty(y))
                rawData.append("<lng>").append(y).append("</lng>");

            rawData.append("</input>");
            rawData.append("</ws:" + func + ">");

            Log.i("RowData", rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData.toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope, Constant.BCCS_GW_URL,
                    mActivity, "mbccs_" + func);
            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i("Original", response);

            Serializer serializer = new Persister();
            out = serializer.read(ParseOuput.class, original);
        } catch (Exception e) {
            Log.e(Constant.TAG, "blockSubForTerminate", e);
            description = e.getMessage();
            errorCode = Constant.ERROR_CODE;
        }
        if (CommonActivity.isNullOrEmpty(out)) {
            description = mActivity
                    .getString(R.string.no_return_from_system);
            errorCode = Constant.ERROR_CODE;
            return "";
        } else {
            description = out.getDescription();
            errorCode = out.getErrorCode();
        }
        return out.getJsonResult();
    }
}