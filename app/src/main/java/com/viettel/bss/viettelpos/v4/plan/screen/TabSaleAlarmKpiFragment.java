package com.viettel.bss.viettelpos.v4.plan.screen;

import android.widget.TextView;

import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.base.BaseFragment;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.plan.adapter.TabSaleAlarmKpiAdapter;
import com.viettel.bss.viettelpos.v4.plan.adapter.TabSaleWarningAdapter;
import com.viettel.bss.viettelpos.v4.plan.event.PlanEvent;
import com.viettel.bss.viettelpos.v4.plan.model.AlarmKpiCurrent;
import com.viettel.bss.viettelpos.v4.plan.model.AlarmKpiCurrentModel;
import com.viettel.bss.viettelpos.v4.plan.model.SalePlansResultModel;
import com.viettel.bss.viettelpos.v4.plan.model.SaleResultCtv;
import com.viettel.bss.viettelpos.v4.plan.model.WsResponseSaleResultCTV;
import com.viettel.bss.viettelpos.v4.utils.RecyclerUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by BaVV on 1/05/18.
 */
public class TabSaleAlarmKpiFragment extends BaseFragment implements TabSaleAlarmKpiAdapter.SaleWarningClickListener {

    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;

    private TabSaleAlarmKpiAdapter alarmKpiAdapter;

    List<AlarmKpiCurrent> alarmKpiCurrentList = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_tab_sale_alarm_kpi;
    }

    @Override
    protected void initData() {
        RecyclerUtils.setupVerticalRecyclerView(getContext(), recyclerView.getRecyclerView());
        alarmKpiAdapter = new TabSaleAlarmKpiAdapter(getContext(), alarmKpiCurrentList, this);
        recyclerView.setAdapter(alarmKpiAdapter);
    }

    @Override
    protected boolean shouldListenEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PlanEvent event) {
        PlanEvent.Action action = event.getAction();
        switch (action) {
            case RELOAD_SALE_ALARM_KPI:
                AlarmKpiCurrentModel alarmKpiCurrentModel = (AlarmKpiCurrentModel) event.getData();
                if (null != alarmKpiCurrentModel) {
                    loadData(alarmKpiCurrentModel);
                }

                break;
        }
    }

    private void loadData(AlarmKpiCurrentModel alarmKpiCurrentModel) {
        if (null != alarmKpiCurrentModel) {

            if (null != alarmKpiCurrentModel.getResults()) {
                alarmKpiCurrentList.addAll(alarmKpiCurrentModel.getResults());
                alarmKpiAdapter.notifyDataSetChanged();
            }
        }
    }

    private void clearData() {
        if (null == alarmKpiCurrentList) {
            alarmKpiCurrentList = new ArrayList<>();
        } else {
            alarmKpiCurrentList.clear();
        }
        alarmKpiAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(AlarmKpiCurrent alarmKpiCurrent) {

    }
}
