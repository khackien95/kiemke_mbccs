package com.viettel.bss.viettelpos.v4.plan.screen;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity;
import com.viettel.bss.viettelpos.v4.base.BaseFragment;
import com.viettel.bss.viettelpos.v4.charge.object.AreaObj;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.connecttionService.dal.GetAreaDal;
import com.viettel.bss.viettelpos.v4.dal.CacheDatabaseManager;
import com.viettel.bss.viettelpos.v4.dialog.FixedHoloDatePickerDialog;
import com.viettel.bss.viettelpos.v4.dialog.LoginDialog;
import com.viettel.bss.viettelpos.v4.infrastrucure.adapter.AdapterProvinceSpinner;
import com.viettel.bss.viettelpos.v4.plan.adapter.AdapterAddressSpinner;
import com.viettel.bss.viettelpos.v4.plan.asynctask.CreatePlansAsyncTask;
import com.viettel.bss.viettelpos.v4.plan.asynctask.GetAddressAsyncTask;
import com.viettel.bss.viettelpos.v4.plan.asynctask.InsertSaleCheckinAsyncTask;
import com.viettel.bss.viettelpos.v4.plan.event.PlanEvent;
import com.viettel.bss.viettelpos.v4.plan.model.GetAddressModel;
import com.viettel.bss.viettelpos.v4.plan.model.InfrastureModel;
import com.viettel.bss.viettelpos.v4.plan.model.SalePlansModel;
import com.viettel.bss.viettelpos.v4.plan.model.StaffLocationModel;
import com.viettel.bss.viettelpos.v4.synchronizationdata.XmlDomParse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by root on 05/01/2018.
 */

public class CreatePlanFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.edtAddress)
    EditText mEdtAddress;

    @BindView(R.id.precinctSpin)
    Spinner precinctSpin;

    @BindView(R.id.villageSpin)
    Spinner mVillageSpin;

    @BindView(R.id.edtIns)
    EditText edtIns;

    @BindView(R.id.edtStreet)
    EditText edtStreet;

    @BindView(R.id.edtDate)
    TextView mEdtDate;

    @BindView(R.id.btnCreatePlan)
    Button mBtnCreatePlan;

    @BindView(R.id.titleDateTv)
    TextView mTitleDateTv;

    public static boolean allowBack = true;

    private long type; // 1: update, con lai la them moi
    private ArrayList<AreaObj> mListGroup = new ArrayList<>();
    private int year;
    private int month;
    private int day;
    private Date date = null;
    private String dateString = "";
    private AreaObj areaGroup;
    private GetAddressModel getAddressModel;
    String precinct = "";
    String district = "";
    String province = "";
    InfrastureModel infrastureModel;
    String streetBlockCode;
    private int typeView = Constant.typeLapKeHoach;

    private StaffLocationModel mStaffLocationModel;
    ;
    private List<StaffLocationModel> staffLocationModelList = new ArrayList<>();


    public static CreatePlanFragment getInstance(int typeView) {
        CreatePlanFragment createPlanFragment = new CreatePlanFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("typeView", typeView);
        createPlanFragment.setArguments(bundle);
        return createPlanFragment;
    }

    public static CreatePlanFragment getInstance(InfrastureModel infrastureModel, int typeView) {
        CreatePlanFragment createPlanFragment = new CreatePlanFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("infrastureModel", infrastureModel);
        bundle.putInt("typeView", typeView);
        createPlanFragment.setArguments(bundle);
        return createPlanFragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.create_plan_fragment;
    }

    @Override
    protected void initUI() {
        super.initUI();
        if (getArguments() != null) {
            this.typeView = getArguments().getInt("typeView", Constant.typeLapKeHoach);
            this.infrastureModel = getArguments().getParcelable("infrastureModel");
        }
        if (infrastureModel != null && infrastureModel.getObjectName() != null) {
            edtIns.setText(infrastureModel.getObjectName());
        }
        controllTypeView();
    }

    @Override
    protected boolean shouldListenEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PlanEvent event) {
        PlanEvent.Action action = event.getAction();
        switch (action) {
            case CHOOSE_INFRASTRUCTURE:
                Object data = event.getData();
                if (data instanceof InfrastureModel) {
                    this.infrastureModel = (InfrastureModel) data;
                }

                if (infrastureModel != null && infrastureModel.getObjectName() != null) {
                    edtIns.setText(infrastureModel.getObjectName());
                }

                break;
            case SAVE_PLAN:

                break;
            case BACK:
                controllTypeView();
                break;
        }
    }

    @Override
    protected void initData() {
        super.initData();

        initValue();

        edtIns.setOnClickListener(this);

        getAddressFromServer();

        mVillageSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) return;

                AreaObj areaObj = mListGroup.get(position);
                streetBlockCode = precinct + areaObj.getAreaCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        precinctSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (!staffLocationModelList.isEmpty()) {
                    StaffLocationModel staffLocationModel = staffLocationModelList.get(position);
                    precinct = staffLocationModel.getPrecinctCode();
                    district = staffLocationModel.getDistrictCode();
                    if (null != mStaffLocationModel && null != mStaffLocationModel.getPrecinctCode() && mStaffLocationModel.getPrecinctCode().equals(staffLocationModel.getPrecinctCode())) {
                    } else {
                        addListGroupSpinner(precinct);
                        streetBlockCode = null;
                        infrastureModel = null;
                        edtIns.setText("");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    void getAddressFromServer() {
        new GetAddressAsyncTask(getActivity(), new OnPostExecuteListener<GetAddressModel>() {
            @Override
            public void onPostExecute(GetAddressModel result, String errorCode, String description) {
                getAddressModel = result;
                if (null == getAddressModel) return;

                staffLocationModelList = getAddressModel.getStaffLocationModelList();
                setAddress();
            }
        }, moveLogInAct).execute();
    }

    private void setAddress() {
        if (null == staffLocationModelList || staffLocationModelList.isEmpty()) return;
        try {
            GetAreaDal dal = new GetAreaDal(getContext());
            dal.open();
            for (StaffLocationModel staffLocationModel : staffLocationModelList) {
                String precinct = dal.getNamePrecintAreaCode(staffLocationModel.getPrecinctCode());
                staffLocationModel.setPrecinctName(precinct);
            }
            dal.close();

            AdapterAddressSpinner adapterGroup = new AdapterAddressSpinner(staffLocationModelList, getActivity());
            precinctSpin.setAdapter(adapterGroup);
            precinctSpin.setSelection(0);

        } catch (Exception ex) {
            Log.e("initDistrict", ex.toString());
        }
    }

    private void savePlan() {
        if (null == precinct) {
            showDialogMessage("Bạn chưa chọn xã");
            return;
        }

        if (typeView == Constant.typeLapKeHoach) {
            if (null == streetBlockCode) {
                showDialogMessage("Bạn chưa chọn Tổ thôn");
                return;
            }

            if (null == infrastureModel) {
                showDialogMessage("Bạn chưa chọn Hạ tầng");
                return;
            }

            Calendar calToDate = Calendar.getInstance();
            calToDate.setTime(DateTimeUtils.convertStringToTime(dateString, "yyyy-MM-dd"));
            if (DateTimeUtils.daysBetween(DateTimeUtils.truncDate(calToDate.getTime()), new Date()) > 0) {
                CommonActivity.createAlertDialog(getContext(),
                        "Ngày thực hiện không được nhỏ hơn ngày hiện tại",
                        getString(R.string.app_name)).show();
                return;
            }
            createPlan();

//                    CommonActivity.createDialog(MainActivity.getInstance(),
//                            MainActivity.getInstance().getResources().getString(R.string.msg_create_plan),
//                            MainActivity.getInstance().getString(R.string.app_name),
//                            MainActivity.getInstance().getString(R.string.cancel),
//                            MainActivity.getInstance().getString(R.string.ok), null, new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    createPlan();
//                                }
//                            }).show();
        } else {
            if (null == streetBlockCode) {
                showDialogMessage("Bạn chưa chọn Tổ thôn");
                return;
            }

            if (null == infrastureModel) {
                showDialogMessage("Bạn chưa chọn Hạ tầng");
                return;
            }

            CommonActivity.createDialog(MainActivity.getInstance(),
                    MainActivity.getInstance().getResources().getString(R.string.msg_checkin_not_plan),
                    MainActivity.getInstance().getString(R.string.app_name),
                    MainActivity.getInstance().getString(R.string.cancel),
                    MainActivity.getInstance().getString(R.string.ok), null, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            checkinNotPlan();
                        }
                    }).show();
        }
    }

    @OnClick({R.id.edtAddress, R.id.edtDate, R.id.btnCreatePlan})
    public void onClickView(View view) {
        switch (view.getId()) {
            case R.id.edtAddress:
                break;
            case R.id.edtDate:
                DatePickerDialog fromDateDialog = new FixedHoloDatePickerDialog(
                        getActivity(), AlertDialog.THEME_HOLO_LIGHT, fromDatePickerListener, year, month, day);
                fromDateDialog.show();
                break;
            case R.id.btnCreatePlan:

                savePlan();
                break;
        }
    }

    private void createPlan() {

        String districtCode = district;
        String precinctCode = precinct;
        String objectCode = infrastureModel.getObjectCode();
        Long objectType = infrastureModel.getObjectType();
        String planDate = dateString;
        String lat = infrastureModel.getLat();
        String lng = infrastureModel.getLng();

        if(CommonActivity.isNullOrEmpty(lat) || CommonActivity.isNullOrEmpty(lng)) {
            CommonActivity.createAlertDialog(getContext(),
                    "Thông tin hạ tầng đã chọn đang thiếu tọa độ, vui lòng liên hệ với quản trị hệ thống để kiểm tra!",
                    getString(R.string.app_name)).show();
            return;
        }

        SalePlansModel salePlansModel = new SalePlansModel();
        salePlansModel.setDistrictCode(districtCode);
        salePlansModel.setPrecinctCode(precinctCode);
        salePlansModel.setObjectCode(objectCode);
        salePlansModel.setObjectType(null == objectType ? 0 : Integer.parseInt(String.valueOf(objectType)));
        salePlansModel.setPlanDate(planDate);
        salePlansModel.setLat(lat);
        salePlansModel.setLng(lng);
        salePlansModel.setStreetBlockCode(streetBlockCode);

        try {
            StaffLocationModel staffLocationModel = staffLocationModelList.get(precinctSpin.getSelectedItemPosition());
            AreaObj areaObj = mListGroup.get(mVillageSpin.getSelectedItemPosition());
            salePlansModel.setFullAddress(areaObj.getName() + ", " + staffLocationModel.getPrecinctName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        EventBus.getDefault().post(new PlanEvent(PlanEvent.Action.CHOOSE_NEW_PLAN, salePlansModel));
        getActivity().onBackPressed();

//        new CreatePlansAsyncTask(getActivity(), new OnPostExecuteListener<String>() {
//            @Override
//            public void onPostExecute(String result, String errorCode, String description) {
//                if ("0".equals(errorCode)) {
//                    showDialogMessage("Lập kế hoạch thành công");
//                    resetUI();
//
//                    SalePlansModel salePlansModel = new SalePlansModel();
//                    salePlansModel.setFullAddress("");
//                    salePlansModel.setPlanDate(dateString);
//
//                    EventBus.getDefault().post(new PlanEvent(PlanEvent.Action.CHOOSE_NEW_PLAN, salePlansModel));
//                    getActivity().onBackPressed();
//                } else if ("TOKEN_INVALID".equals(errorCode)) {
//                    Dialog dialog = CommonActivity
//                            .createAlertDialog(getActivity(),
//                                    R.string.token_invalid, R.string.app_name,
//                                    moveLogInAct);
//                    dialog.show();
//                } else {
//                    showDialogMessage(description);
//                }
//            }
//        }, moveLogInAct).execute(districtCode, precinctCode, streetBlockCode, objectCode, objectType, planDate, lat, lng);
    }

    private void checkinNotPlan() {

        String objectCode = null;
        String objectType = null;

        if (null != infrastureModel) {
            objectCode = infrastureModel.getObjectCode();
            objectType = infrastureModel.getObjectType() + "";
        }

        new InsertSaleCheckinAsyncTask(getActivity(), new OnPostExecuteListener<String>() {
            @Override
            public void onPostExecute(String result, String errorCode, String description) {
                if ("0".equals(errorCode)) {
                    showDialogMessage("Checkin thành công");
                    resetUI();
                } else if ("TOKEN_INVALID".equals(errorCode)) {
                    Dialog dialog = CommonActivity
                            .createAlertDialog(getActivity(),
                                    R.string.token_invalid, R.string.app_name,
                                    moveLogInAct);
                    dialog.show();
                } else {
                    showDialogMessage(description);
                }
            }
        }, moveLogInAct).execute(null, streetBlockCode, objectCode, objectType);
    }

    private final DatePickerDialog.OnDateSetListener fromDatePickerListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int selectedYear,
                                      int selectedMonth, int selectedDay) {
                    month = selectedMonth;
                    day = selectedDay;
                    StringBuilder strDate = new StringBuilder();
                    if (day < 10) {
                        strDate.append("0");
                    }
                    strDate.append(day).append("/");
                    if (month < 9) {
                        strDate.append("0");
                    }
                    strDate.append(month + 1).append("/");
                    strDate.append(selectedYear);

                    String dayString = day + "";
                    if (day < 10) {
                        dayString = "0" + day;
                    }

                    String monthString = month + "";
                    if (month < 9) {
                        monthString = "0" + (month + 1);
                    }

                    dateString = String.valueOf(selectedYear) + "-" +
                            monthString + "-" + dayString;
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        date = sdf.parse(dateString);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mEdtDate.setText(strDate);
                }
            };

    private void initValue() {
        Calendar cal = Calendar.getInstance();
        year = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        day = cal.get(Calendar.DAY_OF_MONTH);
        mEdtDate.setText(DateTimeUtils.convertDateTimeToString(cal.getTime(),
                "dd/MM/yyyy"));

        String dayString = day + "";
        if (day < 10) {
            dayString = "0" + day;
        }

        String monthString = month + "";
        if (month < 9) {
            monthString = "0" + (month + 1);
        }

        dateString = String.valueOf(year) + "-" +
                monthString + "-" + dayString;

        Log.d("dateString", dateString);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date = sdf.parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetUI() {
        try {
            if (!CommonActivity.isNullOrEmpty(staffLocationModelList)) {
                StaffLocationModel staffLocationModel = staffLocationModelList.get(0);
                precinct = staffLocationModel.getPrecinctCode();
                addListGroupSpinner(precinct);
            }

            streetBlockCode = null;
            infrastureModel = null;
            edtIns.setText("");
            initValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addListGroupSpinner(String parentCode) {
        GetListVillageAdressAsyncTask getlistVillageAsynctask = new GetListVillageAdressAsyncTask(getActivity());
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        String original = sharedPref.getString(parentCode, null);
        Log.d("addListGroupSpinner", "check original save sharePref: " + original);
        if (original != null && original.length() > 0) {
            Log.d("addListGroupSpinner", "check original get  frome sharePref: ");

            ArrayList<AreaObj> result = getlistVillageAsynctask.parserListGroup(original);
            getlistVillageAsynctask.updatePrecintSpinner(result);
        } else {
            Log.d("addListGroupSpinner", "check original get  frome Internet");
            getlistVillageAsynctask.execute(parentCode);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relaBackHome:
                getActivity().onBackPressed();
                break;
            case R.id.edtIns:

                if (null == precinct) {
                    showDialogMessage("Bạn chưa chọn xã");
                    return;
                }

//                String villageCode = "";
//                if (!mListGroup.isEmpty()) {
//                    villageCode = mListGroup.get(mVillageSpin.getSelectedItemPosition()).getAreaCode();
//                }
//                ReplaceFragment.replaceFragment(getActivity(),
//                        ChooseInfrastructureFragment.getInstance(villageCode, typeView), false);

                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                String tag = ChooseInfrastructureFragment.class.getName();
                Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = ChooseInfrastructureFragment.getInstance(precinct/*villageCode*/, typeView);
                    ft.add(R.id.frame_container, fragment, tag);
                    ft.addToBackStack(tag);
                } else {
                    ft.show(fragment);
                }
                Fragment newsfeedFragment =
                        getActivity().getSupportFragmentManager().findFragmentByTag(CreatePlanFragment.class.getName());
                if (null != newsfeedFragment) {
                    ft.hide(newsfeedFragment);
                }

                ft.commit();

            default:
                break;
        }
    }

    private class GetListVillageAdressAsyncTask extends AsyncTask<String, Void, ArrayList<AreaObj>> {

        private Context context = null;
        final XmlDomParse parse = new XmlDomParse();
        String errorCode = "";
        String description = "";

        @SuppressWarnings("unused")
        public GetListVillageAdressAsyncTask(Context context) {
            this.context = context;
//            prbStreetBlock.setVisibility(View.VISIBLE);
//            btnRefreshStreetBlock.setVisibility(View.GONE);
        }

        @Override
        protected ArrayList<AreaObj> doInBackground(String... params) {

            return getListGroupAddress(params[0]);
        }

        @Override
        protected void onPostExecute(ArrayList<AreaObj> result) {
            try {
                super.onPostExecute(result);
                // progress.dismiss();

                if (errorCode.equals("0")) {
                    if (result != null && result.size() > 0) {
//                        btnRefreshStreetBlock.setVisibility(View.VISIBLE);
//                        prbStreetBlock.setVisibility(View.GONE);
                        updatePrecintSpinner(result);
                    } else {
                        updatePrecintSpinner(result);
                        CommonActivity.createAlertDialog(getActivity(), getString(R.string.notStreetBlock),
                                getString(R.string.app_name)).show();
                    }
                } else {
                    if (errorCode.equals(Constant.INVALID_TOKEN2)) {
                        Dialog dialog = CommonActivity.createAlertDialog((Activity) context, description,
                                context.getResources().getString(R.string.app_name), moveLogInAct);
                        dialog.show();
                    } else {
                        if (description == null || description.isEmpty()) {
                            description = context.getString(R.string.checkdes);
                        }
                        Dialog dialog = CommonActivity.createAlertDialog(getActivity(), description,
                                getResources().getString(R.string.app_name));
                        dialog.show();

                    }
                }
            } catch (Exception e) {
                Log.d("exxxxx", e.toString());
            }
        }

        public void updatePrecintSpinner(ArrayList<AreaObj> result) {
            mListGroup = result;
            Collections.sort(mListGroup, AreaObj.ASCENDING_COMPARATOR_NAME);
            AdapterProvinceSpinner adapterGroup = new AdapterProvinceSpinner(mListGroup, getActivity());
            mVillageSpin.setAdapter(adapterGroup);
            mVillageSpin.setSelection(0);

//            if (type == 1) {
//                if (areaGroup != null) {
//                    for (int i = 0; i < mListGroup.size(); i++) {
//                        AreaObj areaGroupObject = mListGroup.get(i);
//                        if (areaGroupObject.getAreaCode().equals(areaGroup.getAreaCode())) {
//                            spGroup.setSelection(i);
//                        }
//                    }
//                }
//                if(!CommonActivity.isNullOrEmpty(strStreetBlock)){
//                    for (int i = 0; i < mListGroup.size(); i++) {
//                        AreaObj areaGroupObject = mListGroup.get(i);
//                        if (areaGroupObject.getAreaCode().equals(strStreetBlock)) {
//                            spGroup.setSelection(i);
//                        }
//                    }
//                }
//
//            } else {
//
//                if (areaGroup != null) {
//                    for (int i = 0; i < mListGroup.size(); i++) {
//                        AreaObj areaGroupObject = mListGroup.get(i);
//                        if (areaGroupObject.getName().equals(areaGroup.getName())) {
//                            spGroup.setSelection(i);
//                        }
//                    }
//                }
//                if(!CommonActivity.isNullOrEmpty(strStreetBlock)){
//                    for (int i = 0; i < mListGroup.size(); i++) {
//                        AreaObj areaGroupObject = mListGroup.get(i);
//                        if (areaGroupObject.getAreaCode().equals(strStreetBlock)) {
//                            spGroup.setSelection(i);
//                        }
//                    }
//                }
//            }
        }

        private ArrayList<AreaObj> getListGroupAddress(String parentCode) {
            ArrayList<AreaObj> listGroupAdress = new ArrayList<>();
            if (parentCode.isEmpty()) {
                AreaObj first = new AreaObj();
                first.setAreaCode("");
                first.setName(getString(R.string.choose_street_block));
                listGroupAdress.add(first);
                return listGroupAdress;
            }
            listGroupAdress = new CacheDatabaseManager(MainActivity.getInstance()).getListCacheStreetBlock(parentCode);
            if (!listGroupAdress.isEmpty()) {
                AreaObj first = new AreaObj();
                first.setAreaCode("");
                first.setName(getString(R.string.choose_street_block));
                listGroupAdress.add(0, first);
                errorCode = "0";
                return listGroupAdress;
            }
            String original = "";
            try {

                BCCSGateway input = new BCCSGateway();
                input.addValidateGateway("username", Constant.BCCSGW_USER);
                input.addValidateGateway("password", Constant.BCCSGW_PASS);
                input.addValidateGateway("wscode", "mbccs_getListArea");
                StringBuilder rawData = new StringBuilder();
                rawData.append("<ws:getListArea>");
                rawData.append("<input>");
                rawData.append("<token>").append(Session.getToken()).append("</token>");
                rawData.append("<parentCode>").append(parentCode).append("</parentCode>");
                rawData.append("</input>");
                rawData.append("</ws:getListArea>");
                Log.i("RowData", rawData.toString());
                String envelope = input.buildInputGatewayWithRawData(rawData.toString());
                Log.d("Send evelop", envelope);
                Log.i("LOG", Constant.BCCS_GW_URL);
                String response = input.sendRequest(envelope, Constant.BCCS_GW_URL, getActivity(),
                        "mbccs_getListArea");
                Log.i("Responseeeeeeeeee", response);
                CommonOutput output = input.parseGWResponse(response);
                original = output.getOriginal();
                Log.i("Responseeeeeeeeee ", response);

                // SharedPreferences sharedPref = CreateAddressCommon.this
                // .getPreferences(Context.MODE_PRIVATE);
                // Editor editor = sharedPref.edit();
                // editor.putString(parentCode, original);
                // editor.commit();

                // ==== parse xml list ip
                listGroupAdress = parserListGroup(original);
                AreaObj first = new AreaObj();
                first.setAreaCode("");
                first.setName(getString(R.string.choose_street_block));
                if (listGroupAdress == null) {
                    listGroupAdress = new ArrayList<>();
                    listGroupAdress.add(first);
                } else if (listGroupAdress.isEmpty()) {
                    listGroupAdress.add(0, first);
                } else {
                    new CacheDatabaseManager(MainActivity.getInstance()).insertCacheStreetBlock(listGroupAdress,
                            parentCode);
                    listGroupAdress.add(0, first);
                }

            } catch (Exception e) {
                Log.d("getListIP", e.toString());
            }
            return listGroupAdress;
        }

        public ArrayList<AreaObj> parserListGroup(String original) {
            ArrayList<AreaObj> listGroupAdress = new ArrayList<>();
            try {
                Document doc = parse.getDomElement(original);
                NodeList nl = doc.getElementsByTagName("return");
                NodeList nodechild = null;
                for (int i = 0; i < nl.getLength(); i++) {
                    Element e2 = (Element) nl.item(i);
                    errorCode = parse.getValue(e2, "errorCode");
                    description = parse.getValue(e2, "description");
                    Log.d("errorCode", errorCode);
                    nodechild = doc.getElementsByTagName("lstArea");
                    for (int j = 0; j < nodechild.getLength(); j++) {
                        Element e1 = (Element) nodechild.item(j);
                        AreaObj areaObject = new AreaObj();
                        areaObject.setName(parse.getValue(e1, "name"));
                        Log.d("name area: ", areaObject.getName());
                        areaObject.setAreaCode(parse.getValue(e1, "streetBlock"));
                        listGroupAdress.add(areaObject);
                    }
                }
            } catch (Exception ignored) {

            }

            return listGroupAdress;
        }
    }

    void controllTypeView() {
        if (typeView == Constant.typeLapKeHoach) {
            MainActivity.getInstance().setTitleActionBar(R.string.title_create_plan);
            mBtnCreatePlan.setText("LẬP KẾ HOẠCH");
            mTitleDateTv.setVisibility(View.VISIBLE);
            mEdtDate.setVisibility(View.VISIBLE);
        } else {
            MainActivity.getInstance().setTitleActionBar(R.string.title_checkin_not_plan);
            mBtnCreatePlan.setText("CHECKIN");
            mTitleDateTv.setVisibility(View.GONE);
            mEdtDate.setVisibility(View.GONE);
        }
        MainActivity.getInstance().disableMenu();
    }

    protected final View.OnClickListener moveLogInAct = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LoginDialog dialog = new LoginDialog(getActivity(), "");
            dialog.show();
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().post(new PlanEvent(PlanEvent.Action.BACK_FROM_CREATE_PLAN));
    }
}
