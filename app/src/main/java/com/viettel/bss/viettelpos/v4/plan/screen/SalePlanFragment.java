package com.viettel.bss.viettelpos.v4.plan.screen;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity;
import com.viettel.bss.viettelpos.v4.base.BaseFragment;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DataUtils;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.dialog.FixedHoloDatePickerDialog;
import com.viettel.bss.viettelpos.v4.dialog.LoginDialog;
import com.viettel.bss.viettelpos.v4.object.Location;
import com.viettel.bss.viettelpos.v4.plan.adapter.SalePlansAdapter;
import com.viettel.bss.viettelpos.v4.plan.asynctask.DeleteSalePlansCTVAsyncTask;
import com.viettel.bss.viettelpos.v4.plan.asynctask.GetSalePlansOfDayAsyncTask;
import com.viettel.bss.viettelpos.v4.plan.asynctask.InsertSaleCheckinAsyncTask;
import com.viettel.bss.viettelpos.v4.plan.event.PlanEvent;
import com.viettel.bss.viettelpos.v4.plan.model.InfrastureModel;
import com.viettel.bss.viettelpos.v4.plan.model.SalePlansModel;
import com.viettel.bss.viettelpos.v4.plan.model.SalePlansOfDayModel;
import com.viettel.bss.viettelpos.v4.utils.FileUtils;
import com.viettel.bss.viettelpos.v4.utils.RecyclerUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by BaVV on 1/05/18.
 */
public class SalePlanFragment extends BaseFragment implements SalePlansAdapter.SalePlansClickListener, OnMoreListener, View.OnClickListener {

    @BindView(R.id.edtFromDate)
    EditText edtFromDate;

    @BindView(R.id.edtToDate)
    EditText edtToDate;

    @BindView(R.id.listSalePlan)
    SuperRecyclerView rcvListSalePlan;

    @BindView(R.id.fabActionMenu)
    FloatingActionMenu fabActionMenu;

    private int fromYear;
    private int fromMonth;
    private int fromDay;
    private int toYear;
    private int toMonth;
    private int toDay;
    private Date dateFrom = null;
    private Date dateTo = null;
    private String dateFromString = "";
    private String dateToString = "";

    private int startRow = 0;
    private int pageLength = 20;
    private boolean mIsLoadingMore = false;

    private SalePlansAdapter salePlansAdapter;

    List<SalePlansModel> salePlansModelList = new ArrayList<>();

    private final DatePickerDialog.OnDateSetListener fromDatePickerListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            fromYear = selectedYear;
            fromMonth = selectedMonth;
            fromDay = selectedDay;
            StringBuilder strDate = new StringBuilder();
            if (fromDay < 10) {
                strDate.append("0");
            }
            strDate.append(fromDay).append("/");
            if (fromMonth < 9) {
                strDate.append("0");
            }
            strDate.append(fromMonth + 1).append("/");
            strDate.append(selectedYear);

            String fromDayString = fromDay + "";
            if (fromDay < 10) {
                fromDayString = "0" + fromDay;
            }

            String fromMonthString = fromMonth + "";
            if (fromMonth < 9) {
                fromMonthString = "0" + (fromMonth + 1);
            }

            dateFromString = String.valueOf(selectedYear) + "-" +
                    fromMonthString + "-" + fromDayString;

            Log.d("dateFromString", dateFromString);
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                dateFrom = sdf.parse(dateFromString);
            } catch (Exception e) {
                e.printStackTrace();
            }
            edtFromDate.setText(strDate);
        }
    };

    private final DatePickerDialog.OnDateSetListener toDatePickerListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            toYear = selectedYear;
            toMonth = selectedMonth;
            toDay = selectedDay;
            StringBuilder strDate = new StringBuilder();
            if (toDay < 10) {
                strDate.append("0");
            }
            strDate.append(toDay).append("/");
            if (toMonth < 9) {
                strDate.append("0");
            }
            strDate.append(toMonth + 1).append("/");
            strDate.append(toYear);

            String toDayString = toDay + "";
            if (toDay < 10) {
                toDayString = "0" + toDay;
            }

            String toMonthString = toMonth + "";
            if (toMonth < 9) {
                toMonthString = "0" + (toMonth + 1);
            }

            dateToString = String.valueOf(toYear) + "-" +
                    toMonthString + "-" + toDayString;
            Log.d("dateToString", dateToString);
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                dateTo = sdf.parse(dateToString);
            } catch (Exception e) {
                e.printStackTrace();
            }
            edtToDate.setText(strDate);
        }
    };

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_sale_plan;
    }

    @Override
    protected void initData() {
        RecyclerUtils.setupVerticalRecyclerView(getContext(), rcvListSalePlan.getRecyclerView());
        rcvListSalePlan.setupMoreListener(this, 1);

        initFloatButton();
        initValue();

        salePlansAdapter = new SalePlansAdapter(getContext(), salePlansModelList, SalePlanFragment.this);
        rcvListSalePlan.setAdapter(salePlansAdapter);

    }

    @Override
    public void onResume() {
        setupTitle();
        super.onResume();
    }

    private void setupTitle() {
        MainActivity.getInstance().setTitleActionBar(R.string.sale_plan);
        MainActivity.getInstance().disableMenu();
    }

    @Override
    protected boolean shouldListenEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PlanEvent event) {
        PlanEvent.Action action = event.getAction();
        switch (action) {
            case BACK_FROM_CREATE_PLAN:
            case BACK_FROM_PLAN_LIST:
                setupTitle();
                break;
        }
    }

    private void initFloatButton() {
        FloatingActionButton planButton = new FloatingActionButton(getContext());
        planButton.setLabelText("Lập kế hoạch");
        planButton.setColorNormal(getResources().getColor(R.color.background));
        planButton.setColorPressed(getResources().getColor(R.color.colorPrimary));
        planButton.setButtonSize(1);
        fabActionMenu.addMenuButton(planButton);
        planButton.setImageResource(R.drawable.ic_lap_ke_hoach_24dp);
        planButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replacePlanListFragment(PlanListFragment.getInstance(Constant.typeLapKeHoach));
//                replaceCreatePlanFragment(CreatePlanFragment.getInstance(Constant.typeLapKeHoach));
//                ReplaceFragment.replaceFragment(getActivity(), CreatePlanFragment.getInstance(Constant.typeLapKeHoach), false);
                fabActionMenu.toggle(false);
            }
        });

        FloatingActionButton checkinButton = new FloatingActionButton(getContext());
        checkinButton.setLabelText("Checkin không theo kế hoạch");
        checkinButton.setColorNormal(getResources().getColor(R.color.background));
        checkinButton.setColorPressed(getResources().getColor(R.color.colorPrimary));
        checkinButton.setButtonSize(1);
        fabActionMenu.addMenuButton(checkinButton);
        checkinButton.setImageResource(R.drawable.ic_checkin_khong_ke_hoach_24dp);
        checkinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceCreatePlanFragment(CreatePlanFragment.getInstance(Constant.typeCheckIn));
//                ReplaceFragment.replaceFragment(getActivity(), CreatePlanFragment.getInstance(Constant.typeCheckIn), false);
                fabActionMenu.toggle(false);
            }
        });

        FloatingActionButton resultButton = new FloatingActionButton(getContext());
        resultButton.setLabelText("Kết quả cảnh báo bán hàng");
        resultButton.setColorNormal(getResources().getColor(R.color.background));
        resultButton.setColorPressed(getResources().getColor(R.color.colorPrimary));
        resultButton.setButtonSize(1);
        fabActionMenu.addMenuButton(resultButton);
        resultButton.setImageResource(R.drawable.ic_ket_qua_canh_bao_24dp);
        resultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);

                DatePickerDialog fromDateDialog = new FixedHoloDatePickerDialog(
                        getActivity(), AlertDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                        int month = selectedMonth;
                        int day = selectedDay;
                        StringBuilder strDate = new StringBuilder();
                        if (day < 10) {
                            strDate.append("0");
                        }
                        strDate.append(day).append("/");
                        if (month < 9) {
                            strDate.append("0");
                        }
                        strDate.append(month + 1).append("/");
                        strDate.append(selectedYear);

                        String dayString = day + "";
                        if (day < 10) {
                            dayString = "0" + day;
                        }

                        String monthString = month + "";
                        if (month < 9) {
                            monthString = "0" + (month + 1);
                        }

                        String dateString = String.valueOf(selectedYear) + "-" +
                                monthString + "-" + dayString;

                        Calendar calToDate = Calendar.getInstance();
                        calToDate.setTime(DateTimeUtils.convertStringToTime(dateString, "yyyy-MM-dd"));
                        if (calToDate.after(Calendar.getInstance())) {
                            CommonActivity.createAlertDialog(getContext(),
                                    "Thời gian không được lớn hơn ngày hiện tại",
                                    getString(R.string.app_name)).show();
                            return;
                        }
                        Intent intent = new Intent(getActivity(), SaleWarningActivity.class);
                        Bundle b = new Bundle();
                        b.putString("planDate", dateString);
                        intent.putExtras(b);
                        startActivity(intent);
                    }
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
                fromDateDialog.show();

                fabActionMenu.toggle(false);
            }
        });
    }

    private void replaceCreatePlanFragment(Fragment createPlanFragment) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        String tag = CreatePlanFragment.class.getName();
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            fragment = createPlanFragment;
            ft.add(R.id.frame_container, fragment, tag);
            ft.addToBackStack(tag);
        } else {
            ft.show(fragment);
        }
        Fragment newsfeedFragment =
                getActivity().getSupportFragmentManager().findFragmentByTag(SalePlanFragment.class.getName());
        if (null != newsfeedFragment) {
            ft.hide(newsfeedFragment);
        }

        ft.commit();
    }

    private void replacePlanListFragment(Fragment planListFragment) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        String tag = PlanListFragment.class.getName();
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            fragment = planListFragment;
            ft.add(R.id.frame_container, fragment, tag);
            ft.addToBackStack(tag);
        } else {
            ft.show(fragment);
        }
        Fragment newsfeedFragment =
                getActivity().getSupportFragmentManager().findFragmentByTag(SalePlanFragment.class.getName());
        if (null != newsfeedFragment) {
            ft.hide(newsfeedFragment);
        }

        ft.commit();
    }

    private void initValue() {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 1);
        fromYear = cal.get(Calendar.YEAR);
        fromMonth = cal.get(Calendar.MONTH);
        fromDay = cal.get(Calendar.DAY_OF_MONTH);
        edtFromDate.setText(DateTimeUtils.convertDateTimeToString(cal.getTime(),
                "dd/MM/yyyy"));

        String fromDayString = fromDay + "";
        if (fromDay < 10) {
            fromDayString = "0" + fromDay;
        }

        String fromMonthString = fromMonth + "";
        if (fromMonth < 9) {
            fromMonthString = "0" + (fromMonth + 1);
        }

        dateFromString = String.valueOf(fromYear) + "-" +
                fromMonthString + "-" + fromDayString;

        Log.d("dateFromString", dateFromString);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dateFrom = sdf.parse(dateFromString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Calendar cal2 = Calendar.getInstance();
        toYear = cal2.get(Calendar.YEAR);
        toMonth = cal2.get(Calendar.MONTH);
        toDay = cal2.get(Calendar.DAY_OF_MONTH);

        String toDayString = toDay + "";
        if (toDay < 10) {
            toDayString = "0" + toDay;
        }

        String toMonthString = toMonth + "";
        if (toMonth < 9) {
            toMonthString = "0" + (toMonth + 1);
        }

        dateToString = String.valueOf(toYear) + "-" +
                toMonthString + "-" + toDayString;

        Log.d("dateToString", dateToString);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dateTo = sdf.parse(dateToString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        edtToDate.setText(DateTimeUtils.convertDateTimeToString(cal2.getTime(),
                "dd/MM/yyyy"));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                R.layout.simple_list_item_single_row, R.id.text1);
        adapter.add(getResources().getString(R.string.pending));
        adapter.add(getResources().getString(R.string.have_handled));
    }

    @OnClick(R.id.edtFromDate)
    void clickFromDate() {
        DatePickerDialog fromDateDialog = new FixedHoloDatePickerDialog(
                getActivity(), AlertDialog.THEME_HOLO_LIGHT, fromDatePickerListener, fromYear, fromMonth,
                fromDay);
        fromDateDialog.show();
    }

    @OnClick(R.id.edtToDate)
    void clickToDate() {
        DatePickerDialog toDateDialog = new FixedHoloDatePickerDialog(getActivity(),
                AlertDialog.THEME_HOLO_LIGHT, toDatePickerListener, toYear, toMonth, toDay);
        toDateDialog.show();
    }

    @OnClick(R.id.btnSearch)
    void search() {
        getData();
    }

    private void fake() {
        //FAKE
        String response = FileUtils.loadContentFromFile(getContext(), "xmls/sale_plan_1.xml");
        Serializer serializer = new Persister();
        SalePlansOfDayModel result = null;
        try {
            result = serializer.read(SalePlansOfDayModel.class, response);

            if (null != result
                    && null != result.getSalePlansModelList()
                    && !result.getSalePlansModelList().isEmpty()) {

                // Check can load more
                int dataSize = result.getSalePlansModelList().size();
                if (dataSize < pageLength) {
                    stopLoadMore();
                }

                // Pass data to view
                if (!mIsLoadingMore) {
                    if (null == salePlansModelList) {
                        salePlansModelList = new ArrayList<>();
                    } else {
                        salePlansModelList.clear();
                    }
                }

                salePlansModelList.addAll(result.getSalePlansModelList());

                salePlansAdapter.notifyDataSetChanged();

                if (mIsLoadingMore || startRow == 0) {
                    startRow++;
                }

                Log.e("@@@@", result.toString());
            } else {
                //todo - no data
                stopLoadMore();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (null != result) {
            Log.e("@@@@", result.toString());
        } else {
            Log.e("@@@@", "nulllllll");
        }
    }

    protected final View.OnClickListener moveLogInAct = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LoginDialog dialog = new LoginDialog(getActivity(), "");
            dialog.show();
        }
    };

    public void getData() {
        startRow = 0;
        mIsLoadingMore = false;
        getSalePlansOfDay(startRow, pageLength);
    }

    public void loadMore() {
        mIsLoadingMore = true;
        getSalePlansOfDay(startRow, pageLength);
    }

    public void stopLoadMore() {
        rcvListSalePlan.hideMoreProgress();
        rcvListSalePlan.removeMoreListener();
    }

    private void getSalePlansOfDay(int mStartRow, final int pageLength) {

//        fake();

        if (!CommonActivity.isNetworkConnected(getActivity())) {
            CommonActivity.createAlertDialog(getActivity(),
                    R.string.errorNetwork, R.string.app_name).show();
            return;
        }

        if (dateFrom != null && dateTo != null) {
            if (dateFrom.before(dateTo) || dateFrom.compareTo(dateTo) == 0) {
            } else {
                CommonActivity.createAlertDialog(getContext(),
                        getResources().getString(
                                R.string.checktimeupdatejob),
                        getString(R.string.app_name)).show();
                return;
            }
        }

        new GetSalePlansOfDayAsyncTask(getActivity(), new OnPostExecuteListener<SalePlansOfDayModel>() {
            @Override
            public void onPostExecute(SalePlansOfDayModel result, String errorCode, String description) {
                if ("0".equals(errorCode)) {
                    if (null != result
                            && null != result.getSalePlansModelList()
                            && !result.getSalePlansModelList().isEmpty()) {

                        // Check can load more
                        int dataSize = result.getSalePlansModelList().size();
                        if (dataSize < pageLength) {
                            stopLoadMore();
                        }

                        // Pass data to view
                        if (!mIsLoadingMore) {
                            if (null == salePlansModelList) {
                                salePlansModelList = new ArrayList<>();
                            } else {
                                salePlansModelList.clear();
                            }
                        }

                        salePlansModelList.addAll(result.getSalePlansModelList());

                        salePlansAdapter.notifyDataSetChanged();

                        if (mIsLoadingMore || startRow == 0) {
                            startRow++;
                        }

                        Log.e("@@@@", result.toString());
                    } else {
                        //todo - no data
                        stopLoadMore();
                        clearData();
                    }
                } else if ("TOKEN_INVALID".equals(errorCode)) {
                    Dialog dialog = CommonActivity
                            .createAlertDialog(getActivity(),
                                    R.string.token_invalid, R.string.app_name,
                                    moveLogInAct);
                    dialog.show();
                } else {
                    //todo - no data
                    stopLoadMore();
                    clearData();
                }
            }
        }, moveLogInAct).execute(dateFromString, dateToString, mStartRow + "", pageLength + "");
    }

    private void clearData() {
        if (null == salePlansModelList) {
            salePlansModelList = new ArrayList<>();
        } else {
            salePlansModelList.clear();
        }
        salePlansAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        loadMore();
    }

    @Override
    public void onClick(final SalePlansModel salePlansModel) {

        CommonActivity.createDialog(MainActivity.getInstance(),
                MainActivity.getInstance().getResources().getString(R.string.msg_checkin_by_plan),
                MainActivity.getInstance().getString(R.string.app_name),
                MainActivity.getInstance().getString(R.string.cancel),
                MainActivity.getInstance().getString(R.string.ok), null, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Calendar calToDate = Calendar.getInstance();
                        String planDate = salePlansModel.getPlanDate();

                        calToDate.setTime(DateTimeUtils.convertStringToTime(DateTimeUtils.convertDateSoapToNewFormat(planDate, "yyyy-MM-dd"), "yyyy-MM-dd"));
                        if(DateTimeUtils.calculateDays2Date(calToDate.getTime(), new Date()) != 0) {
                            CommonActivity.createAlertDialog(getContext(),
                                    "Kế hoạch đã quá hạn hoặc chưa đến ngày thực hiện",
                                    getString(R.string.app_name)).show();
                            return;
                        }

                        String x = null, y = null;
                        Location location = CommonActivity.findMyLocation(getContext());
                        if(null != location) {
                            x = location.getX();
                            y = location.getY();
                        }

                        if(!DataUtils.distanceValid(x, y, salePlansModel.getLat(), salePlansModel.getLng())) {
                            CommonActivity.createAlertDialog(getContext(),
                                    "Vị trí checkin so với vị trí của hạ tầng chọn checkin > 200m",
                                    getString(R.string.app_name)).show();
                            return;
                        }

                        new InsertSaleCheckinAsyncTask(getActivity(), new OnPostExecuteListener<String>() {
                            @Override
                            public void onPostExecute(String result, String errorCode, String description) {
                                if ("0".equals(errorCode)) {
                                    showDialogMessage("Checkin thành công");
                                    salePlansModelList.remove(salePlansModel);
                                    salePlansAdapter.notifyDataSetChanged();
                                } else if ("TOKEN_INVALID".equals(errorCode)) {
                                    Dialog dialog = CommonActivity
                                            .createAlertDialog(getActivity(),
                                                    R.string.token_invalid, R.string.app_name,
                                                    moveLogInAct);
                                    dialog.show();
                                } else {
                                    showDialogMessage(description);
                                }
                            }
                        }, moveLogInAct).execute(salePlansModel.getSalePlansCtvId() + "", null, null, null);
                    }
                }).show();
    }

    @Override
    public void editPlan(SalePlansModel salePlansModel) {

    }

    @Override
    public void deletePlan(final SalePlansModel salePlansModel) {
        CommonActivity.createDialog(MainActivity.getInstance(),
                MainActivity.getInstance().getResources().getString(R.string.msg_delete_plan),
                MainActivity.getInstance().getString(R.string.app_name),
                MainActivity.getInstance().getString(R.string.cancel),
                MainActivity.getInstance().getString(R.string.ok), null, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Calendar calToDate = Calendar.getInstance();

                        String planDate = salePlansModel.getPlanDate();

                        calToDate.setTime(DateTimeUtils.convertStringToTime(DateTimeUtils.convertDateSoapToNewFormat(planDate, "yyyy-MM-dd"), "yyyy-MM-dd"));
                        if(DateTimeUtils.daysBetween(DateTimeUtils.truncDate(calToDate.getTime()), new Date()) > 0) {
                            CommonActivity.createAlertDialog(getContext(),
                                    "Bạn không thể xóa các KH đã quá hạn thực hiện",
                                    getString(R.string.app_name)).show();
                            return;
                        }

                        new DeleteSalePlansCTVAsyncTask(getActivity(), new OnPostExecuteListener<String>() {
                            @Override
                            public void onPostExecute(String result, String errorCode, String description) {
                                if ("0".equals(errorCode)) {
                                    showDialogMessage("Xóa kế hoạch thành công");
                                    salePlansModelList.remove(salePlansModel);
                                    salePlansAdapter.notifyDataSetChanged();
                                } else if ("TOKEN_INVALID".equals(errorCode)) {
                                    Dialog dialog = CommonActivity
                                            .createAlertDialog(getActivity(),
                                                    R.string.token_invalid, R.string.app_name,
                                                    moveLogInAct);
                                    dialog.show();
                                } else {
                                    showDialogMessage(description);
                                }
                            }
                        }, moveLogInAct).execute(salePlansModel.getSalePlansCtvId() + "");
                    }
                }).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relaBackHome:
                getActivity().onBackPressed();
                break;
            default:
                break;
        }
    }
}
