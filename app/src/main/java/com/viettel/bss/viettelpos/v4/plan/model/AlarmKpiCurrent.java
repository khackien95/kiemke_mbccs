package com.viettel.bss.viettelpos.v4.plan.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by root on 05/01/2018.
 */
@Root(name = "return", strict = false)
public class AlarmKpiCurrent implements Parcelable {

    @Element(name = "kpiCode", required = false)
    private String kpiCode;// mã chỉ tiêu
    @Element(name = "kpiTargetDay", required = false)
    private Double kpiTargetDay;// Kết quả thực hiện lũy kế
    @Element(name = "perProgressDay", required = false)
    private Double perProgressDay;// % hoàn thành lũy kế
    @Element(name = "staffProcess", required = false)
    private String staffProcess;// người xử lý
    @Element(name = "valueDay", required = false)
    private Double valueDay;

    public String getKpiCode() {
        return kpiCode;
    }

    public void setKpiCode(String kpiCode) {
        this.kpiCode = kpiCode;
    }

    public Double getKpiTargetDay() {
        return kpiTargetDay;
    }

    public void setKpiTargetDay(Double kpiTargetDay) {
        this.kpiTargetDay = kpiTargetDay;
    }

    public Double getPerProgressDay() {
        return perProgressDay;
    }

    public void setPerProgressDay(Double perProgressDay) {
        this.perProgressDay = perProgressDay;
    }

    public String getStaffProcess() {
        return staffProcess;
    }

    public void setStaffProcess(String staffProcess) {
        this.staffProcess = staffProcess;
    }

    public Double getValueDay() {
        return valueDay;
    }

    public void setValueDay(Double valueDay) {
        this.valueDay = valueDay;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
