package com.viettel.bss.viettelpos.v4.plan.screen;

import com.viettel.bss.viettelpos.v4.plan.model.InfrastureModel;

/**
 * Created by root on 06/01/2018.
 */

public class ChooseInsEvent {

    InfrastureModel infrastureModel;
    public ChooseInsEvent(InfrastureModel infrastureModel) {
        this.infrastureModel = infrastureModel;
    }

    public InfrastureModel getInfrastureModel() {
        return infrastureModel;
    }

    public void setInfrastureModel(InfrastureModel infrastureModel) {
        this.infrastureModel = infrastureModel;
    }
}
