package com.viettel.bss.viettelpos.v4.omichanel.dialog;

import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DateTimeDialogWrapper;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.customer.object.Spin;
import com.viettel.bss.viettelpos.v4.dialog.FixedHoloDatePickerDialog;
import com.viettel.bss.viettelpos.v4.omichanel.adapter.ProductVasPlusAdapter;
import com.viettel.bss.viettelpos.v4.omichanel.adapter.TaskResultOptionAdapter;
import com.viettel.bss.viettelpos.v4.omichanel.asynctask.CompleteHPCAsyncTask;
import com.viettel.bss.viettelpos.v4.omichanel.dao.TaskResultOption;
import com.viettel.bss.viettelpos.v4.omichanel.fragment.DetailOrderOmniFragment;
import com.viettel.bss.viettelpos.v4.omichanel.message.RemoveConnectionOrderEvent;
import com.viettel.bss.viettelpos.v4.ui.DateTime;
import com.viettel.bss.viettelpos.v4.ui.DateTimePicker;
import com.viettel.bss.viettelpos.v4.ui.SimpleDateTimePicker;
import com.viettel.bss.viettelpos.v4.ui.image.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by toancx on 3/5/2018.
 */

public class UpdateJobDialog extends DialogFragment {

    @BindView(R.id.edtDate)
    EditText edtDate;
    @BindView(R.id.lnDate)
    LinearLayout lnDate;
    @BindView(R.id.lnTime)
    LinearLayout lnTime;
    @BindView(R.id.actvTime)
    AutoCompleteTextView actvTime;
    @BindView(R.id.recListOptionSelect)
    RecyclerView recListOptionSelect;
    @BindView(R.id.edtDescription)
    TextInputEditText edtDescription;

    private View mView;
    private String processId;
    private List<TaskResultOption> taskResultOptions;
    private TaskResultOptionAdapter taskResultOptionAdapter;
    private TaskResultOption taskResultOptionSelected;
    private DetailOrderOmniFragment detailOrderOmniFragment;

    public UpdateJobDialog(DetailOrderOmniFragment detailOrderOmniFragment,
                           String processId, List<TaskResultOption> taskResultOptions) {

        this.detailOrderOmniFragment = detailOrderOmniFragment;
        this.processId = processId;
        this.taskResultOptions = taskResultOptions;

        for (TaskResultOption taskResultOption : taskResultOptions) {
            taskResultOption.setSelected(false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        this.setCancelable(false);
        this.getDialog().setCanceledOnTouchOutside(false);

        if (mView == null) {
            mView = inflater.inflate(R.layout.omni_layout_update_job, container, false);
            ButterKnife.bind(this, mView);
            initUI();
        }
        return mView;
    }

    private void initUI() {
        new DateTimeDialogWrapper(lnDate, edtDate, getActivity());
        lnDate.setVisibility(View.GONE);
        lnTime.setVisibility(View.GONE);

        this.recListOptionSelect.setHasFixedSize(true);
        this.recListOptionSelect.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.recListOptionSelect.setNestedScrollingEnabled(false);

        taskResultOptionAdapter = new TaskResultOptionAdapter(getActivity(),
                this, taskResultOptions);
        recListOptionSelect.setAdapter(taskResultOptionAdapter);
    }

    @OnClick(R.id.closeButton)
    public void lnCloseOnClick() {
        this.dismiss();
    }

    @OnClick({R.id.lnTime, R.id.actvTime})
    public void showDropDownTime() {
        actvTime.showDropDown();
    }

    @OnTextChanged(value = R.id.edtDate, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void edtDateOnTextChanged() {
        initDataTime(edtDate);
    }

    @OnClick(R.id.btnUpdate)
    public void btnUpdateOnClick() {
        if (validateUpdateCompleteHPC()) {
            CommonActivity.createDialog(getActivity(),
                    getActivity().getResources().getString(R.string.omni_update_work_process),
                    getActivity().getResources().getString(R.string.app_name),
                    getActivity().getResources().getString(R.string.cancel),
                    getActivity().getResources().getString(R.string.buttonOk),
                    null, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            completeHPC();
                        }
                    })
                    .show();
        }
    }

    private void completeHPC() {
        new CompleteHPCAsyncTask(getActivity(), new OnPostExecuteListener<String>() {
            @Override
            public void onPostExecute(String result, String errorCode, String description) {
                if ("0".equals(errorCode)) {
                    CommonActivity.createAlertDialog(getActivity(),
                            getActivity().getString(R.string.updatesucess),
                            getActivity().getString(R.string.app_name)).show();

                    if (Constant.ColHPCResult.NOT_NEED.equals(taskResultOptionSelected.getResultCode())) {
                        detailOrderOmniFragment.disableActionButton();
                        EventBus.getDefault().postSticky(new RemoveConnectionOrderEvent(processId));
                    }
                    dismiss();
                } else {
                    if (CommonActivity.isNullOrEmpty(description)) {
                        description = getActivity().getString(R.string.no_data_return);
                    }
                    CommonActivity.createAlertDialog(getActivity(),
                            description,
                            getActivity().getString(R.string.app_name)).show();
                }
            }
        }, null).execute(processId, new Gson().toJson(initData()));
    }

    private boolean validateUpdateCompleteHPC() {
        if (CommonActivity.isNullOrEmpty(taskResultOptionSelected)) {
            CommonActivity.createAlertDialog(getActivity(),
                    getActivity().getString(R.string.omni_update_job_select_reason),
                    getActivity().getString(R.string.app_name)).show();
            return false;
        }
        else if (Constant.ColHPCResult.CREATE_PLAN.equals(taskResultOptionSelected.getResultCode())) {
            Date date = DateTimeUtils.convertStringToTime(edtDate.getText().toString(), "dd/MM/yyyy");
            if (date.before(DateTimeUtils.truncDate(new Date()))) {
                CommonActivity.createAlertDialog(getActivity(),
                        getActivity().getString(R.string.omni_update_job_date_receive_invalid),
                        getActivity().getString(R.string.app_name)).show();
                return false;
            }
        }
        return true;
    }

    private Data initData() {
        Data data = new Data();
        data.setColHPCResult(taskResultOptionSelected.getResultCode());
        data.setCollDescription(edtDescription.getText().toString());
        if (Constant.ColHPCResult.CREATE_PLAN.equals(taskResultOptionSelected.getResultCode())) {
            String timeFrom = actvTime.getText().toString().split("-")[0].trim() + ":00";
            String timeTo = actvTime.getText().toString().split("-")[1].trim() + ":00";
            data.setPlanFrom(edtDate.getText().toString() + " " + timeFrom);
            if ("23:30 - 00:00".equals(actvTime.getText().toString())) {
                Date datePlus = DateTimeUtils.addDays(DateTimeUtils
                        .convertStringToTime(edtDate.getText().toString(), "dd/MM/yyyy"), 1);
                data.setPlanTo(DateTimeUtils.convertDateTimeToString(
                        datePlus, "dd/MM/yyyy") + " " + timeTo);
            } else {
                data.setPlanTo(edtDate.getText().toString() + " " + timeTo);
            }
        }
        return data;
    }

    private void initDataTime(EditText edtDate) {
        Date date = DateTimeUtils.convertStringToTime(edtDate.getText().toString(), "dd/MM/yyyy");
        List<String> lstTime = new ArrayList<>();
        if (DateTimeUtils.calculateDays2Date(date, new Date()) == 0) {
            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            if (hour < 23 || minute <= 30) {
                if (minute < 30) {
                    if (hour < 23) {
                        lstTime.add(hour + ":30" + " - " + (hour + 1) + ":00");
                    } else {
                        lstTime.add(hour + ":30" + " - " + "00:00");
                    }
                }
                for (hour = hour + 1; hour <= 23; hour++) {
                    lstTime.add(hour + ":00" + " - " + hour + ":30");
                    if (hour < 23) {
                        lstTime.add(hour + ":30" + " - " + (hour + 1) + ":00");
                    } else {
                        lstTime.add(hour + ":30" + " - " + "00:00");
                    }
                }
            }
        } else {
            for (int hour = 0; hour <= 23; hour++) {
                lstTime.add(hour + ":00" + " - " + hour + ":30");
                if (hour < 23) {
                    lstTime.add(hour + ":30" + " - " + (hour + 1) + ":00");
                } else {
                    lstTime.add(hour + ":30" + " - " + "00:00");
                }
            }
        }

        if (lstTime.isEmpty()) {
            actvTime.setText("");
        } else {
            actvTime.setText(lstTime.get(0));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, lstTime);
        actvTime.setAdapter(adapter);
    }

    public class Data {
        @SerializedName("MBCCS_CollHPCResult")
        private String colHPCResult;
        @SerializedName("MBCCS_PlanFrom")
        private String planFrom;
        @SerializedName("MBCCS_PlanTo")
        private String planTo;
        @SerializedName("MBCCS_CollHPCResultDescription")
        private String collDescription;

        public String getCollDescription() {
            return collDescription;
        }

        public void setCollDescription(String collDescription) {
            this.collDescription = collDescription;
        }

        public String getColHPCResult() {
            return colHPCResult;
        }

        public void setColHPCResult(String colHPCResult) {
            this.colHPCResult = colHPCResult;
        }

        public String getPlanFrom() {
            return planFrom;
        }

        public void setPlanFrom(String planFrom) {
            this.planFrom = planFrom;
        }

        public String getPlanTo() {
            return planTo;
        }

        public void setPlanTo(String planTo) {
            this.planTo = planTo;
        }
    }

    public void processSelectItem(int position) {
        for (int index = 0; index < taskResultOptions.size(); index++) {
            if (position == index) {
                taskResultOptions.get(index).setSelected(true);
                taskResultOptionSelected = taskResultOptions.get(index);
            } else {
                taskResultOptions.get(index).setSelected(false);
            }
        }

        taskResultOptionAdapter.notifyDataSetChanged();

        if (Constant.ColHPCResult.CREATE_PLAN.equals(taskResultOptionSelected.getResultCode())) {
            lnDate.setVisibility(View.VISIBLE);
            lnTime.setVisibility(View.VISIBLE);
        } else {
            lnDate.setVisibility(View.GONE);
            lnTime.setVisibility(View.GONE);
        }
    }
}
