package com.viettel.bss.viettelpos.v4.plan.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by root on 05/01/2018.
 */

@Root(name = "lstDStaffLocations", strict = false)
public class StaffLocationModel {

    @Element(name = "provinceCode", required = false)
    private String provinceCode;

    @Element(name = "districtCode", required = false)
    private String districtCode;

    @Element(name = "precinctCode", required = false)
    private String precinctCode;

    @Element(name = "staffCode", required = false)
    private String staffCode;

    private String precinctName;

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getPrecinctCode() {
        return precinctCode;
    }

    public void setPrecinctCode(String precinctCode) {
        this.precinctCode = precinctCode;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getPrecinctName() {
        return precinctName;
    }

    public void setPrecinctName(String precinctName) {
        this.precinctName = precinctName;
    }
}
