package com.viettel.bss.viettelpos.v4.plan.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by bavv on 1/9/18.
 */

@Root(name = "wsResponseSaleCheckinCTV", strict = false)
public class WsResponseSaleCheckinCTV {

    @Element(name = "checkinNum", required = false)
    protected Integer checkinNum;

    @ElementList(name = "listSalePoint", entry = "listSalePoint", required = false, inline = true)
    protected List<SalePointInfo> listSalePoint;

    @Element(name = "planNum", required = false)
    protected Integer planNum;

    @Element(name = "planToDate", required = false)
    protected String planToDate;

    @Element(name = "remainNum", required = false)
    protected Integer remainNum;

    @Element(name = "staffCode", required = false)
    protected String staffCode;

    public Integer getCheckinNum() {
        return checkinNum;
    }

    public void setCheckinNum(Integer checkinNum) {
        this.checkinNum = checkinNum;
    }

    public List<SalePointInfo> getListSalePoint() {
        return listSalePoint;
    }

    public void setListSalePoint(List<SalePointInfo> listSalePoint) {
        this.listSalePoint = listSalePoint;
    }

    public Integer getPlanNum() {
        return planNum;
    }

    public void setPlanNum(Integer planNum) {
        this.planNum = planNum;
    }

    public String getPlanToDate() {
        return planToDate;
    }

    public void setPlanToDate(String planToDate) {
        this.planToDate = planToDate;
    }

    public Integer getRemainNum() {
        return remainNum;
    }

    public void setRemainNum(Integer remainNum) {
        this.remainNum = remainNum;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }
}
