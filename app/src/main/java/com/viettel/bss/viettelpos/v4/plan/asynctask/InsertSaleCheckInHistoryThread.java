package com.viettel.bss.viettelpos.v4.plan.asynctask;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.object.Location;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

/**
 * Created by hungtv64 on 3/20/2018.
 */

public class InsertSaleCheckInHistoryThread extends Thread {

    private Activity activity;

    public InsertSaleCheckInHistoryThread(Activity activity) {
        super("InsertSaleCheckInHistoryThread");
        this.activity = activity;
    }

    @Override
    public void run() {
        super.run();

        if ("0".equals(insertSaleCheckInHistory())) {
            Toast.makeText(activity, "MBCCS Check-In success!", Toast.LENGTH_SHORT);
        }
    }

    private String insertSaleCheckInHistory() {

        String original = "";
        ParseOuput out = null;
        String func = "insertSaleCheckInHistory";
        String x = "";
        String y = "";

        try {
            Location location = CommonActivity.findMyLocation(activity);
            if(null != location) {
                x = location.getX();
                y = location.getY();
            }

            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_" + func);
            StringBuilder rawData = new StringBuilder();

            rawData.append("<ws:" + func + ">");
            rawData.append("<input>");
            rawData.append("<token>" + Session.getToken() + "</token>");

            if (!CommonActivity.isNullOrEmpty(x))
                rawData.append("<lat>").append(x).append("</lat>");
            if (!CommonActivity.isNullOrEmpty(y))
                rawData.append("<lng>").append(y).append("</lng>");

            rawData.append("</input>");
            rawData.append("</ws:" + func + ">");

            Log.i("RowData", rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData.toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope, Constant.BCCS_GW_URL,
                    activity, "mbccs_" + func);

            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i(" Original", response);
            // parser
            Serializer serializer = new Persister();
            out = serializer.read(ParseOuput.class, original);
        } catch (Exception e) {
            Log.e(Constant.TAG, "insertSaleCheckInHistory", e);
        }
        if (CommonActivity.isNullOrEmpty(out)) {
            return "1";
        }
        return out.getErrorCode();
    }
}