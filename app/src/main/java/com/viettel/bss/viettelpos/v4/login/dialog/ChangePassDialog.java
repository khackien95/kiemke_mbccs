package com.viettel.bss.viettelpos.v4.login.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.login.asynctask.ChangePassAsyncTask;

/**
 * Created by hungtv64 on 1/25/2018.
 */

public class ChangePassDialog extends Dialog {

    private Activity activity;
    private String userName;

    private EditText editUserName;
    private EditText edtOldPassword;
    private EditText edtNewPassword;
    private EditText edtConfirmPassword;

    private Button btnchangepass;
    private Button btncanel;
    private final OnPostExecuteListener<String> onPostChangePasslistener;

    public ChangePassDialog(
            Activity activity,
            String userName,
            OnPostExecuteListener<String> onPostChangePasslistener) {

        super(activity);
        this.activity = activity;
        this.userName = userName;
        this.onPostChangePasslistener = onPostChangePasslistener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_change_pass);

        editUserName = (EditText) findViewById(R.id.edtUserName);
        edtOldPassword = (EditText) findViewById(R.id.edtOldPassword);
        edtNewPassword = (EditText) findViewById(R.id.edtNewPassword);
        edtConfirmPassword = (EditText) findViewById(R.id.edtConfirmPassword);
        btnchangepass = (Button) findViewById(R.id.btnchangepass);
        btncanel = (Button) findViewById(R.id.btncanel);

        if (!CommonActivity.isNullOrEmpty(userName)) {
            editUserName.setText(userName);
        }

        editUserName.setText(editUserName.getText().toString().trim());
        edtOldPassword.setText(edtOldPassword.getText().toString().trim());
        edtNewPassword.setText(edtNewPassword.getText().toString().trim());
        edtConfirmPassword.setText(edtConfirmPassword.getText().toString().trim());

        btnchangepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (!editUserName.getText().toString().isEmpty()) {
                    if (!edtOldPassword.getText().toString().isEmpty()) {
                        if (!edtNewPassword.getText().toString().isEmpty()) {
                            if (!edtConfirmPassword.getText().toString().isEmpty()) {
                                // check newpass # oldpass
                                if (!edtOldPassword.getText().toString().equalsIgnoreCase(edtNewPassword.getText().toString())) {
                                    // check new password same repeat
                                    // pass
                                    if (edtNewPassword.getText().toString().equalsIgnoreCase(edtConfirmPassword.getText().toString())) {
                                        if (CommonActivity.isNetworkConnected(activity)) {
                                            CommonActivity.createDialog(activity,
                                                    activity.getString(R.string.confirmchangepassaction),
                                                    activity.getString(R.string.app_name),
                                                    activity.getString(R.string.cancel),
                                                    activity.getString(R.string.ok),
                                                    null, onclickConfirmChangePass).show();
                                        } else {
                                            CommonActivity.createAlertDialog(activity,
                                                    activity.getString(R.string.errorNetwork),
                                                    activity.getString(R.string.app_name)).show();
                                        }

                                    } else {
                                        String message = activity.getString(R.string.password_not_same_repass);
                                        String title = activity.getString(R.string.app_name);
                                        Dialog dialog = CommonActivity.createAlertDialog(
                                                activity, message, title);
                                        dialog.show();
                                    }

                                } else {
                                    String message = activity.getString(R.string.password_not_same);
                                    String title = activity.getString(R.string.app_name);
                                    Dialog dialog = CommonActivity.createAlertDialog(activity, message, title);
                                    dialog.show();
                                }
                            } else {
                                String message = activity.getString(R.string.passwordConfirmRequired);
                                String title = activity.getString(R.string.app_name);
                                Dialog dialog = CommonActivity.createAlertDialog(
                                        activity, message, title);
                                dialog.show();
                                edtConfirmPassword.requestFocus();
                            }
                        } else {
                            String message = activity.getString(R.string.passwordNewRequired);
                            String title = activity.getString(R.string.app_name);
                            Dialog dialog = CommonActivity.createAlertDialog(activity, message, title);
                            dialog.show();
                            edtNewPassword.requestFocus();
                        }
                    } else {
                        String message = activity.getString(R.string.passwordOldRequired);
                        String title = activity.getString(R.string.app_name);
                        Dialog dialog = CommonActivity.createAlertDialog(activity, message, title);
                        dialog.show();
                        edtOldPassword.requestFocus();
                    }
                } else {
                    String message = activity.getString(R.string.userNameRequired);
                    String title = activity.getString(R.string.app_name);
                    Dialog dialog = CommonActivity.createAlertDialog(
                            activity, message, title);
                    dialog.show();
                    editUserName.requestFocus();
                }
            }
        });

        btncanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                cancel();
            }
        });
    }

    // ONCLICK CONFIRM CHANGE PASS
    private final View.OnClickListener onclickConfirmChangePass = new View.OnClickListener() {
        @Override
        public void onClick(View arg0) {
            ChangePassAsyncTask changePassAsyncTask = new ChangePassAsyncTask(
                    activity, onPostChangePasslistener);

            changePassAsyncTask.execute(editUserName.getText().toString(),
                    edtOldPassword.getText().toString(),
                    edtNewPassword.getText().toString(),
                    edtConfirmPassword.getText().toString());
        }
    };

    public String getUserName() {
        return editUserName.getText().toString().trim();
    }

    public String getPassword() {
        return edtNewPassword.getText().toString().trim();
    }
}
