package com.viettel.bss.viettelpos.v4.plan.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "return", strict = false)
public class SalePlansModel {

    @Element(name = "success", required = false)
    private boolean success;

    @Element(name = "fullAddress", required = false)
    private String fullAddress;

    @Element(name = "objectCode", required = false)
    private String objectCode;

    @Element(name = "objectName", required = false)
    private String objectName;

    @Element(name = "objectType", required = false)
    private int objectType;

    @Element(name = "precinctCode", required = false)
    private String precinctCode;

    @Element(name = "districtCode", required = false)
    private String districtCode;

    @Element(name = "precinctName", required = false)
    private String precinctName;

    @Element(name = "salePlansCtvId", required = false)
    private int salePlansCtvId;

    @Element(name = "staffCode", required = false)
    private String staffCode;

    @Element(name = "status", required = false)
    private int status;

    @Element(name = "planDate", required = false)
    private String planDate;

    @Element(name = "streetBlockCode", required = false)
    private String streetBlockCode;

    @Element(name = "streetBlockName", required = false)
    private String streetBlockName;

    @Element(name = "lat", required = false)
    private String lat;

    @Element(name = "lng", required = false)
    private String lng;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getObjectCode() {
        return objectCode;
    }

    public void setObjectCode(String objectCode) {
        this.objectCode = objectCode;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public int getObjectType() {
        return objectType;
    }

    public void setObjectType(int objectType) {
        this.objectType = objectType;
    }

    public String getPrecinctCode() {
        return precinctCode;
    }

    public void setPrecinctCode(String precinctCode) {
        this.precinctCode = precinctCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getPrecinctName() {
        return precinctName;
    }

    public void setPrecinctName(String precinctName) {
        this.precinctName = precinctName;
    }

    public int getSalePlansCtvId() {
        return salePlansCtvId;
    }

    public void setSalePlansCtvId(int salePlansCtvId) {
        this.salePlansCtvId = salePlansCtvId;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPlanDate() {
        return planDate;
    }

    public void setPlanDate(String planDate) {
        this.planDate = planDate;
    }

    public String getStreetBlockCode() {
        return streetBlockCode;
    }

    public void setStreetBlockCode(String streetBlockCode) {
        this.streetBlockCode = streetBlockCode;
    }

    public String getStreetBlockName() {
        return streetBlockName;
    }

    public void setStreetBlockName(String streetBlockName) {
        this.streetBlockName = streetBlockName;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
