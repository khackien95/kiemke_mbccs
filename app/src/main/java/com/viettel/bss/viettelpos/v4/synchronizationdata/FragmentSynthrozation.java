package com.viettel.bss.viettelpos.v4.synchronizationdata;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.Define;
import com.viettel.bss.viettelpos.v4.commons.FingerManager;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.ReplaceFragment;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.infrastrucure.dal.InfrastrucureDB;
import com.viettel.bss.viettelpos.v4.login.asynctask.SyncAsyncTask;
import com.viettel.bss.viettelpos.v4.login.asynctask.UpdateSendOTPAsyncTask;
import com.viettel.bss.viettelpos.v4.sale.business.StaffBusiness;

public class FragmentSynthrozation extends Fragment implements OnClickListener {

	private Context context;
	protected Bundle mBundle;
	public String TAG = "FragmentSynthrozation";

	private LinearLayout linSynData;
	private LinearLayout linUpdateVersion;
	private LinearLayout linUpgradeFix;
	private LinearLayout linManageApParam;
	private LinearLayout linOtpSetting;
	private LinearLayout linFingerSetting;
	private LinearLayout lnUpgradePublicVersion;

	private SwitchCompat otpSwitch;
	private SwitchCompat fingerPrintSwitch;
	protected TextView txtNameActionBar;
	protected TextView txtAddressActionBar;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
        MainActivity.getInstance().setTitleActionBar(R.string.system);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.d("TAG", "onDetach onCreateView");
		View mView = inflater.inflate(R.layout.synchrozation_layout, container, false);

		context = MainActivity.getInstance();
		Unit(mView);
		return mView;
	}

	private void Unit(View v) {

		linSynData = (LinearLayout) v.findViewById(R.id.linSynData);
		linUpdateVersion = (LinearLayout) v.findViewById(R.id.linUpdateVersion);
		linUpgradeFix = (LinearLayout) v.findViewById(R.id.linUpgradeFix);
		linManageApParam = (LinearLayout) v.findViewById(R.id.linManageApParam);
		linOtpSetting = (LinearLayout) v.findViewById(R.id.linOtpSetting);
		linFingerSetting = (LinearLayout) v.findViewById(R.id.linFingerSetting);
		lnUpgradePublicVersion = (LinearLayout) v.findViewById(R.id.lnUpgradePublicVersion);

		otpSwitch = (SwitchCompat) v.findViewById(R.id.otpSwitch);
		fingerPrintSwitch = (SwitchCompat) v.findViewById(R.id.fingerPrintSwitch);

		linSynData.setOnClickListener(this);
		linUpdateVersion.setOnClickListener(this);
		linUpgradeFix.setOnClickListener(this);
		linManageApParam.setOnClickListener(this);
		lnUpgradePublicVersion.setOnClickListener(this);

		if(Constant.versionType == Constant.PUBLIC_INTERNET_VERSION){
			linOtpSetting.setVisibility(View.VISIBLE);
			lnUpgradePublicVersion.setVisibility(View.VISIBLE);
			linUpdateVersion.setVisibility(View.GONE);
		} else {
			linOtpSetting.setVisibility(View.GONE);
			lnUpgradePublicVersion.setVisibility(View.GONE);
			linUpdateVersion.setVisibility(View.VISIBLE);
		}


		linUpgradeFix.setVisibility(View.GONE);


		if (CommonActivity.isNullOrEmpty(Session.getFixErrorVersion())) {
			linUpgradeFix.setVisibility(View.GONE);
		}
		if("PMVT_HUYPQ15".equalsIgnoreCase(Session.userName)){
			linManageApParam.setVisibility(View.VISIBLE);
		} else {
			linManageApParam.setVisibility(View.GONE);
		}

		if (!FingerManager.checkOSCompatible() || !FingerManager.checkFingerSupported(getActivity())) {
			linFingerSetting.setVisibility(View.GONE);
		} else {
			linFingerSetting.setVisibility(View.VISIBLE);
			if (FingerManager.checkFingerEnable(getActivity())) {
				fingerPrintSwitch.setChecked(true);
			} else {
				fingerPrintSwitch.setChecked(false);
			}
		}

		if (Session.isUserOTP) {
			otpSwitch.setChecked(true);
		} else {
			otpSwitch.setChecked(false);
		}

		otpSwitch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String messageOTp = "";
				if (Session.isUserOTP) {
					messageOTp = getResources().getString(R.string.msg_disable_using_otp);
				} else {
					messageOTp = getResources().getString(R.string.msg_enable_using_otp);
				}

				Dialog dialog = CommonActivity.createDialog(
						getActivity(), messageOTp
						,getResources().getString(R.string.app_name),
						getResources().getString(R.string.cancel),
						getResources().getString(R.string.ok),
						null, updateSendOTPClick);
				dialog.show();
			}
		});

		fingerPrintSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
				if (FingerManager.checkFingerEnable(getActivity())) {
					FingerManager.disableFinger(getActivity());
					Toast.makeText(getActivity(),
							getString(R.string.finger_inactive_ok),
							Toast.LENGTH_LONG).show();
				} else {
					FingerManager.enableFinger(getActivity());
					Toast.makeText(getActivity(),
							getString(R.string.finger_active_ok),
							Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	// ==== syndata click listenner ===============
    private final OnClickListener SynDataClick = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (CommonActivity.isNetworkConnected(getActivity())) {
				try {
					// getActivity().deleteDatabase(Define.DB_NAME);
					SyncAsyncTask syncAsncTask = new SyncAsyncTask(
							getActivity(), onPostSyncListener);
					syncAsncTask.execute(Session.getToken(), Session.userName);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
						getResources().getString(R.string.errorNetwork),
						getResources().getString(R.string.app_name));
				dialog.show();
			}

		}
	};

	// ==== update otp click =============
	private final OnClickListener updateSendOTPClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			final String status;
			// gan status nguoc voi trang thai hien tai de thay doi:
			if (Session.isUserOTP) {
				status = "0";
			} else {
				status = "1";
			}

			if (CommonActivity.isNetworkConnected(getActivity())) {
				UpdateSendOTPAsyncTask updateSendOTPAsyncTask =
						new UpdateSendOTPAsyncTask(getActivity(), new OnPostExecuteListener<String>() {

					@Override
					public void onPostExecute(String result, String errorCode, String description) {
						if ("0".equals(result)) {
							Toast.makeText(getActivity(),
									getString(R.string.setting_otp_success),
									Toast.LENGTH_LONG).show();
							Session.isUserOTP = status.equals("1") ? true : false;
							if (Session.isUserOTP) {
								otpSwitch.setChecked(true);
							} else {
								otpSwitch.setChecked(false);
							}
						} else {
							CommonActivity.createAlertDialog(getActivity(),
									CommonActivity.isNullOrEmpty(description)
											? getString(R.string.exception) : description,
									getString(R.string.app_name)).show();
						}
					}
				}, null);
				updateSendOTPAsyncTask.execute(status);
			} else {
				Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
						getResources().getString(R.string.errorNetwork),
						getResources().getString(R.string.app_name));
				dialog.show();
			}
		}
	};

	// ==== update version click =============
    private final OnClickListener UpdateVersionSyn = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// call Asyntask========
			if (CommonActivity.isNetworkConnected(getActivity())) {
				UpdateVersionAsyn updateVersionAsyn = new UpdateVersionAsyn(
						getActivity(), Constant.PATH_UPDATE_MBCCS_VERSION
								+ Session.token);
				updateVersionAsyn.execute();
			} else {
				Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
						getResources().getString(R.string.errorNetwork),
						getResources().getString(R.string.app_name));
				dialog.show();
			}

		}
	};

	// ==== update version click =============
	private final OnClickListener updatePublicVersion = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// call Asyntask========
			if (CommonActivity.isNetworkConnected(getActivity())) {
				UpdateVersionAsyn updateVersionAsyn = new UpdateVersionAsyn(
						getActivity(), Constant.PATH_UPDATE_MBCCS_PUBLIC_VERSION
						+ Session.token);
				updateVersionAsyn.execute();
			} else {
				Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
						getResources().getString(R.string.errorNetwork),
						getResources().getString(R.string.app_name));
				dialog.show();
			}

		}
	};

	private final OnPostExecuteListener<String> onPostSyncListener = new OnPostExecuteListener<String>() {
		@Override
		public void onPostExecute(String result, String errorCode, String description) {
			if (Constant.SUCCESS_CODE.equals(result)) {
				// Dong bo du lieu thanh cong,
				// luu bien staff vao session,
				// vao man hinh chinh
				InfrastrucureDB mInfrastrucureDB = new InfrastrucureDB(getActivity());
				String shop[] = mInfrastrucureDB.getProvince();
				mInfrastrucureDB.close();
				Session.loginUser = StaffBusiness.getStaffByStaffCode(
						getActivity(), Session.userName);
				Session.province = shop[0];
				Session.district = shop[1];
				Dialog dialog = CommonActivity.createAlertDialog(MainActivity.getInstance(),
						context.getResources().getString(R.string.syndatasucess),
						context.getResources().getString(R.string.syndata));
				dialog.show();
			} else if (Constant.ERROR_CODE.equals(result)) {
				// String message = errorMessage;
				String title = getString(R.string.app_name);
				Dialog dialog = CommonActivity.createAlertDialog(getActivity(),
						getActivity().getString(R.string.syndatafails), title);
				dialog.show();
			}
		}
	};

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.btnHome:
			CommonActivity.callphone(getActivity(), Constant.phoneNumber);
			break;
		case R.id.relaBackHome:
			ReplaceFragment.replaceFragmentToHome(getActivity(), true);
			break;
		case R.id.linSynData:
			Log.d("OnClick", "OnClick");
			// // =========hoi co chac chan dong bo ko============
			if (CommonActivity.isNetworkConnected(getActivity())) {
				Dialog dialog = CommonActivity.createDialog(getActivity(),
						getResources().getString(R.string.issyn),
						getResources().getString(R.string.syndata),
						getResources().getString(R.string.cancel),
						getResources().getString(R.string.ok),
						null, SynDataClick);
				dialog.show();

			} else {
				Dialog dialog = CommonActivity.createAlertDialog(
						getActivity(),
						getResources().getString(R.string.errorNetwork),
						getResources().getString(R.string.app_name));
				dialog.show();
			}

			break;
			case R.id.linUpdateVersion:
				if (CommonActivity.isNetworkConnected(getActivity())) {
						Dialog dialog = CommonActivity.createDialog(
								getActivity(),
								getResources().getString(R.string.update_version_private),
								getResources()
										.getString(R.string.updateversion),
								getResources().getString(R.string.cancel),
								getResources().getString(R.string.ok),
								null, UpdateVersionSyn);
						dialog.show();
				} else {
					Dialog dialog = CommonActivity.createAlertDialog(
							getActivity(),
							getResources().getString(R.string.errorNetwork),
							getResources().getString(R.string.app_name));
					dialog.show();
				}
				break;
			case R.id.linUpgradeFix:
				OnClickListener updateFixClick = new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						if (CommonActivity.isNetworkConnected(getActivity())) {
							UpdateVersionAsyn updateVersionAsyn = new UpdateVersionAsyn(
									getActivity(),
									Constant.PATH_UPDATE_FIX_ERROR_VERSION
											+ Session.token);
							updateVersionAsyn.execute();
						} else {
							Dialog dialog = CommonActivity
									.createAlertDialog(
											getActivity(),
											getResources().getString(
													R.string.errorNetwork),
											getResources().getString(
													R.string.app_name));
							dialog.show();
						}
					}
				};
				String msg = getString(R.string.confirmUpgradeVersion2);
				Dialog dialog = CommonActivity.createDialog(getActivity(), msg,
						getString(R.string.app_name), getString(R.string.ok),
						getString(R.string.cancel), updateFixClick, null);
				dialog.show();
				break;
			case R.id.lnUpgradePublicVersion:
				if (CommonActivity.isNetworkConnected(getActivity())) {
					dialog = CommonActivity.createDialog(
							getActivity(),
							getResources().getString(R.string.update_version_public),
							getResources()
									.getString(R.string.updateversion),
							getResources().getString(R.string.cancel),
							getResources().getString(R.string.ok),
							null, updatePublicVersion);
					dialog.show();
				} else {
					dialog = CommonActivity.createAlertDialog(
							getActivity(),
							getResources().getString(R.string.errorNetwork),
							getResources().getString(R.string.app_name));
					dialog.show();
				}
				break;
			case R.id.linManageApParam:
				ManagerApParamFragment fragment =new ManagerApParamFragment();
				ReplaceFragment.replaceFragment(getActivity(), fragment, true);
				break;
		}
	}
}
