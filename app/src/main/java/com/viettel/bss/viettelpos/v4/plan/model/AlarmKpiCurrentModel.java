package com.viettel.bss.viettelpos.v4.plan.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by root on 05/01/2018.
 */

@Root(name = "return", strict = false)
public class AlarmKpiCurrentModel {

    @Element(name = "errorCode", required = false)
    private String errorCode;

    @Element(name = "success", required = false)
    private boolean success;

    @Element(name = "notification", required = false)
    private String notification;

    @ElementList(name = "lstAlarmKpiCurrentDTOs", entry = "lstAlarmKpiCurrentDTOs", required = false, inline = true)
    private ArrayList<AlarmKpiCurrent> results = new ArrayList<>();

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public ArrayList<AlarmKpiCurrent> getResults() {
        return results;
    }

    public void setResults(ArrayList<AlarmKpiCurrent> results) {
        this.results = results;
    }
}

