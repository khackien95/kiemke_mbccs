package com.viettel.bss.viettelpos.v4.plan.event;

/**
 * Created by BaVV on 4/29/16.
 */
public class PlanEvent {

    private Object data;

    private Action action;

    public PlanEvent(Action action) {
        this.action = action;
    }

    public PlanEvent(Action action, Object data) {
        this.action = action;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public enum Action {
        CHOOSE_INFRASTRUCTURE,
        BACK,
        BACK_FROM_CREATE_PLAN,
        BACK_FROM_PLAN_LIST,
        RELOAD_SALE_RESULT,
        RELOAD_SALE_WARNING,
        RELOAD_SALE_ALARM_KPI,
        MENU_CREATE_PLAN,
        CHOOSE_NEW_PLAN,
        SAVE_PLAN
    }
}
