package com.viettel.bss.viettelpos.v4.customer.object;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 05/01/2018.
 */

@Root(name = "qrInfoBean", strict = false)
public class QRCodeModel {

    @Element(name = "msisdn", required = false)
    private String msisdn;

    @Element(name = "idNo", required = false)
    private String idNo;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }
}
