package com.viettel.bss.viettelpos.v3.connecttionService.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by knight on 18/01/2018.
 */

@Root(name = "orderIdCombo", strict = false)
public class OrderIdCombo {
    @Element(name = "orderId", required = false)
    private String orderId;
    @Element(name = "subId", required = false)
    private Long subId;


    public String getOrderId() {
        return orderId;
    }

    public Long getSubId() {
        return subId;
    }
}
