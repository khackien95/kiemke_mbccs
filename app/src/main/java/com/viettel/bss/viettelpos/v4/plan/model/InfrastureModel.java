package com.viettel.bss.viettelpos.v4.plan.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by root on 05/01/2018.
 */
@Root(name = "return", strict = false)
public class InfrastureModel implements Parcelable {

    @Element(name = "address", required = false)
    private String address;

    @Element(name = "objectCode", required = false)
    private String objectCode;

    @Element(name = "objectType", required = false)
    private Long objectType;

    @Element(name = "objectName", required = false)
    private String objectName;

    @Element(name = "lat", required = false)
    private String lat;

    @Element(name = "lng", required = false)
    private String lng;

    @Element(name = "perUsage", required = false)
    private Double perUsage;

    @Element(name = "distance", required = false)
    private Double distance;

    @Element(name = "description", required = false)
    private String description;

    private boolean checked = false;

    private boolean viewDetail = false;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getObjectCode() {
        return objectCode;
    }

    public void setObjectCode(String objectCode) {
        this.objectCode = objectCode;
    }

    public Long getObjectType() {
        return objectType;
    }

    public void setObjectType(Long objectType) {
        this.objectType = objectType;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public Double getPerUsage() {
        return perUsage;
    }

    public void setPerUsage(Double perUsage) {
        this.perUsage = perUsage;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isViewDetail() {
        return viewDetail;
    }

    public void setViewDetail(boolean viewDetail) {
        this.viewDetail = viewDetail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.objectCode);
        dest.writeValue(this.objectType);
        dest.writeString(this.objectName);
        dest.writeString(this.lat);
        dest.writeString(this.lng);
        dest.writeValue(this.perUsage);
        dest.writeValue(this.distance);
        dest.writeString(this.description);
        dest.writeByte(this.checked ? (byte) 1 : (byte) 0);
    }

    public InfrastureModel() {
    }

    public InfrastureModel(String objectCode, long objectType, String objectName, String lat, String lng,
                           Double perUsage, Double distance, String description, boolean checked) {
        this.objectCode = objectCode;
        this.objectType = objectType;
        this.objectName = objectName;
        this.lat = lat;
        this.lng = lng;
        this.perUsage = perUsage;
        this.distance = distance;
        this.description = description;
        this.checked = checked;
    }

    protected InfrastureModel(Parcel in) {
        this.objectCode = in.readString();
        this.objectType = (Long) in.readValue(Long.class.getClassLoader());
        this.objectName = in.readString();
        this.lat = in.readString();
        this.lng = in.readString();
        this.perUsage = (Double) in.readValue(Double.class.getClassLoader());
        this.distance = (Double) in.readValue(Double.class.getClassLoader());
        this.description = in.readString();
        this.checked = in.readByte() != 0;
    }

    public static final Parcelable.Creator<InfrastureModel> CREATOR = new Parcelable.Creator<InfrastureModel>() {
        @Override
        public InfrastureModel createFromParcel(Parcel source) {
            return new InfrastureModel(source);
        }

        @Override
        public InfrastureModel[] newArray(int size) {
            return new InfrastureModel[size];
        }
    };
}
