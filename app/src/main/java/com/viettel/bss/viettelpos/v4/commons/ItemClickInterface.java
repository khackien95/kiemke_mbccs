package com.viettel.bss.viettelpos.v4.commons;

/**
 * Created by hungtv64 on 1/24/2018.
 */

public interface ItemClickInterface {
    void onItemClick(Object item);
}
