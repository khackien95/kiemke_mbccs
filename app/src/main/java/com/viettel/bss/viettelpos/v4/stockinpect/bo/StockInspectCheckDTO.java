package com.viettel.bss.viettelpos.v4.stockinpect.bo;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Created by leekien on 5/11/2018.
 */

@Root(name = "stockInspectCheckDTO", strict = false)
public class StockInspectCheckDTO extends BaseDTO implements Serializable {
    @Element(name = "approveNote", required = false)
    protected String approveNote;
    @Element(name = "availableQuantity", required = false)
    protected Long availableQuantity;
    @Element(name = "currentQuantity", required = false)
    protected Long currentQuantity;
    @Element(name = "disableQuantityPoor", required = false)
    protected boolean disableQuantityPoor;
    @Element(name = "fromSerial", required = false)
    protected String fromSerial;
    @Element(name = "impType", required = false)
    protected Long impType;
    @Element(name = "inspectStatus", required = false)
    protected Long inspectStatus;
    @Element(name = "note", required = false)
    protected String note;
    @Element(name = "ownerId", required = false)
    protected Long ownerId;
    @Element(name = "ownerType", required = false)
    protected Long ownerType;
    @Element(name = "prodOfferId", required = false)
    protected Long prodOfferId;
    @Element(name = "productCode", required = false)
    protected String productCode;
    @Element(name = "productName", required = false)
    protected String productName;
    @Element(name = "productOfferTypeId", required = false)
    protected Long productOfferTypeId;
    @Element(name = "quanittyReal", required = false)
    protected Long quanittyReal;
    @Element(name = "quantity", required = false)
    protected Long quantity;
    @Element(name = "quantityPoor", required = false)
    protected Long quantityPoor;
    @Element(name = "quantityStockCheck", required = false)
    protected Long quantityStockCheck;
    @Element(name = "reason", required = false)
    protected String reason;
    @Element(name = "saleOrExportDate", required = false)
    protected XMLGregorianCalendar saleOrExportDate;
    @Element(name = "stateId", required = false)
    protected Long stateId;
    @Element(name = "stateName", required = false)
    protected String stateName;
    @Element(name = "stockInspectRealId", required = false)
    protected Long stockInspectRealId;
    @Element(name = "toSerial", required = false)
    protected String toSerial;
    public String getApproveNote() {
        return approveNote;
    }

    public void setApproveNote(String approveNote) {
        this.approveNote = approveNote;
    }

    public Long getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(Long availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public Long getCurrentQuantity() {
        return currentQuantity;
    }

    public void setCurrentQuantity(Long currentQuantity) {
        this.currentQuantity = currentQuantity;
    }

    public boolean isDisableQuantityPoor() {
        return disableQuantityPoor;
    }

    public void setDisableQuantityPoor(boolean disableQuantityPoor) {
        this.disableQuantityPoor = disableQuantityPoor;
    }

    public String getFromSerial() {
        return fromSerial;
    }

    public void setFromSerial(String fromSerial) {
        this.fromSerial = fromSerial;
    }

    public Long getImpType() {
        return impType;
    }

    public void setImpType(Long impType) {
        this.impType = impType;
    }

    public Long getInspectStatus() {
        return inspectStatus;
    }

    public void setInspectStatus(Long inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Long getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(Long ownerType) {
        this.ownerType = ownerType;
    }

    public Long getProdOfferId() {
        return prodOfferId;
    }

    public void setProdOfferId(Long prodOfferId) {
        this.prodOfferId = prodOfferId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getProductOfferTypeId() {
        return productOfferTypeId;
    }

    public void setProductOfferTypeId(Long productOfferTypeId) {
        this.productOfferTypeId = productOfferTypeId;
    }

    public Long getQuanittyReal() {
        return quanittyReal;
    }

    public void setQuanittyReal(Long quanittyReal) {
        this.quanittyReal = quanittyReal;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getQuantityPoor() {
        return quantityPoor;
    }

    public void setQuantityPoor(Long quantityPoor) {
        this.quantityPoor = quantityPoor;
    }

    public Long getQuantityStockCheck() {
        return quantityStockCheck;
    }

    public void setQuantityStockCheck(Long quantityStockCheck) {
        this.quantityStockCheck = quantityStockCheck;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getStateId() {
        return stateId;
    }

    public void setStateId(Long stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Long getStockInspectRealId() {
        return stockInspectRealId;
    }

    public void setStockInspectRealId(Long stockInspectRealId) {
        this.stockInspectRealId = stockInspectRealId;
    }

    public String getToSerial() {
        return toSerial;
    }

    public void setToSerial(String toSerial) {
        this.toSerial = toSerial;
    }
}
