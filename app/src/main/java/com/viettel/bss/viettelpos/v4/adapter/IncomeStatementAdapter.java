package com.viettel.bss.viettelpos.v4.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.bo.IncomeDTO;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.DataUtils;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.commons.StringUtils;
import com.viettel.bss.viettelpos.v4.listener.OnItemClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by BaVV on 02/06/2018.
 */
public class IncomeStatementAdapter extends RecyclerView.Adapter<IncomeStatementAdapter.ViewHolder> {

    private List<IncomeDTO> lstData;
    private OnIncomeClickListener onIncomeClickListener;
    private Context mContext;
    private boolean isTemporary;
    private boolean isShowSalaryTypeName;

    public IncomeStatementAdapter(Context mContext, List<IncomeDTO> lstData, OnIncomeClickListener onIncomeClickListener) {
        this.mContext = mContext;
        this.lstData = lstData;
        this.onIncomeClickListener = onIncomeClickListener;
    }

    public void setTemporary(boolean temporary) {
        isTemporary = temporary;
    }

    public void setShowSalaryTypeName(boolean showSalaryTypeName) {
        isShowSalaryTypeName = showSalaryTypeName;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item_income_statement, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final IncomeDTO incomeDTO = lstData.get(position);

        holder.tvTitle.setText(DataUtils.safeToString(incomeDTO.getTitle()));

        String value = DataUtils.safeToString(incomeDTO.getValue());
        if(CommonActivity.isNullOrEmpty(value)) value = "0";
        holder.tvValue.setText(value/* + " VNĐ"*/);
        holder.tvFormula.setText("(= " + DataUtils.safeToString(incomeDTO.getFormula()) + ")");
        holder.tvFormula.setVisibility(CommonActivity.isNullOrEmpty(DataUtils.safeToString(incomeDTO.getFormula())) ? View.GONE : View.VISIBLE);

        if(isTemporary) {
            if("1".equals(incomeDTO.getIsTemporary())) {
                holder.tvTitle.setText(DataUtils.safeToString(incomeDTO.getTitle() + " (Tạm tính)"));
            }
        }

        if(isShowSalaryTypeName && !CommonActivity.isNullOrEmpty(DataUtils.safeToString(incomeDTO.getSalaryTypeName()))) {
            holder.tvSalaryTypeName.setText(DataUtils.safeToString(incomeDTO.getSalaryTypeName()));
            holder.tvSalaryTypeName.setVisibility(View.VISIBLE);
        } else {
            holder.tvSalaryTypeName.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(null != onIncomeClickListener) onIncomeClickListener.onItemClick(incomeDTO);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lstData == null ? 0 : lstData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvValue)
        TextView tvValue;
        @BindView(R.id.tvSalaryTypeName)
        TextView tvSalaryTypeName;
        @BindView(R.id.tvFormula)
        TextView tvFormula;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnIncomeClickListener {
        void onItemClick(IncomeDTO item);
    }
}
