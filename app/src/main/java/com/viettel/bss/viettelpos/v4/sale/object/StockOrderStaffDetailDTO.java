package com.viettel.bss.viettelpos.v4.sale.object;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by thinhhq1 on 4/3/2018.
 */
@Root(name = "StockOrderStaffDetailDTO", strict = false)
public class StockOrderStaffDetailDTO implements Serializable {
    @ElementList(name = "listStockTransSerialDTOs", entry = "listStockTransSerialDTOs", required = false, inline = true)
    private ArrayList<StockTransSerialSM> listStockTransSerialDTOs;
    @Element(name = "prodOfferId", required = false)
    private Long prodOfferId;
    @Element(name = "quantity", required = false)
    private Long quantity;
    @Element(name = "stateId", required = false)
    private Long stateId;
    @Element(name = "stockOrderDetailId", required = false)
    private Long stockOrderDetailId;
    @Element(name = "stockOrderId", required = false)
    private Long stockOrderId;
    @Element(name = "prodOfferCode", required = false)
    private String prodOfferCode;
    @Element(name = "prodOfferName", required = false)
    private String prodOfferName;
    @Element(name = "stateName", required = false)
    private String stateName;
    @Element(name = "checkSerial", required = false)
    private Long checkSerial;

    public String getProdOfferCode() {
        return prodOfferCode;
    }

    public void setProdOfferCode(String prodOfferCode) {
        this.prodOfferCode = prodOfferCode;
    }

    public String getProdOfferName() {
        return prodOfferName;
    }

    public void setProdOfferName(String prodOfferName) {
        this.prodOfferName = prodOfferName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Long getCheckSerial() {
        return checkSerial;
    }

    public void setCheckSerial(Long checkSerial) {
        this.checkSerial = checkSerial;
    }

    public ArrayList<StockTransSerialSM> getListStockTransSerialDTOs() {
        return listStockTransSerialDTOs;
    }

    public void setListStockTransSerialDTOs(ArrayList<StockTransSerialSM> listStockTransSerialDTOs) {
        this.listStockTransSerialDTOs = listStockTransSerialDTOs;
    }

    public Long getProdOfferId() {
        return prodOfferId;
    }

    public void setProdOfferId(Long prodOfferId) {
        this.prodOfferId = prodOfferId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getStateId() {
        return stateId;
    }

    public void setStateId(Long stateId) {
        this.stateId = stateId;
    }

    public Long getStockOrderDetailId() {
        return stockOrderDetailId;
    }

    public void setStockOrderDetailId(Long stockOrderDetailId) {
        this.stockOrderDetailId = stockOrderDetailId;
    }

    public Long getStockOrderId() {
        return stockOrderId;
    }

    public void setStockOrderId(Long stockOrderId) {
        this.stockOrderId = stockOrderId;
    }
}
