package com.viettel.bss.viettelpos.v4.channel.pager;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.viettel.bss.viettelpos.v4.channel.activity.FragmentCusCareByDay;
import com.viettel.bss.viettelpos.v4.charge.wizardpager.wizard.model.ModelCallbacks;
import com.viettel.bss.viettelpos.v4.charge.wizardpager.wizard.model.Page;
import com.viettel.bss.viettelpos.v4.charge.wizardpager.wizard.model.ReviewItem;
import com.viettel.gem.base.BaseActivity;
import com.viettel.gem.base.viper.ViewFragment;
import com.viettel.gem.base.viper.interfaces.ContainerView;
import com.viettel.gem.base.viper.interfaces.IPresenter;
import com.viettel.gem.base.viper.interfaces.IView;
import com.viettel.gem.screen.listnetworkinfor.NetworkCollectPresenter;

import java.util.ArrayList;

/**
 * Created by Toancx on 3/24/2018.
 */

public class NetworkCollectPager extends Page {
    public NetworkCollectPager(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Fragment createFragment() {
        return new NetworkCollectPresenter((ContainerView) FragmentCusCareByDay.getInstance()).setIsChannelCare(true).getFragment();
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

    }
}
