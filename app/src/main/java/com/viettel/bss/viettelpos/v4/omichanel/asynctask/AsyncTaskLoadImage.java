package com.viettel.bss.viettelpos.v4.omichanel.asynctask;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by thuandq on 11/09/2017.
 */

public class AsyncTaskLoadImage extends AsyncTask<String, Void, Bitmap> {
    OnPostExecuteListener<Bitmap> listener;
    private String errorCode;
    private String description;

    public AsyncTaskLoadImage(Activity context, OnPostExecuteListener<Bitmap> listener, View.OnClickListener moveLogInAct) {
        this.listener = listener;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        return loadImageFromWebOperations(params[0]);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        listener.onPostExecute(bitmap, errorCode, description);
    }

    public Bitmap loadImageFromWebOperations(String url) {
        Bitmap bitmap = null;
        try {
            Log.d("loadImageFromWebOperations", "url = " + url);
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
            errorCode = "0";
        } catch (MalformedURLException e) {
            errorCode = "1";
            description = e.getMessage();
        } catch (IOException e) {
            errorCode = "1";
            description = e.getMessage();
        }
        return bitmap;
    }
}
