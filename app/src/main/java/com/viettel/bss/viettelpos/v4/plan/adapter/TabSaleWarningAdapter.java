package com.viettel.bss.viettelpos.v4.plan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.StringUtils;
import com.viettel.bss.viettelpos.v4.plan.model.SaleResultCtv;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by BaVV on 9/1/2018.
 */

public class TabSaleWarningAdapter extends RecyclerView.Adapter<TabSaleWarningAdapter.SaleWarningHolder> {

    private Context context;
    private List<SaleResultCtv> saleResultCtvList;
    private SaleWarningClickListener clickListener;

    public TabSaleWarningAdapter(Context context, List<SaleResultCtv> saleResultCtvList, SaleWarningClickListener listener) {
        this.context = context;
        this.saleResultCtvList = saleResultCtvList;
        this.clickListener = listener;
    }

    @Override
    public SaleWarningHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SaleWarningHolder(LayoutInflater.from(context).inflate(R.layout.adapter_item_tab_sale_warning_view, parent, false));
    }

    @Override
    public void onBindViewHolder(SaleWarningHolder holder, int position) {
        final SaleResultCtv resultCtv = saleResultCtvList.get(position);
        if (null == resultCtv) return;

        holder.tvIndex.setText((position + 1) + ".");
        holder.tvKpiCode.setText(resultCtv.getKpiCode());
        holder.tvKpiName.setText(resultCtv.getKpiName());
        holder.tvPercentComplete.setText(
                StringUtils.formatNumber(resultCtv.getTotalAchieved())
                + "/"
                + StringUtils.formatNumber(resultCtv.getTotalPlanned())
                + "/"
                + StringUtils.formatNumber(resultCtv.getPercentComplete())
                + "%");
        holder.tvRemainPaid.setText(StringUtils.formatMoney(/*resultCtv.getRemainPaid() < 0 ? "0" : */resultCtv.getRemainPaid() + ""));

        holder.tvTotalAchieved.setText(StringUtils.formatNumber(resultCtv.getTotalAchieved()));
        holder.tvTotalPlanned.setText(StringUtils.formatNumber(resultCtv.getTotalPlanned()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != clickListener) clickListener.onClick(resultCtv);
            }
        });
    }

    @Override
    public int getItemCount() {
        return null == saleResultCtvList ? 0 : saleResultCtvList.size();
    }

    public class SaleWarningHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvIndex)
        TextView tvIndex;

        @BindView(R.id.tvKpiCode)
        TextView tvKpiCode;

        @BindView(R.id.tvKpiName)
        TextView tvKpiName;

        @BindView(R.id.tvPercentComplete)
        TextView tvPercentComplete;

        @BindView(R.id.tvRemainPaid)
        TextView tvRemainPaid;

        @BindView(R.id.tvTotalAchieved)
        TextView tvTotalAchieved;

        @BindView(R.id.tvTotalPlanned)
        TextView tvTotalPlanned;

        private SaleWarningHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface SaleWarningClickListener {
        void onClick(SaleResultCtv resultCtv);
    }
}
