package com.viettel.bss.viettelpos.v4.plan.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by root on 05/01/2018.
 */

@Root(name = "return", strict = false)
public class GetListInfrastureModel {

    @Element(name = "errorCode", required = false)
    private String errorCode;

    @Element(name = "success", required = false)
    private boolean success;

    @ElementList(name = "lstInfrastructureObjects", entry = "lstInfrastructureObjects", required = false, inline = true)
    private ArrayList<InfrastureModel> results = new ArrayList<>();

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<InfrastureModel> getResults() {
        return results;
    }

    public void setResults(ArrayList<InfrastureModel> results) {
        this.results = results;
    }
}
