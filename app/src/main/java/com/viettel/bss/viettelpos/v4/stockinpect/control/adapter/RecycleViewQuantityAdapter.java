package com.viettel.bss.viettelpos.v4.stockinpect.control.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.stockinpect.bo.StockInspectCheckDTO;

import java.util.ArrayList;

public class RecycleViewQuantityAdapter extends RecyclerView.Adapter<RecycleViewQuantityAdapter.ViewHolder> {
    ArrayList<StockInspectCheckDTO> stockInspectCheckDTOS;
    private LayoutInflater mInflater;

    public RecycleViewQuantityAdapter(ArrayList<StockInspectCheckDTO> stockInspectCheckDTOS, Context context) {
        this.stockInspectCheckDTOS = stockInspectCheckDTOS;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_list_stock_quantity, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        StockInspectCheckDTO stockInspectCheckDTO = stockInspectCheckDTOS.get(position);
        holder.status.setText(position + 1 + "");
        holder.name.setText(stockInspectCheckDTO.getProductName());
        holder.status.setText(stockInspectCheckDTO.getStateName());
        holder.quantity_system.setText(stockInspectCheckDTO.getQuantity() + "");
        if (!CommonActivity.isNullOrEmpty(stockInspectCheckDTO.getQuanittyReal()))
            holder.quantity_fact.setText(stockInspectCheckDTO.getQuanittyReal() + "");
        else holder.quantity_fact.setText("0");
    }

    @Override
    public int getItemCount() {
        return stockInspectCheckDTOS.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView stt, name, status, quantity_system;
        EditText quantity_fact;

        public ViewHolder(View itemView) {
            super(itemView);
            stt = (TextView) itemView.findViewById(R.id.stt);
            name = (TextView) itemView.findViewById(R.id.merchandise_name);
            status = (TextView) itemView.findViewById(R.id.state_stock_model);
            quantity_system = (TextView) itemView.findViewById(R.id.quantity_in_system);
            quantity_fact = (EditText) itemView.findViewById(R.id.quantity_in_fact);
        }
    }
}
