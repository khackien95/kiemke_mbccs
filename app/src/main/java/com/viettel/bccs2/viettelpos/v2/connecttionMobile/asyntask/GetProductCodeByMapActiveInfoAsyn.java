package com.viettel.bccs2.viettelpos.v2.connecttionMobile.asyntask;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.beans.ProductOfferingDTO;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.connecttionMobile.handler.ProductOfferringHanlder;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncTaskCommon;

import android.app.Activity;
import android.util.Log;
import android.view.View.OnClickListener;

public class GetProductCodeByMapActiveInfoAsyn extends AsyncTaskCommon<String, Void, ProductOfferringHanlder> {
	private boolean isSmartSim = false;
	private Activity activity;

	public GetProductCodeByMapActiveInfoAsyn(Activity context,
											 OnPostExecuteListener<ProductOfferringHanlder> listener, OnClickListener moveLogInAct,
											 boolean isSmart) {
		super(context, listener, moveLogInAct);
		this.isSmartSim = isSmart;
		this.activity = context;
	}

	@Override
	protected ProductOfferringHanlder doInBackground(String... arg0) {
		return getListProductCode(arg0[0], arg0[1], arg0[2], arg0[3]);
	}

	private ProductOfferringHanlder getListProductCode(String telecomServiceId, String payType,
															 String actionCode, String offerId) {
		String original = null;
//		ArrayList<ProductOfferingDTO> lisPakageChargeBeans = new ArrayList<ProductOfferingDTO>();
		FileInputStream in = null;
		ProductOfferringHanlder handler = null;
		try {

			BCCSGateway input = new BCCSGateway();
			input.addValidateGateway("username", Constant.BCCSGW_USER);
			input.addValidateGateway("password", Constant.BCCSGW_PASS);
			input.addValidateGateway("wscode",
					"mbccs_getProductCodeByMapActiveInfo");
			StringBuilder rawData = new StringBuilder();
			rawData.append("<ws:getProductCodeByMapActiveInfo>");
			rawData.append("<input>");
			HashMap<String, String> paramToken = new HashMap<String, String>();
			paramToken.put("token", Session.getToken());
			rawData.append(input.buildXML(paramToken));
			rawData.append("<telecomServiceId>" + telecomServiceId);
			rawData.append("</telecomServiceId>");
			rawData.append("<payType>" + payType);
			rawData.append("</payType>");
			rawData.append("<actionCode>" + actionCode);
			rawData.append("</actionCode>");
			rawData.append("<isSmart>" + isSmartSim);
			rawData.append("</isSmart>");

			rawData.append("</input>");
			rawData.append("</ws:getProductCodeByMapActiveInfo>");
			Log.i("RowData", rawData.toString());
			String envelope = input.buildInputGatewayWithRawData(rawData.toString());
			Log.e("Send evelop", envelope);
			Log.i("LOG", Constant.BCCS_GW_URL);
			String response = input.sendRequest(envelope,
					Constant.BCCS_GW_URL,
					activity,
					"mbccs_getProductCodeByMapActiveInfo");
			Log.i("Responseeeeeeeeee", response);
			CommonOutput output = input.parseGWResponse(response);
			original = output.getOriginal();
			Log.d("original", original);

			handler = (ProductOfferringHanlder) CommonActivity
					.parseXMLHandler(new ProductOfferringHanlder(), original);
			output = handler.getItem();
			if (output.getToken() != null && !output.getToken().isEmpty()) {
				Session.setToken(output.getToken());
			}

			if (output.getErrorCode() != null) {
				errorCode = output.getErrorCode();
			}
			if (output.getDescription() != null) {
				description = output.getDescription();
			}
//			lisPakageChargeBeans = handler.getLstData();
		} catch (Exception e) {
			Log.e("Exception", e.toString(), e);
		}

		return handler;

	}
}
