package com.viettel.gem.screen.collectinfo.custom.spinnerspin;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.work.object.ProductSpecCharValueDTOList;
import com.viettel.gem.base.view.CustomView;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by BaVV on 5/31/16.
 */
public class SpinnerSpinItemView
        extends CustomView {

    @BindView(R.id.spnName)
    Spinner spnName;

    @BindView(R.id.spnValue)
    Spinner spnValue;

    SpinnerSpinBoxView mSpinnerTextBoxView;

    List<ProductSpecCharValueDTOList> mProductSpecCharValueDTOList;

    List<ProductSpecCharValueDTOList> mProductSpecCharValueDTOListValue;

    ProductSpecCharValueDTOList mSpecCharValueDTOList;

    private static final String AGE = "AGE";
    private int position = 0;

    public void setPosition(int position){
        this.position = position;
    }

    private Callback mCallback;

    public SpinnerSpinItemView(Context context) {
        super(context);
    }

    public SpinnerSpinItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpinnerSpinItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.cs_survey_spinner_spin_box_item_view;
    }

    @Override
    public boolean validateView() {
        return spnValue.getSelectedItemPosition() != 0 && spnName.getSelectedItemPosition() != 0;
    }

    public void build(SpinnerSpinBoxView spinnerTextBoxView,
                      ProductSpecCharValueDTOList specCharValueDTOList,
                      List<ProductSpecCharValueDTOList> productSpecCharValueDTOList,
                      List<ProductSpecCharValueDTOList> mProductSpecCharValueDTOListValue, Callback callback) {
        mSpinnerTextBoxView = spinnerTextBoxView;
        this.mSpecCharValueDTOList = specCharValueDTOList;
        this.mProductSpecCharValueDTOList = productSpecCharValueDTOList;
        this.mProductSpecCharValueDTOListValue = mProductSpecCharValueDTOListValue;
        mCallback = callback;
        initSpinner();
    }

    boolean userSelect = false;

    private void initSpinner() {

        spnName.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                userSelect = true;
                return false;
            }
        });

        spnName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int position, long arg3) {
                try {
                    if (userSelect) {
                        ProductSpecCharValueDTOList specCharValueDTOList = mProductSpecCharValueDTOList.get(position);

                        if (null != mCallback &&
                                null != specCharValueDTOList) {
                            specCharValueDTOList.setValueData(mSpecCharValueDTOList.getValueData());
                            mSpecCharValueDTOList.setValueData(null);
                            mSpecCharValueDTOList = specCharValueDTOList;

                        }
                        userSelect = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        if (null != this.mProductSpecCharValueDTOList && !this.mProductSpecCharValueDTOList.isEmpty()) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    getContext(), R.layout.adapter_spinner_item_view, R.id.tvContent);
            adapter.setDropDownViewResource(R.layout.adapter_spinner_dropdown_view);

            for (ProductSpecCharValueDTOList specCharValueDTOList : mProductSpecCharValueDTOList) {
                adapter.add(specCharValueDTOList.getName());
            }
            spnName.setAdapter(adapter);
            spnName.setSelection(mProductSpecCharValueDTOList.indexOf(mSpecCharValueDTOList));
        }

        if (null != this.mProductSpecCharValueDTOListValue && !this.mProductSpecCharValueDTOListValue.isEmpty()) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    getContext(), R.layout.adapter_spinner_item_view, R.id.tvContent);
            adapter.setDropDownViewResource(R.layout.adapter_spinner_dropdown_view);
            int selection = 0;
            int index = 0;
            for (ProductSpecCharValueDTOList specCharValueDTOList : mProductSpecCharValueDTOListValue) {
                adapter.add(specCharValueDTOList.getName());
                if(specCharValueDTOList.getName().equals(mSpecCharValueDTOList.getValueData()))
                    selection = index;
                index++;
            }
            spnValue.setAdapter(adapter);
            spnValue.setSelection(selection);
        }
    }

    @OnClick(R.id.imvMinus)
    void removeRow(){
        mCallback.onRemove(position, this);
    }

    public ProductSpecCharValueDTOList getAnswerModel() {
        return mSpecCharValueDTOList;
    }

    public String getName() {
        return (String) spnName.getSelectedItem();
    }

    public interface Callback {
        void onRemove(int position, SpinnerSpinItemView spinnerSpinItemView);
    }
}
