package com.viettel.gem.screen.collectinfo.custom.province;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.beans.ProductSpecCharDTO;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.gem.base.view.CustomView;

import butterknife.BindView;


/**
 * Created by BaVV on 5/31/16.
 */
public class ProvinceBoxView
        extends CustomView
        implements ProvinceItemView.Callback {

    @BindView(R.id.boxInput)
    LinearLayout boxInput;

    @BindView(R.id.tvName)
    TextView tvName;

    Activity mActivity;

    ProductSpecCharDTO productSpecCharDTO;

    private ProvinceItemView mSelectedView;

    public ProvinceBoxView(Context context) {
        super(context);
    }

    public ProvinceBoxView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProvinceBoxView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.cs_survey_edittext_box_view;
    }

    @Override
    public boolean validateView() {
        return mSelectedView.validateView();
    }

    @Override
    public void removeError() {
        super.removeError();
        mSelectedView.removeError();
    }

    public void build(ProductSpecCharDTO productSpecCharDTO, Activity mActivity) {
        this.productSpecCharDTO = productSpecCharDTO;
        if (null == this.productSpecCharDTO) return;

        this.mActivity = mActivity;

        tvName.setText(this.productSpecCharDTO.getName());

        boxInput.removeAllViews();

        mSelectedView = new ProvinceItemView(getContext());
        mSelectedView.build(productSpecCharDTO.getCode(), productSpecCharDTO.getValueData(), mActivity, this);

        boxInput.addView(mSelectedView);
    }

    public ProductSpecCharDTO getSelectedAnswerModel() {
        if (null != mSelectedView) {
            return this.productSpecCharDTO;
        }

        return null;
    }

    public void onResult(String value, String code){
        if(productSpecCharDTO.getCode() != null && productSpecCharDTO.getCode().equals(code))
            mSelectedView.edtInput.setText(value);
    }

    @Override
    public void onTextChanged(String text) {
        productSpecCharDTO.setValueData(text);
    }
}
