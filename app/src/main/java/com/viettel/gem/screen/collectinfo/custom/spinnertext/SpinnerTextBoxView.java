package com.viettel.gem.screen.collectinfo.custom.spinnertext;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.beans.ProductSpecCharDTO;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.work.object.ProductSpecCharValueDTOList;
import com.viettel.gem.base.view.CustomView;
import com.viettel.gem.screen.collectinfo.CollectCustomerInfoPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by BaVV on 5/31/16.
 */
public class SpinnerTextBoxView
        extends CustomView
        implements SpinnerTextItemView.Callback {

    public static final String ID_DEFAULT = "chon_mot_linh_vuc_lam_viec";

    public static final String LINH_VUC_LAM_VIEC_KEY = "JOB";
    public static final String SERVICE_VT = "SER_VT"; //cac dich vu viettel dang su dung
    public static final String AGE = "AGE"; //nam sinh
    public static final String NETWORK = "NETWORK"; //gia dinh KH dung bao nhieu sum cua nha mang nao

    private static final String SPLIT = "@@@";

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.boxSpinner)
    LinearLayout boxSpinner;

    boolean required = true; //true bat buoc tat ca, false khong bat buoc tat ca

    ProductSpecCharDTO productSpecCharDTO;

    Activity mActivity;

    List<ProductSpecCharValueDTOList> productSpecCharValueDTOList = new ArrayList<>();

    private List<SpinnerTextItemView> spinnerTextItemViews;

    public SpinnerTextBoxView(Context context) {
        super(context);
    }

    public SpinnerTextBoxView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpinnerTextBoxView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.cs_survey_spinner_text_box_view;
    }

    @Override
    public boolean validateView() {
        if (null != productSpecCharDTO && (SERVICE_VT.equals(productSpecCharDTO.getCode()) || AGE.equals(productSpecCharDTO.getCode()))) {
            if(SERVICE_VT.equals(productSpecCharDTO.getCode())){
                if(CollectCustomerInfoPresenter.mServiceVT != 0 && CollectCustomerInfoPresenter.mServiceVT < spinnerTextItemViews.size()){
                    CommonActivity.toast(mActivity, mActivity.getString(R.string.number_invalid));
                    return false;
                }
            }

            if(NETWORK.equals(productSpecCharDTO.getCode())){
                if(CollectCustomerInfoPresenter.mServiceMB != 0 && CollectCustomerInfoPresenter.mServiceMB < spinnerTextItemViews.size()){
                    CommonActivity.toast(mActivity, mActivity.getString(R.string.number_invalid));
                    return false;
                }
            }

            if(AGE.equals(productSpecCharDTO.getCode())){
                if(CollectCustomerInfoPresenter.mTotalMember < spinnerTextItemViews.size()){
                    CommonActivity.toast(mActivity, mActivity.getString(R.string.number_invalid));
                    return false;
                }
            }
        }

        boolean flag = true;
        if(required){
            for (SpinnerTextItemView spinnerTextItemView : spinnerTextItemViews) {
                if (!spinnerTextItemView.validateView()) {
                    if(flag) flag = spinnerTextItemView.validateView();
                }
            }
        } else {
            flag = false;
            for (SpinnerTextItemView spinnerTextItemView : spinnerTextItemViews) {
                if (spinnerTextItemView.validateView()) {
                    flag = true;
                    break;
                }
            }
        }

        if(flag)
            buildProductSpecCharValueDTOList();
        return flag;
    }

    private void buildProductSpecCharValueDTOList(){
        //clear data truoc khi rebuild
        for(ProductSpecCharValueDTOList specCharValueDTOList : productSpecCharDTO.getProductSpecCharValueDTOList()){
            specCharValueDTOList.setValueData("");
        }

        for(SpinnerTextItemView spinnerTextItemView : spinnerTextItemViews){
            ProductSpecCharValueDTOList productSpecCharValueDTOList = spinnerTextItemView.mSpecCharValueDTOList;
            for(ProductSpecCharValueDTOList specCharValueDTOList : productSpecCharDTO.getProductSpecCharValueDTOList()){
                if(specCharValueDTOList.getId().equals(productSpecCharValueDTOList.getId())){
                    specCharValueDTOList.setValueData(specCharValueDTOList.getValueData() + spinnerTextItemView.edtInput.getText().toString().trim() + SPLIT);
                }
            }
        }
    }

    public SpinnerTextBoxView setRequired(boolean required){
        this.required = required;
        return this;
    }

    public void build(ProductSpecCharDTO productSpecCharDTO, Activity mActivity) {
        if (null == productSpecCharDTO) return;

        this.productSpecCharDTO = productSpecCharDTO;

        this.mActivity = mActivity;

        tvName.setText(productSpecCharDTO.getName());

        spinnerTextItemViews = new ArrayList<>();
        productSpecCharValueDTOList = productSpecCharDTO.getProductSpecCharValueDTOList();

        productSpecCharValueDTOList.add(0, new ProductSpecCharValueDTOList(getContext().getString(R.string.spinnerSelectValue), ID_DEFAULT));

        if (null != productSpecCharValueDTOList && !productSpecCharValueDTOList.isEmpty()) {
            boolean flag = false;
            for (ProductSpecCharValueDTOList specCharValueDTOList : productSpecCharValueDTOList) {
                try {
                    if (null != specCharValueDTOList.getValueData()) {
                        flag = true;
                        String[] values = specCharValueDTOList.getValueData().split(SPLIT);

                        for(String value : values){
                            specCharValueDTOList.setValueData(value);
                            addChildView(specCharValueDTOList);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (!flag) {
                if (null != productSpecCharValueDTOList && !productSpecCharValueDTOList.isEmpty()) {
                    addChildView(productSpecCharValueDTOList.get(0));
                }
            }
        }
    }

    @OnClick({R.id.imvAdd, R.id.tvAdd})
    void addRow() {
        if (null != productSpecCharValueDTOList && !productSpecCharValueDTOList.isEmpty()) {
            if(SERVICE_VT.equals(productSpecCharDTO.getCode())){
                if(CollectCustomerInfoPresenter.mServiceVT <= spinnerTextItemViews.size()) {
                    CommonActivity.toast(mActivity, mActivity.getString(R.string.number_invalid));
                    return;
                }
            }

            if(NETWORK.equals(productSpecCharDTO.getCode())){
                if(CollectCustomerInfoPresenter.mServiceMB <= spinnerTextItemViews.size()) {
                    CommonActivity.toast(mActivity, mActivity.getString(R.string.number_invalid));
                    return;
                }
            }


            if(AGE.equals(productSpecCharDTO.getCode())){
                if(CollectCustomerInfoPresenter.mTotalMember <= spinnerTextItemViews.size()) {
                    CommonActivity.toast(mActivity, mActivity.getString(R.string.number_invalid));
                    return;
                }
            }

            addChildView(productSpecCharValueDTOList.get(0));

        }
    }

    void addChildView(ProductSpecCharValueDTOList specCharValueDTOList) {
        SpinnerTextItemView itemView = new SpinnerTextItemView(getContext());
        itemView.setPosition(spinnerTextItemViews.size());
        spinnerTextItemViews.add(itemView);
        itemView.build(this, specCharValueDTOList, productSpecCharValueDTOList, this, mActivity);
        boxSpinner.addView(itemView);
    }

    @Override
    public void onRemove(int position, SpinnerTextItemView spinnerTextItemView) {
        spinnerTextItemViews.remove(spinnerTextItemView);
        boxSpinner.removeView(spinnerTextItemView);
    }
}
