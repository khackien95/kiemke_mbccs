package com.viettel.gem.screen;

import android.content.Intent;
import android.support.v4.app.FragmentManager;

import com.viettel.gem.base.ContainerActivity;
import com.viettel.gem.base.viper.ViewFragment;
import com.viettel.gem.screen.searchcustomer.SearchCustomerPresenter;

/**
 * Created by root on 21/11/2017.
 */

public class SearchCustomerActivity extends ContainerActivity {
    @Override
    public ViewFragment onCreateFirstFragment() {
        return (ViewFragment) new SearchCustomerPresenter(this).getFragment();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FragmentManager manager = getSupportFragmentManager();
        onHandleFragmentResult(manager, requestCode, resultCode, data);
    }
}
