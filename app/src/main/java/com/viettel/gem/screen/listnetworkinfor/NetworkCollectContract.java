package com.viettel.gem.screen.listnetworkinfor;


import android.support.v7.widget.RecyclerView;

import com.viettel.bss.viettelpos.v4.object.ApParam;
import com.viettel.gem.base.viper.interfaces.IInteractor;
import com.viettel.gem.base.viper.interfaces.IPresenter;
import com.viettel.gem.base.viper.interfaces.PresentView;

import java.util.List;

/**
 * Created by root on 21/11/2017.
 */

interface NetworkCollectContract {

    interface Interactor extends IInteractor<Presenter> {
    }

    interface View extends PresentView<Presenter> {
        void setAdapter(RecyclerView.Adapter adapter);
    }

    interface Presenter extends IPresenter<View, Interactor> {
//        void doSearch(String isdn, String idno);
//        void showDialogNoData(String isdn, String idno);
        void fakeSearch();
        void collectInfor(String isdn, String idNo, String collectType, String title);
        void initListCollect();
        void collect();
    }
}
