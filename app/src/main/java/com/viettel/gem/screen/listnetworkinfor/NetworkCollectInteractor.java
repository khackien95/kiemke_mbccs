package com.viettel.gem.screen.listnetworkinfor;


import com.viettel.gem.base.viper.Interactor;

/**
 * Created by root on 21/11/2017.
 */

public class NetworkCollectInteractor extends Interactor<NetworkCollectContract.Presenter>
        implements NetworkCollectContract.Interactor {
    public NetworkCollectInteractor(NetworkCollectContract.Presenter presenter) {
        super(presenter);
    }
}
