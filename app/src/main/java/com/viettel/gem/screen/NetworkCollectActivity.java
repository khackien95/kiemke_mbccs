package com.viettel.gem.screen;

import com.viettel.gem.base.ContainerActivity;
import com.viettel.gem.base.viper.ViewFragment;
import com.viettel.gem.screen.listnetworkinfor.NetworkCollectPresenter;

/**
 * Created by Toancx on 3/23/2018.
 */

public class NetworkCollectActivity extends ContainerActivity{
    @Override
    public ViewFragment onCreateFirstFragment() {
        return (ViewFragment) new NetworkCollectPresenter(this).getFragment();
    }
}
