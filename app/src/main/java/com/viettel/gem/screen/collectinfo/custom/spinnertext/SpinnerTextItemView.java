package com.viettel.gem.screen.collectinfo.custom.spinnertext;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.StringUtils;
import com.viettel.bss.viettelpos.v4.work.object.ProductSpecCharValueDTOList;
import com.viettel.gem.base.view.CustomView;
import com.viettel.gem.screen.collectinfo.InputValueDialog;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by BaVV on 5/31/16.
 */
public class SpinnerTextItemView
        extends CustomView {

    @BindView(R.id.spnName)
    Spinner spnName;

    @BindView(R.id.edtInput)
    EditText edtInput;

    SpinnerTextBoxView mSpinnerTextBoxView;

    Activity mActivity;

    List<ProductSpecCharValueDTOList> mProductSpecCharValueDTOList;

    ProductSpecCharValueDTOList mSpecCharValueDTOList;

    private static final String AGE = "AGE";

    private Callback mCallback;

    private int position = 0;

    public SpinnerTextItemView(Context context) {
        super(context);
    }

    public SpinnerTextItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpinnerTextItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setPosition(int position){
        this.position = position;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.cs_survey_spinner_text_box_item_view;
    }

    @Override
    public boolean validateView() {
        if(CommonActivity.isNullOrEmpty(edtInput)){
            edtInput.setError(mActivity.getString(R.string.information_required));
            return false;
        }

        if(mSpinnerTextBoxView.productSpecCharDTO != null
                && AGE.equals(mSpinnerTextBoxView.productSpecCharDTO.getCode())){
            String year = edtInput.getText().toString();
            if(StringUtils.isDigit(year)){
                int nYear = Integer.valueOf(year);
                if(nYear < 1900 || nYear > Calendar.getInstance().get(Calendar.YEAR)) {
                    CommonActivity.toast(mActivity, mActivity.getString(R.string.birthday_invalid));
                    return false;
                }
            } else {
                CommonActivity.toast(mActivity, mActivity.getString(R.string.birthday_invalid));
                return false;
            }
        }
        return spnName.getSelectedItemPosition() != 0;
    }


    public void build(SpinnerTextBoxView spinnerTextBoxView, ProductSpecCharValueDTOList specCharValueDTOList, List<ProductSpecCharValueDTOList> productSpecCharValueDTOList, Callback callback, Activity mActivity) {
        mSpinnerTextBoxView = spinnerTextBoxView;
        this.mSpecCharValueDTOList = specCharValueDTOList;
        this.mProductSpecCharValueDTOList = productSpecCharValueDTOList;
        mCallback = callback;
        this.mActivity = mActivity;
        initSpinner();
    }

    boolean userSelect = false;

    private void initSpinner() {

        spnName.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                userSelect = true;
                return false;
            }
        });

        spnName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int position, long arg3) {
                try {
                    if (userSelect) {
                        ProductSpecCharValueDTOList specCharValueDTOList = mProductSpecCharValueDTOList.get(position);
                        if (null != mCallback &&
                                null != specCharValueDTOList) {
                            specCharValueDTOList.setValueData(mSpecCharValueDTOList.getValueData());
                            mSpecCharValueDTOList.setValueData(null);
                            mSpecCharValueDTOList = specCharValueDTOList;
                        }
                        userSelect = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        if (null != this.mProductSpecCharValueDTOList && !this.mProductSpecCharValueDTOList.isEmpty()) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    getContext(), R.layout.adapter_spinner_item_view, R.id.tvContent);
            adapter.setDropDownViewResource(R.layout.adapter_spinner_dropdown_view);

            for (ProductSpecCharValueDTOList specCharValueDTOList : mProductSpecCharValueDTOList) {
                adapter.add(specCharValueDTOList.getName());
            }
            spnName.setAdapter(adapter);
            if(mProductSpecCharValueDTOList.indexOf(mSpecCharValueDTOList) > 0){
                spnName.setSelection(mProductSpecCharValueDTOList.indexOf(mSpecCharValueDTOList));
            } else {
                spnName.setSelection(0);
            }
        }

        if(AGE.equals(mSpinnerTextBoxView.productSpecCharDTO.getCode()))
            edtInput.setInputType(InputType.TYPE_CLASS_NUMBER);

        try {
            edtInput.setText(this.mSpecCharValueDTOList.getValueData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.imvMinus)
    void minusRow(){
        mCallback.onRemove(position, this);
    }

    public ProductSpecCharValueDTOList getAnswerModel() {
        return mSpecCharValueDTOList;
    }

    public String getName() {
        return (String) spnName.getSelectedItem();
    }

    public interface Callback {
        void onRemove(int position, SpinnerTextItemView spinnerTextItemView);
    }
}
