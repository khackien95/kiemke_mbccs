package com.viettel.gem.screen.collectinfo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DataUtils;
import com.viettel.bss.viettelpos.v4.commons.PreferenceUtils;
import com.viettel.bss.viettelpos.v4.connecttionService.activity.FragmentSearchLocation;
import com.viettel.bss.viettelpos.v4.connecttionService.beans.AreaBean;
import com.viettel.bss.viettelpos.v4.object.ProductSpecificationDTO;
import com.viettel.gem.base.viper.ViewFragment;
import com.viettel.gem.screen.collectinfo.custom.GroupBoxView;
import com.viettel.gem.screen.collectinfo.custom.picture.PictureBoxView;
import com.viettel.gem.utils.PathUtils;

import java.net.URISyntaxException;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 */
public class CollectCustomerInfoFragment extends ViewFragment<CollectCustomerInfoContract.Presenter> implements CollectCustomerInfoContract.View {

    @BindView(R.id.back_iv)
    ImageView mBackIv;

    @BindView(R.id.title_tv)
    TextView mTitleTv;

    @BindView(R.id.action_iv)
    ImageView mActioniv;

    @BindView(R.id.svRoot)
    ScrollView svRoot;

    @BindView(R.id.contentRoot)
    LinearLayout contentRoot;

    @BindView(R.id.rlTitle)
    RelativeLayout rlTitle;

    @BindView(R.id.btnUpdate)
    Button btnUpdate;

    String province;

    String district;

    String address;

    public static CollectCustomerInfoFragment getInstance() {
        return new CollectCustomerInfoFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_collect_customer_info;
    }

    @Override
    public void initLayout() {
        super.initLayout();
    }

    @Override
    public void setTitle(String title) {
        mTitleTv.setText(title);
    }

    @Override
    public void addView(List<ProductSpecificationDTO> productSpecificationDTOList, String collectType) {
        if (null == productSpecificationDTOList || productSpecificationDTOList.isEmpty()) return;

        contentRoot.removeAllViews();

        for (ProductSpecificationDTO productSpecificationDTO : productSpecificationDTOList) {
            GroupBoxView groupBoxView = new GroupBoxView(getViewContext());
            groupBoxView.setIsdn(mPresenter.getIsdn());
            groupBoxView.setIdno(mPresenter.getIdno());
            groupBoxView.setAddress(mPresenter.getAddress());
            groupBoxView.setCreateNew(mPresenter.isCreateNew());
            groupBoxView.setCollectType(collectType);
            groupBoxView.build(productSpecificationDTO, getBaseActivity());
            contentRoot.addView(groupBoxView);
            mPresenter.put(groupBoxView);
        }

        contentRoot.clearFocus();

        btnUpdate.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTitle(boolean isShow) {
        rlTitle.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public ScrollView getScrollView() {
        return svRoot;
    }

    @OnClick({R.id.back_iv, R.id.title_tv, R.id.action_iv, R.id.btnUpdate})
    void onClickView(View view) {
        switch (view.getId()) {
            case R.id.back_iv:
                mPresenter.back();
                break;
            case R.id.action_iv:
            case R.id.btnUpdate:
                mPresenter.collect();
                break;
        }
    }

    @Override
    public String getTitle() {
        return mTitleTv.getText().toString();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String code = PreferenceUtils.get(getContext(), "mCode");
            switch (requestCode) {
                case PictureBoxView.CAPTURE_IMAGE:
                    mPresenter.resultPicture(null, code);
                    break;
                case PictureBoxView.PICK_FROM_FILE:
                    try {
                        Uri uri = data.getData();
                        String filePath = PathUtils.getPath(getViewContext(), uri);
                        if (null != filePath)
                            mPresenter.resultPicture(filePath, code);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    break;
                case Constant.REQUEST_CODE.GET_PROVINCE:
                    if (data != null) {
                        AreaBean areaBean = (AreaBean) data.getExtras().getSerializable("provinceKey");
                        address = areaBean.getNameProvince();

                        province = areaBean.getProvince();

                        Intent intent = new Intent(getActivity(), FragmentSearchLocation.class);
                        intent.putExtra("arrDistrictKey", DataUtils.initDistrict(getActivity(), province));
                        Bundle mBundle = new Bundle();
                        mBundle.putString("checkKey", "2");
                        intent.putExtras(mBundle);
                        startActivityForResult(intent, Constant.REQUEST_CODE.GET_DISTRICT);
                    }
                    break;
                case Constant.REQUEST_CODE.GET_DISTRICT:
                    if (data != null) {
                        AreaBean areaBean = (AreaBean) data.getExtras().getSerializable("districtKey");
                        district = areaBean.getDistrict();
                        address = areaBean.getNameDistrict() + "-" + address;

                        Intent intent = new Intent(getActivity(), FragmentSearchLocation.class);
                        intent.putExtra("arrPrecinctKey", DataUtils.initPrecinct(getActivity(), province, district));
                        Bundle mBundle = new Bundle();
                        mBundle.putString("checkKey", "3");
                        intent.putExtras(mBundle);
                        startActivityForResult(intent, Constant.REQUEST_CODE.GET_PRECINT);
                    }
                    break;
                case Constant.REQUEST_CODE.GET_PRECINT:
                    if (data != null) {
                        AreaBean areaBean = (AreaBean) data.getExtras().getSerializable("precinctKey");
                        address = areaBean.getNamePrecint() + "-" + address;
                        mPresenter.resultProvince(address, code);
                    }
                    break;
            }

        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }
}
