package com.viettel.gem.screen.collectinfo.custom;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.beans.ProductSpecCharDTO;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.object.ProductSpecCharValueDTO;
import com.viettel.bss.viettelpos.v4.object.ProductSpecificationDTO;
import com.viettel.bss.viettelpos.v4.work.object.ProductSpecCharUseDTO;
import com.viettel.bss.viettelpos.v4.work.object.ProductSpecCharValueDTOList;
import com.viettel.gem.base.view.CustomView;
import com.viettel.gem.common.Constants;
import com.viettel.gem.screen.collectinfo.custom.checkbox.CheckBoxView;
import com.viettel.gem.screen.collectinfo.custom.date.DateBoxView;
import com.viettel.gem.screen.collectinfo.custom.edittext.EditTextBoxView;
import com.viettel.gem.screen.collectinfo.custom.edittextplus.EditTextPlusBoxView;
import com.viettel.gem.screen.collectinfo.custom.picture.PictureBoxView;
import com.viettel.gem.screen.collectinfo.custom.province.ProvinceBoxView;
import com.viettel.gem.screen.collectinfo.custom.radio.RadioBoxView;
import com.viettel.gem.screen.collectinfo.custom.spinner.SpinnerBoxView;
import com.viettel.gem.screen.collectinfo.custom.spinnerspin.SpinnerSpinBoxView;
import com.viettel.gem.screen.collectinfo.custom.spinnertext.SpinnerTextBoxView;
import com.viettel.gem.screen.collectinfo.custom.textplus.TextPlusBoxView;
import com.viettel.gem.screen.collectinfo.custom.textview.TextBoxView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;


/**
 * Created by BaVV on 5/31/16.
 */
public class GroupBoxView
        extends CustomView {

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.boxRoot)
    LinearLayout boxRoot;

    @BindView(R.id.boxGroup)
    LinearLayout boxGroup;

    private Activity activity;

    String isdn;
    String idno;
    String address;
    String collectType; //loai thu thap
    boolean createNew;

    private ProductSpecificationDTO mProductSpecificationDTO;

    //    private Map<String, CustomView> mSelectedViews;
    List<CustomView> mSelectedViews = new ArrayList<>();

    public GroupBoxView(Context context) {
        super(context);
    }

    public GroupBoxView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GroupBoxView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.cs_survey_group_box_view;
    }

    public void build(ProductSpecificationDTO productSpecificationDTO, Activity activity) {
        this.activity = activity;
        boxGroup.removeAllViews();

        if (null != productSpecificationDTO) {
            mProductSpecificationDTO = productSpecificationDTO;

            tvName.setText(productSpecificationDTO.getName());
            List<ProductSpecCharUseDTO> lstProductSpecCharUseDTO = productSpecificationDTO.getLstProductSpecCharUseDTO();
            if (null != lstProductSpecCharUseDTO && !lstProductSpecCharUseDTO.isEmpty()) {
                //xu ly truong hop du lieu LIST_SPIN_SPIN_PLUS
                Map<String, List<ProductSpecCharValueDTOList>> mapProductSpecCharUseDTO = new HashMap<>();
                for (ProductSpecCharUseDTO productSpecCharUseDTO : lstProductSpecCharUseDTO) {
                    ProductSpecCharDTO productSpecCharDTO = productSpecCharUseDTO.getListProductSpecCharDTOs();
                    if(productSpecCharDTO.getValueType() == Constants.LIST_SPIN_SPIN_PLUS || productSpecCharDTO.getValueType() == Constants.LISTPLUSSTRING){
                        //lay gia tri thuoc tinh mapping
                        for (Iterator<ProductSpecCharValueDTOList> it = productSpecCharDTO.getProductSpecCharValueDTOList().iterator(); it.hasNext(); ){
                            ProductSpecCharValueDTOList productSpecCharValueDTOList = it.next();
                            if(productSpecCharValueDTOList.getName() != null
                                    && productSpecCharValueDTOList.getName().startsWith("MAPPING")){
                                if("MAPPING".equals(productSpecCharValueDTOList.getName())) {
                                    for (ProductSpecCharUseDTO productSpecCharUseDTO1 : lstProductSpecCharUseDTO) {
                                        if(productSpecCharUseDTO1.getListProductSpecCharDTOs().getCode().equals(productSpecCharValueDTOList.getId())){
                                            mapProductSpecCharUseDTO.put(productSpecCharDTO.getCode(), productSpecCharUseDTO1.getListProductSpecCharDTOs().getProductSpecCharValueDTOList());
                                            break;
                                        }
                                    }
                                } else {
                                    for (ProductSpecCharUseDTO productSpecCharUseDTO1 : lstProductSpecCharUseDTO) {
                                        if (productSpecCharUseDTO1.getListProductSpecCharDTOs().getCode().equals(productSpecCharValueDTOList.getId())) {
                                            mapProductSpecCharUseDTO.put(productSpecCharValueDTOList.getId(), productSpecCharUseDTO1.getListProductSpecCharDTOs().getProductSpecCharValueDTOList());
                                            break;
                                        }
                                    }
                                }

                                //remove thuoc tinh mapping truoc khi build giao dien
                                it.remove();
                            }
                        }
                    }
                }

                //duyet key remove
                for(String key : mapProductSpecCharUseDTO.keySet()){
                    for (Iterator<ProductSpecCharUseDTO> item =lstProductSpecCharUseDTO.iterator(); item.hasNext();) {
                        ProductSpecCharUseDTO productSpecCharUseDTO = item.next();
                        ProductSpecCharDTO productSpecCharDTO = productSpecCharUseDTO.getListProductSpecCharDTOs();
                        int valueType = productSpecCharDTO.getValueType();

                        if(!valueTypeValid(valueType)) {
                            if (productSpecCharUseDTO.getListProductSpecCharDTOs().getCode().equals(key)) {
                                item.remove();
                                break;
                            }
                        }
                    }
                }

                //duyet danh sach cau hoi
                for (ProductSpecCharUseDTO productSpecCharUseDTO : lstProductSpecCharUseDTO) {
                    /*
                    * productSpecCharDTO: 1 loai cau hoi
                    * */
                    ProductSpecCharDTO productSpecCharDTO = productSpecCharUseDTO.getListProductSpecCharDTOs();
                    //type xac dinh loai giao dien cau hoi
                    /*
                    *   STRING            1
                        DIGIT             2
                        TIME              3
                        BOOLEAN           4
                        LIST_ONE          5
                        LIST_MANY         6
                        TEXT_AREA         7
                        EDITOR            8
                        LIST_OR_TEXT      9
                        IMAGE             10
                        TREE              11
                        RADIO             12
                        ADDRESS           13
                        LISTPLUS          14
                        LISTSPINPLUS      15
                        CHECKBOX          16
                        BIRTHDAY         17
                        LISTPLUSSTRING 18
                        PROVINCE 19
                        LIST_SPIN_TEXT_PLUS 20
                        LIST_SPIN_SPIN_PLUS 21
                    * */
                    int valueType = productSpecCharDTO.getValueType();
                    switch (valueType) {
                        case Constants.STRING:
                        case Constants.DIGIT:
                            EditTextBoxView editTextView = new EditTextBoxView(getContext());
                            editTextView.setIsdn(isdn);
                            editTextView.setIdno(idno);
                            editTextView.setCreateNew(isCreateNew());
                            editTextView.build(productSpecCharDTO);
                            addCustomView(editTextView);
                            break;
                        case Constants.TIME:
                        case Constants.BIRTHDAY:
                            DateBoxView dateBoxView = new DateBoxView(getContext());
                            dateBoxView.build(productSpecCharDTO);
                            addCustomView(dateBoxView);
                            break;
                        case Constants.IMAGE:
                            PictureBoxView pictureBoxView = new PictureBoxView(getContext());
                            pictureBoxView.setIsdn(getIsdn());
                            pictureBoxView.build(productSpecCharDTO, activity);
                            addCustomView(pictureBoxView);
                            break;
                        case Constants.RADIO:
                            RadioBoxView radioBoxView = new RadioBoxView(getContext());
                            radioBoxView.build(productSpecCharDTO);
                            addCustomView(radioBoxView);
                            break;
                        case Constants.ADDRESS:
                            EditTextBoxView addressTextView = new EditTextBoxView(getContext());
                            addressTextView.setAdddress(address);
                            addressTextView.build(productSpecCharDTO);
                            addCustomView(addressTextView);
                            break;
                        case Constants.LISTPLUSNUMBER:
                            TextBoxView textBoxView = new TextBoxView(getContext());
                            textBoxView.setRequired(isRequired());
                            textBoxView.build(productSpecCharDTO);
                            addCustomView(textBoxView);
                            break;
                        case Constants.LISTSPINPLUS:
                            SpinnerBoxView spinnerBoxView = new SpinnerBoxView(getContext());
                            spinnerBoxView.setRequired(isRequired());
                            spinnerBoxView.build(productSpecCharDTO);
                            addCustomView(spinnerBoxView);
                            break;
                        case Constants.CHECKBOX:
                            CheckBoxView checkBoxView = new CheckBoxView(getContext());
                            checkBoxView.setRequired(isRequired());
                            checkBoxView.build(productSpecCharDTO);
                            addCustomView(checkBoxView);
                            break;
                        case Constants.LISTPLUSSTRING:
                            EditTextPlusBoxView editTextPlusBoxView = new EditTextPlusBoxView(getContext());
                            editTextPlusBoxView.setRequired(isRequired());
                            editTextPlusBoxView.build(productSpecCharDTO, mapProductSpecCharUseDTO);
                            addCustomView(editTextPlusBoxView);
                            break;
                        case Constants.PROVINCE:
                            ProvinceBoxView provinceBoxView = new ProvinceBoxView(getContext());
                            provinceBoxView.build(productSpecCharDTO, activity);
                            addCustomView(provinceBoxView);
                            break;
                        case Constants.LIST_SPIN_TEXT_PLUS:
                            SpinnerTextBoxView spinnerTextBoxView = new SpinnerTextBoxView(getContext());
                            spinnerTextBoxView.setRequired(isRequired());
                            spinnerTextBoxView.build(productSpecCharDTO, activity);
                            addCustomView(spinnerTextBoxView);
                            break;
                        case Constants.LIST_SPIN_SPIN_PLUS:
                            SpinnerSpinBoxView spinnerSpinBoxView = new SpinnerSpinBoxView(getContext());
                            spinnerSpinBoxView.setRequired(isRequired());
                            spinnerSpinBoxView.build(productSpecCharDTO, mapProductSpecCharUseDTO.get(productSpecCharDTO.getCode()), activity);
                            addCustomView(spinnerSpinBoxView);
                            break;
                        case Constants.TEXTPLUS:
                            TextPlusBoxView textPlus = new TextPlusBoxView(getContext());
                            textPlus.setRequired(isRequired());
                            textPlus.build(productSpecCharDTO);
                            addCustomView(textPlus);
                            break;
                        default:
                            break;
                    }
                }
            }

        }
    }

    private boolean valueTypeValid(int valueType){
        switch (valueType){
            case Constants.STRING:
            case Constants.DIGIT:
            case Constants.BIRTHDAY:
            case Constants.TIME:
            case Constants.IMAGE:
            case Constants.RADIO:
            case Constants.ADDRESS:
            case Constants.LISTPLUSNUMBER:
            case Constants.LISTSPINPLUS:
            case Constants.CHECKBOX:
            case Constants.LISTPLUSSTRING:
            case Constants.PROVINCE:
            case Constants.LIST_SPIN_TEXT_PLUS:
            case Constants.LIST_SPIN_SPIN_PLUS:
            case Constants.TEXTPLUS:
                return true;
            default:
                return false;
        }
    }

    public void addCustomView(CustomView customView) {
        boxGroup.addView(customView);
        mSelectedViews.add(customView);
    }

    public boolean validateView() {
        boolean flag = true;
        if(isRequired()) {
            for (CustomView customView : mSelectedViews) {
                if (null != customView) {
                    flag = customView.validateView();
                    if (!flag) {
                        break;
                    }
                }
            }
        } else {
            flag = false;
            for (CustomView customView : mSelectedViews) {
                if (null != customView) {
                    if(customView.validateView()){
                        if(!flag) flag = customView.validateView();
                    }
                }
            }

            if(flag){
                for (CustomView customView : mSelectedViews) {
                    customView.removeError();
                }
            }
        }

        if (!flag) {
            this.requestFocus();
            boxRoot.setBackgroundResource(R.drawable.bg_group_view_error);
        } else {
            boxRoot.setBackgroundResource(R.drawable.bg_group_view_normal);
        }

        return flag;
    }

    @Override
    public void removeError() {
        super.removeError();
        boxRoot.setBackgroundResource(R.drawable.bg_group_view_normal);
    }

    public boolean isRequired(){
        return Constant.RULE_TTTTKH_CA_NHAN.equals(collectType)
                || CommonActivity.isNullOrEmpty(collectType);
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getIdno() {
        return idno;
    }

    public GroupBoxView setIdno(String idno) {
        this.idno = idno;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public GroupBoxView setAddress(String address) {
        this.address = address;
        return this;
    }

    public boolean isCreateNew() {
        return createNew;
    }

    public GroupBoxView setCreateNew(boolean createNew) {
        this.createNew = createNew;
        return this;
    }

    public void resultPicture(String filePath, String code) {
        for (CustomView customView : mSelectedViews) {
            if (customView instanceof PictureBoxView) {
                ((PictureBoxView) customView).onResult(filePath, code);
            }
        }
    }

    public void resultProvince(String value, String code){
        for(CustomView customView : mSelectedViews){
            if(customView instanceof ProvinceBoxView){
                ((ProvinceBoxView) customView).onResult(value, code);
            }
        }
    }


    public String getTitle(){
        return tvName.getText().toString().trim();
    }

    public String getCollectType() {
        return collectType;
    }

    public GroupBoxView setCollectType(String collectType) {
        this.collectType = collectType;
        return this;
    }
}
