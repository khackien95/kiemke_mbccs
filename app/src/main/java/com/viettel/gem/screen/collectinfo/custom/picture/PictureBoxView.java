package com.viettel.gem.screen.collectinfo.custom.picture;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.beans.ProductSpecCharDTO;
import com.viettel.bss.viettelpos.v4.BuildConfig;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.FileUtils;
import com.viettel.bss.viettelpos.v4.commons.PreferenceUtils;
import com.viettel.bss.viettelpos.v4.customview.obj.FileObj;
import com.viettel.gem.base.view.CustomView;
import com.viettel.gem.screen.collectinfo.TakePictureDialog;
import com.viettel.gem.screen.picturedetail.PictureDetailActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by root on 23/11/2017.
 */

public class PictureBoxView extends CustomView implements PictureItemView.Callback {

    private static final int REQUEST_PERMISSIONS_CAMERA = 1;
    public static final int CAPTURE_IMAGE = 234;
    public static final int PICK_FROM_FILE = 345;

    Activity mActivity;

    Uri capturedImageUri;

    int mode = -1;//1 - Chup 2 - Gallery

    String pictureAvatarTempPath;

    String isdn;

    String collectType;

    @BindView(R.id.hsRoot)
    HorizontalScrollView hsRoot;

    @BindView(R.id.picture_box)
    LinearLayout mPictureBox;

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.layoutTakePhoto)
    RelativeLayout layoutTakePhoto;

    ProductSpecCharDTO productSpecCharDTO;

    private List<String> filePathList = new ArrayList<>();

    private HashMap<String, ArrayList<FileObj>> hashmapFileObj = new HashMap<>();

    public PictureBoxView(Context context) {
        super(context);
    }

    public void build(ProductSpecCharDTO productSpecCharDTO, Activity activity) {
        mActivity = activity;
        this.productSpecCharDTO = productSpecCharDTO;
        if (null == this.productSpecCharDTO) return;

        tvName.setText(productSpecCharDTO.getName());
        mPictureBox.removeAllViews();

        File f = new File(Environment.getExternalStorageDirectory(), "/mBCCS");
        if (!f.exists()) {
            f.mkdir();
        }

        File f1 = new File(Environment.getExternalStorageDirectory(), "/mBCCS/" + getIsdn());
        if (!f1.exists()) {
            f1.mkdir();
        } else {
            FileUtils.deleteDir(f1);
        }

        File f2 = new File(Environment.getExternalStorageDirectory(), "/mBCCS/" + getIsdn() + "/zip");
        if (!f2.exists()) {
            f2.mkdir();
        } else {
            FileUtils.deleteDir(f2);
        }

        String base64 = productSpecCharDTO.getValueData();
        if (null != base64) {
            byte[] decodedString = Base64.decode(base64, Activity.TRIM_MEMORY_BACKGROUND);
            File zipFile = writeBytesToFile(decodedString);
            try {

                File targetDirectory = new File(Environment.getExternalStorageDirectory(), "/mBCCS/" + getIsdn() + "/images");
                if (!targetDirectory.exists()) {
                    targetDirectory.mkdir();
                } else {
                    FileUtils.deleteDir(targetDirectory);
                }
                FileUtils.unzip(zipFile, targetDirectory);

                List<File> fileList = FileUtils.getListFiles(targetDirectory);
                if (!CommonActivity.isNullOrEmpty(fileList)) {
                    for (File file : fileList) {
                        filePathList.add(file.getAbsolutePath());
                        PictureItemView pictureItemView = new PictureItemView(getContext());
                        pictureItemView.setPath(file.getAbsolutePath());

                        pictureItemView.build(file.getAbsolutePath(), this);

                        mPictureBox.addView(pictureItemView);
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private File writeBytesToFile(byte[] bFile) {

        File folder = new File(Environment.getExternalStorageDirectory(), "/mBCCS/" + getIsdn() + "/unzip");
        if (!folder.exists()) {
            folder.mkdir();
        } else {
            FileUtils.deleteDir(folder);
        }

        String fileZipPath = Environment.getExternalStorageDirectory() + "/mBCCS/" + getIsdn() + "/unzip" + File.separator
                + "image_collect_unzip.zip";

        FileOutputStream fileOuputStream = null;

        try {
            fileOuputStream = new FileOutputStream(fileZipPath);
            fileOuputStream.write(bFile);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOuputStream != null) {
                try {
                    fileOuputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return new File(fileZipPath);
    }

    @OnClick(R.id.layoutTakePhoto)
    void takePhoto() {
        requestPermission();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.cs_survey_picture_box_view;
    }

    @Override
    public boolean validateView() {
        boolean valid = null != this.filePathList && !this.filePathList.isEmpty();
        if (valid) {
            List<File> listZip = new ArrayList<>();
            for (String path : filePathList) {
                listZip.add(new File(path));
            }

            String base64 = "";
            File folder = new File(Environment.getExternalStorageDirectory(), "/mBCCS/" + getIsdn() + "/zip");
            if (!folder.exists()) {
                folder.mkdir();
            }

            String fileZipPath = Environment.getExternalStorageDirectory() + "/mBCCS/" + getIsdn() + "/zip" + File.separator
                    + "image_collect.zip";

            File isdnZip = FileUtils.zip(listZip, fileZipPath);
            try {
                byte[] bytes = FileUtils.fileToBytes(isdnZip);
                base64 = Base64.encodeToString(bytes, Activity.TRIM_MEMORY_BACKGROUND);
                productSpecCharDTO.setValueData(base64);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            productSpecCharDTO.setValueData(null);
        }

        return valid;
    }

    @Override
    public void onPictureClick(String url) {
        Intent intent = new Intent(getContext(), PictureDetailActivity.class);
        Bundle b = new Bundle();
        b.putString("url", url);
        intent.putExtras(b);
        getContext().startActivity(intent);
    }

    @Override
    public void onRemoveClick(View view) {
        mPictureBox.removeView(view);
        mPictureBox.invalidate();

        deletePictureView((PictureItemView) view);
    }

    public PictureBoxView setCollectType(String collectType){
        this.collectType = collectType;
        return this;
    }

    public void takePicture() {
        TakePictureDialog dialog = new TakePictureDialog(mActivity)
                .setListener(new TakePictureDialog.GetAvatarListener() {
                    @Override
                    public void fromCam() {
                        mode = 1;
                        File dirToSaveFile = new File(Environment.getExternalStorageDirectory(), "/mBCCS/" + getIsdn() + "/temps");
                        if (!dirToSaveFile.exists()) {
                            dirToSaveFile.mkdir();
                        } else {
                            FileUtils.deleteDir(dirToSaveFile);
                        }
                        pictureAvatarTempPath = Environment.getExternalStorageDirectory() + "/mBCCS/" + getIsdn() + "/temps/picture_temp_" + System.currentTimeMillis() + ".jpg";
                        File destination = new File(pictureAvatarTempPath);
                        if (!destination.exists()) {
                            try {
                                destination.createNewFile();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            destination.delete();
                            try {
                                destination.createNewFile();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            capturedImageUri = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".fileprovider", destination);
                        } else {
                            capturedImageUri = Uri.fromFile(destination);
                        }

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);
                        PreferenceUtils.save(getContext(), "mCode", productSpecCharDTO.getCode());
                        mActivity.startActivityForResult(intent, CAPTURE_IMAGE);
                    }

                    public void fromGal() {
                        mode = 2;
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);

                        PreferenceUtils.save(getContext(), "mCode", productSpecCharDTO.getCode());
                        mActivity.startActivityForResult(Intent.createChooser(intent, "Choose Picture"), PICK_FROM_FILE);
                    }
                });
        dialog.show();
    }

    public void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (!mActivity.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA}, REQUEST_PERMISSIONS_CAMERA);
                } else {
                    Snackbar.make(mActivity.findViewById(android.R.id.content), getResources().getString(R.string.permisson_explain_camera), Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
                            intent.setData(uri);
                            mActivity.startActivity(intent);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        }
                    }).show();
                }
            } else {
                takePicture();

            }
        } else {
            takePicture();
        }
    }

    public void onResult(String filePath, String code) {
        Log.d("onResult", code);
        if(productSpecCharDTO.getCode() != null && productSpecCharDTO.getCode().equals(code)) {
            PictureItemView pictureItemView = new PictureItemView(getContext());
            switch (mode) {
                case 1:
                    addPictureView(pictureAvatarTempPath, pictureItemView);
                    break;
                case 2:
                    addPictureView(filePath, pictureItemView);
                    break;
                default:
                    break;
            }
        }
    }

    void addPictureView(String filePath, PictureItemView pictureItemView) {
        File oldFile = new File(filePath);
        String fileZipPath = Environment.getExternalStorageDirectory() + "/mBCCS/" + getIsdn() + "/zip" + File.separator
                + "image_zip_" + System.currentTimeMillis() + ".jpg";
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(fileZipPath);
            long length = oldFile.length();
            int percent = length < Constant.MAX_SIZE_IMG ? 100
                    : (int) (Constant.MAX_SIZE_IMG * 100 / length);
            Bitmap bitmap;
            if (oldFile.length() < Constant.MAX_SIZE_IMG) {
                bitmap = BitmapFactory.decodeFile(oldFile.getPath());
            } else {
                bitmap = FileUtils.decodeBitmapFromFile2(oldFile
                        .getPath(), 500, 500);
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] buffer = baos.toByteArray();
            fos.write(buffer);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        filePathList.add(fileZipPath);

        pictureItemView.build(fileZipPath, this);
        mPictureBox.addView(pictureItemView);

        hsRoot.postDelayed(new Runnable() {
            public void run() {
                hsRoot.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        }, 100L);
    }

    void deletePictureView(PictureItemView pictureItemView) {
        if (null == this.filePathList || this.filePathList.isEmpty())
            return;

        if (this.filePathList.contains(pictureItemView.getPath())) {
            this.filePathList.remove(pictureItemView.getPath());
        }
    }

    public String getIsdn() {
        return CommonActivity.isNullOrEmpty(isdn) ? "test" : productSpecCharDTO.getCode();
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }
}
