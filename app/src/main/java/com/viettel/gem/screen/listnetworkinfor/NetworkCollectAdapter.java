package com.viettel.gem.screen.listnetworkinfor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.bo.ApParamBO;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.object.ApParam;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Toancx on 3/27/2018.
 */

public class NetworkCollectAdapter extends RecyclerView.Adapter<NetworkCollectAdapter.NetworkCollectHolder>{

    private Context mContext;
    private List<ApParamBO> mApParamList;
    private NetworkCollectClickListener collectClickListener;

    public NetworkCollectAdapter(Context mContext, List<ApParamBO> mApParamList, NetworkCollectClickListener collectClickListener){
        this.mContext = mContext;
        this.mApParamList = mApParamList;
        this.collectClickListener = collectClickListener;
    }

    @Override
    public NetworkCollectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NetworkCollectHolder(LayoutInflater.from(mContext).inflate(R.layout.network_collect_item, parent, false));
    }

    @Override
    public void onBindViewHolder(NetworkCollectHolder holder, int position) {
        ApParamBO apParam = mApParamList.get(position);
        if(CommonActivity.isNullOrEmpty(apParam)){
            return;
        }

        holder.tvName.setText(apParam.getParValue());
        if(apParam.getParValue().equals(mContext.getString(R.string.txt_collect_kit_info))
                || apParam.getParValue().equals(mContext.getString(R.string.txt_collect_product_sale_most))){
            holder.tvRequired.setVisibility(View.VISIBLE);
        } else {
            holder.tvRequired.setVisibility(View.GONE);
        }

        if(apParam.isCollect()){
            holder.imgIcon.setImageResource(R.drawable.ic_gem_collectinfor_infor_green);
        } else {
            holder.imgIcon.setImageResource(R.drawable.next_black);
        }
    }

    @Override
    public int getItemCount() {
        return mApParamList == null ? 0 : mApParamList.size();
    }

    public class NetworkCollectHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvRequired)
        TextView tvRequired;
        @BindView(R.id.imgIcon)
        ImageView imgIcon;

        public NetworkCollectHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    collectClickListener.onClick(itemView, getAdapterPosition());
                }
            });
        }
    }

    public interface NetworkCollectClickListener{
        void onClick(View view, int position);
    }
}
