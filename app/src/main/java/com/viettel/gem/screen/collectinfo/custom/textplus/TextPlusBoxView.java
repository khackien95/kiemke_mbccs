package com.viettel.gem.screen.collectinfo.custom.textplus;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.beans.ProductSpecCharDTO;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.work.object.ProductSpecCharValueDTOList;
import com.viettel.gem.base.view.CustomView;
import com.viettel.gem.screen.collectinfo.CollectCustomerInfoPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


/**
 * Created by BaVV on 11/21/17.
 */
public class TextPlusBoxView
        extends CustomView
        implements TextPlusItemView.Callback {

    public final static String NUMBER_FAMILY_KEY = "FAMILY_NUMBER";
    public final static String SER_VT_NUMBER = "SER_VT_NUMBER";
    public final static String QUATITY_NETWORK = "QUATITY_NETWORK";

    @BindView(R.id.boxInput)
    LinearLayout boxInput;

    @BindView(R.id.tvName)
    TextView tvName;

    boolean required = true; //true bat buoc tat ca, false khong bat buoc tat ca

    List<TextPlusItemView> textItemViewList = new ArrayList<>();

    ProductSpecCharDTO productSpecCharDTO;

    public TextPlusBoxView(Context context) {
        super(context);
    }

    public TextPlusBoxView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextPlusBoxView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.cs_survey_text_plus_box_view;
    }

    @Override
    public boolean validateView() {
        boolean flag = false;
        for (TextPlusItemView textItemView : textItemViewList) {
            if (textItemView.validateView()) {
                flag = true;
                break;
            }
        }

        return flag;
    }

    public TextPlusBoxView setRequired(boolean required){
        this.required = required;
        return this;
    }

    public void build(ProductSpecCharDTO productSpecCharDTO) {
        this.productSpecCharDTO = productSpecCharDTO;
        if (null == this.productSpecCharDTO) return;

        tvName.setText(this.productSpecCharDTO.getName());

        boxInput.removeAllViews();

        List<ProductSpecCharValueDTOList> productSpecCharValueDTOList = productSpecCharDTO.getProductSpecCharValueDTOList();

        if (null != productSpecCharValueDTOList && !productSpecCharValueDTOList.isEmpty()) {
            for (ProductSpecCharValueDTOList specCharValueDTOList : productSpecCharValueDTOList) {
                TextPlusItemView itemView = new TextPlusItemView(getContext());

                itemView.build(this, specCharValueDTOList, this);
                boxInput.addView(itemView);
                textItemViewList.add(itemView);
            }
        }
    }

    public void calCount() {
        int totalCount = 0;
        if (null == productSpecCharDTO || (!NUMBER_FAMILY_KEY.equals(productSpecCharDTO.getCode())
                && !SER_VT_NUMBER.equals(productSpecCharDTO.getCode()) && !QUATITY_NETWORK.equals(productSpecCharDTO.getCode())))
            return;
        List<ProductSpecCharValueDTOList> productSpecCharValueDTOList = productSpecCharDTO.getProductSpecCharValueDTOList();

        if (null != productSpecCharValueDTOList && !productSpecCharValueDTOList.isEmpty()) {
            for (ProductSpecCharValueDTOList specCharValueDTOList : productSpecCharValueDTOList) {
                int count = 0;
                try {
                    count = null == specCharValueDTOList.getValueData() ? 0 : Integer.parseInt(specCharValueDTOList.getValueData());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                totalCount += count;
            }
        }
        if(NUMBER_FAMILY_KEY.equals(productSpecCharDTO.getCode()))
            CollectCustomerInfoPresenter.mTotalMember = totalCount;

        if(SER_VT_NUMBER.equals(productSpecCharDTO.getCode()))
            CollectCustomerInfoPresenter.mServiceVT = totalCount;

        if(QUATITY_NETWORK.equals(productSpecCharDTO.getCode()))
            CollectCustomerInfoPresenter.mServiceMB = totalCount;
    }
}
