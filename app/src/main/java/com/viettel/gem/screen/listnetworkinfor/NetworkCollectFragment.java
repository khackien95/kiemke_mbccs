package com.viettel.gem.screen.listnetworkinfor;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.object.ApParam;
import com.viettel.gem.base.viper.ViewFragment;
import com.viettel.gem.screen.event.CollectEvent;
import com.viettel.gem.utils.RecyclerUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by root on 21/11/2017.
 */

public class NetworkCollectFragment extends ViewFragment<NetworkCollectContract.Presenter>
        implements NetworkCollectContract.View {

    @BindView(R.id.rvCollect)
    RecyclerView rvCollect;

    public static NetworkCollectFragment getInstance() {
        return new NetworkCollectFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_network_collect;
    }

    @Override
    public void initLayout() {
        super.initLayout();
        RecyclerUtils.setupVerticalRecyclerView(getContext(), rvCollect);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        rvCollect.setAdapter(adapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageCollectEvent(CollectEvent event){
        Log.d("NetworkCollectFragment", "onMessage");
        EventBus.getDefault().removeStickyEvent(event);
        getPresenter().collect();
    }

    //    void checkCondition() {
//        String isdn = mNumberAccEdt.getText().toString().trim();
//        String idno = mNumberExhibitEdt.getText().toString().trim();
//
//        if ("".equals(isdn) &&
//                "".equals(idno)) {
//            Toast.makeText(getViewContext(), R.string.check_condition_toast, Toast.LENGTH_SHORT).show();
//        } else {
//            if (isMatch(isdn) && isMatch(idno))
//                mPresenter.doSearch(isdn, idno);
//            else
//                Toast.makeText(getViewContext(), R.string.msg_isdn_or_idno_invalid, Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    public boolean isMatch(String s) {
//        if (s == null || s.trim().isEmpty()) {
//            return true;
//        }
//        Pattern p = Pattern.compile("^[A-Za-z0-9_]{1,50}$");
//        Matcher m = p.matcher(s);
//
//        return m.matches();
//
//    }
}
