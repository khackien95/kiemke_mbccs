package com.viettel.gem.screen.collectinfo.custom.edittextplus;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.StringUtils;
import com.viettel.bss.viettelpos.v4.work.object.ProductSpecCharValueDTOList;
import com.viettel.gem.base.view.CustomView;
import com.viettel.gem.screen.collectinfo.InputValueDialog;
import com.viettel.gem.screen.collectinfo.custom.textview.TextBoxView;
import com.viettel.gem.screen.collectinfo.custom.textview.TextItemView;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by Toancx on 3/23/2018.
 */

public class EditTextPlusItemtView extends CustomView {

    //NUMBER_FAMILY
    //CUS_INFO_JOB_GROUP

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.edtInput)
    EditText edtInput;

    @BindView(R.id.spnValue)
    Spinner spnValue;

    @BindView(R.id.lnContain)
    LinearLayout lnContain;

    EditTextPlusBoxView mEditTextPlusBoxView;

    ProductSpecCharValueDTOList answerModel;

    List<ProductSpecCharValueDTOList> productSpecCharValueDTOLists;

    private EditTextPlusItemtView.Callback mCallback;

    int count = 0;

    public EditTextPlusItemtView(Context context) {
        super(context);
    }

    public EditTextPlusItemtView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditTextPlusItemtView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.cs_survey_editext_plus_box_item_view;
    }

    @Override
    public boolean validateView() {
        if(isSpinner()){
            if(spnValue.getSelectedItemPosition() == 0)
                return false;
        } else {
            if (edtInput.getVisibility() == View.GONE) {
                return true;
            }

            String content = edtInput.getText().toString().trim();
            if (content.isEmpty()) {
                edtInput.setError("Bạn chưa nhập " + answerModel.getName());
                return false;
            } else {
                edtInput.setError(null);
                edtInput.clearFocus();
            }
        }

        if(isSpinner()){
            answerModel.setValueData(spnValue.getSelectedItem().toString());
        } else {
            answerModel.setValueData(edtInput.getText().toString().trim());
        }
        return true;
    }

    @Override
    public void removeError() {
        super.removeError();
        edtInput.setError(null);
        edtInput.clearFocus();
    }

    public boolean ignoredValidateView(){
        return edtInput.getVisibility() == View.GONE && spnValue.getVisibility() == View.GONE;
    }

    private boolean isSpinner(){
        return !CommonActivity.isNullOrEmpty(productSpecCharValueDTOLists);
    }

    public void build(EditTextPlusBoxView editTextPlusBoxView, final ProductSpecCharValueDTOList answerModel, List<ProductSpecCharValueDTOList> productSpecCharValueDTOLists, EditTextPlusItemtView.Callback callback) {
        mEditTextPlusBoxView = editTextPlusBoxView;
        this.answerModel = answerModel;
        mCallback = callback;
        tvName.setText(answerModel.getName());
        this.productSpecCharValueDTOLists = productSpecCharValueDTOLists;

        edtInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String value = edtInput.getText().toString().trim();
                if(answerModel.getId().startsWith("NUM10") || answerModel.getId().startsWith("NUM11")){
                    // chi cho phep nhap so hoac ,
                    String partern = "0123456789.,";
                    if(!partern.matches(value)) {
                        if(value.length() > 1) {
                            value.substring(0, value.length() - 2);
                        } else {
                            value = "";
                        }
                    }
                }
                answerModel.setValueData(value);

                String valueNumber = StringUtils.getTextDefault(edtInput, "[%s.]");
                if(StringUtils.isDigit(valueNumber)){
                    edtInput.removeTextChangedListener(this);
                    edtInput.setText(StringUtils.formatMoney(valueNumber));
                    edtInput.setSelection(edtInput.getText().length());
                    edtInput.addTextChangedListener(this);
                }
            }
        });

        if(!CommonActivity.isNullOrEmpty(answerModel.getValueData())){
            edtInput.setText(answerModel.getValueData());
        }

        if(!answerModel.getId().startsWith("TITLE")){
            edtInput.setVisibility(View.VISIBLE);
            int padding = getResources().getDimensionPixelOffset(R.dimen.activity_horizontal_margin);
            tvName.setPadding(padding, 0, 0, 0);
        } else {
            edtInput.setVisibility(View.GONE);
        }

        if(isSpinner()){
            edtInput.setVisibility(View.GONE);
            spnValue.setVisibility(View.VISIBLE);

            int padding = getContext().getResources().getDimensionPixelOffset(R.dimen.step_pager_tab_spacing);
            lnContain.setPadding(padding, 0, padding, padding);

            //cai dat adapter
            if (null != this.productSpecCharValueDTOLists && !this.productSpecCharValueDTOLists.isEmpty()) {
                ArrayAdapter<String> adapter = new ArrayAdapter<>(
                        getContext(), R.layout.adapter_spinner_item_view, R.id.tvContent);
                adapter.setDropDownViewResource(R.layout.adapter_spinner_dropdown_view);
                adapter.add(getContext().getString(R.string.spinnerSelectValue));
                int selection = 0;
                int index = 1;
                for (ProductSpecCharValueDTOList specCharValueDTOList : productSpecCharValueDTOLists) {
                    adapter.add(specCharValueDTOList.getName());
                    if(specCharValueDTOList.getName().equals(answerModel.getValueData()))
                        selection = index;
                    index++;
                }
                spnValue.setAdapter(adapter);
                spnValue.setSelection(selection);
            }
        }
//        else {
//            if(answerModel.getId().startsWith("NUM10") || answerModel.getId().startsWith("NUM11")){
////                edtInput.setInputType(InputType.TYPE_CLASS_NUMBER);
//                edtInput.setKeyListener(DigitsKeyListener.getInstance("0123456789.,"));
//            }
//        }
    }


    public String getInput() {
        return edtInput.getText().toString().trim();
    }


    public interface Callback {
    }
}


