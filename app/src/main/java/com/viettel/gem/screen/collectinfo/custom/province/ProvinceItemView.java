package com.viettel.gem.screen.collectinfo.custom.province;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DataUtils;
import com.viettel.bss.viettelpos.v4.commons.PreferenceUtils;
import com.viettel.bss.viettelpos.v4.connecttionService.activity.FragmentSearchLocation;
import com.viettel.gem.base.view.CustomView;
import com.viettel.gem.common.Constants;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by BaVV on 5/31/16.
 */
public class ProvinceItemView
        extends CustomView {

    private static final String ISDN = "ISDN";
    private static final String ID_NO = "ID_NO";
    private static final String ADDRESS = "CUST_INSTRUCTRUE";

    Activity mActivity;

    @BindView(R.id.edtInput)
    EditText edtInput;

    String mTitle;

    String mCode;

    Callback mCallback;

    public ProvinceItemView(Context context) {
        super(context);
    }

    public ProvinceItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProvinceItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.cs_survey_province_box_item_view;
    }

    @Override
    public boolean validateView() {
        String content = edtInput.getText().toString().trim();
        if (content.isEmpty()) {
            edtInput.setError("Bạn chưa nhập " + mTitle);
            return false;
        } else {
            edtInput.setError(null);
            edtInput.clearFocus();
        }

        if (null != mCallback) {
            mCallback.onTextChanged(edtInput.getText().toString());
        }
        return true;
    }

    @Override
    public void removeError() {
        super.removeError();
        edtInput.setError(null);
        edtInput.clearFocus();
    }

    @OnClick(R.id.edtInput)
    public void onClickEdtInput(){
        Intent intent = new Intent(getContext(), FragmentSearchLocation.class);
        intent.putExtra("arrProvincesKey", DataUtils.getProvince(getContext()));
        Bundle mBundle = new Bundle();
        mBundle.putString("checkKey", "1");
        intent.putExtras(mBundle);

        PreferenceUtils.save(getContext(), "mCode", mCode);
        mActivity.startActivityForResult(intent, Constant.REQUEST_CODE.GET_PROVINCE);
    }

    public void build(String code, String name, Activity mActivity, Callback mCallback) {
        this.mActivity = mActivity;
        this.mCode = code;
        this.edtInput.setText(name);
        this.mCallback = mCallback;
    }

    public String getInput() {
        return edtInput.getText().toString().trim();
    }

    public interface Callback {
        void onTextChanged(String text);
    }
}
