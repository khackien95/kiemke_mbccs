package com.viettel.gem.screen.collectinfo.custom.spinnerspin;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.beans.ProductSpecCharDTO;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.work.object.ProductSpecCharValueDTOList;
import com.viettel.gem.base.view.CustomView;
import com.viettel.gem.screen.collectinfo.CollectCustomerInfoPresenter;
import com.viettel.gem.screen.collectinfo.custom.spinnertext.SpinnerTextItemView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by BaVV on 5/31/16.
 */
public class SpinnerSpinBoxView
        extends CustomView
        implements SpinnerSpinItemView.Callback {

    public static final String ID_DEFAULT = "chon_mot_linh_vuc_lam_viec";

    public static final String LINH_VUC_LAM_VIEC_KEY = "JOB";

    public static final String PERSON_JOB = "PERSON_JOB";  //anh chi lam viec trong linh vuc gi

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.boxSpinner)
    LinearLayout boxSpinner;

    Activity mActivity;

    private static final String SPLIT = "@@@";

    boolean required = true; //true bat buoc tat ca, false khong bat buoc tat ca

    ProductSpecCharDTO productSpecCharDTO;

    List<ProductSpecCharValueDTOList> productSpecCharValueDTOList = new ArrayList<>();
    List<ProductSpecCharValueDTOList> productSpecCharValueDTOListValue = new ArrayList<>();

    private List<SpinnerSpinItemView> spinnerSpinItemViews;

    public SpinnerSpinBoxView(Context context) {
        super(context);
    }

    public SpinnerSpinBoxView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpinnerSpinBoxView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.cs_survey_spinner_spin_box_view;
    }

    @Override
    public boolean validateView() {
        if(PERSON_JOB.equals(productSpecCharDTO.getCode())){
            if(CollectCustomerInfoPresenter.mTotalMember < spinnerSpinItemViews.size()){
                CommonActivity.toast(mActivity, mActivity.getString(R.string.number_invalid));
                return false;
            }
        }

        boolean flag = false;
        for (SpinnerSpinItemView spinnerSpinItemView : spinnerSpinItemViews) {
            if (spinnerSpinItemView.validateView()) {
                flag = true;
                break;
            }
        }

        if(flag){
            buildProductSpecCharValueDTOList();
        }

        return flag;
    }

    private void buildProductSpecCharValueDTOList(){
        //clear data truoc khi rebuild
        for(ProductSpecCharValueDTOList specCharValueDTOList : productSpecCharDTO.getProductSpecCharValueDTOList()){
            specCharValueDTOList.setValueData("");
        }

        for(SpinnerSpinItemView spinnerSpinItemView : spinnerSpinItemViews){
            ProductSpecCharValueDTOList productSpecCharValueDTOList = spinnerSpinItemView.mSpecCharValueDTOList;
            for(ProductSpecCharValueDTOList specCharValueDTOList : productSpecCharDTO.getProductSpecCharValueDTOList()){
                if(specCharValueDTOList.getId().equals(productSpecCharValueDTOList.getId())){
                    specCharValueDTOList.setValueData(specCharValueDTOList.getValueData() + spinnerSpinItemView.spnValue.getSelectedItem().toString() + SPLIT);
                }
            }
        }
    }

    public SpinnerSpinBoxView setRequired(boolean required){
        this.required = required;
        return this;
    }

    public void build(ProductSpecCharDTO productSpecCharDTO, List<ProductSpecCharValueDTOList> productSpecCharValueDTOListValue, Activity mActivity) {
        if (null == productSpecCharDTO) return;

        this.mActivity = mActivity;
        this.productSpecCharDTO = productSpecCharDTO;
        this.productSpecCharValueDTOListValue = productSpecCharValueDTOListValue;

        tvName.setText(productSpecCharDTO.getName());

        spinnerSpinItemViews = new ArrayList<>();

        productSpecCharValueDTOList = productSpecCharDTO.getProductSpecCharValueDTOList();

        productSpecCharValueDTOList.add(0, new ProductSpecCharValueDTOList(getContext().getString(R.string.spinnerSelectValue), ID_DEFAULT));
        productSpecCharValueDTOListValue.add(0, new ProductSpecCharValueDTOList(getContext().getString(R.string.spinnerSelectValue), ID_DEFAULT));

        if (null != productSpecCharValueDTOList && !productSpecCharValueDTOList.isEmpty()) {
            boolean flag = false;
            for (ProductSpecCharValueDTOList specCharValueDTOList : productSpecCharValueDTOList) {
                try {
                    if (null != specCharValueDTOList.getValueData()) {
                        flag = true;
                        String[] values = specCharValueDTOList.getValueData().split(SPLIT);

                        for(String value : values) {
                            specCharValueDTOList.setValueData(value);
                            addChildView(specCharValueDTOList);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!flag) {
                if (null != productSpecCharValueDTOList && !productSpecCharValueDTOList.isEmpty()) {
                    addChildView(productSpecCharValueDTOList.get(0));
                }
            }
        }
    }

    @OnClick(R.id.imvAdd)
    void addRow() {
        if (null != productSpecCharValueDTOList && !productSpecCharValueDTOList.isEmpty()) {
            if(PERSON_JOB.equals(productSpecCharDTO.getCode())){
                if(CollectCustomerInfoPresenter.mTotalMember <= spinnerSpinItemViews.size()){
                    CommonActivity.toast(mActivity, mActivity.getString(R.string.number_invalid));
                    return;
                }
            }
            addChildView(productSpecCharValueDTOList.get(0));
        }
    }

    @Override
    public void onRemove(int position, SpinnerSpinItemView spinnerSpinItemView) {
//        if(position < spinnerSpinItemViews.size()) {
//            spinnerSpinItemViews.remove(position);
            spinnerSpinItemViews.remove(spinnerSpinItemView);
            boxSpinner.removeView(spinnerSpinItemView);
//        }
    }

    void addChildView(ProductSpecCharValueDTOList specCharValueDTOList) {
        SpinnerSpinItemView itemView = new SpinnerSpinItemView(getContext());
        itemView.setPosition(spinnerSpinItemViews.size());
        spinnerSpinItemViews.add(itemView);
        itemView.build(this, specCharValueDTOList, productSpecCharValueDTOList, productSpecCharValueDTOListValue, this);
        boxSpinner.addView(itemView);
    }

}
