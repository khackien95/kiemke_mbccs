package com.viettel.gem.screen.collectinfo;

import android.widget.ScrollView;

import com.viettel.bss.viettelpos.v4.object.ProductSpecificationDTO;
import com.viettel.gem.base.viper.interfaces.IInteractor;
import com.viettel.gem.base.viper.interfaces.IPresenter;
import com.viettel.gem.base.viper.interfaces.PresentView;
import com.viettel.gem.screen.collectinfo.custom.GroupBoxView;

import java.util.List;

/**
 */
interface CollectCustomerInfoContract {

    interface Interactor extends IInteractor<Presenter> {
    }

    interface View extends PresentView<Presenter> {
        void setTitle(String title);

        void showTitle(boolean isShow);

        String getTitle();

        void addView(List<ProductSpecificationDTO> productSpecificationDTOList, String collectType);

        ScrollView getScrollView();
    }

    interface Presenter extends IPresenter<View, Interactor> {
        void collect();

        void put(GroupBoxView groupBoxView);

        CollectCustomerInfoPresenter setIsdn(String mIsdn);

        CollectCustomerInfoPresenter setIdno(String mIdNo);

        boolean isCreateNew();

        void resultPicture(String filePath, String code);

        void resultProvince(String value, String code);

        String getIsdn();

        String getIdno();

        String getAddress();

    }
}
