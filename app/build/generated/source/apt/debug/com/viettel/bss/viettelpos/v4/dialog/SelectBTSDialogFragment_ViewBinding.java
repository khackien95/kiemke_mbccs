// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.dialog;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SelectBTSDialogFragment_ViewBinding<T extends SelectBTSDialogFragment> implements Unbinder {
  protected T target;

  private View view2131756615;

  @UiThread
  public SelectBTSDialogFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.checkBox = Utils.findRequiredViewAsType(source, R.id.checkbox, "field 'checkBox'", CheckBox.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.edtSearchBTS = Utils.findRequiredViewAsType(source, R.id.edtSearchBTS, "field 'edtSearchBTS'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btnAccept, "method 'btnAcceptOnClick'");
    view2131756615 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.btnAcceptOnClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.checkBox = null;
    target.recyclerView = null;
    target.edtSearchBTS = null;

    view2131756615.setOnClickListener(null);
    view2131756615 = null;

    this.target = null;
  }
}
