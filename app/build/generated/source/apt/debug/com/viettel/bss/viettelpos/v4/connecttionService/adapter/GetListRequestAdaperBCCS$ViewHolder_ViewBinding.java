// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.connecttionService.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GetListRequestAdaperBCCS$ViewHolder_ViewBinding<T extends GetListRequestAdaperBCCS.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public GetListRequestAdaperBCCS$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvOrder = Utils.findRequiredViewAsType(source, R.id.tvOrder, "field 'tvOrder'", TextView.class);
    target.tvidrequest = Utils.findRequiredViewAsType(source, R.id.tvidrequest, "field 'tvidrequest'", TextView.class);
    target.tvIsdnAccount = Utils.findRequiredViewAsType(source, R.id.tvIsdnAccount, "field 'tvIsdnAccount'", TextView.class);
    target.tvContactName = Utils.findRequiredViewAsType(source, R.id.tvContactName, "field 'tvContactName'", TextView.class);
    target.viewIsdnAccount = Utils.findRequiredViewAsType(source, R.id.viewIdnAccount, "field 'viewIsdnAccount'", LinearLayout.class);
    target.tvcreater = Utils.findRequiredViewAsType(source, R.id.tvcreater, "field 'tvcreater'", TextView.class);
    target.tvService = Utils.findRequiredViewAsType(source, R.id.tvService, "field 'tvService'", TextView.class);
    target.tvDateRequest = Utils.findRequiredViewAsType(source, R.id.tvDateRequest, "field 'tvDateRequest'", TextView.class);
    target.tvStatusRequest = Utils.findRequiredViewAsType(source, R.id.tvStatusRequest, "field 'tvStatusRequest'", TextView.class);
    target.tvPhone = Utils.findRequiredViewAsType(source, R.id.tvPhone, "field 'tvPhone'", TextView.class);
    target.tvTech = Utils.findRequiredViewAsType(source, R.id.tvTech, "field 'tvTech'", TextView.class);
    target.tvBoxCode = Utils.findRequiredViewAsType(source, R.id.tvBoxCode, "field 'tvBoxCode'", TextView.class);
    target.btndelete = Utils.findRequiredViewAsType(source, R.id.btndelete, "field 'btndelete'", LinearLayout.class);
    target.btnApproved = Utils.findRequiredViewAsType(source, R.id.imgApproved, "field 'btnApproved'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvOrder = null;
    target.tvidrequest = null;
    target.tvIsdnAccount = null;
    target.tvContactName = null;
    target.viewIsdnAccount = null;
    target.tvcreater = null;
    target.tvService = null;
    target.tvDateRequest = null;
    target.tvStatusRequest = null;
    target.tvPhone = null;
    target.tvTech = null;
    target.tvBoxCode = null;
    target.btndelete = null;
    target.btnApproved = null;

    this.target = null;
  }
}
