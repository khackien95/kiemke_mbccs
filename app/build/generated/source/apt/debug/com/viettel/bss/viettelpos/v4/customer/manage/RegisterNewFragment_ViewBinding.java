// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.customer.manage;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RegisterNewFragment_ViewBinding<T extends RegisterNewFragment> implements Unbinder {
  protected T target;

  @UiThread
  public RegisterNewFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.lnChinhChu = Utils.findRequiredViewAsType(source, R.id.lnChinhChu, "field 'lnChinhChu'", LinearLayout.class);
    target.tvThueBaoChinhChu = Utils.findRequiredViewAsType(source, R.id.tvThueBaoChinhChu, "field 'tvThueBaoChinhChu'", TextView.class);
    target.mEdtISDN = Utils.findRequiredViewAsType(source, R.id.edtISDN, "field 'mEdtISDN'", EditText.class);
    target.mTvGoiCuoc = Utils.findRequiredViewAsType(source, R.id.tvGoiCuoc, "field 'mTvGoiCuoc'", EditText.class);
    target.mEdtSerial = Utils.findRequiredViewAsType(source, R.id.edtSerial, "field 'mEdtSerial'", EditText.class);
    target.btnBar = Utils.findRequiredViewAsType(source, R.id.btnBar, "field 'btnBar'", Button.class);
    target.mEdtTenKH = Utils.findRequiredViewAsType(source, R.id.edtTenKH, "field 'mEdtTenKH'", EditText.class);
    target.mEdtNgaySinh = Utils.findRequiredViewAsType(source, R.id.edtNgaySinh, "field 'mEdtNgaySinh'", EditText.class);
    target.mEdtMaGiayTo = Utils.findRequiredViewAsType(source, R.id.edtMaGiayTo, "field 'mEdtMaGiayTo'", EditText.class);
    target.mEdtNgayCap = Utils.findRequiredViewAsType(source, R.id.edtNgayCap, "field 'mEdtNgayCap'", EditText.class);
    target.mEdtNoiCap = Utils.findRequiredViewAsType(source, R.id.edtNoiCap, "field 'mEdtNoiCap'", EditText.class);
    target.mSpGioiTinh = Utils.findRequiredViewAsType(source, R.id.spGioiTinh, "field 'mSpGioiTinh'", Spinner.class);
    target.mSpLoaiGiayTo = Utils.findRequiredViewAsType(source, R.id.spLoaiGiayTo, "field 'mSpLoaiGiayTo'", EditText.class);
    target.mLnGoiDacBiet = Utils.findRequiredViewAsType(source, R.id.lnTTGoiCuocDacBiet, "field 'mLnGoiDacBiet'", LinearLayout.class);
    target.mEdtQuocTich = Utils.findRequiredViewAsType(source, R.id.edtQuocTich, "field 'mEdtQuocTich'", EditText.class);
    target.mTvDonVi = Utils.findRequiredViewAsType(source, R.id.tvDonVi, "field 'mTvDonVi'", EditText.class);
    target.mEdtMaGiayToDacBiet = Utils.findRequiredViewAsType(source, R.id.edtMaGiayToDacBiet, "field 'mEdtMaGiayToDacBiet'", EditText.class);
    target.mEdtNgayBD = Utils.findRequiredViewAsType(source, R.id.edtNgayBD, "field 'mEdtNgayBD'", EditText.class);
    target.mEdtNgayKT = Utils.findRequiredViewAsType(source, R.id.edtNgayKT, "field 'mEdtNgayKT'", EditText.class);
    target.mSpDoiTuong = Utils.findRequiredViewAsType(source, R.id.spDoiTuong, "field 'mSpDoiTuong'", Spinner.class);
    target.rlchondonvi = Utils.findRequiredViewAsType(source, R.id.rlchondonvi, "field 'rlchondonvi'", LinearLayout.class);
    target.rlquoctich = Utils.findRequiredViewAsType(source, R.id.rlquoctich, "field 'rlquoctich'", LinearLayout.class);
    target.spnReason = Utils.findRequiredViewAsType(source, R.id.spnReason, "field 'spnReason'", EditText.class);
    target.prbReason = Utils.findRequiredView(source, R.id.prbReason, "field 'prbReason'");
    target.btnRefreshReason = Utils.findRequiredViewAsType(source, R.id.btnRefreshReason, "field 'btnRefreshReason'", Button.class);
    target.mBtnRegister = Utils.findRequiredViewAsType(source, R.id.btnRegister, "field 'mBtnRegister'", Button.class);
    target.lnHocSinh = Utils.findRequiredViewAsType(source, R.id.lnHocSinh, "field 'lnHocSinh'", LinearLayout.class);
    target.edtnote = Utils.findRequiredViewAsType(source, R.id.edtnote, "field 'edtnote'", EditText.class);
    target.tvAddress = Utils.findRequiredViewAsType(source, R.id.tvAddress, "field 'tvAddress'", TextView.class);
    target.edtAgeHS = Utils.findRequiredViewAsType(source, R.id.edtAgeHS, "field 'edtAgeHS'", EditText.class);
    target.edtNameHS = Utils.findRequiredViewAsType(source, R.id.edtNameHS, "field 'edtNameHS'", EditText.class);
    target.lnSelectProfile = Utils.findRequiredViewAsType(source, R.id.lnSelectProfile, "field 'lnSelectProfile'", LinearLayout.class);
    target.thumbnails = Utils.findRequiredViewAsType(source, R.id.thumbnails, "field 'thumbnails'", LinearLayout.class);
    target.horizontalScrollView = Utils.findRequiredViewAsType(source, R.id.horizontalScrollView, "field 'horizontalScrollView'", HorizontalScrollView.class);
    target.lnngayhethanNew = Utils.findRequiredViewAsType(source, R.id.lnngayhethan, "field 'lnngayhethanNew'", LinearLayout.class);
    target.edit_ngayhethan = Utils.findRequiredViewAsType(source, R.id.edit_ngayhethan, "field 'edit_ngayhethan'", EditText.class);
    target.edit_ngayky = Utils.findRequiredViewAsType(source, R.id.edit_ngayky, "field 'edit_ngayky'", EditText.class);
    target.expandedImageView = Utils.findRequiredViewAsType(source, R.id.expandedImageView, "field 'expandedImageView'", ImageView.class);
    target.frlMain = Utils.findRequiredViewAsType(source, R.id.frlMain, "field 'frlMain'", FrameLayout.class);
    target.spnDoituong = Utils.findRequiredViewAsType(source, R.id.spnDoituong, "field 'spnDoituong'", Spinner.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.lnChinhChu = null;
    target.tvThueBaoChinhChu = null;
    target.mEdtISDN = null;
    target.mTvGoiCuoc = null;
    target.mEdtSerial = null;
    target.btnBar = null;
    target.mEdtTenKH = null;
    target.mEdtNgaySinh = null;
    target.mEdtMaGiayTo = null;
    target.mEdtNgayCap = null;
    target.mEdtNoiCap = null;
    target.mSpGioiTinh = null;
    target.mSpLoaiGiayTo = null;
    target.mLnGoiDacBiet = null;
    target.mEdtQuocTich = null;
    target.mTvDonVi = null;
    target.mEdtMaGiayToDacBiet = null;
    target.mEdtNgayBD = null;
    target.mEdtNgayKT = null;
    target.mSpDoiTuong = null;
    target.rlchondonvi = null;
    target.rlquoctich = null;
    target.spnReason = null;
    target.prbReason = null;
    target.btnRefreshReason = null;
    target.mBtnRegister = null;
    target.lnHocSinh = null;
    target.edtnote = null;
    target.tvAddress = null;
    target.edtAgeHS = null;
    target.edtNameHS = null;
    target.lnSelectProfile = null;
    target.thumbnails = null;
    target.horizontalScrollView = null;
    target.lnngayhethanNew = null;
    target.edit_ngayhethan = null;
    target.edit_ngayky = null;
    target.expandedImageView = null;
    target.frlMain = null;
    target.spnDoituong = null;

    this.target = null;
  }
}
