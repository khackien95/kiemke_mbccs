// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.province;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProvinceBoxView_ViewBinding<T extends ProvinceBoxView> implements Unbinder {
  protected T target;

  @UiThread
  public ProvinceBoxView_ViewBinding(T target, View source) {
    this.target = target;

    target.boxInput = Utils.findRequiredViewAsType(source, R.id.boxInput, "field 'boxInput'", LinearLayout.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.boxInput = null;
    target.tvName = null;

    this.target = null;
  }
}
