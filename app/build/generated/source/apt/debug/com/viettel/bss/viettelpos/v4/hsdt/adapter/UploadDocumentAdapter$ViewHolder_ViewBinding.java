// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.hsdt.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UploadDocumentAdapter$ViewHolder_ViewBinding<T extends UploadDocumentAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public UploadDocumentAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvIsdn = Utils.findRequiredViewAsType(source, R.id.tvIsdn, "field 'tvIsdn'", TextView.class);
    target.imShow = Utils.findRequiredViewAsType(source, R.id.imShow, "field 'imShow'", ImageView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvIsdn = null;
    target.imShow = null;
    target.recyclerView = null;

    this.target = null;
  }
}
