// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.province;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProvinceItemView_ViewBinding<T extends ProvinceItemView> implements Unbinder {
  protected T target;

  private View view2131756265;

  @UiThread
  public ProvinceItemView_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.edtInput, "field 'edtInput' and method 'onClickEdtInput'");
    target.edtInput = Utils.castView(view, R.id.edtInput, "field 'edtInput'", EditText.class);
    view2131756265 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickEdtInput();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtInput = null;

    view2131756265.setOnClickListener(null);
    view2131756265 = null;

    this.target = null;
  }
}
