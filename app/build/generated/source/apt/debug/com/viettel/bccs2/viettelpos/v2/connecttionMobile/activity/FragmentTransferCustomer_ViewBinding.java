// Generated code from Butter Knife. Do not modify!
package com.viettel.bccs2.viettelpos.v2.connecttionMobile.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentTransferCustomer_ViewBinding<T extends FragmentTransferCustomer> implements Unbinder {
  protected T target;

  @UiThread
  public FragmentTransferCustomer_ViewBinding(T target, View source) {
    this.target = target;

    target.lnSelectProfile = Utils.findRequiredViewAsType(source, R.id.lnSelectProfile, "field 'lnSelectProfile'", LinearLayout.class);
    target.thumbnails = Utils.findRequiredViewAsType(source, R.id.thumbnails, "field 'thumbnails'", LinearLayout.class);
    target.horizontalScrollView = Utils.findRequiredViewAsType(source, R.id.horizontalScrollView, "field 'horizontalScrollView'", HorizontalScrollView.class);
    target.lnngayhethan = Utils.findRequiredViewAsType(source, R.id.lnngayhethan, "field 'lnngayhethan'", LinearLayout.class);
    target.lnAddressCus = Utils.findRequiredViewAsType(source, R.id.lnAddressCus, "field 'lnAddressCus'", LinearLayout.class);
    target.edit_ngayhethan = Utils.findRequiredViewAsType(source, R.id.edit_ngayhethan, "field 'edit_ngayhethan'", EditText.class);
    target.edit_ngayky = Utils.findRequiredViewAsType(source, R.id.edit_ngayky, "field 'edit_ngayky'", EditText.class);
    target.spnDoituong = Utils.findRequiredViewAsType(source, R.id.spnDoituong, "field 'spnDoituong'", Spinner.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.lnSelectProfile = null;
    target.thumbnails = null;
    target.horizontalScrollView = null;
    target.lnngayhethan = null;
    target.lnAddressCus = null;
    target.edit_ngayhethan = null;
    target.edit_ngayky = null;
    target.spnDoituong = null;

    this.target = null;
  }
}
