// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.charge.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentPayment_ViewBinding<T extends FragmentPayment> implements Unbinder {
  protected T target;

  @UiThread
  public FragmentPayment_ViewBinding(T target, View source) {
    this.target = target;

    target.lnReasonBlock = Utils.findRequiredViewAsType(source, R.id.lnReasonBlock, "field 'lnReasonBlock'", LinearLayout.class);
    target.lnFeeType = Utils.findRequiredViewAsType(source, R.id.lnFeeType, "field 'lnFeeType'", LinearLayout.class);
    target.listView = Utils.findRequiredViewAsType(source, R.id.listView, "field 'listView'", ListView.class);
    target.edtSelectReason = Utils.findRequiredViewAsType(source, R.id.edtSelectReason, "field 'edtSelectReason'", EditText.class);
    target.btnBlock = Utils.findRequiredViewAsType(source, R.id.btnBlock, "field 'btnBlock'", Button.class);
    target.lnWarning = Utils.findRequiredViewAsType(source, R.id.lnWarning, "field 'lnWarning'", LinearLayout.class);
    target.txtWarning = Utils.findRequiredViewAsType(source, R.id.txtWarning, "field 'txtWarning'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.lnReasonBlock = null;
    target.lnFeeType = null;
    target.listView = null;
    target.edtSelectReason = null;
    target.btnBlock = null;
    target.lnWarning = null;
    target.txtWarning = null;

    this.target = null;
  }
}
