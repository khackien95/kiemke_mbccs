// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.staff.warning;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CauseActivity_ViewBinding<T extends CauseActivity> implements Unbinder {
  protected T target;

  private View view2131755268;

  private View view2131755269;

  @UiThread
  public CauseActivity_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.mTVName = Utils.findRequiredViewAsType(source, R.id.tv_name, "field 'mTVName'", TextView.class);
    target.mTVCode = Utils.findRequiredViewAsType(source, R.id.tv_code, "field 'mTVCode'", TextView.class);
    target.mTVTimeStart = Utils.findRequiredViewAsType(source, R.id.tv_time_start, "field 'mTVTimeStart'", TextView.class);
    target.mTVTimeAccrued = Utils.findRequiredViewAsType(source, R.id.tv_time_accrued, "field 'mTVTimeAccrued'", TextView.class);
    target.mTVTimeNok = Utils.findRequiredViewAsType(source, R.id.tv_time_nok, "field 'mTVTimeNok'", TextView.class);
    target.mTVContent = Utils.findRequiredViewAsType(source, R.id.tv_content, "field 'mTVContent'", TextView.class);
    target.mEdtReason = Utils.findRequiredViewAsType(source, R.id.edt_reason, "field 'mEdtReason'", EditText.class);
    target.mSpCause = Utils.findRequiredViewAsType(source, R.id.sp_cause, "field 'mSpCause'", AppCompatSpinner.class);
    view = Utils.findRequiredView(source, R.id.btn_close, "method 'onClose'");
    view2131755268 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClose();
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_update, "method 'onUpdate'");
    view2131755269 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onUpdate(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mTVName = null;
    target.mTVCode = null;
    target.mTVTimeStart = null;
    target.mTVTimeAccrued = null;
    target.mTVTimeNok = null;
    target.mTVContent = null;
    target.mEdtReason = null;
    target.mSpCause = null;

    view2131755268.setOnClickListener(null);
    view2131755268 = null;
    view2131755269.setOnClickListener(null);
    view2131755269 = null;

    this.target = null;
  }
}
