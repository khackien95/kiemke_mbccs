// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.listnetworkinfor;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NetworkCollectFragment_ViewBinding<T extends NetworkCollectFragment> implements Unbinder {
  protected T target;

  @UiThread
  public NetworkCollectFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.rvCollect = Utils.findRequiredViewAsType(source, R.id.rvCollect, "field 'rvCollect'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.rvCollect = null;

    this.target = null;
  }
}
