// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.synchronizationdata.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ApParamAdapter$ViewHolder_ViewBinding<T extends ApParamAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public ApParamAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.llId = Utils.findRequiredViewAsType(source, R.id.llId, "field 'llId'", LinearLayout.class);
    target.txtId = Utils.findRequiredViewAsType(source, R.id.txtId, "field 'txtId'", TextView.class);
    target.llStatus = Utils.findRequiredViewAsType(source, R.id.llStatus, "field 'llStatus'", LinearLayout.class);
    target.txtStatus = Utils.findRequiredViewAsType(source, R.id.txtStatus, "field 'txtStatus'", TextView.class);
    target.txtTypeParam = Utils.findRequiredViewAsType(source, R.id.txtTypeParam, "field 'txtTypeParam'", TextView.class);
    target.txtNameParam = Utils.findRequiredViewAsType(source, R.id.txtNameParam, "field 'txtNameParam'", TextView.class);
    target.txtValueParam = Utils.findRequiredViewAsType(source, R.id.txtValueParam, "field 'txtValueParam'", TextView.class);
    target.txtDescriptionParam = Utils.findRequiredViewAsType(source, R.id.txtDescriptionParam, "field 'txtDescriptionParam'", TextView.class);
    target.llDescription = Utils.findRequiredViewAsType(source, R.id.llDescription, "field 'llDescription'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.llId = null;
    target.txtId = null;
    target.llStatus = null;
    target.txtStatus = null;
    target.txtTypeParam = null;
    target.txtNameParam = null;
    target.txtValueParam = null;
    target.txtDescriptionParam = null;
    target.llDescription = null;

    this.target = null;
  }
}
