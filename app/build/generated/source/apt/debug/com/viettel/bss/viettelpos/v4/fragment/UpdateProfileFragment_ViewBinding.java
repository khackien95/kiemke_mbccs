// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UpdateProfileFragment_ViewBinding<T extends UpdateProfileFragment> implements Unbinder {
  protected T target;

  private View view2131756620;

  @UiThread
  public UpdateProfileFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.lvdetail = Utils.findRequiredViewAsType(source, R.id.lvdetail, "field 'lvdetail'", ListView.class);
    target.btnCancel = Utils.findRequiredViewAsType(source, R.id.btnCancel, "field 'btnCancel'", Button.class);
    view = Utils.findRequiredView(source, R.id.btnUpdate, "field 'btnUpdate' and method 'btnUpdateOnClick'");
    target.btnUpdate = Utils.castView(view, R.id.btnUpdate, "field 'btnUpdate'", Button.class);
    view2131756620 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.btnUpdateOnClick();
      }
    });
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.tvTitle, "field 'tvTitle'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.lvdetail = null;
    target.btnCancel = null;
    target.btnUpdate = null;
    target.tvTitle = null;

    view2131756620.setOnClickListener(null);
    view2131756620 = null;

    this.target = null;
  }
}
