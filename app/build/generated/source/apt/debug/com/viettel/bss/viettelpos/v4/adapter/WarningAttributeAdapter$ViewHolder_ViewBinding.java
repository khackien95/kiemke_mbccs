// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WarningAttributeAdapter$ViewHolder_ViewBinding<T extends WarningAttributeAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public WarningAttributeAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvCode = Utils.findRequiredViewAsType(source, R.id.tvCode, "field 'tvCode'", TextView.class);
    target.tvValue = Utils.findRequiredViewAsType(source, R.id.tvValue, "field 'tvValue'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvCode = null;
    target.tvValue = null;

    this.target = null;
  }
}
