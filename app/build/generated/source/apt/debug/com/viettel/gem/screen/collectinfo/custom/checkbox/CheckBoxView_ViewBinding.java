// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.checkbox;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.android.flexbox.FlexboxLayout;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CheckBoxView_ViewBinding<T extends CheckBoxView> implements Unbinder {
  protected T target;

  @UiThread
  public CheckBoxView_ViewBinding(T target, View source) {
    this.target = target;

    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
    target.boxCheckBox = Utils.findRequiredViewAsType(source, R.id.boxCheckBox, "field 'boxCheckBox'", FlexboxLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvName = null;
    target.boxCheckBox = null;

    this.target = null;
  }
}
