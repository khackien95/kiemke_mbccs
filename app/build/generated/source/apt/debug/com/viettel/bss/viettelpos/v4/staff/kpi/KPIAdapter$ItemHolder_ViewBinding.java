// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.staff.kpi;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class KPIAdapter$ItemHolder_ViewBinding<T extends KPIAdapter.ItemHolder> implements Unbinder {
  protected T target;

  @UiThread
  public KPIAdapter$ItemHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.mTVCode = Utils.findRequiredViewAsType(source, R.id.tv_code, "field 'mTVCode'", TextView.class);
    target.mTVName = Utils.findRequiredViewAsType(source, R.id.tv_name, "field 'mTVName'", TextView.class);
    target.mTVTotal = Utils.findRequiredViewAsType(source, R.id.tv_total, "field 'mTVTotal'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mTVCode = null;
    target.mTVName = null;
    target.mTVTotal = null;

    this.target = null;
  }
}
