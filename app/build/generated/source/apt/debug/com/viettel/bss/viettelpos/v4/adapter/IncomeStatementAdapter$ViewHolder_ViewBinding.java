// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class IncomeStatementAdapter$ViewHolder_ViewBinding<T extends IncomeStatementAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public IncomeStatementAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.tvTitle, "field 'tvTitle'", TextView.class);
    target.tvValue = Utils.findRequiredViewAsType(source, R.id.tvValue, "field 'tvValue'", TextView.class);
    target.tvSalaryTypeName = Utils.findRequiredViewAsType(source, R.id.tvSalaryTypeName, "field 'tvSalaryTypeName'", TextView.class);
    target.tvFormula = Utils.findRequiredViewAsType(source, R.id.tvFormula, "field 'tvFormula'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvTitle = null;
    target.tvValue = null;
    target.tvSalaryTypeName = null;
    target.tvFormula = null;

    this.target = null;
  }
}
