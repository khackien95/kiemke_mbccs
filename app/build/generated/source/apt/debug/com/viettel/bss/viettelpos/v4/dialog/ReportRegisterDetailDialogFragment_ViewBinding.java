// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.dialog;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ReportRegisterDetailDialogFragment_ViewBinding<T extends ReportRegisterDetailDialogFragment> implements Unbinder {
  protected T target;

  private View view2131756615;

  @UiThread
  public ReportRegisterDetailDialogFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.tvCustomerName = Utils.findRequiredViewAsType(source, R.id.tvCustomerName, "field 'tvCustomerName'", TextView.class);
    target.tvIdNo = Utils.findRequiredViewAsType(source, R.id.tvIdNo, "field 'tvIdNo'", TextView.class);
    target.tvIssueDate = Utils.findRequiredViewAsType(source, R.id.tvIssueDate, "field 'tvIssueDate'", TextView.class);
    target.tvIssuePlace = Utils.findRequiredViewAsType(source, R.id.tvIssuePlace, "field 'tvIssuePlace'", TextView.class);
    target.tvAddress = Utils.findRequiredViewAsType(source, R.id.tvAddress, "field 'tvAddress'", TextView.class);
    target.tvBirthDay = Utils.findRequiredViewAsType(source, R.id.tvBirthDay, "field 'tvBirthDay'", TextView.class);
    target.tvRegisterDate = Utils.findRequiredViewAsType(source, R.id.tvRegisterDate, "field 'tvRegisterDate'", TextView.class);
    target.tvActiveDate = Utils.findRequiredViewAsType(source, R.id.tvActiveDate, "field 'tvActiveDate'", TextView.class);
    target.tvProfileStatus = Utils.findRequiredViewAsType(source, R.id.tvProfileStatus, "field 'tvProfileStatus'", TextView.class);
    target.tvIsdn = Utils.findRequiredViewAsType(source, R.id.tvIsdn, "field 'tvIsdn'", TextView.class);
    target.tvCustType = Utils.findRequiredViewAsType(source, R.id.tvCustType, "field 'tvCustType'", TextView.class);
    target.tvSubType = Utils.findRequiredViewAsType(source, R.id.tvSubType, "field 'tvSubType'", TextView.class);
    target.tvShopName = Utils.findRequiredViewAsType(source, R.id.tvShopName, "field 'tvShopName'", TextView.class);
    target.tvStaffChecked = Utils.findRequiredViewAsType(source, R.id.tvStaffChecked, "field 'tvStaffChecked'", TextView.class);
    target.tvCheckedDatetime = Utils.findRequiredViewAsType(source, R.id.tvCheckedDatetime, "field 'tvCheckedDatetime'", TextView.class);
    target.tvCountCheckAgain = Utils.findRequiredViewAsType(source, R.id.tvCountCheckAgain, "field 'tvCountCheckAgain'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btnAccept, "method 'btnAcceptOnClick'");
    view2131756615 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.btnAcceptOnClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvCustomerName = null;
    target.tvIdNo = null;
    target.tvIssueDate = null;
    target.tvIssuePlace = null;
    target.tvAddress = null;
    target.tvBirthDay = null;
    target.tvRegisterDate = null;
    target.tvActiveDate = null;
    target.tvProfileStatus = null;
    target.tvIsdn = null;
    target.tvCustType = null;
    target.tvSubType = null;
    target.tvShopName = null;
    target.tvStaffChecked = null;
    target.tvCheckedDatetime = null;
    target.tvCountCheckAgain = null;

    view2131756615.setOnClickListener(null);
    view2131756615 = null;

    this.target = null;
  }
}
