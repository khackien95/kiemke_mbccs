// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.picturedetail;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PictureDetailFragment_ViewBinding<T extends PictureDetailFragment> implements Unbinder {
  protected T target;

  private View view2131755238;

  @UiThread
  public PictureDetailFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.imvPictureDetail = Utils.findRequiredViewAsType(source, R.id.imvPictureDetail, "field 'imvPictureDetail'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.back_iv, "method 'onClickView'");
    view2131755238 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickView(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.imvPictureDetail = null;

    view2131755238.setOnClickListener(null);
    view2131755238 = null;

    this.target = null;
  }
}
