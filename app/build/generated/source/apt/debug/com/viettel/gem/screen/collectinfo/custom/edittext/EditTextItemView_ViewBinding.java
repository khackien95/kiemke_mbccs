// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.edittext;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EditTextItemView_ViewBinding<T extends EditTextItemView> implements Unbinder {
  protected T target;

  @UiThread
  public EditTextItemView_ViewBinding(T target, View source) {
    this.target = target;

    target.edtInput = Utils.findRequiredViewAsType(source, R.id.edtInput, "field 'edtInput'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtInput = null;

    this.target = null;
  }
}
