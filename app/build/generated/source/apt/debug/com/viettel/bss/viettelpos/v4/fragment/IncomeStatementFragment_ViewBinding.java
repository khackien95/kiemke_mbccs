// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class IncomeStatementFragment_ViewBinding<T extends IncomeStatementFragment> implements Unbinder {
  protected T target;

  private View view2131756678;

  @UiThread
  public IncomeStatementFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.edtDate = Utils.findRequiredViewAsType(source, R.id.edtDate, "field 'edtDate'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btnXemLuong, "field 'btnXemLuong' and method 'onClickXemLuong'");
    target.btnXemLuong = Utils.castView(view, R.id.btnXemLuong, "field 'btnXemLuong'", Button.class);
    view2131756678 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickXemLuong();
      }
    });
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtDate = null;
    target.btnXemLuong = null;
    target.recyclerView = null;

    view2131756678.setOnClickListener(null);
    view2131756678 = null;

    this.target = null;
  }
}
