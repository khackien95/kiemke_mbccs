// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.sale.confirm.debt.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ConfirmDebitFragment_ViewBinding<T extends ConfirmDebitFragment> implements Unbinder {
  protected T target;

  private View view2131757489;

  private View view2131755370;

  private View view2131757490;

  private View view2131757496;

  @UiThread
  public ConfirmDebitFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.spnDebtCycle, "field 'spnDebtCycle' and method 'clearData'");
    target.spnDebtCycle = Utils.castView(view, R.id.spnDebtCycle, "field 'spnDebtCycle'", Spinner.class);
    view2131757489 = view;
    ((AdapterView<?>) view).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> p0, View p1, int p2, long p3) {
        target.clearData();
      }

      @Override
      public void onNothingSelected(AdapterView<?> p0) {
      }
    });
    view = Utils.findRequiredView(source, R.id.spnStatus, "field 'spnStatus' and method 'clearData1'");
    target.spnStatus = Utils.castView(view, R.id.spnStatus, "field 'spnStatus'", Spinner.class);
    view2131755370 = view;
    ((AdapterView<?>) view).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> p0, View p1, int p2, long p3) {
        target.clearData1();
      }

      @Override
      public void onNothingSelected(AdapterView<?> p0) {
      }
    });
    view = Utils.findRequiredView(source, R.id.btnSearchDebt, "field 'btnSearchDebt' and method 'onSearchDebt'");
    target.btnSearchDebt = Utils.castView(view, R.id.btnSearchDebt, "field 'btnSearchDebt'", Button.class);
    view2131757490 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onSearchDebt();
      }
    });
    target.lnDebt = Utils.findRequiredView(source, R.id.lnDebt, "field 'lnDebt'");
    target.tvDebtCycle = Utils.findRequiredViewAsType(source, R.id.tvDebtCycle, "field 'tvDebtCycle'", TextView.class);
    target.tvApproveTime = Utils.findRequiredViewAsType(source, R.id.tvApproveTime, "field 'tvApproveTime'", TextView.class);
    target.tvDebtStatus = Utils.findRequiredViewAsType(source, R.id.tvDebtStatus, "field 'tvDebtStatus'", TextView.class);
    target.tvConfirmTime = Utils.findRequiredViewAsType(source, R.id.tvConfirmTime, "field 'tvConfirmTime'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btnDebtDetail, "field 'btnDebtDetail' and method 'showDetailDebit'");
    target.btnDebtDetail = Utils.castView(view, R.id.btnDebtDetail, "field 'btnDebtDetail'", Button.class);
    view2131757496 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showDetailDebit();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.spnDebtCycle = null;
    target.spnStatus = null;
    target.btnSearchDebt = null;
    target.lnDebt = null;
    target.tvDebtCycle = null;
    target.tvApproveTime = null;
    target.tvDebtStatus = null;
    target.tvConfirmTime = null;
    target.btnDebtDetail = null;

    ((AdapterView<?>) view2131757489).setOnItemSelectedListener(null);
    view2131757489 = null;
    ((AdapterView<?>) view2131755370).setOnItemSelectedListener(null);
    view2131755370 = null;
    view2131757490.setOnClickListener(null);
    view2131757490 = null;
    view2131757496.setOnClickListener(null);
    view2131757496 = null;

    this.target = null;
  }
}
