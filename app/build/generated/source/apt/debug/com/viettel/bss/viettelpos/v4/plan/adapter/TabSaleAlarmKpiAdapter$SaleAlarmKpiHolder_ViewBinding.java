// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.plan.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TabSaleAlarmKpiAdapter$SaleAlarmKpiHolder_ViewBinding<T extends TabSaleAlarmKpiAdapter.SaleAlarmKpiHolder> implements Unbinder {
  protected T target;

  @UiThread
  public TabSaleAlarmKpiAdapter$SaleAlarmKpiHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvIndex = Utils.findRequiredViewAsType(source, R.id.tvIndex, "field 'tvIndex'", TextView.class);
    target.tvKpiCode = Utils.findRequiredViewAsType(source, R.id.tvKpiCode, "field 'tvKpiCode'", TextView.class);
    target.tvKpiTargetDay = Utils.findRequiredViewAsType(source, R.id.tvKpiTargetDay, "field 'tvKpiTargetDay'", TextView.class);
    target.tvPerProgressDay = Utils.findRequiredViewAsType(source, R.id.tvPerProgressDay, "field 'tvPerProgressDay'", TextView.class);
    target.tvStaffProcess = Utils.findRequiredViewAsType(source, R.id.tvStaffProcess, "field 'tvStaffProcess'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvIndex = null;
    target.tvKpiCode = null;
    target.tvKpiTargetDay = null;
    target.tvPerProgressDay = null;
    target.tvStaffProcess = null;

    this.target = null;
  }
}
