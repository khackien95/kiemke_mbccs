// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.spinnertext;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SpinnerTextItemView_ViewBinding<T extends SpinnerTextItemView> implements Unbinder {
  protected T target;

  private View view2131756291;

  @UiThread
  public SpinnerTextItemView_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.spnName = Utils.findRequiredViewAsType(source, R.id.spnName, "field 'spnName'", Spinner.class);
    target.edtInput = Utils.findRequiredViewAsType(source, R.id.edtInput, "field 'edtInput'", EditText.class);
    view = Utils.findRequiredView(source, R.id.imvMinus, "method 'minusRow'");
    view2131756291 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.minusRow();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.spnName = null;
    target.edtInput = null;

    view2131756291.setOnClickListener(null);
    view2131756291 = null;

    this.target = null;
  }
}
