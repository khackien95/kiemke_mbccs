// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.connecttionService.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentModifyProfile_ViewBinding<T extends FragmentModifyProfile> implements Unbinder {
  protected T target;

  @UiThread
  public FragmentModifyProfile_ViewBinding(T target, View source) {
    this.target = target;

    target.edtIsdnAccount = Utils.findRequiredViewAsType(source, R.id.edtIsdnAccount, "field 'edtIsdnAccount'", EditText.class);
    target.edtContractNo = Utils.findRequiredViewAsType(source, R.id.edtContractNo, "field 'edtContractNo'", EditText.class);
    target.edtFromDate = Utils.findRequiredViewAsType(source, R.id.edtFromDate, "field 'edtFromDate'", EditText.class);
    target.edtToDate = Utils.findRequiredViewAsType(source, R.id.edtToDate, "field 'edtToDate'", EditText.class);
    target.lvdetail = Utils.findRequiredViewAsType(source, R.id.lvdetail, "field 'lvdetail'", ListView.class);
    target.btnSearch = Utils.findRequiredViewAsType(source, R.id.btnSearch, "field 'btnSearch'", Button.class);
    target.lnResult = Utils.findRequiredViewAsType(source, R.id.lnResult, "field 'lnResult'", LinearLayout.class);
    target.rbModifyProfile = Utils.findRequiredViewAsType(source, R.id.rbModifyProfile, "field 'rbModifyProfile'", RadioButton.class);
    target.rbVerifyProfile = Utils.findRequiredViewAsType(source, R.id.rbVerifyProfile, "field 'rbVerifyProfile'", RadioButton.class);
    target.spinStatus = Utils.findRequiredViewAsType(source, R.id.spinStatus, "field 'spinStatus'", Spinner.class);
    target.lnStatus = Utils.findRequiredViewAsType(source, R.id.lnStatus, "field 'lnStatus'", LinearLayout.class);
    target.lnAction = Utils.findRequiredViewAsType(source, R.id.lnAction, "field 'lnAction'", LinearLayout.class);
    target.spinAction = Utils.findRequiredViewAsType(source, R.id.spinAction, "field 'spinAction'", Spinner.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtIsdnAccount = null;
    target.edtContractNo = null;
    target.edtFromDate = null;
    target.edtToDate = null;
    target.lvdetail = null;
    target.btnSearch = null;
    target.lnResult = null;
    target.rbModifyProfile = null;
    target.rbVerifyProfile = null;
    target.spinStatus = null;
    target.lnStatus = null;
    target.lnAction = null;
    target.spinAction = null;

    this.target = null;
  }
}
