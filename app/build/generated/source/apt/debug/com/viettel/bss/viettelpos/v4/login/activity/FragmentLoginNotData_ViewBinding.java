// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.login.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.github.clans.fab.FloatingActionMenu;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentLoginNotData_ViewBinding<T extends FragmentLoginNotData> implements Unbinder {
  protected T target;

  @UiThread
  public FragmentLoginNotData_ViewBinding(T target, View source) {
    this.target = target;

    target.fabActionMenu = Utils.findRequiredViewAsType(source, R.id.fabActionMenu, "field 'fabActionMenu'", FloatingActionMenu.class);
    target.lnChannelManager = Utils.findRequiredViewAsType(source, R.id.lnChannelManager, "field 'lnChannelManager'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.fabActionMenu = null;
    target.lnChannelManager = null;

    this.target = null;
  }
}
