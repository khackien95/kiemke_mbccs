// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.bankplus.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ResendCodeCardFragment_ViewBinding<T extends ResendCodeCardFragment> implements Unbinder {
  protected T target;

  @UiThread
  public ResendCodeCardFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.edtFromDate = Utils.findRequiredViewAsType(source, R.id.edtFromDate, "field 'edtFromDate'", EditText.class);
    target.edtToDate = Utils.findRequiredViewAsType(source, R.id.edtToDate, "field 'edtToDate'", EditText.class);
    target.edtPhoneNumber = Utils.findRequiredViewAsType(source, R.id.edtPhoneNumber, "field 'edtPhoneNumber'", EditText.class);
    target.btnSearch = Utils.findRequiredViewAsType(source, R.id.btnSearch, "field 'btnSearch'", Button.class);
    target.lvCardInfo = Utils.findRequiredViewAsType(source, R.id.lvCardInfo, "field 'lvCardInfo'", ListView.class);
    target.tvNoData = Utils.findRequiredViewAsType(source, R.id.tvNoData, "field 'tvNoData'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtFromDate = null;
    target.edtToDate = null;
    target.edtPhoneNumber = null;
    target.btnSearch = null;
    target.lvCardInfo = null;
    target.tvNoData = null;

    this.target = null;
  }
}
