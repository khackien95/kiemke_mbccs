// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.plan.screen;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PlanListFragment_ViewBinding<T extends PlanListFragment> implements Unbinder {
  protected T target;

  private View view2131756485;

  @UiThread
  public PlanListFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.rcvListSalePlan = Utils.findRequiredViewAsType(source, R.id.listPlan, "field 'rcvListSalePlan'", SuperRecyclerView.class);
    view = Utils.findRequiredView(source, R.id.btnSave, "method 'save'");
    view2131756485 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.save();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.rcvListSalePlan = null;

    view2131756485.setOnClickListener(null);
    view2131756485 = null;

    this.target = null;
  }
}
