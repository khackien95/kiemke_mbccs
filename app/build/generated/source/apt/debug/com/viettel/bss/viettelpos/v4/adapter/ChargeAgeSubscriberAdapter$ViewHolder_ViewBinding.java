// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChargeAgeSubscriberAdapter$ViewHolder_ViewBinding<T extends ChargeAgeSubscriberAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public ChargeAgeSubscriberAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvNo = Utils.findRequiredViewAsType(source, R.id.tvNo, "field 'tvNo'", TextView.class);
    target.tvMonth = Utils.findRequiredViewAsType(source, R.id.tvMonth, "field 'tvMonth'", TextView.class);
    target.tvCharge = Utils.findRequiredViewAsType(source, R.id.tvCharge, "field 'tvCharge'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvNo = null;
    target.tvMonth = null;
    target.tvCharge = null;

    this.target = null;
  }
}
