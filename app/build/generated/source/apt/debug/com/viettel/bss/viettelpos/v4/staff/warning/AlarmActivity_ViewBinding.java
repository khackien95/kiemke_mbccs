// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.staff.warning;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AlarmActivity_ViewBinding<T extends AlarmActivity> implements Unbinder {
  protected T target;

  @UiThread
  public AlarmActivity_ViewBinding(T target, View source) {
    this.target = target;

    target.mRVList = Utils.findRequiredViewAsType(source, R.id.rv_list, "field 'mRVList'", RecyclerView.class);
    target.mRefresh = Utils.findRequiredViewAsType(source, R.id.swiperefresh, "field 'mRefresh'", SwipeRefreshLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mRVList = null;
    target.mRefresh = null;

    this.target = null;
  }
}
