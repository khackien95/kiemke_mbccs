// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.plan.screen;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TabSaleResultFragment_ViewBinding<T extends TabSaleResultFragment> implements Unbinder {
  protected T target;

  @UiThread
  public TabSaleResultFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.tvPlanDate = Utils.findRequiredViewAsType(source, R.id.tvPlanDate, "field 'tvPlanDate'", TextView.class);
    target.tvCheckinNum = Utils.findRequiredViewAsType(source, R.id.tvCheckinNum, "field 'tvCheckinNum'", TextView.class);
    target.tvRemainNum = Utils.findRequiredViewAsType(source, R.id.tvRemainNum, "field 'tvRemainNum'", TextView.class);
    target.tvPlanNum = Utils.findRequiredViewAsType(source, R.id.tvPlanNum, "field 'tvPlanNum'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", SuperRecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvPlanDate = null;
    target.tvCheckinNum = null;
    target.tvRemainNum = null;
    target.tvPlanNum = null;
    target.recyclerView = null;

    this.target = null;
  }
}
