// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LookupProfileAdapter$ViewHolder_ViewBinding<T extends LookupProfileAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public LookupProfileAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvCustomerName = Utils.findRequiredViewAsType(source, R.id.tvCustomerName, "field 'tvCustomerName'", TextView.class);
    target.tvIsdnAccount = Utils.findRequiredViewAsType(source, R.id.tvIsdnAccount, "field 'tvIsdnAccount'", TextView.class);
    target.tvIdNo = Utils.findRequiredViewAsType(source, R.id.tvIdNo, "field 'tvIdNo'", TextView.class);
    target.tvService = Utils.findRequiredViewAsType(source, R.id.tvService, "field 'tvService'", TextView.class);
    target.tvStatusSub = Utils.findRequiredViewAsType(source, R.id.tvStatusSub, "field 'tvStatusSub'", TextView.class);
    target.tvStatusProfile = Utils.findRequiredViewAsType(source, R.id.tvStatusProfile, "field 'tvStatusProfile'", TextView.class);
    target.tvDateActive = Utils.findRequiredViewAsType(source, R.id.tvDateActive, "field 'tvDateActive'", TextView.class);
    target.tvOrder = Utils.findRequiredViewAsType(source, R.id.tvOrder, "field 'tvOrder'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvCustomerName = null;
    target.tvIsdnAccount = null;
    target.tvIdNo = null;
    target.tvService = null;
    target.tvStatusSub = null;
    target.tvStatusProfile = null;
    target.tvDateActive = null;
    target.tvOrder = null;

    this.target = null;
  }
}
