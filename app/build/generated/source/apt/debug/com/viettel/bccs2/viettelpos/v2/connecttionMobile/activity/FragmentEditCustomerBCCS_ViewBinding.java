// Generated code from Butter Knife. Do not modify!
package com.viettel.bccs2.viettelpos.v2.connecttionMobile.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentEditCustomerBCCS_ViewBinding<T extends FragmentEditCustomerBCCS> implements Unbinder {
  protected T target;

  @UiThread
  public FragmentEditCustomerBCCS_ViewBinding(T target, View source) {
    this.target = target;

    target.lntitleEditCus = Utils.findRequiredViewAsType(source, R.id.lntitle, "field 'lntitleEditCus'", LinearLayout.class);
    target.txtinfoEditCus = Utils.findRequiredViewAsType(source, R.id.txtinfo, "field 'txtinfoEditCus'", TextView.class);
    target.viewheadEditCus = Utils.findRequiredView(source, R.id.viewhead, "field 'viewheadEditCus'");
    target.edtloaikhEditCus = Utils.findRequiredViewAsType(source, R.id.edtloaikh, "field 'edtloaikhEditCus'", EditText.class);
    target.edit_tenKHEditCus = Utils.findRequiredViewAsType(source, R.id.edit_tenKH, "field 'edit_tenKHEditCus'", EditText.class);
    target.edit_ngaysinhEditCus = Utils.findRequiredViewAsType(source, R.id.edit_ngaysinh, "field 'edit_ngaysinhEditCus'", EditText.class);
    target.spinnerGioitinhEditCus = Utils.findRequiredViewAsType(source, R.id.spinner_gioitinh, "field 'spinnerGioitinhEditCus'", Spinner.class);
    target.lnSexEditCus = Utils.findRequiredViewAsType(source, R.id.ln_sex, "field 'lnSexEditCus'", LinearLayout.class);
    target.spinnerTypeGiayToEditCus = Utils.findRequiredViewAsType(source, R.id.spinner_type_giay_to, "field 'spinnerTypeGiayToEditCus'", Spinner.class);
    target.prbtypePaperEditEditCus = Utils.findRequiredViewAsType(source, R.id.prbtypePaperEdit, "field 'prbtypePaperEditEditCus'", ProgressBar.class);
    target.lnsogiaytoEditCus = Utils.findRequiredViewAsType(source, R.id.lnsogiayto, "field 'lnsogiaytoEditCus'", LinearLayout.class);
    target.edit_socmndEditCus = Utils.findRequiredViewAsType(source, R.id.edit_socmnd, "field 'edit_socmndEditCus'", EditText.class);
    target.linearCMTEditCus = Utils.findRequiredViewAsType(source, R.id.linearCMT, "field 'linearCMTEditCus'", LinearLayout.class);
    target.editSoGQEditCus = Utils.findRequiredViewAsType(source, R.id.edit_soGQ, "field 'editSoGQEditCus'", EditText.class);
    target.linearsoGPKDEditCus = Utils.findRequiredViewAsType(source, R.id.linearsoGPKD, "field 'linearsoGPKDEditCus'", LinearLayout.class);
    target.edtTinEditCus = Utils.findRequiredViewAsType(source, R.id.edt_tin, "field 'edtTinEditCus'", EditText.class);
    target.lnTinEditCus = Utils.findRequiredViewAsType(source, R.id.ln_tin, "field 'lnTinEditCus'", LinearLayout.class);
    target.edit_ngaycapEditCus = Utils.findRequiredViewAsType(source, R.id.edit_ngaycap, "field 'edit_ngaycapEditCus'", EditText.class);
    target.lnngaycapEditCus = Utils.findRequiredViewAsType(source, R.id.lnngaycapnew, "field 'lnngaycapEditCus'", LinearLayout.class);
    target.editNgayhethanEditCus = Utils.findRequiredViewAsType(source, R.id.edit_ngayhethan, "field 'editNgayhethanEditCus'", EditText.class);
    target.lnngayhethanEditCus = Utils.findRequiredViewAsType(source, R.id.lnngayhethan, "field 'lnngayhethanEditCus'", LinearLayout.class);
    target.editNoicapEditCus = Utils.findRequiredViewAsType(source, R.id.edit_noicap, "field 'editNoicapEditCus'", EditText.class);
    target.lnnoicapEditCus = Utils.findRequiredViewAsType(source, R.id.lnnoicapnew, "field 'lnnoicapEditCus'", LinearLayout.class);
    target.edtprovinceEditCus = Utils.findRequiredViewAsType(source, R.id.edtprovince, "field 'edtprovinceEditCus'", EditText.class);
    target.edtdistrictEditCus = Utils.findRequiredViewAsType(source, R.id.edtdistrict, "field 'edtdistrictEditCus'", EditText.class);
    target.edtprecinctEditCus = Utils.findRequiredViewAsType(source, R.id.edtprecinct, "field 'edtprecinctEditCus'", EditText.class);
    target.edtStreetBlockEditCus = Utils.findRequiredViewAsType(source, R.id.edtStreetBlock, "field 'edtStreetBlockEditCus'", EditText.class);
    target.btnRefreshStreetBlockEditCus = Utils.findRequiredViewAsType(source, R.id.btnRefreshStreetBlock, "field 'btnRefreshStreetBlockEditCus'", Button.class);
    target.edtStreetNameEditCus = Utils.findRequiredViewAsType(source, R.id.edt_streetName, "field 'edtStreetNameEditCus'", EditText.class);
    target.edtHomeNumberEditCus = Utils.findRequiredViewAsType(source, R.id.edtHomeNumber, "field 'edtHomeNumberEditCus'", EditText.class);
    target.spinnerQuoctichEditCus = Utils.findRequiredViewAsType(source, R.id.spinner_quoctichnew, "field 'spinnerQuoctichEditCus'", Spinner.class);
    target.lnquoctichdialog = Utils.findRequiredViewAsType(source, R.id.lnquoctichdialog, "field 'lnquoctichdialog'", LinearLayout.class);
    target.editNote = Utils.findRequiredViewAsType(source, R.id.edit_note, "field 'editNote'", EditText.class);
    target.spnReason = Utils.findRequiredViewAsType(source, R.id.spn_reason, "field 'spnReason'", Spinner.class);
    target.prbreason = Utils.findRequiredViewAsType(source, R.id.prbreason, "field 'prbreason'", ProgressBar.class);
    target.edtOTPIsdn = Utils.findRequiredViewAsType(source, R.id.edtOTPIsdn, "field 'edtOTPIsdn'", EditText.class);
    target.btnSendOTP = Utils.findRequiredViewAsType(source, R.id.btnSendOTP, "field 'btnSendOTP'", Button.class);
    target.prbreasonBtn = Utils.findRequiredViewAsType(source, R.id.prbreasonBtn, "field 'prbreasonBtn'", ProgressBar.class);
    target.edtOTPCode = Utils.findRequiredViewAsType(source, R.id.edtOTPCode, "field 'edtOTPCode'", EditText.class);
    target.lnOTP = Utils.findRequiredViewAsType(source, R.id.lnOTP, "field 'lnOTP'", LinearLayout.class);
    target.lnotpChangeCus = Utils.findRequiredViewAsType(source, R.id.lnotpChangeCus, "field 'lnotpChangeCus'", LinearLayout.class);
    target.btnsuadoi = Utils.findRequiredViewAsType(source, R.id.btnsuadoi, "field 'btnsuadoi'", Button.class);
    target.btncancel = Utils.findRequiredViewAsType(source, R.id.btncancel, "field 'btncancel'", Button.class);
    target.lnButton = Utils.findRequiredViewAsType(source, R.id.lnButton, "field 'lnButton'", LinearLayout.class);
    target.lnhoso = Utils.findRequiredViewAsType(source, R.id.lnhoso, "field 'lnhoso'", LinearLayout.class);
    target.lvUploadImage = Utils.findRequiredViewAsType(source, R.id.lvUploadImage, "field 'lvUploadImage'", ListView.class);
    target.spnDoituong = Utils.findRequiredViewAsType(source, R.id.spnDoituong, "field 'spnDoituong'", Spinner.class);
    target.lnSelectProfile = Utils.findRequiredViewAsType(source, R.id.lnSelectProfile, "field 'lnSelectProfile'", LinearLayout.class);
    target.thumbnails = Utils.findRequiredViewAsType(source, R.id.thumbnails, "field 'thumbnails'", LinearLayout.class);
    target.horizontalScrollView = Utils.findRequiredViewAsType(source, R.id.horizontalScrollView, "field 'horizontalScrollView'", HorizontalScrollView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.lntitleEditCus = null;
    target.txtinfoEditCus = null;
    target.viewheadEditCus = null;
    target.edtloaikhEditCus = null;
    target.edit_tenKHEditCus = null;
    target.edit_ngaysinhEditCus = null;
    target.spinnerGioitinhEditCus = null;
    target.lnSexEditCus = null;
    target.spinnerTypeGiayToEditCus = null;
    target.prbtypePaperEditEditCus = null;
    target.lnsogiaytoEditCus = null;
    target.edit_socmndEditCus = null;
    target.linearCMTEditCus = null;
    target.editSoGQEditCus = null;
    target.linearsoGPKDEditCus = null;
    target.edtTinEditCus = null;
    target.lnTinEditCus = null;
    target.edit_ngaycapEditCus = null;
    target.lnngaycapEditCus = null;
    target.editNgayhethanEditCus = null;
    target.lnngayhethanEditCus = null;
    target.editNoicapEditCus = null;
    target.lnnoicapEditCus = null;
    target.edtprovinceEditCus = null;
    target.edtdistrictEditCus = null;
    target.edtprecinctEditCus = null;
    target.edtStreetBlockEditCus = null;
    target.btnRefreshStreetBlockEditCus = null;
    target.edtStreetNameEditCus = null;
    target.edtHomeNumberEditCus = null;
    target.spinnerQuoctichEditCus = null;
    target.lnquoctichdialog = null;
    target.editNote = null;
    target.spnReason = null;
    target.prbreason = null;
    target.edtOTPIsdn = null;
    target.btnSendOTP = null;
    target.prbreasonBtn = null;
    target.edtOTPCode = null;
    target.lnOTP = null;
    target.lnotpChangeCus = null;
    target.btnsuadoi = null;
    target.btncancel = null;
    target.lnButton = null;
    target.lnhoso = null;
    target.lvUploadImage = null;
    target.spnDoituong = null;
    target.lnSelectProfile = null;
    target.thumbnails = null;
    target.horizontalScrollView = null;

    this.target = null;
  }
}
