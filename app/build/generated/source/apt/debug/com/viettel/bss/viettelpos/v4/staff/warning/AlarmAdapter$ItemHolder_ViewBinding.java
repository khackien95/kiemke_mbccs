// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.staff.warning;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AlarmAdapter$ItemHolder_ViewBinding<T extends AlarmAdapter.ItemHolder> implements Unbinder {
  protected T target;

  @UiThread
  public AlarmAdapter$ItemHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.mTVCode = Utils.findRequiredViewAsType(source, R.id.tv_code, "field 'mTVCode'", TextView.class);
    target.mTVName = Utils.findRequiredViewAsType(source, R.id.tv_name, "field 'mTVName'", TextView.class);
    target.mTVTimeStart = Utils.findRequiredViewAsType(source, R.id.tv_time_start, "field 'mTVTimeStart'", TextView.class);
    target.mTVTimeAccrued = Utils.findRequiredViewAsType(source, R.id.tv_time_accrued, "field 'mTVTimeAccrued'", TextView.class);
    target.mTVTimeNOk = Utils.findRequiredViewAsType(source, R.id.tv_time_nok, "field 'mTVTimeNOk'", TextView.class);
    target.mTVContent = Utils.findRequiredViewAsType(source, R.id.tv_content, "field 'mTVContent'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mTVCode = null;
    target.mTVName = null;
    target.mTVTimeStart = null;
    target.mTVTimeAccrued = null;
    target.mTVTimeNOk = null;
    target.mTVContent = null;

    this.target = null;
  }
}
