// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.advisory.screen;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ConsumersInfoDirectFragment_ViewBinding<T extends ConsumersInfoDirectFragment> implements Unbinder {
  protected T target;

  @UiThread
  public ConsumersInfoDirectFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.tvMonthOne = Utils.findRequiredViewAsType(source, R.id.tvMonthOne, "field 'tvMonthOne'", TextView.class);
    target.tvMonthTwo = Utils.findRequiredViewAsType(source, R.id.tvMonthTwo, "field 'tvMonthTwo'", TextView.class);
    target.tvMonthThree = Utils.findRequiredViewAsType(source, R.id.tvMonthThree, "field 'tvMonthThree'", TextView.class);
    target.tvMonthOneFlow = Utils.findRequiredViewAsType(source, R.id.tvMonthOneFlow, "field 'tvMonthOneFlow'", TextView.class);
    target.tvMonthTwoFlow = Utils.findRequiredViewAsType(source, R.id.tvMonthTwoFlow, "field 'tvMonthTwoFlow'", TextView.class);
    target.tvMonthThreeFlow = Utils.findRequiredViewAsType(source, R.id.tvMonthThreeFlow, "field 'tvMonthThreeFlow'", TextView.class);
    target.tvMonthOnePhoneIn = Utils.findRequiredViewAsType(source, R.id.tvMonthOnePhoneIn, "field 'tvMonthOnePhoneIn'", TextView.class);
    target.tvMonthTwoPhoneIn = Utils.findRequiredViewAsType(source, R.id.tvMonthTwoPhoneIn, "field 'tvMonthTwoPhoneIn'", TextView.class);
    target.tvMonthThreePhoneIn = Utils.findRequiredViewAsType(source, R.id.tvMonthThreePhoneIn, "field 'tvMonthThreePhoneIn'", TextView.class);
    target.tvMonthOnePhoneOut = Utils.findRequiredViewAsType(source, R.id.tvMonthOnePhoneOut, "field 'tvMonthOnePhoneOut'", TextView.class);
    target.tvMonthTwoPhoneOut = Utils.findRequiredViewAsType(source, R.id.tvMonthTwoPhoneOut, "field 'tvMonthTwoPhoneOut'", TextView.class);
    target.tvMonthThreePhoneOut = Utils.findRequiredViewAsType(source, R.id.tvMonthThreePhoneOut, "field 'tvMonthThreePhoneOut'", TextView.class);
    target.tvMonthOnePhoneGlobal = Utils.findRequiredViewAsType(source, R.id.tvMonthOnePhoneGlobal, "field 'tvMonthOnePhoneGlobal'", TextView.class);
    target.tvMonthTwoPhoneGlobal = Utils.findRequiredViewAsType(source, R.id.tvMonthTwoPhoneGlobal, "field 'tvMonthTwoPhoneGlobal'", TextView.class);
    target.tvMonthThreePhoneGlobal = Utils.findRequiredViewAsType(source, R.id.tvMonthThreePhoneGlobal, "field 'tvMonthThreePhoneGlobal'", TextView.class);
    target.tvMonthOneSMSIn = Utils.findRequiredViewAsType(source, R.id.tvMonthOneSMSIn, "field 'tvMonthOneSMSIn'", TextView.class);
    target.tvMonthTwoSMSIn = Utils.findRequiredViewAsType(source, R.id.tvMonthTwoSMSIn, "field 'tvMonthTwoSMSIn'", TextView.class);
    target.tvMonthThreeSMSIn = Utils.findRequiredViewAsType(source, R.id.tvMonthThreeSMSIn, "field 'tvMonthThreeSMSIn'", TextView.class);
    target.tvMonthOneSMSOut = Utils.findRequiredViewAsType(source, R.id.tvMonthOneSMSOut, "field 'tvMonthOneSMSOut'", TextView.class);
    target.tvMonthTwoSMSOut = Utils.findRequiredViewAsType(source, R.id.tvMonthTwoSMSOut, "field 'tvMonthTwoSMSOut'", TextView.class);
    target.tvMonthThreeSMSOut = Utils.findRequiredViewAsType(source, R.id.tvMonthThreeSMSOut, "field 'tvMonthThreeSMSOut'", TextView.class);
    target.tvMonthOneSMSGlobal = Utils.findRequiredViewAsType(source, R.id.tvMonthOneSMSGlobal, "field 'tvMonthOneSMSGlobal'", TextView.class);
    target.tvMonthTwoSMSGlobal = Utils.findRequiredViewAsType(source, R.id.tvMonthTwoSMSGlobal, "field 'tvMonthTwoSMSGlobal'", TextView.class);
    target.tvMonthThreeSMSGlobal = Utils.findRequiredViewAsType(source, R.id.tvMonthThreeSMSGlobal, "field 'tvMonthThreeSMSGlobal'", TextView.class);
    target.tvMonthOneData = Utils.findRequiredViewAsType(source, R.id.tvMonthOneData, "field 'tvMonthOneData'", TextView.class);
    target.tvMonthTwoData = Utils.findRequiredViewAsType(source, R.id.tvMonthTwoData, "field 'tvMonthTwoData'", TextView.class);
    target.tvMonthThreeData = Utils.findRequiredViewAsType(source, R.id.tvMonthThreeData, "field 'tvMonthThreeData'", TextView.class);
    target.tvMonthOneVas = Utils.findRequiredViewAsType(source, R.id.tvMonthOneVas, "field 'tvMonthOneVas'", TextView.class);
    target.tvMonthTwoVas = Utils.findRequiredViewAsType(source, R.id.tvMonthTwoVas, "field 'tvMonthTwoVas'", TextView.class);
    target.tvMonthThreeVas = Utils.findRequiredViewAsType(source, R.id.tvMonthThreeVas, "field 'tvMonthThreeVas'", TextView.class);
    target.tvMonthOneTotal = Utils.findRequiredViewAsType(source, R.id.tvMonthOneTotal, "field 'tvMonthOneTotal'", TextView.class);
    target.tvMonthTwoTotal = Utils.findRequiredViewAsType(source, R.id.tvMonthTwoTotal, "field 'tvMonthTwoTotal'", TextView.class);
    target.tvMonthThreeTotal = Utils.findRequiredViewAsType(source, R.id.tvMonthThreeTotal, "field 'tvMonthThreeTotal'", TextView.class);
    target.tvTitlePieChart = Utils.findRequiredViewAsType(source, R.id.tvTitlePieChart, "field 'tvTitlePieChart'", TextView.class);
    target.lineChart = Utils.findRequiredViewAsType(source, R.id.lineChart, "field 'lineChart'", LineChart.class);
    target.barChart = Utils.findRequiredViewAsType(source, R.id.barChart, "field 'barChart'", BarChart.class);
    target.pieChart = Utils.findRequiredViewAsType(source, R.id.pieChart, "field 'pieChart'", PieChart.class);
    target.llChartOnce = Utils.findRequiredViewAsType(source, R.id.llChartOnce, "field 'llChartOnce'", LinearLayout.class);
    target.llChartTwo = Utils.findRequiredViewAsType(source, R.id.llChartTwo, "field 'llChartTwo'", LinearLayout.class);
    target.tbDataMoney = Utils.findRequiredViewAsType(source, R.id.tbDataMoney, "field 'tbDataMoney'", TableLayout.class);
    target.tbDataFlow = Utils.findRequiredViewAsType(source, R.id.tbDataFlow, "field 'tbDataFlow'", TableLayout.class);
    target.btChangeShet = Utils.findRequiredViewAsType(source, R.id.btChangeShet, "field 'btChangeShet'", ImageButton.class);
    target.btChangeChart = Utils.findRequiredViewAsType(source, R.id.btChangeChart, "field 'btChangeChart'", ImageButton.class);
    target.cbPhoneIn = Utils.findRequiredViewAsType(source, R.id.cbPhoneIn, "field 'cbPhoneIn'", CheckBox.class);
    target.cbPhoneOut = Utils.findRequiredViewAsType(source, R.id.cbPhoneOut, "field 'cbPhoneOut'", CheckBox.class);
    target.cbPhoneGlobal = Utils.findRequiredViewAsType(source, R.id.cbPhoneGlobal, "field 'cbPhoneGlobal'", CheckBox.class);
    target.cbSmsIn = Utils.findRequiredViewAsType(source, R.id.cbSmsIn, "field 'cbSmsIn'", CheckBox.class);
    target.cbSmsOut = Utils.findRequiredViewAsType(source, R.id.cbSmsOut, "field 'cbSmsOut'", CheckBox.class);
    target.cbSmsGlobal = Utils.findRequiredViewAsType(source, R.id.cbSmsGlobal, "field 'cbSmsGlobal'", CheckBox.class);
    target.cbData = Utils.findRequiredViewAsType(source, R.id.cbData, "field 'cbData'", CheckBox.class);
    target.cbVas = Utils.findRequiredViewAsType(source, R.id.cbVas, "field 'cbVas'", CheckBox.class);
    target.cbTotal = Utils.findRequiredViewAsType(source, R.id.cbTotal, "field 'cbTotal'", CheckBox.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvMonthOne = null;
    target.tvMonthTwo = null;
    target.tvMonthThree = null;
    target.tvMonthOneFlow = null;
    target.tvMonthTwoFlow = null;
    target.tvMonthThreeFlow = null;
    target.tvMonthOnePhoneIn = null;
    target.tvMonthTwoPhoneIn = null;
    target.tvMonthThreePhoneIn = null;
    target.tvMonthOnePhoneOut = null;
    target.tvMonthTwoPhoneOut = null;
    target.tvMonthThreePhoneOut = null;
    target.tvMonthOnePhoneGlobal = null;
    target.tvMonthTwoPhoneGlobal = null;
    target.tvMonthThreePhoneGlobal = null;
    target.tvMonthOneSMSIn = null;
    target.tvMonthTwoSMSIn = null;
    target.tvMonthThreeSMSIn = null;
    target.tvMonthOneSMSOut = null;
    target.tvMonthTwoSMSOut = null;
    target.tvMonthThreeSMSOut = null;
    target.tvMonthOneSMSGlobal = null;
    target.tvMonthTwoSMSGlobal = null;
    target.tvMonthThreeSMSGlobal = null;
    target.tvMonthOneData = null;
    target.tvMonthTwoData = null;
    target.tvMonthThreeData = null;
    target.tvMonthOneVas = null;
    target.tvMonthTwoVas = null;
    target.tvMonthThreeVas = null;
    target.tvMonthOneTotal = null;
    target.tvMonthTwoTotal = null;
    target.tvMonthThreeTotal = null;
    target.tvTitlePieChart = null;
    target.lineChart = null;
    target.barChart = null;
    target.pieChart = null;
    target.llChartOnce = null;
    target.llChartTwo = null;
    target.tbDataMoney = null;
    target.tbDataFlow = null;
    target.btChangeShet = null;
    target.btChangeChart = null;
    target.cbPhoneIn = null;
    target.cbPhoneOut = null;
    target.cbPhoneGlobal = null;
    target.cbSmsIn = null;
    target.cbSmsOut = null;
    target.cbSmsGlobal = null;
    target.cbData = null;
    target.cbVas = null;
    target.cbTotal = null;

    this.target = null;
  }
}
