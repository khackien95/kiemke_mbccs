// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EditVasPlusFragment_ViewBinding<T extends EditVasPlusFragment> implements Unbinder {
  protected T target;

  @UiThread
  public EditVasPlusFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.btnSave = Utils.findRequiredViewAsType(source, R.id.btnSave, "field 'btnSave'", Button.class);
    target.recListVas = Utils.findRequiredViewAsType(source, R.id.recListVas, "field 'recListVas'", RecyclerView.class);
    target.tvCount = Utils.findRequiredViewAsType(source, R.id.tvCount, "field 'tvCount'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.btnSave = null;
    target.recListVas = null;
    target.tvCount = null;

    this.target = null;
  }
}
