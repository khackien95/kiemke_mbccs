// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.sale.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AdapterIsdnOwnerObjectIsdn$LoadingViewHolder_ViewBinding<T extends AdapterIsdnOwnerObjectIsdn.LoadingViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public AdapterIsdnOwnerObjectIsdn$LoadingViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progressBar, "field 'progressBar'", ProgressBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.progressBar = null;

    this.target = null;
  }
}
