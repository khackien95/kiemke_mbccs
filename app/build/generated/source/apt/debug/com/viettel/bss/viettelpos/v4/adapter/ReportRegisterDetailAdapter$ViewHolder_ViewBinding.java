// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ReportRegisterDetailAdapter$ViewHolder_ViewBinding<T extends ReportRegisterDetailAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public ReportRegisterDetailAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvIsdn = Utils.findRequiredViewAsType(source, R.id.tvIsdn, "field 'tvIsdn'", TextView.class);
    target.tvDateRegister = Utils.findRequiredViewAsType(source, R.id.tvDateRegister, "field 'tvDateRegister'", TextView.class);
    target.tvIdNo = Utils.findRequiredViewAsType(source, R.id.tvIdNo, "field 'tvIdNo'", TextView.class);
    target.tvProfileStatus = Utils.findRequiredViewAsType(source, R.id.tvProfileStatus, "field 'tvProfileStatus'", TextView.class);
    target.lnInfo = Utils.findRequiredViewAsType(source, R.id.lnInfo, "field 'lnInfo'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvIsdn = null;
    target.tvDateRegister = null;
    target.tvIdNo = null;
    target.tvProfileStatus = null;
    target.lnInfo = null;

    this.target = null;
  }
}
