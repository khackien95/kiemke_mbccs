// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.dialog;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChooseFileDialogFragment_ViewBinding<T extends ChooseFileDialogFragment> implements Unbinder {
  protected T target;

  private View view2131756615;

  private View view2131756419;

  @UiThread
  public ChooseFileDialogFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.tvTitle, "field 'tvTitle'", TextView.class);
    target.lvUploadImage = Utils.findRequiredViewAsType(source, R.id.lvUploadImage, "field 'lvUploadImage'", ListView.class);
    view = Utils.findRequiredView(source, R.id.btnAccept, "field 'btnAccept' and method 'btnAcceptOnClick'");
    target.btnAccept = Utils.castView(view, R.id.btnAccept, "field 'btnAccept'", Button.class);
    view2131756615 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.btnAcceptOnClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.imgClose, "method 'imgCloseOnClick'");
    view2131756419 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.imgCloseOnClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvTitle = null;
    target.lvUploadImage = null;
    target.btnAccept = null;

    view2131756615.setOnClickListener(null);
    view2131756615 = null;
    view2131756419.setOnClickListener(null);
    view2131756419 = null;

    this.target = null;
  }
}
