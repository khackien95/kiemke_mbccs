// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.customer.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.github.clans.fab.FloatingActionButton;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LookupComplainFragment_ViewBinding<T extends LookupComplainFragment> implements Unbinder {
  protected T target;

  private View view2131757825;

  private View view2131757823;

  @UiThread
  public LookupComplainFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.edtFromDate = Utils.findRequiredViewAsType(source, R.id.edtFromDate, "field 'edtFromDate'", EditText.class);
    target.edtToDate = Utils.findRequiredViewAsType(source, R.id.edtToDate, "field 'edtToDate'", EditText.class);
    target.edtIsdnComplain = Utils.findRequiredViewAsType(source, R.id.edtIsdnComplain, "field 'edtIsdnComplain'", EditText.class);
    target.rvTransComplain = Utils.findRequiredViewAsType(source, R.id.rvTransComplain, "field 'rvTransComplain'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.fabAddComplain, "field 'fabAddComplain' and method 'fabAddComplainOnClick'");
    target.fabAddComplain = Utils.castView(view, R.id.fabAddComplain, "field 'fabAddComplain'", FloatingActionButton.class);
    view2131757825 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.fabAddComplainOnClick();
      }
    });
    target.lnHeader = Utils.findRequiredViewAsType(source, R.id.lnHeader, "field 'lnHeader'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.btnLookup, "method 'btnLookupOnClick'");
    view2131757823 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.btnLookupOnClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtFromDate = null;
    target.edtToDate = null;
    target.edtIsdnComplain = null;
    target.rvTransComplain = null;
    target.fabAddComplain = null;
    target.lnHeader = null;

    view2131757825.setOnClickListener(null);
    view2131757825 = null;
    view2131757823.setOnClickListener(null);
    view2131757823 = null;

    this.target = null;
  }
}
