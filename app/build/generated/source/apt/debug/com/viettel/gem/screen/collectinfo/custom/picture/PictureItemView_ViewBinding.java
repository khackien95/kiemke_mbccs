// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.picture;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PictureItemView_ViewBinding<T extends PictureItemView> implements Unbinder {
  protected T target;

  @UiThread
  public PictureItemView_ViewBinding(T target, View source) {
    this.target = target;

    target.mPictureIv = Utils.findRequiredViewAsType(source, R.id.picture_iv, "field 'mPictureIv'", ImageView.class);
    target.mRemoveIv = Utils.findRequiredViewAsType(source, R.id.remove_iv, "field 'mRemoveIv'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mPictureIv = null;
    target.mRemoveIv = null;

    this.target = null;
  }
}
