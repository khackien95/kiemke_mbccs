// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.work.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ViewFlipper;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class JobManagerFragment_ViewBinding<T extends JobManagerFragment> implements Unbinder {
  protected T target;

  @UiThread
  public JobManagerFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.viewPager = Utils.findRequiredViewAsType(source, R.id.vpPager, "field 'viewPager'", ViewPager.class);
    target.tabLayout = Utils.findRequiredViewAsType(source, R.id.tab_layout, "field 'tabLayout'", TabLayout.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.viewFlipper = Utils.findRequiredViewAsType(source, R.id.viewFlipper, "field 'viewFlipper'", ViewFlipper.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.viewPager = null;
    target.tabLayout = null;
    target.recyclerView = null;
    target.viewFlipper = null;

    this.target = null;
  }
}
