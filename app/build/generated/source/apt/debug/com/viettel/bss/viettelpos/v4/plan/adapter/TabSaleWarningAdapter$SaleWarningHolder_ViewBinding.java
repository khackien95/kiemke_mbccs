// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.plan.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TabSaleWarningAdapter$SaleWarningHolder_ViewBinding<T extends TabSaleWarningAdapter.SaleWarningHolder> implements Unbinder {
  protected T target;

  @UiThread
  public TabSaleWarningAdapter$SaleWarningHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvIndex = Utils.findRequiredViewAsType(source, R.id.tvIndex, "field 'tvIndex'", TextView.class);
    target.tvKpiCode = Utils.findRequiredViewAsType(source, R.id.tvKpiCode, "field 'tvKpiCode'", TextView.class);
    target.tvKpiName = Utils.findRequiredViewAsType(source, R.id.tvKpiName, "field 'tvKpiName'", TextView.class);
    target.tvPercentComplete = Utils.findRequiredViewAsType(source, R.id.tvPercentComplete, "field 'tvPercentComplete'", TextView.class);
    target.tvRemainPaid = Utils.findRequiredViewAsType(source, R.id.tvRemainPaid, "field 'tvRemainPaid'", TextView.class);
    target.tvTotalAchieved = Utils.findRequiredViewAsType(source, R.id.tvTotalAchieved, "field 'tvTotalAchieved'", TextView.class);
    target.tvTotalPlanned = Utils.findRequiredViewAsType(source, R.id.tvTotalPlanned, "field 'tvTotalPlanned'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvIndex = null;
    target.tvKpiCode = null;
    target.tvKpiName = null;
    target.tvPercentComplete = null;
    target.tvRemainPaid = null;
    target.tvTotalAchieved = null;
    target.tvTotalPlanned = null;

    this.target = null;
  }
}
