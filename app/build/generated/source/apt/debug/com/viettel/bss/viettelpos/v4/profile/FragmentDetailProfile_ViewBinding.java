// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentDetailProfile_ViewBinding<T extends FragmentDetailProfile> implements Unbinder {
  protected T target;

  private View view2131756426;

  private View view2131756436;

  @UiThread
  public FragmentDetailProfile_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.btnInfoGeneral, "field 'btnInfoGeneral' and method 'onBtnInfoGeneralClick'");
    target.btnInfoGeneral = Utils.castView(view, R.id.btnInfoGeneral, "field 'btnInfoGeneral'", Button.class);
    view2131756426 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBtnInfoGeneralClick();
      }
    });
    target.expanInfoGeneral = Utils.findRequiredViewAsType(source, R.id.expanInfoGeneral, "field 'expanInfoGeneral'", ExpandableLinearLayout.class);
    view = Utils.findRequiredView(source, R.id.btnLstProfile, "field 'btnLstProfile' and method 'onBtnLstProfileClick'");
    target.btnLstProfile = Utils.castView(view, R.id.btnLstProfile, "field 'btnLstProfile'", Button.class);
    view2131756436 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBtnLstProfileClick();
      }
    });
    target.expanLstProfile = Utils.findRequiredViewAsType(source, R.id.expanLstProfile, "field 'expanLstProfile'", ExpandableLinearLayout.class);
    target.tvCustomerName = Utils.findRequiredViewAsType(source, R.id.tvCustomerName, "field 'tvCustomerName'", TextView.class);
    target.tvIsdnAccount = Utils.findRequiredViewAsType(source, R.id.tvIsdnAccount, "field 'tvIsdnAccount'", TextView.class);
    target.tvIdNo = Utils.findRequiredViewAsType(source, R.id.tvIdNo, "field 'tvIdNo'", TextView.class);
    target.tvService = Utils.findRequiredViewAsType(source, R.id.tvService, "field 'tvService'", TextView.class);
    target.tvStatusSub = Utils.findRequiredViewAsType(source, R.id.tvStatusSub, "field 'tvStatusSub'", TextView.class);
    target.tvStatusProfile = Utils.findRequiredViewAsType(source, R.id.tvStatusProfile, "field 'tvStatusProfile'", TextView.class);
    target.tvDateActive = Utils.findRequiredViewAsType(source, R.id.tvDateActive, "field 'tvDateActive'", TextView.class);
    target.recyclerViewProfile = Utils.findRequiredViewAsType(source, R.id.recyclerViewProfile, "field 'recyclerViewProfile'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.btnInfoGeneral = null;
    target.expanInfoGeneral = null;
    target.btnLstProfile = null;
    target.expanLstProfile = null;
    target.tvCustomerName = null;
    target.tvIsdnAccount = null;
    target.tvIdNo = null;
    target.tvService = null;
    target.tvStatusSub = null;
    target.tvStatusProfile = null;
    target.tvDateActive = null;
    target.recyclerViewProfile = null;

    view2131756426.setOnClickListener(null);
    view2131756426 = null;
    view2131756436.setOnClickListener(null);
    view2131756436 = null;

    this.target = null;
  }
}
