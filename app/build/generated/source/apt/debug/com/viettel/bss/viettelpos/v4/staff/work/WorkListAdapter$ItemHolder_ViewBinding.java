// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.staff.work;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WorkListAdapter$ItemHolder_ViewBinding<T extends WorkListAdapter.ItemHolder> implements Unbinder {
  protected T target;

  @UiThread
  public WorkListAdapter$ItemHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.mTVAssign = Utils.findRequiredViewAsType(source, R.id.tv_assign, "field 'mTVAssign'", TextView.class);
    target.mTVName = Utils.findRequiredViewAsType(source, R.id.tv_name, "field 'mTVName'", TextView.class);
    target.mTVFromDate = Utils.findRequiredViewAsType(source, R.id.tv_time_from_date, "field 'mTVFromDate'", TextView.class);
    target.mTVToDate = Utils.findRequiredViewAsType(source, R.id.tv_time_to_date, "field 'mTVToDate'", TextView.class);
    target.mTVContent = Utils.findRequiredViewAsType(source, R.id.tv_content, "field 'mTVContent'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mTVAssign = null;
    target.mTVName = null;
    target.mTVFromDate = null;
    target.mTVToDate = null;
    target.mTVContent = null;

    this.target = null;
  }
}
