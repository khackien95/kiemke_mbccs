// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewFlipper;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.maps.MapView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LookupRevenueStationFragment_ViewBinding<T extends LookupRevenueStationFragment> implements Unbinder {
  protected T target;

  private View view2131756694;

  private View view2131756754;

  @UiThread
  public LookupRevenueStationFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.btnViewRevenue, "field 'btnViewRevenue' and method 'lookupRevenueStation'");
    target.btnViewRevenue = Utils.castView(view, R.id.btnViewRevenue, "field 'btnViewRevenue'", Button.class);
    view2131756694 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.lookupRevenueStation();
      }
    });
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.edtLookupDate = Utils.findRequiredViewAsType(source, R.id.edtLookupDate, "field 'edtLookupDate'", EditText.class);
    target.spnCriteria = Utils.findRequiredViewAsType(source, R.id.spnCriteria, "field 'spnCriteria'", Spinner.class);
    target.edtSearchBTS = Utils.findRequiredViewAsType(source, R.id.edtSearchBTS, "field 'edtSearchBTS'", EditText.class);
    target.lnInfo = Utils.findRequiredViewAsType(source, R.id.lnInfo, "field 'lnInfo'", LinearLayout.class);
    target.mapView = Utils.findRequiredViewAsType(source, R.id.idMapViewStaff, "field 'mapView'", MapView.class);
    target.lnTotal = Utils.findRequiredViewAsType(source, R.id.lnTotal, "field 'lnTotal'", LinearLayout.class);
    target.tvCriteria = Utils.findRequiredViewAsType(source, R.id.tvCriteria, "field 'tvCriteria'", TextView.class);
    target.tvSln = Utils.findRequiredViewAsType(source, R.id.tvSln, "field 'tvSln'", TextView.class);
    target.tvDeltaSln = Utils.findRequiredViewAsType(source, R.id.tvDeltaSln, "field 'tvDeltaSln'", TextView.class);
    target.tvSlt = Utils.findRequiredViewAsType(source, R.id.tvSlt, "field 'tvSlt'", TextView.class);
    target.tvDeltaSlt = Utils.findRequiredViewAsType(source, R.id.tvDeltaSlt, "field 'tvDeltaSlt'", TextView.class);
    target.mViewFlipper = Utils.findRequiredViewAsType(source, R.id.vfListAndMap, "field 'mViewFlipper'", ViewFlipper.class);
    target.recyclerViewCriterial = Utils.findRequiredViewAsType(source, R.id.recyclerViewCriterial, "field 'recyclerViewCriterial'", RecyclerView.class);
    target.autoCompleteTextViewBTS = Utils.findRequiredViewAsType(source, R.id.autoCompleteTextViewBTS, "field 'autoCompleteTextViewBTS'", AutoCompleteTextView.class);
    view = Utils.findRequiredView(source, R.id.imgSearch, "field 'imgSearch' and method 'imgSearchOnClick'");
    target.imgSearch = Utils.castView(view, R.id.imgSearch, "field 'imgSearch'", ImageView.class);
    view2131756754 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.imgSearchOnClick();
      }
    });
    target.edtBTS = Utils.findRequiredViewAsType(source, R.id.edtBTS, "field 'edtBTS'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.btnViewRevenue = null;
    target.recyclerView = null;
    target.edtLookupDate = null;
    target.spnCriteria = null;
    target.edtSearchBTS = null;
    target.lnInfo = null;
    target.mapView = null;
    target.lnTotal = null;
    target.tvCriteria = null;
    target.tvSln = null;
    target.tvDeltaSln = null;
    target.tvSlt = null;
    target.tvDeltaSlt = null;
    target.mViewFlipper = null;
    target.recyclerViewCriterial = null;
    target.autoCompleteTextViewBTS = null;
    target.imgSearch = null;
    target.edtBTS = null;

    view2131756694.setOnClickListener(null);
    view2131756694 = null;
    view2131756754.setOnClickListener(null);
    view2131756754 = null;

    this.target = null;
  }
}
