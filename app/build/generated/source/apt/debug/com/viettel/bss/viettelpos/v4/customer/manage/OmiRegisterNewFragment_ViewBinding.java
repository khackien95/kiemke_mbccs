// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.customer.manage;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OmiRegisterNewFragment_ViewBinding<T extends OmiRegisterNewFragment> implements Unbinder {
  protected T target;

  @UiThread
  public OmiRegisterNewFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.lnSelectProfile = Utils.findRequiredViewAsType(source, R.id.lnSelectProfile, "field 'lnSelectProfile'", LinearLayout.class);
    target.thumbnails = Utils.findRequiredViewAsType(source, R.id.thumbnails, "field 'thumbnails'", LinearLayout.class);
    target.horizontalScrollView = Utils.findRequiredViewAsType(source, R.id.horizontalScrollView, "field 'horizontalScrollView'", HorizontalScrollView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.spnDoituong = Utils.findRequiredViewAsType(source, R.id.spnDoituong, "field 'spnDoituong'", Spinner.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.lnSelectProfile = null;
    target.thumbnails = null;
    target.horizontalScrollView = null;
    target.toolbar = null;
    target.spnDoituong = null;

    this.target = null;
  }
}
