// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.radio;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RadioItemView_ViewBinding<T extends RadioItemView> implements Unbinder {
  protected T target;

  private View view2131756282;

  @UiThread
  public RadioItemView_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.rbtnValue, "field 'rbtnValue' and method 'onChecked'");
    target.rbtnValue = Utils.castView(view, R.id.rbtnValue, "field 'rbtnValue'", RadioButton.class);
    view2131756282 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onChecked(p1);
      }
    });
    target.edtInput = Utils.findRequiredViewAsType(source, R.id.edtInput, "field 'edtInput'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.rbtnValue = null;
    target.edtInput = null;

    ((CompoundButton) view2131756282).setOnCheckedChangeListener(null);
    view2131756282 = null;

    this.target = null;
  }
}
