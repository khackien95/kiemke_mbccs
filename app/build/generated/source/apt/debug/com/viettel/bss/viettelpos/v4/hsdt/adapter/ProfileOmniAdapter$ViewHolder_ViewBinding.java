// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.hsdt.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProfileOmniAdapter$ViewHolder_ViewBinding<T extends ProfileOmniAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public ProfileOmniAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.spUploadImage = Utils.findRequiredViewAsType(source, R.id.spUploadImage, "field 'spUploadImage'", Spinner.class);
    target.ibUploadImage = Utils.findRequiredViewAsType(source, R.id.ibUploadImage, "field 'ibUploadImage'", ImageButton.class);
    target.btnViewPdf = Utils.findRequiredViewAsType(source, R.id.btnViewPdf, "field 'btnViewPdf'", Button.class);
    target.tvRequire = Utils.findRequiredViewAsType(source, R.id.tvRequire, "field 'tvRequire'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.spUploadImage = null;
    target.ibUploadImage = null;
    target.btnViewPdf = null;
    target.tvRequire = null;

    this.target = null;
  }
}
