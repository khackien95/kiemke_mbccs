// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.dialog;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.CharSequence;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UpdateJobDialog_ViewBinding<T extends UpdateJobDialog> implements Unbinder {
  protected T target;

  private View view2131756261;

  private TextWatcher view2131756261TextWatcher;

  private View view2131758366;

  private View view2131758367;

  private View view2131758363;

  private View view2131756620;

  @UiThread
  public UpdateJobDialog_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.edtDate, "field 'edtDate' and method 'edtDateOnTextChanged'");
    target.edtDate = Utils.castView(view, R.id.edtDate, "field 'edtDate'", EditText.class);
    view2131756261 = view;
    view2131756261TextWatcher = new TextWatcher() {
      @Override
      public void onTextChanged(CharSequence p0, int p1, int p2, int p3) {
      }

      @Override
      public void beforeTextChanged(CharSequence p0, int p1, int p2, int p3) {
      }

      @Override
      public void afterTextChanged(Editable p0) {
        target.edtDateOnTextChanged();
      }
    };
    ((TextView) view).addTextChangedListener(view2131756261TextWatcher);
    target.lnDate = Utils.findRequiredViewAsType(source, R.id.lnDate, "field 'lnDate'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.lnTime, "field 'lnTime' and method 'showDropDownTime'");
    target.lnTime = Utils.castView(view, R.id.lnTime, "field 'lnTime'", LinearLayout.class);
    view2131758366 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showDropDownTime();
      }
    });
    view = Utils.findRequiredView(source, R.id.actvTime, "field 'actvTime' and method 'showDropDownTime'");
    target.actvTime = Utils.castView(view, R.id.actvTime, "field 'actvTime'", AutoCompleteTextView.class);
    view2131758367 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showDropDownTime();
      }
    });
    target.recListOptionSelect = Utils.findRequiredViewAsType(source, R.id.recListOptionSelect, "field 'recListOptionSelect'", RecyclerView.class);
    target.edtDescription = Utils.findRequiredViewAsType(source, R.id.edtDescription, "field 'edtDescription'", TextInputEditText.class);
    view = Utils.findRequiredView(source, R.id.closeButton, "method 'lnCloseOnClick'");
    view2131758363 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.lnCloseOnClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.btnUpdate, "method 'btnUpdateOnClick'");
    view2131756620 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.btnUpdateOnClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtDate = null;
    target.lnDate = null;
    target.lnTime = null;
    target.actvTime = null;
    target.recListOptionSelect = null;
    target.edtDescription = null;

    ((TextView) view2131756261).removeTextChangedListener(view2131756261TextWatcher);
    view2131756261TextWatcher = null;
    view2131756261 = null;
    view2131758366.setOnClickListener(null);
    view2131758366 = null;
    view2131758367.setOnClickListener(null);
    view2131758367 = null;
    view2131758363.setOnClickListener(null);
    view2131758363 = null;
    view2131756620.setOnClickListener(null);
    view2131756620 = null;

    this.target = null;
  }
}
