// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProblemHistoryAdapter$ViewHolder_ViewBinding<T extends ProblemHistoryAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public ProblemHistoryAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvIsdn = Utils.findRequiredViewAsType(source, R.id.tvIsdn, "field 'tvIsdn'", TextView.class);
    target.tvComplainType = Utils.findRequiredViewAsType(source, R.id.tvComplainType, "field 'tvComplainType'", TextView.class);
    target.tvStatus = Utils.findRequiredViewAsType(source, R.id.tvStatus, "field 'tvStatus'", TextView.class);
    target.tvCreateDate = Utils.findRequiredViewAsType(source, R.id.tvCreateDate, "field 'tvCreateDate'", TextView.class);
    target.tvCustNote = Utils.findRequiredViewAsType(source, R.id.tvCustNote, "field 'tvCustNote'", TextView.class);
    target.tvDeadline = Utils.findRequiredViewAsType(source, R.id.tvDeadline, "field 'tvDeadline'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvIsdn = null;
    target.tvComplainType = null;
    target.tvStatus = null;
    target.tvCreateDate = null;
    target.tvCustNote = null;
    target.tvDeadline = null;

    this.target = null;
  }
}
