// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.searchcustomer;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ConfirmDialog_ViewBinding<T extends ConfirmDialog> implements Unbinder {
  protected T target;

  private View view2131755531;

  private View view2131755530;

  private View view2131755528;

  @UiThread
  public ConfirmDialog_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.add_btn, "method 'onClickView'");
    view2131755531 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickView(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.cancel_btn, "method 'onClickView'");
    view2131755530 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickView(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.close_btn, "method 'onClickView'");
    view2131755528 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickView(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (this.target == null) throw new IllegalStateException("Bindings already cleared.");

    view2131755531.setOnClickListener(null);
    view2131755531 = null;
    view2131755530.setOnClickListener(null);
    view2131755530 = null;
    view2131755528.setOnClickListener(null);
    view2131755528 = null;

    this.target = null;
  }
}
