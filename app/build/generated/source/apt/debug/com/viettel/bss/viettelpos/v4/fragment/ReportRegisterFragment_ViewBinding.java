// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ReportRegisterFragment_ViewBinding<T extends ReportRegisterFragment> implements Unbinder {
  protected T target;

  private View view2131756717;

  private View view2131755414;

  private View view2131756716;

  @UiThread
  public ReportRegisterFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.edtFromDate = Utils.findRequiredViewAsType(source, R.id.edtFromDate, "field 'edtFromDate'", EditText.class);
    target.edtToDate = Utils.findRequiredViewAsType(source, R.id.edtToDate, "field 'edtToDate'", EditText.class);
    target.spnStatus = Utils.findRequiredViewAsType(source, R.id.spnStatus, "field 'spnStatus'", Spinner.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.lnStatus = Utils.findRequiredViewAsType(source, R.id.lnStatus, "field 'lnStatus'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.rbDetail, "field 'rbDetail' and method 'rbDetailOnClick'");
    target.rbDetail = Utils.castView(view, R.id.rbDetail, "field 'rbDetail'", RadioButton.class);
    view2131756717 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.rbDetailOnClick();
      }
    });
    target.lnResultDetail = Utils.findRequiredViewAsType(source, R.id.lnResultDetail, "field 'lnResultDetail'", LinearLayout.class);
    target.lnResultTotal = Utils.findRequiredViewAsType(source, R.id.lnResultTotal, "field 'lnResultTotal'", LinearLayout.class);
    target.tvNumDKTT = Utils.findRequiredViewAsType(source, R.id.tvNumDKTT, "field 'tvNumDKTT'", TextView.class);
    target.tvNumSubProfileValid = Utils.findRequiredViewAsType(source, R.id.tvNumSubProfileValid, "field 'tvNumSubProfileValid'", TextView.class);
    target.tvNumProfileNotExpire = Utils.findRequiredViewAsType(source, R.id.tvNumProfileNotExpire, "field 'tvNumProfileNotExpire'", TextView.class);
    target.tvNumProfileExpire = Utils.findRequiredViewAsType(source, R.id.tvNumProfileExpire, "field 'tvNumProfileExpire'", TextView.class);
    target.tvNumSubCheckInvalid = Utils.findRequiredViewAsType(source, R.id.tvNumSubCheckInvalid, "field 'tvNumSubCheckInvalid'", TextView.class);
    target.tvNumSubNotCheck = Utils.findRequiredViewAsType(source, R.id.tvNumSubNotCheck, "field 'tvNumSubNotCheck'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btnSearch, "method 'btnSearchOnClick'");
    view2131755414 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.btnSearchOnClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.rbTotal, "method 'rbTotalOnClick'");
    view2131756716 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.rbTotalOnClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtFromDate = null;
    target.edtToDate = null;
    target.spnStatus = null;
    target.recyclerView = null;
    target.lnStatus = null;
    target.rbDetail = null;
    target.lnResultDetail = null;
    target.lnResultTotal = null;
    target.tvNumDKTT = null;
    target.tvNumSubProfileValid = null;
    target.tvNumProfileNotExpire = null;
    target.tvNumProfileExpire = null;
    target.tvNumSubCheckInvalid = null;
    target.tvNumSubNotCheck = null;

    view2131756717.setOnClickListener(null);
    view2131756717 = null;
    view2131755414.setOnClickListener(null);
    view2131755414 = null;
    view2131756716.setOnClickListener(null);
    view2131756716 = null;

    this.target = null;
  }
}
