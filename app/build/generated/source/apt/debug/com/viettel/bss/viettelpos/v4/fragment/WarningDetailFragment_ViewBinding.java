// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WarningDetailFragment_ViewBinding<T extends WarningDetailFragment> implements Unbinder {
  protected T target;

  @UiThread
  public WarningDetailFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.tvTotalWarning = Utils.findRequiredViewAsType(source, R.id.tvTotalWarning, "field 'tvTotalWarning'", TextView.class);
    target.lvWarning = Utils.findRequiredViewAsType(source, R.id.lvWarning, "field 'lvWarning'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvTotalWarning = null;
    target.lvWarning = null;

    this.target = null;
  }
}
