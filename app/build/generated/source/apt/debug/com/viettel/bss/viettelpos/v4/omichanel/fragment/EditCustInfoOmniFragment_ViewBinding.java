// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EditCustInfoOmniFragment_ViewBinding<T extends EditCustInfoOmniFragment> implements Unbinder {
  protected T target;

  @UiThread
  public EditCustInfoOmniFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.btnSave = Utils.findRequiredViewAsType(source, R.id.btnSave, "field 'btnSave'", Button.class);
    target.btnCancel = Utils.findRequiredViewAsType(source, R.id.btnCancel, "field 'btnCancel'", Button.class);
    target.editCustName = Utils.findRequiredViewAsType(source, R.id.editCustName, "field 'editCustName'", EditText.class);
    target.editBirthDay = Utils.findRequiredViewAsType(source, R.id.editBirthDay, "field 'editBirthDay'", EditText.class);
    target.editId = Utils.findRequiredViewAsType(source, R.id.editId, "field 'editId'", EditText.class);
    target.editIdIssueDate = Utils.findRequiredViewAsType(source, R.id.editIdIssueDate, "field 'editIdIssueDate'", EditText.class);
    target.editIdIssuePlace = Utils.findRequiredViewAsType(source, R.id.editIdIssuePlace, "field 'editIdIssuePlace'", EditText.class);
    target.editProvince = Utils.findRequiredViewAsType(source, R.id.editProvince, "field 'editProvince'", EditText.class);
    target.editDistrict = Utils.findRequiredViewAsType(source, R.id.editDistrict, "field 'editDistrict'", EditText.class);
    target.editHomeNumber = Utils.findRequiredViewAsType(source, R.id.editHomeNumber, "field 'editHomeNumber'", EditText.class);
    target.editPrecinct = Utils.findRequiredViewAsType(source, R.id.editPrecinct, "field 'editPrecinct'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.btnSave = null;
    target.btnCancel = null;
    target.editCustName = null;
    target.editBirthDay = null;
    target.editId = null;
    target.editIdIssueDate = null;
    target.editIdIssuePlace = null;
    target.editProvince = null;
    target.editDistrict = null;
    target.editHomeNumber = null;
    target.editPrecinct = null;

    this.target = null;
  }
}
