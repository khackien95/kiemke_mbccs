// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustIdentityDTOAdapter$ViewHolder_ViewBinding<T extends CustIdentityDTOAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public CustIdentityDTOAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.txtCustName = Utils.findRequiredViewAsType(source, R.id.tvCustName, "field 'txtCustName'", TextView.class);
    target.txtIdNo = Utils.findRequiredViewAsType(source, R.id.txtIdNo, "field 'txtIdNo'", TextView.class);
    target.txtIdIssueDate = Utils.findRequiredViewAsType(source, R.id.txtIdIssueDate, "field 'txtIdIssueDate'", TextView.class);
    target.txtIdIssuePlace = Utils.findRequiredViewAsType(source, R.id.txtIdIssuePlace, "field 'txtIdIssuePlace'", TextView.class);
    target.txtBirthDate = Utils.findRequiredViewAsType(source, R.id.txtBirthDate, "field 'txtBirthDate'", TextView.class);
    target.txtAddress = Utils.findRequiredViewAsType(source, R.id.tvAddress, "field 'txtAddress'", TextView.class);
    target.imgEditCusInfo = Utils.findRequiredViewAsType(source, R.id.imgEditCusInfo, "field 'imgEditCusInfo'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.txtCustName = null;
    target.txtIdNo = null;
    target.txtIdIssueDate = null;
    target.txtIdIssuePlace = null;
    target.txtBirthDate = null;
    target.txtAddress = null;
    target.imgEditCusInfo = null;

    this.target = null;
  }
}
