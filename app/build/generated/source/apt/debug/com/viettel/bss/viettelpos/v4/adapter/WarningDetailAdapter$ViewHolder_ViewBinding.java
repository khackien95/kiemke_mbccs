// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WarningDetailAdapter$ViewHolder_ViewBinding<T extends WarningDetailAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public WarningDetailAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvOrder = Utils.findRequiredViewAsType(source, R.id.tvOrder, "field 'tvOrder'", TextView.class);
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.tvTitle, "field 'tvTitle'", TextView.class);
    target.rvLstAttribute = Utils.findRequiredViewAsType(source, R.id.rvLstAttribute, "field 'rvLstAttribute'", RecyclerView.class);
    target.imgWarning = Utils.findRequiredViewAsType(source, R.id.imgWarning, "field 'imgWarning'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvOrder = null;
    target.tvTitle = null;
    target.rvLstAttribute = null;
    target.imgWarning = null;

    this.target = null;
  }
}
