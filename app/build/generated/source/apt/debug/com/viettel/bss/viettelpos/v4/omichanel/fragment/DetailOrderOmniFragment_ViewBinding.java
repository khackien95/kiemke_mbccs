// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DetailOrderOmniFragment_ViewBinding<T extends DetailOrderOmniFragment> implements Unbinder {
  protected T target;

  @UiThread
  public DetailOrderOmniFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.imgEditCusInfo = Utils.findRequiredViewAsType(source, R.id.imgEditCusInfo, "field 'imgEditCusInfo'", ImageView.class);
    target.llCusOld = Utils.findRequiredViewAsType(source, R.id.llCusOld, "field 'llCusOld'", LinearLayout.class);
    target.llCMTImage = Utils.findRequiredViewAsType(source, R.id.llCMTImage, "field 'llCMTImage'", LinearLayout.class);
    target.tvOldInfo = Utils.findRequiredViewAsType(source, R.id.tvOldInfo, "field 'tvOldInfo'", TextView.class);
    target.tvAlertCusInvalidShop = Utils.findRequiredViewAsType(source, R.id.tvAlertCusInvalidShop, "field 'tvAlertCusInvalidShop'", TextView.class);
    target.tvCustName = Utils.findRequiredViewAsType(source, R.id.tvCustName, "field 'tvCustName'", TextView.class);
    target.tvBirthDay = Utils.findRequiredViewAsType(source, R.id.tvBirthDay, "field 'tvBirthDay'", TextView.class);
    target.tvCusIdInfo = Utils.findRequiredViewAsType(source, R.id.tvCusIdInfo, "field 'tvCusIdInfo'", TextView.class);
    target.tvRecipientPhone = Utils.findRequiredViewAsType(source, R.id.tvRecipientPhone, "field 'tvRecipientPhone'", TextView.class);
    target.tvCusAddress = Utils.findRequiredViewAsType(source, R.id.tvCusAddress, "field 'tvCusAddress'", TextView.class);
    target.imgEditCMT = Utils.findRequiredViewAsType(source, R.id.imgEditCMT, "field 'imgEditCMT'", ImageView.class);
    target.imgCMTBefore = Utils.findRequiredViewAsType(source, R.id.imgCMTBefore, "field 'imgCMTBefore'", ImageView.class);
    target.imgCMTAfter = Utils.findRequiredViewAsType(source, R.id.imgCMTAfter, "field 'imgCMTAfter'", ImageView.class);
    target.linNumberInfoContent = Utils.findRequiredViewAsType(source, R.id.linNumberInfoContent, "field 'linNumberInfoContent'", LinearLayout.class);
    target.llNumberInfoDetail = Utils.findRequiredViewAsType(source, R.id.llNumberInfoDetail, "field 'llNumberInfoDetail'", LinearLayout.class);
    target.tvIsdn = Utils.findRequiredViewAsType(source, R.id.tvIsdn, "field 'tvIsdn'", TextView.class);
    target.tvFeeIsdn = Utils.findRequiredViewAsType(source, R.id.tvFeeIsdn, "field 'tvFeeIsdn'", TextView.class);
    target.imgEditNumber = Utils.findRequiredViewAsType(source, R.id.imgEditNumber, "field 'imgEditNumber'", ImageView.class);
    target.tvOderTypeDesc = Utils.findRequiredViewAsType(source, R.id.tvOderTypeDesc, "field 'tvOderTypeDesc'", TextView.class);
    target.llIsdnPrice = Utils.findRequiredViewAsType(source, R.id.llIsdnPrice, "field 'llIsdnPrice'", LinearLayout.class);
    target.tvIsdnPrice = Utils.findRequiredViewAsType(source, R.id.tvIsdnPrice, "field 'tvIsdnPrice'", TextView.class);
    target.llPostpaidInfo = Utils.findRequiredViewAsType(source, R.id.llPostpaidInfo, "field 'llPostpaidInfo'", LinearLayout.class);
    target.tvIsdnAmount = Utils.findRequiredViewAsType(source, R.id.tvIsdnAmount, "field 'tvIsdnAmount'", TextView.class);
    target.tvIsdnTime = Utils.findRequiredViewAsType(source, R.id.tvIsdnTime, "field 'tvIsdnTime'", TextView.class);
    target.llBundlePacket = Utils.findRequiredViewAsType(source, R.id.llBundlePacket, "field 'llBundlePacket'", LinearLayout.class);
    target.tvBundleName = Utils.findRequiredViewAsType(source, R.id.tvBundleName, "field 'tvBundleName'", TextView.class);
    target.llBundlePacketDetail = Utils.findRequiredViewAsType(source, R.id.llBundlePacketDetail, "field 'llBundlePacketDetail'", LinearLayout.class);
    target.imgEditBundle = Utils.findRequiredViewAsType(source, R.id.imgEditBundle, "field 'imgEditBundle'", ImageView.class);
    target.tvBundleDesc = Utils.findRequiredViewAsType(source, R.id.tvBundleDesc, "field 'tvBundleDesc'", TextView.class);
    target.llBundleConvertFee = Utils.findRequiredViewAsType(source, R.id.llBundleConvertFee, "field 'llBundleConvertFee'", LinearLayout.class);
    target.llBundlePrice = Utils.findRequiredViewAsType(source, R.id.llBundlePrice, "field 'llBundlePrice'", LinearLayout.class);
    target.tvBundleConvertFee = Utils.findRequiredViewAsType(source, R.id.tvBundleConvertFee, "field 'tvBundleConvertFee'", TextView.class);
    target.tvBundlePrice = Utils.findRequiredViewAsType(source, R.id.tvBundlePrice, "field 'tvBundlePrice'", TextView.class);
    target.llvasInfo = Utils.findRequiredViewAsType(source, R.id.llvasInfo, "field 'llvasInfo'", LinearLayout.class);
    target.imgEditVasPlus = Utils.findRequiredViewAsType(source, R.id.imgEditVasPlus, "field 'imgEditVasPlus'", ImageView.class);
    target.recVasInfo = Utils.findRequiredViewAsType(source, R.id.recVasInfo, "field 'recVasInfo'", RecyclerView.class);
    target.llChargeCardInfo = Utils.findRequiredViewAsType(source, R.id.llChargeCardInfo, "field 'llChargeCardInfo'", LinearLayout.class);
    target.llTotalFee = Utils.findRequiredViewAsType(source, R.id.llTotalFee, "field 'llTotalFee'", LinearLayout.class);
    target.tvChargeCardAmound = Utils.findRequiredViewAsType(source, R.id.tvChargeCardAmound, "field 'tvChargeCardAmound'", TextView.class);
    target.imgChargeCard = Utils.findRequiredViewAsType(source, R.id.imgChargeCard, "field 'imgChargeCard'", ImageView.class);
    target.llSignature = Utils.findRequiredViewAsType(source, R.id.llSignature, "field 'llSignature'", LinearLayout.class);
    target.frlSignatureLayout = Utils.findRequiredViewAsType(source, R.id.frlSignatureLayout, "field 'frlSignatureLayout'", FrameLayout.class);
    target.imageSignature = Utils.findRequiredViewAsType(source, R.id.imgSignature, "field 'imageSignature'", ImageView.class);
    target.imgShowSignature = Utils.findRequiredViewAsType(source, R.id.imgShowSignature, "field 'imgShowSignature'", ImageView.class);
    target.cbConfirmAccept = Utils.findRequiredViewAsType(source, R.id.cbConfirmAccept, "field 'cbConfirmAccept'", CheckBox.class);
    target.tvSignature = Utils.findRequiredViewAsType(source, R.id.tvSignature, "field 'tvSignature'", TextView.class);
    target.tvTransactionPlace = Utils.findRequiredViewAsType(source, R.id.tvTransactionPlace, "field 'tvTransactionPlace'", TextView.class);
    target.llFeeTrans = Utils.findRequiredViewAsType(source, R.id.llFeeTrans, "field 'llFeeTrans'", LinearLayout.class);
    target.tvFeeTrans = Utils.findRequiredViewAsType(source, R.id.tvFeeTrans, "field 'tvFeeTrans'", TextView.class);
    target.tvAddressTrans = Utils.findRequiredViewAsType(source, R.id.tvAddressTrans, "field 'tvAddressTrans'", TextView.class);
    target.tvTotalFee = Utils.findRequiredViewAsType(source, R.id.tvTotalFee, "field 'tvTotalFee'", TextView.class);
    target.tvPayType = Utils.findRequiredViewAsType(source, R.id.tvPayType, "field 'tvPayType'", TextView.class);
    target.tvPayState = Utils.findRequiredViewAsType(source, R.id.tvPayState, "field 'tvPayState'", TextView.class);
    target.llCondition = Utils.findRequiredViewAsType(source, R.id.llCondition, "field 'llCondition'", LinearLayout.class);
    target.tvConditionerInfo = Utils.findRequiredViewAsType(source, R.id.tvConditionerInfo, "field 'tvConditionerInfo'", TextView.class);
    target.btnAction = Utils.findRequiredViewAsType(source, R.id.btnAction, "field 'btnAction'", Button.class);
    target.linCusName = Utils.findRequiredViewAsType(source, R.id.linCusName, "field 'linCusName'", LinearLayout.class);
    target.linBirthDay = Utils.findRequiredViewAsType(source, R.id.linBirthDay, "field 'linBirthDay'", LinearLayout.class);
    target.linCusIdInfo = Utils.findRequiredViewAsType(source, R.id.linCusIdInfo, "field 'linCusIdInfo'", LinearLayout.class);
    target.linRecipientPhone = Utils.findRequiredViewAsType(source, R.id.linRecipientPhone, "field 'linRecipientPhone'", LinearLayout.class);
    target.linCusAddress = Utils.findRequiredViewAsType(source, R.id.linCusAddress, "field 'linCusAddress'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.imgEditCusInfo = null;
    target.llCusOld = null;
    target.llCMTImage = null;
    target.tvOldInfo = null;
    target.tvAlertCusInvalidShop = null;
    target.tvCustName = null;
    target.tvBirthDay = null;
    target.tvCusIdInfo = null;
    target.tvRecipientPhone = null;
    target.tvCusAddress = null;
    target.imgEditCMT = null;
    target.imgCMTBefore = null;
    target.imgCMTAfter = null;
    target.linNumberInfoContent = null;
    target.llNumberInfoDetail = null;
    target.tvIsdn = null;
    target.tvFeeIsdn = null;
    target.imgEditNumber = null;
    target.tvOderTypeDesc = null;
    target.llIsdnPrice = null;
    target.tvIsdnPrice = null;
    target.llPostpaidInfo = null;
    target.tvIsdnAmount = null;
    target.tvIsdnTime = null;
    target.llBundlePacket = null;
    target.tvBundleName = null;
    target.llBundlePacketDetail = null;
    target.imgEditBundle = null;
    target.tvBundleDesc = null;
    target.llBundleConvertFee = null;
    target.llBundlePrice = null;
    target.tvBundleConvertFee = null;
    target.tvBundlePrice = null;
    target.llvasInfo = null;
    target.imgEditVasPlus = null;
    target.recVasInfo = null;
    target.llChargeCardInfo = null;
    target.llTotalFee = null;
    target.tvChargeCardAmound = null;
    target.imgChargeCard = null;
    target.llSignature = null;
    target.frlSignatureLayout = null;
    target.imageSignature = null;
    target.imgShowSignature = null;
    target.cbConfirmAccept = null;
    target.tvSignature = null;
    target.tvTransactionPlace = null;
    target.llFeeTrans = null;
    target.tvFeeTrans = null;
    target.tvAddressTrans = null;
    target.tvTotalFee = null;
    target.tvPayType = null;
    target.tvPayState = null;
    target.llCondition = null;
    target.tvConditionerInfo = null;
    target.btnAction = null;
    target.linCusName = null;
    target.linBirthDay = null;
    target.linCusIdInfo = null;
    target.linRecipientPhone = null;
    target.linCusAddress = null;

    this.target = null;
  }
}
