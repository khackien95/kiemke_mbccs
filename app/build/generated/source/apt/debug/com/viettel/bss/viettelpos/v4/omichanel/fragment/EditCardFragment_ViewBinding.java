// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EditCardFragment_ViewBinding<T extends EditCardFragment> implements Unbinder {
  protected T target;

  @UiThread
  public EditCardFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.btnSave = Utils.findRequiredViewAsType(source, R.id.btnSave, "field 'btnSave'", Button.class);
    target.tvStart = Utils.findRequiredViewAsType(source, R.id.tvStart, "field 'tvStart'", TextView.class);
    target.tvEnd = Utils.findRequiredViewAsType(source, R.id.tvEnd, "field 'tvEnd'", TextView.class);
    target.seekBar = Utils.findRequiredViewAsType(source, R.id.seekBar, "field 'seekBar'", SeekBar.class);
    target.editSelectCard = Utils.findRequiredViewAsType(source, R.id.editSelectCard, "field 'editSelectCard'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.btnSave = null;
    target.tvStart = null;
    target.tvEnd = null;
    target.seekBar = null;
    target.editSelectCard = null;

    this.target = null;
  }
}
