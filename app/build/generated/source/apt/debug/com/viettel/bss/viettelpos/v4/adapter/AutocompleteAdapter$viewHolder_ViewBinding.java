// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AutocompleteAdapter$viewHolder_ViewBinding<T extends AutocompleteAdapter.viewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public AutocompleteAdapter$viewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.name = Utils.findRequiredViewAsType(source, R.id.spinner_value, "field 'name'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.name = null;

    this.target = null;
  }
}
