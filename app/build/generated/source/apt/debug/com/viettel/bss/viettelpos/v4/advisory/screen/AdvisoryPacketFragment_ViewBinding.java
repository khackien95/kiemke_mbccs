// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.advisory.screen;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AdvisoryPacketFragment_ViewBinding<T extends AdvisoryPacketFragment> implements Unbinder {
  protected T target;

  @UiThread
  public AdvisoryPacketFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.contentLayout = Utils.findRequiredViewAsType(source, R.id.contentLayout, "field 'contentLayout'", LinearLayout.class);
    target.expandListView = Utils.findRequiredViewAsType(source, R.id.expandListView, "field 'expandListView'", ExpandableListView.class);
    target.tvNoData = Utils.findRequiredViewAsType(source, R.id.tvNoData, "field 'tvNoData'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.contentLayout = null;
    target.expandListView = null;
    target.tvNoData = null;

    this.target = null;
  }
}
