// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.dialog;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AgeSubscriberDialog_ViewBinding<T extends AgeSubscriberDialog> implements Unbinder {
  protected T target;

  private View view2131757270;

  private View view2131757271;

  private View view2131757272;

  @UiThread
  public AgeSubscriberDialog_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.scrollView, "field 'scrollView'", NestedScrollView.class);
    target.noMonth = Utils.findRequiredViewAsType(source, R.id.noMonth, "field 'noMonth'", TextView.class);
    target.noMonthNoData = Utils.findRequiredViewAsType(source, R.id.noMonthNoData, "field 'noMonthNoData'", TextView.class);
    target.totalChargeTitle = Utils.findRequiredViewAsType(source, R.id.totalChargeTitle, "field 'totalChargeTitle'", TextView.class);
    target.totalCharge = Utils.findRequiredViewAsType(source, R.id.totalCharge, "field 'totalCharge'", TextView.class);
    target.totalChargeNotData = Utils.findRequiredViewAsType(source, R.id.totalChargeNotData, "field 'totalChargeNotData'", TextView.class);
    target.avgCharge = Utils.findRequiredViewAsType(source, R.id.avgCharge, "field 'avgCharge'", TextView.class);
    target.avgChargeNoData = Utils.findRequiredViewAsType(source, R.id.avgChargeNoData, "field 'avgChargeNoData'", TextView.class);
    target.tvListNotData = Utils.findRequiredViewAsType(source, R.id.tvListNotData, "field 'tvListNotData'", TextView.class);
    target.pieChart = Utils.findRequiredViewAsType(source, R.id.pieChart, "field 'pieChart'", PieChart.class);
    target.lineChart = Utils.findRequiredViewAsType(source, R.id.lineChart, "field 'lineChart'", LineChart.class);
    view = Utils.findRequiredView(source, R.id.icTableChart, "field 'icTableChart' and method 'tableChartOnClick'");
    target.icTableChart = Utils.castView(view, R.id.icTableChart, "field 'icTableChart'", ImageView.class);
    view2131757270 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.tableChartOnClick();
      }
    });
    target.tableChart = Utils.findRequiredViewAsType(source, R.id.tableChart, "field 'tableChart'", LinearLayout.class);
    target.rvCharge = Utils.findRequiredViewAsType(source, R.id.rvCharge, "field 'rvCharge'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.icPieChart, "method 'pieChartOnClick'");
    view2131757271 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.pieChartOnClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.icChartLine, "method 'lineChartOnClick'");
    view2131757272 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.lineChartOnClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.scrollView = null;
    target.noMonth = null;
    target.noMonthNoData = null;
    target.totalChargeTitle = null;
    target.totalCharge = null;
    target.totalChargeNotData = null;
    target.avgCharge = null;
    target.avgChargeNoData = null;
    target.tvListNotData = null;
    target.pieChart = null;
    target.lineChart = null;
    target.icTableChart = null;
    target.tableChart = null;
    target.rvCharge = null;

    view2131757270.setOnClickListener(null);
    view2131757270 = null;
    view2131757271.setOnClickListener(null);
    view2131757271 = null;
    view2131757272.setOnClickListener(null);
    view2131757272 = null;

    this.target = null;
  }
}
