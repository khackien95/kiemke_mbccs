// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.report.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentLookupLogMBCCS_ViewBinding<T extends FragmentLookupLogMBCCS> implements Unbinder {
  protected T target;

  private View view2131755412;

  private View view2131755413;

  private View view2131755414;

  private View view2131756685;

  @UiThread
  public FragmentLookupLogMBCCS_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.edtFromDate, "field 'edtFromDate' and method 'onClickFromDate'");
    target.edtFromDate = Utils.castView(view, R.id.edtFromDate, "field 'edtFromDate'", EditText.class);
    view2131755412 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickFromDate();
      }
    });
    view = Utils.findRequiredView(source, R.id.edtToDate, "field 'edtToDate' and method 'onClickToDate'");
    target.edtToDate = Utils.castView(view, R.id.edtToDate, "field 'edtToDate'", EditText.class);
    view2131755413 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickToDate();
      }
    });
    target.edtUserCall = Utils.findRequiredViewAsType(source, R.id.edtUserCall, "field 'edtUserCall'", EditText.class);
    target.edtInputValue = Utils.findRequiredViewAsType(source, R.id.edtInputValue, "field 'edtInputValue'", EditText.class);
    target.edtResultValue = Utils.findRequiredViewAsType(source, R.id.edtResultValue, "field 'edtResultValue'", EditText.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerViewTrans, "field 'recyclerView'", RecyclerView.class);
    target.tvTotalRecord = Utils.findRequiredViewAsType(source, R.id.tvTotalRecord, "field 'tvTotalRecord'", TextView.class);
    target.actxtAction = Utils.findRequiredViewAsType(source, R.id.actxtAction, "field 'actxtAction'", AutoCompleteTextView.class);
    view = Utils.findRequiredView(source, R.id.btnSearch, "method 'search'");
    view2131755414 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.search();
      }
    });
    view = Utils.findRequiredView(source, R.id.btnReport, "method 'report'");
    view2131756685 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.report();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtFromDate = null;
    target.edtToDate = null;
    target.edtUserCall = null;
    target.edtInputValue = null;
    target.edtResultValue = null;
    target.recyclerView = null;
    target.tvTotalRecord = null;
    target.actxtAction = null;

    view2131755412.setOnClickListener(null);
    view2131755412 = null;
    view2131755413.setOnClickListener(null);
    view2131755413 = null;
    view2131755414.setOnClickListener(null);
    view2131755414 = null;
    view2131756685.setOnClickListener(null);
    view2131756685 = null;

    this.target = null;
  }
}
