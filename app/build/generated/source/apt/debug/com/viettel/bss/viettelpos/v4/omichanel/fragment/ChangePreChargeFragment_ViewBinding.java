// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChangePreChargeFragment_ViewBinding<T extends ChangePreChargeFragment> implements Unbinder {
  protected T target;

  @UiThread
  public ChangePreChargeFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.tvCustName = Utils.findRequiredViewAsType(source, R.id.tvCustName, "field 'tvCustName'", TextView.class);
    target.tvCustId = Utils.findRequiredViewAsType(source, R.id.tvCustId, "field 'tvCustId'", TextView.class);
    target.tvCustBirthday = Utils.findRequiredViewAsType(source, R.id.tvCustBirthday, "field 'tvCustBirthday'", TextView.class);
    target.tvIssuePlace = Utils.findRequiredViewAsType(source, R.id.tvIssuePlace, "field 'tvIssuePlace'", TextView.class);
    target.tvAccount = Utils.findRequiredViewAsType(source, R.id.tvAccount, "field 'tvAccount'", TextView.class);
    target.rgStartDate = Utils.findRequiredViewAsType(source, R.id.rgStartDate, "field 'rgStartDate'", RadioGroup.class);
    target.rbThisDay = Utils.findRequiredViewAsType(source, R.id.rbThisDay, "field 'rbThisDay'", RadioButton.class);
    target.rbOtherDay = Utils.findRequiredViewAsType(source, R.id.rbOtherDay, "field 'rbOtherDay'", RadioButton.class);
    target.edtEffectDate = Utils.findRequiredViewAsType(source, R.id.edtEffectDate, "field 'edtEffectDate'", EditText.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.btnAction = Utils.findRequiredViewAsType(source, R.id.btnAction, "field 'btnAction'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvCustName = null;
    target.tvCustId = null;
    target.tvCustBirthday = null;
    target.tvIssuePlace = null;
    target.tvAccount = null;
    target.rgStartDate = null;
    target.rbThisDay = null;
    target.rbOtherDay = null;
    target.edtEffectDate = null;
    target.recyclerView = null;
    target.btnAction = null;

    this.target = null;
  }
}
