// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.stockinpect.view;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StockInpectWithQuantityActivity_ViewBinding<T extends StockInpectWithQuantityActivity> implements Unbinder {
  protected T target;

  @UiThread
  public StockInpectWithQuantityActivity_ViewBinding(T target, View source) {
    this.target = target;

    target.recListSerialNumber = Utils.findRequiredViewAsType(source, R.id.recListSerialNumber, "field 'recListSerialNumber'", RecyclerView.class);
    target.confirm_inspect_serial = Utils.findRequiredViewAsType(source, R.id.confirm_inspect_serial, "field 'confirm_inspect_serial'", CheckBox.class);
    target.btnInspect = Utils.findRequiredViewAsType(source, R.id.btnInspect, "field 'btnInspect'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.recListSerialNumber = null;
    target.confirm_inspect_serial = null;
    target.btnInspect = null;

    this.target = null;
  }
}
