// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.edittextplus;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EditTextPlusItemtView_ViewBinding<T extends EditTextPlusItemtView> implements Unbinder {
  protected T target;

  @UiThread
  public EditTextPlusItemtView_ViewBinding(T target, View source) {
    this.target = target;

    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
    target.edtInput = Utils.findRequiredViewAsType(source, R.id.edtInput, "field 'edtInput'", EditText.class);
    target.spnValue = Utils.findRequiredViewAsType(source, R.id.spnValue, "field 'spnValue'", Spinner.class);
    target.lnContain = Utils.findRequiredViewAsType(source, R.id.lnContain, "field 'lnContain'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvName = null;
    target.edtInput = null;
    target.spnValue = null;
    target.lnContain = null;

    this.target = null;
  }
}
