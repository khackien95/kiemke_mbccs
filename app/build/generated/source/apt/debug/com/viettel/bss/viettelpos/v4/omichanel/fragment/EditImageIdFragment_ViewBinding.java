// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EditImageIdFragment_ViewBinding<T extends EditImageIdFragment> implements Unbinder {
  protected T target;

  @UiThread
  public EditImageIdFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.txtChooseBefore = Utils.findRequiredViewAsType(source, R.id.txtChooseBefore, "field 'txtChooseBefore'", TextView.class);
    target.txtChooseAfter = Utils.findRequiredViewAsType(source, R.id.txtChooseAfter, "field 'txtChooseAfter'", TextView.class);
    target.btnSave = Utils.findRequiredViewAsType(source, R.id.btnSave, "field 'btnSave'", Button.class);
    target.btnCancel = Utils.findRequiredViewAsType(source, R.id.btnCancel, "field 'btnCancel'", Button.class);
    target.imageAfter = Utils.findRequiredViewAsType(source, R.id.imageAfter, "field 'imageAfter'", ImageView.class);
    target.imageBefore = Utils.findRequiredViewAsType(source, R.id.imageBefore, "field 'imageBefore'", ImageView.class);
    target.imageRotateRightBefore = Utils.findRequiredViewAsType(source, R.id.imageRotateRightBefore, "field 'imageRotateRightBefore'", ImageView.class);
    target.imageRotateRightAfter = Utils.findRequiredViewAsType(source, R.id.imageRotateRightAfter, "field 'imageRotateRightAfter'", ImageView.class);
    target.tvNameCus = Utils.findRequiredViewAsType(source, R.id.tvNameCus, "field 'tvNameCus'", TextView.class);
    target.tvBirthDay = Utils.findRequiredViewAsType(source, R.id.tvBirthDay, "field 'tvBirthDay'", TextView.class);
    target.tvCmt = Utils.findRequiredViewAsType(source, R.id.tvCmt, "field 'tvCmt'", TextView.class);
    target.tvPhone = Utils.findRequiredViewAsType(source, R.id.tvPhone, "field 'tvPhone'", TextView.class);
    target.tvAddress = Utils.findRequiredViewAsType(source, R.id.tvAddress, "field 'tvAddress'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.txtChooseBefore = null;
    target.txtChooseAfter = null;
    target.btnSave = null;
    target.btnCancel = null;
    target.imageAfter = null;
    target.imageBefore = null;
    target.imageRotateRightBefore = null;
    target.imageRotateRightAfter = null;
    target.tvNameCus = null;
    target.tvBirthDay = null;
    target.tvCmt = null;
    target.tvPhone = null;
    target.tvAddress = null;

    this.target = null;
  }
}
