// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.CheckBox;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CheckBoxSelectAdapter$ViewHolder_ViewBinding<T extends CheckBoxSelectAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public CheckBoxSelectAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.checkbox1 = Utils.findRequiredViewAsType(source, R.id.checkbox1, "field 'checkbox1'", CheckBox.class);
    target.checkbox2 = Utils.findRequiredViewAsType(source, R.id.checkbox2, "field 'checkbox2'", CheckBox.class);
    target.checkbox3 = Utils.findRequiredViewAsType(source, R.id.checkbox3, "field 'checkbox3'", CheckBox.class);
    target.checkbox4 = Utils.findRequiredViewAsType(source, R.id.checkbox4, "field 'checkbox4'", CheckBox.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.checkbox1 = null;
    target.checkbox2 = null;
    target.checkbox3 = null;
    target.checkbox4 = null;

    this.target = null;
  }
}
