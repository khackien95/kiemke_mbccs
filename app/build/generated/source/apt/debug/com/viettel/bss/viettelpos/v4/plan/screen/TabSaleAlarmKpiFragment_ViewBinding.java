// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.plan.screen;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TabSaleAlarmKpiFragment_ViewBinding<T extends TabSaleAlarmKpiFragment> implements Unbinder {
  protected T target;

  @UiThread
  public TabSaleAlarmKpiFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", SuperRecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.recyclerView = null;

    this.target = null;
  }
}
