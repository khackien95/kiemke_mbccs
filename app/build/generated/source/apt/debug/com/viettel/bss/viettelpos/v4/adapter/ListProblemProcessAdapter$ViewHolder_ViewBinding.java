// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListProblemProcessAdapter$ViewHolder_ViewBinding<T extends ListProblemProcessAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public ListProblemProcessAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvActionName = Utils.findRequiredViewAsType(source, R.id.tvActionName, "field 'tvActionName'", TextView.class);
    target.tvProcessStaff = Utils.findRequiredViewAsType(source, R.id.tvProcessStaff, "field 'tvProcessStaff'", TextView.class);
    target.tvProcessDate = Utils.findRequiredViewAsType(source, R.id.tvProcessDate, "field 'tvProcessDate'", TextView.class);
    target.tvProcessShop = Utils.findRequiredViewAsType(source, R.id.tvProcessShop, "field 'tvProcessShop'", TextView.class);
    target.tvProcessContent = Utils.findRequiredViewAsType(source, R.id.tvProcessContent, "field 'tvProcessContent'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvActionName = null;
    target.tvProcessStaff = null;
    target.tvProcessDate = null;
    target.tvProcessShop = null;
    target.tvProcessContent = null;

    this.target = null;
  }
}
