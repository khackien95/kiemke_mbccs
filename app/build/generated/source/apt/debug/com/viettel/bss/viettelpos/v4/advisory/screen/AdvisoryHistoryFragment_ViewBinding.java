// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.advisory.screen;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AdvisoryHistoryFragment_ViewBinding<T extends AdvisoryHistoryFragment> implements Unbinder {
  protected T target;

  @UiThread
  public AdvisoryHistoryFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.lvHistory = Utils.findRequiredViewAsType(source, R.id.lvHistory, "field 'lvHistory'", ListView.class);
    target.tvNoData = Utils.findRequiredViewAsType(source, R.id.tvNoData, "field 'tvNoData'", TextView.class);
    target.swipeRefreshLayout = Utils.findRequiredViewAsType(source, R.id.swipeRefreshLayout, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.lvHistory = null;
    target.tvNoData = null;
    target.swipeRefreshLayout = null;

    this.target = null;
  }
}
