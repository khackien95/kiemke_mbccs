// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.hsdt.fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kyanogen.signatureview.SignatureView;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SignatureDialogFragment_ViewBinding<T extends SignatureDialogFragment> implements Unbinder {
  protected T target;

  @UiThread
  public SignatureDialogFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.signatureView = Utils.findRequiredViewAsType(source, R.id.signature_view, "field 'signatureView'", SignatureView.class);
    target.clear = Utils.findRequiredViewAsType(source, R.id.clear, "field 'clear'", Button.class);
    target.save = Utils.findRequiredViewAsType(source, R.id.save, "field 'save'", Button.class);
    target.imgBtnClose = Utils.findRequiredViewAsType(source, R.id.imgBtnClose, "field 'imgBtnClose'", ImageButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.signatureView = null;
    target.clear = null;
    target.save = null;
    target.imgBtnClose = null;

    this.target = null;
  }
}
