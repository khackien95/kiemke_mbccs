// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ReportRevenueAdapter$ViewHolder_ViewBinding<T extends ReportRevenueAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public ReportRevenueAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvStation = Utils.findRequiredViewAsType(source, R.id.tvStation, "field 'tvStation'", TextView.class);
    target.tvCriteria = Utils.findRequiredViewAsType(source, R.id.tvCriteria, "field 'tvCriteria'", TextView.class);
    target.tvSln = Utils.findRequiredViewAsType(source, R.id.tvSln, "field 'tvSln'", TextView.class);
    target.tvDeltaSln = Utils.findRequiredViewAsType(source, R.id.tvDeltaSln, "field 'tvDeltaSln'", TextView.class);
    target.tvSlt = Utils.findRequiredViewAsType(source, R.id.tvSlt, "field 'tvSlt'", TextView.class);
    target.tvDeltaSlt = Utils.findRequiredViewAsType(source, R.id.tvDeltaSlt, "field 'tvDeltaSlt'", TextView.class);
    target.view0 = Utils.findRequiredView(source, R.id.view0, "field 'view0'");
    target.view1 = Utils.findRequiredView(source, R.id.view1, "field 'view1'");
    target.view2 = Utils.findRequiredView(source, R.id.view2, "field 'view2'");
    target.view3 = Utils.findRequiredView(source, R.id.view3, "field 'view3'");
    target.view4 = Utils.findRequiredView(source, R.id.view4, "field 'view4'");
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvStation = null;
    target.tvCriteria = null;
    target.tvSln = null;
    target.tvDeltaSln = null;
    target.tvSlt = null;
    target.tvDeltaSlt = null;
    target.view0 = null;
    target.view1 = null;
    target.view2 = null;
    target.view3 = null;
    target.view4 = null;

    this.target = null;
  }
}
