// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.dialog;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DialogSelectProfileFragment_ViewBinding<T extends DialogSelectProfileFragment> implements Unbinder {
  protected T target;

  private View view2131756419;

  private View view2131756674;

  private View view2131756677;

  @UiThread
  public DialogSelectProfileFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.imgClose, "field 'imgClose' and method 'imgCloseClick'");
    target.imgClose = Utils.castView(view, R.id.imgClose, "field 'imgClose'", ImageView.class);
    view2131756419 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.imgCloseClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.lnHSCu, "field 'lnHSCu' and method 'lnHSCuClick'");
    target.lnHSCu = Utils.castView(view, R.id.lnHSCu, "field 'lnHSCu'", LinearLayout.class);
    view2131756674 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.lnHSCuClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.lnHSMoi, "field 'lnHSMoi' and method 'lnHSMoiClick'");
    target.lnHSMoi = Utils.castView(view, R.id.lnHSMoi, "field 'lnHSMoi'", LinearLayout.class);
    view2131756677 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.lnHSMoiClick();
      }
    });
    target.tvViewProfile = Utils.findRequiredViewAsType(source, R.id.tvViewProfile, "field 'tvViewProfile'", TextView.class);
    target.cbSelect = Utils.findRequiredViewAsType(source, R.id.cbSelect, "field 'cbSelect'", CheckBox.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.imgClose = null;
    target.lnHSCu = null;
    target.lnHSMoi = null;
    target.tvViewProfile = null;
    target.cbSelect = null;

    view2131756419.setOnClickListener(null);
    view2131756419 = null;
    view2131756674.setOnClickListener(null);
    view2131756674 = null;
    view2131756677.setOnClickListener(null);
    view2131756677 = null;

    this.target = null;
  }
}
