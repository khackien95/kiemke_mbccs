// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.picture;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PictureBoxView_ViewBinding<T extends PictureBoxView> implements Unbinder {
  protected T target;

  private View view2131756277;

  @UiThread
  public PictureBoxView_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.hsRoot = Utils.findRequiredViewAsType(source, R.id.hsRoot, "field 'hsRoot'", HorizontalScrollView.class);
    target.mPictureBox = Utils.findRequiredViewAsType(source, R.id.picture_box, "field 'mPictureBox'", LinearLayout.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
    view = Utils.findRequiredView(source, R.id.layoutTakePhoto, "field 'layoutTakePhoto' and method 'takePhoto'");
    target.layoutTakePhoto = Utils.castView(view, R.id.layoutTakePhoto, "field 'layoutTakePhoto'", RelativeLayout.class);
    view2131756277 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.takePhoto();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.hsRoot = null;
    target.mPictureBox = null;
    target.tvName = null;
    target.layoutTakePhoto = null;

    view2131756277.setOnClickListener(null);
    view2131756277 = null;

    this.target = null;
  }
}
