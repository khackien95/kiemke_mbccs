// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChooseFileFragment_ViewBinding<T extends ChooseFileFragment> implements Unbinder {
  protected T target;

  @UiThread
  public ChooseFileFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.tvTitle, "field 'tvTitle'", TextView.class);
    target.lvUploadImage = Utils.findRequiredViewAsType(source, R.id.lvUploadImage, "field 'lvUploadImage'", ListView.class);
    target.btnAccept = Utils.findRequiredViewAsType(source, R.id.btnAccept, "field 'btnAccept'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvTitle = null;
    target.lvUploadImage = null;
    target.btnAccept = null;

    this.target = null;
  }
}
