// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.sale.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatImageButton;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AdapterIsdnOwnerObjectIsdn$ViewHolder_ViewBinding<T extends AdapterIsdnOwnerObjectIsdn.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public AdapterIsdnOwnerObjectIsdn$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tv_phoneNumber = Utils.findRequiredViewAsType(source, R.id.tv_phoneNumber, "field 'tv_phoneNumber'", TextView.class);
    target.tv_status_stockmodel = Utils.findRequiredViewAsType(source, R.id.tv_status_stockmodel, "field 'tv_status_stockmodel'", TextView.class);
    target.tv_container = Utils.findRequiredViewAsType(source, R.id.tv_container, "field 'tv_container'", TextView.class);
    target.btn_lockIsdn = Utils.findRequiredViewAsType(source, R.id.btn_lockIsdn, "field 'btn_lockIsdn'", AppCompatImageButton.class);
    target.tvStockmodelName = Utils.findRequiredViewAsType(source, R.id.tvStockmodelName, "field 'tvStockmodelName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tv_phoneNumber = null;
    target.tv_status_stockmodel = null;
    target.tv_container = null;
    target.btn_lockIsdn = null;
    target.tvStockmodelName = null;

    this.target = null;
  }
}
