// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.searchcustomer;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SearchCustomerFragment_ViewBinding<T extends SearchCustomerFragment> implements Unbinder {
  protected T target;

  private View view2131756624;

  private View view2131755238;

  @UiThread
  public SearchCustomerFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.check_btn, "field 'mCheckBtn' and method 'onClickView'");
    target.mCheckBtn = Utils.castView(view, R.id.check_btn, "field 'mCheckBtn'", Button.class);
    view2131756624 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickView(p0);
      }
    });
    target.mNumberExhibitEdt = Utils.findRequiredViewAsType(source, R.id.number_exhibit_edt, "field 'mNumberExhibitEdt'", EditText.class);
    target.mNumberAccEdt = Utils.findRequiredViewAsType(source, R.id.number_acc_edt, "field 'mNumberAccEdt'", EditText.class);
    view = Utils.findRequiredView(source, R.id.back_iv, "method 'onClickView'");
    view2131755238 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickView(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mCheckBtn = null;
    target.mNumberExhibitEdt = null;
    target.mNumberAccEdt = null;

    view2131756624.setOnClickListener(null);
    view2131756624 = null;
    view2131755238.setOnClickListener(null);
    view2131755238 = null;

    this.target = null;
  }
}
