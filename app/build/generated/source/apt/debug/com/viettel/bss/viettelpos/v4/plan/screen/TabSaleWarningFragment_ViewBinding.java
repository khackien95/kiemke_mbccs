// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.plan.screen;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TabSaleWarningFragment_ViewBinding<T extends TabSaleWarningFragment> implements Unbinder {
  protected T target;

  @UiThread
  public TabSaleWarningFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.tvPlanDate = Utils.findRequiredViewAsType(source, R.id.tvPlanDate, "field 'tvPlanDate'", TextView.class);
    target.tvTotalRow = Utils.findRequiredViewAsType(source, R.id.tvTotalRow, "field 'tvTotalRow'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", SuperRecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvPlanDate = null;
    target.tvTotalRow = null;
    target.recyclerView = null;

    this.target = null;
  }
}
