// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GroupBoxView_ViewBinding<T extends GroupBoxView> implements Unbinder {
  protected T target;

  @UiThread
  public GroupBoxView_ViewBinding(T target, View source) {
    this.target = target;

    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
    target.boxRoot = Utils.findRequiredViewAsType(source, R.id.boxRoot, "field 'boxRoot'", LinearLayout.class);
    target.boxGroup = Utils.findRequiredViewAsType(source, R.id.boxGroup, "field 'boxGroup'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvName = null;
    target.boxRoot = null;
    target.boxGroup = null;

    this.target = null;
  }
}
