// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.plan.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SalePlansAdapter$SalePlansHolder_ViewBinding<T extends SalePlansAdapter.SalePlansHolder> implements Unbinder {
  protected T target;

  @UiThread
  public SalePlansAdapter$SalePlansHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvObjectName = Utils.findRequiredViewAsType(source, R.id.tvObjectName, "field 'tvObjectName'", TextView.class);
    target.tvAddress = Utils.findRequiredViewAsType(source, R.id.tvAddress, "field 'tvAddress'", TextView.class);
    target.tvDate = Utils.findRequiredViewAsType(source, R.id.tvDate, "field 'tvDate'", TextView.class);
    target.tvObjectcode = Utils.findRequiredViewAsType(source, R.id.tvObjectcode, "field 'tvObjectcode'", TextView.class);
    target.imvDelete = Utils.findRequiredViewAsType(source, R.id.imvDelete, "field 'imvDelete'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvObjectName = null;
    target.tvAddress = null;
    target.tvDate = null;
    target.tvObjectcode = null;
    target.imvDelete = null;

    this.target = null;
  }
}
