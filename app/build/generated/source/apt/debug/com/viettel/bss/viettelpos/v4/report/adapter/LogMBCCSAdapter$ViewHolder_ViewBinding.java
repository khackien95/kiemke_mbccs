// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.report.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LogMBCCSAdapter$ViewHolder_ViewBinding<T extends LogMBCCSAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public LogMBCCSAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvUserCall = Utils.findRequiredViewAsType(source, R.id.tvUserCall, "field 'tvUserCall'", TextView.class);
    target.tvExecuteDate = Utils.findRequiredViewAsType(source, R.id.tvExecuteDate, "field 'tvExecuteDate'", TextView.class);
    target.tvIpServer = Utils.findRequiredViewAsType(source, R.id.tvIpServer, "field 'tvIpServer'", TextView.class);
    target.tvLogMethodResult = Utils.findRequiredViewAsType(source, R.id.tvLogMethodResult, "field 'tvLogMethodResult'", TextView.class);
    target.tvLogMethodDuration = Utils.findRequiredViewAsType(source, R.id.tvLogMethodDuration, "field 'tvLogMethodDuration'", TextView.class);
    target.tvLogMethodClassName = Utils.findRequiredViewAsType(source, R.id.tvLogMethodClassName, "field 'tvLogMethodClassName'", TextView.class);
    target.tvOrder = Utils.findRequiredViewAsType(source, R.id.tvOrder, "field 'tvOrder'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvUserCall = null;
    target.tvExecuteDate = null;
    target.tvIpServer = null;
    target.tvLogMethodResult = null;
    target.tvLogMethodDuration = null;
    target.tvLogMethodClassName = null;
    target.tvOrder = null;

    this.target = null;
  }
}
