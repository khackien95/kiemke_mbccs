// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.report.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.github.mikephil.charting.charts.BarChart;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentShowReport_ViewBinding<T extends FragmentShowReport> implements Unbinder {
  protected T target;

  @UiThread
  public FragmentShowReport_ViewBinding(T target, View source) {
    this.target = target;

    target.barChart = Utils.findRequiredViewAsType(source, R.id.barChart, "field 'barChart'", BarChart.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.barChart = null;

    this.target = null;
  }
}
