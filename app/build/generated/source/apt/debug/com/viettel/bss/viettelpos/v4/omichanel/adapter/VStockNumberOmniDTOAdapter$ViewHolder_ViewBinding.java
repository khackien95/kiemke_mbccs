// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class VStockNumberOmniDTOAdapter$ViewHolder_ViewBinding<T extends VStockNumberOmniDTOAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public VStockNumberOmniDTOAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.btnChoiseNumber = Utils.findRequiredViewAsType(source, R.id.btnChoiseNumber, "field 'btnChoiseNumber'", Button.class);
    target.tvNumber = Utils.findRequiredViewAsType(source, R.id.tvNumber, "field 'tvNumber'", TextView.class);
    target.tvPrice = Utils.findRequiredViewAsType(source, R.id.tvPrice, "field 'tvPrice'", TextView.class);
    target.tvPledgeAmount = Utils.findRequiredViewAsType(source, R.id.tvPledgeAmount, "field 'tvPledgeAmount'", TextView.class);
    target.tvPledgeTime = Utils.findRequiredViewAsType(source, R.id.tvPledgeTime, "field 'tvPledgeTime'", TextView.class);
    target.linPosInfo = Utils.findRequiredViewAsType(source, R.id.linPosInfo, "field 'linPosInfo'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.btnChoiseNumber = null;
    target.tvNumber = null;
    target.tvPrice = null;
    target.tvPledgeAmount = null;
    target.tvPledgeTime = null;
    target.linPosInfo = null;

    this.target = null;
  }
}
