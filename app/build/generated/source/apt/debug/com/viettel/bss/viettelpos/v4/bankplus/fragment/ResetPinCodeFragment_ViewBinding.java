// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.bankplus.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ResetPinCodeFragment_ViewBinding<T extends ResetPinCodeFragment> implements Unbinder {
  protected T target;

  @UiThread
  public ResetPinCodeFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.lvKpp = Utils.findRequiredViewAsType(source, R.id.lvKpp, "field 'lvKpp'", ListView.class);
    target.searchView = Utils.findRequiredViewAsType(source, R.id.searchView, "field 'searchView'", SearchView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.lvKpp = null;
    target.searchView = null;

    this.target = null;
  }
}
