// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.date;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DateItemView_ViewBinding<T extends DateItemView> implements Unbinder {
  protected T target;

  private View view2131756269;

  private View view2131756267;

  @UiThread
  public DateItemView_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.tvDate = Utils.findRequiredViewAsType(source, R.id.tvDate, "field 'tvDate'", TextView.class);
    view = Utils.findRequiredView(source, R.id.imvClear, "field 'imvClear' and method 'clearDate'");
    target.imvClear = Utils.castView(view, R.id.imvClear, "field 'imvClear'", ImageView.class);
    view2131756269 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.clearDate();
      }
    });
    view = Utils.findRequiredView(source, R.id.layoutDate, "method 'showDate'");
    view2131756267 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showDate();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvDate = null;
    target.imvClear = null;

    view2131756269.setOnClickListener(null);
    view2131756269 = null;
    view2131756267.setOnClickListener(null);
    view2131756267 = null;

    this.target = null;
  }
}
