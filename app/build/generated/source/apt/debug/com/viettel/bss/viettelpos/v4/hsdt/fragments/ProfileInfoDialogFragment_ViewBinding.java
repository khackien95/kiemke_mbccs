// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.hsdt.fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProfileInfoDialogFragment_ViewBinding<T extends ProfileInfoDialogFragment> implements Unbinder {
  protected T target;

  @UiThread
  public ProfileInfoDialogFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.recListProfile = Utils.findRequiredViewAsType(source, R.id.recListProfile, "field 'recListProfile'", RecyclerView.class);
    target.linSignatureContentTotal = Utils.findRequiredViewAsType(source, R.id.linSignatureContentTotal, "field 'linSignatureContentTotal'", LinearLayout.class);
    target.linSignatureContentOne = Utils.findRequiredViewAsType(source, R.id.linSignatureContentOne, "field 'linSignatureContentOne'", LinearLayout.class);
    target.tvSigCusOne = Utils.findRequiredViewAsType(source, R.id.tvSigCusOne, "field 'tvSigCusOne'", TextView.class);
    target.frlSignatureLayoutOne = Utils.findRequiredViewAsType(source, R.id.frlSignatureLayoutOne, "field 'frlSignatureLayoutOne'", FrameLayout.class);
    target.imgShowSignatureOne = Utils.findRequiredViewAsType(source, R.id.imgShowSignatureOne, "field 'imgShowSignatureOne'", ImageView.class);
    target.imgbtEditSignatureOne = Utils.findRequiredViewAsType(source, R.id.imgbtEditSignatureOne, "field 'imgbtEditSignatureOne'", ImageButton.class);
    target.linSignatureContentTwo = Utils.findRequiredViewAsType(source, R.id.linSignatureContentTwo, "field 'linSignatureContentTwo'", LinearLayout.class);
    target.tvSigCusTwo = Utils.findRequiredViewAsType(source, R.id.tvSigCusTwo, "field 'tvSigCusTwo'", TextView.class);
    target.frlSignatureLayoutTwo = Utils.findRequiredViewAsType(source, R.id.frlSignatureLayoutTwo, "field 'frlSignatureLayoutTwo'", FrameLayout.class);
    target.imgShowSignatureTwo = Utils.findRequiredViewAsType(source, R.id.imgShowSignatureTwo, "field 'imgShowSignatureTwo'", ImageView.class);
    target.imgbtEditSignatureTwo = Utils.findRequiredViewAsType(source, R.id.imgbtEditSignatureTwo, "field 'imgbtEditSignatureTwo'", ImageButton.class);
    target.lnCheckBoxSelect = Utils.findRequiredViewAsType(source, R.id.lnCheckBoxSelect, "field 'lnCheckBoxSelect'", LinearLayout.class);
    target.cbSelectUseSignature = Utils.findRequiredViewAsType(source, R.id.cbSelectUseSignature, "field 'cbSelectUseSignature'", CheckBox.class);
    target.btnCancel = Utils.findRequiredViewAsType(source, R.id.btnCancel, "field 'btnCancel'", Button.class);
    target.btnAccept = Utils.findRequiredViewAsType(source, R.id.btnAccept, "field 'btnAccept'", Button.class);
    target.imgBtnClose = Utils.findRequiredViewAsType(source, R.id.imgBtnClose, "field 'imgBtnClose'", ImageButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.recListProfile = null;
    target.linSignatureContentTotal = null;
    target.linSignatureContentOne = null;
    target.tvSigCusOne = null;
    target.frlSignatureLayoutOne = null;
    target.imgShowSignatureOne = null;
    target.imgbtEditSignatureOne = null;
    target.linSignatureContentTwo = null;
    target.tvSigCusTwo = null;
    target.frlSignatureLayoutTwo = null;
    target.imgShowSignatureTwo = null;
    target.imgbtEditSignatureTwo = null;
    target.lnCheckBoxSelect = null;
    target.cbSelectUseSignature = null;
    target.btnCancel = null;
    target.btnAccept = null;
    target.imgBtnClose = null;

    this.target = null;
  }
}
