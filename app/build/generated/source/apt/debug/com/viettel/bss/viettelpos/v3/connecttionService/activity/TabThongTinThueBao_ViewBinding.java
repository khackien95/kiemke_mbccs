// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v3.connecttionService.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TabThongTinThueBao_ViewBinding<T extends TabThongTinThueBao> implements Unbinder {
  protected T target;

  @UiThread
  public TabThongTinThueBao_ViewBinding(T target, View source) {
    this.target = target;

    target.lnSelectProfile = Utils.findRequiredViewAsType(source, R.id.lnSelectProfile, "field 'lnSelectProfile'", LinearLayout.class);
    target.thumbnails = Utils.findRequiredViewAsType(source, R.id.thumbnails, "field 'thumbnails'", LinearLayout.class);
    target.horizontalScrollView = Utils.findRequiredViewAsType(source, R.id.horizontalScrollView, "field 'horizontalScrollView'", HorizontalScrollView.class);
    target.expandedImageView = Utils.findRequiredViewAsType(source, R.id.expandedImageView, "field 'expandedImageView'", ImageView.class);
    target.frlMain = Utils.findRequiredViewAsType(source, R.id.frlMain, "field 'frlMain'", FrameLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.lnSelectProfile = null;
    target.thumbnails = null;
    target.horizontalScrollView = null;
    target.expandedImageView = null;
    target.frlMain = null;

    this.target = null;
  }
}
