// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.connecttionService.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentApprovedManageRequest_ViewBinding<T extends FragmentApprovedManageRequest> implements Unbinder {
  protected T target;

  @UiThread
  public FragmentApprovedManageRequest_ViewBinding(T target, View source) {
    this.target = target;

    target.edtIsdn = Utils.findRequiredViewAsType(source, R.id.edtIsdn, "field 'edtIsdn'", EditText.class);
    target.edtReqNote = Utils.findRequiredViewAsType(source, R.id.edtReqNote, "field 'edtReqNote'", EditText.class);
    target.dpkReqDateTime = Utils.findRequiredViewAsType(source, R.id.dpkReqDateTime, "field 'dpkReqDateTime'", EditText.class);
    target.spnActionType = Utils.findRequiredViewAsType(source, R.id.spnActionType, "field 'spnActionType'", Spinner.class);
    target.edtContent = Utils.findRequiredViewAsType(source, R.id.edtContent, "field 'edtContent'", EditText.class);
    target.btnConfirm = Utils.findRequiredViewAsType(source, R.id.btnConfirm, "field 'btnConfirm'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtIsdn = null;
    target.edtReqNote = null;
    target.dpkReqDateTime = null;
    target.spnActionType = null;
    target.edtContent = null;
    target.btnConfirm = null;

    this.target = null;
  }
}
