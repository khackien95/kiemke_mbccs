// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.report.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentShowDetailLog_ViewBinding<T extends FragmentShowDetailLog> implements Unbinder {
  protected T target;

  private View view2131756426;

  private View view2131757673;

  private View view2131757676;

  @UiThread
  public FragmentShowDetailLog_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.tvUserCall = Utils.findRequiredViewAsType(source, R.id.tvUserCall, "field 'tvUserCall'", TextView.class);
    target.tvExecuteDate = Utils.findRequiredViewAsType(source, R.id.tvExecuteDate, "field 'tvExecuteDate'", TextView.class);
    target.tvIpServer = Utils.findRequiredViewAsType(source, R.id.tvIpServer, "field 'tvIpServer'", TextView.class);
    target.tvLogMethodResult = Utils.findRequiredViewAsType(source, R.id.tvLogMethodResult, "field 'tvLogMethodResult'", TextView.class);
    target.tvLogMethodDuration = Utils.findRequiredViewAsType(source, R.id.tvLogMethodDuration, "field 'tvLogMethodDuration'", TextView.class);
    target.tvLogMethodClassName = Utils.findRequiredViewAsType(source, R.id.tvLogMethodClassName, "field 'tvLogMethodClassName'", TextView.class);
    target.tvInputValue = Utils.findRequiredViewAsType(source, R.id.tvInputValue, "field 'tvInputValue'", TextView.class);
    target.tvResultValue = Utils.findRequiredViewAsType(source, R.id.tvResultValue, "field 'tvResultValue'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btnInfoGeneral, "field 'btnInfoGeneral' and method 'onBtnInfoGeneralClick'");
    target.btnInfoGeneral = Utils.castView(view, R.id.btnInfoGeneral, "field 'btnInfoGeneral'", Button.class);
    view2131756426 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBtnInfoGeneralClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.btnInputValue, "field 'btnInputValue' and method 'onBtnInputValueClick'");
    target.btnInputValue = Utils.castView(view, R.id.btnInputValue, "field 'btnInputValue'", Button.class);
    view2131757673 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBtnInputValueClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.btnResultValue, "field 'btnResultValue' and method 'onBtnResultValueClick'");
    target.btnResultValue = Utils.castView(view, R.id.btnResultValue, "field 'btnResultValue'", Button.class);
    view2131757676 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBtnResultValueClick();
      }
    });
    target.expanInfoGeneral = Utils.findRequiredViewAsType(source, R.id.expanInfoGeneral, "field 'expanInfoGeneral'", ExpandableLinearLayout.class);
    target.expanInputValue = Utils.findRequiredViewAsType(source, R.id.expanInputValue, "field 'expanInputValue'", ExpandableLinearLayout.class);
    target.expanResultValue = Utils.findRequiredViewAsType(source, R.id.expanResultValue, "field 'expanResultValue'", ExpandableLinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvUserCall = null;
    target.tvExecuteDate = null;
    target.tvIpServer = null;
    target.tvLogMethodResult = null;
    target.tvLogMethodDuration = null;
    target.tvLogMethodClassName = null;
    target.tvInputValue = null;
    target.tvResultValue = null;
    target.btnInfoGeneral = null;
    target.btnInputValue = null;
    target.btnResultValue = null;
    target.expanInfoGeneral = null;
    target.expanInputValue = null;
    target.expanResultValue = null;

    view2131756426.setOnClickListener(null);
    view2131756426 = null;
    view2131757673.setOnClickListener(null);
    view2131757673 = null;
    view2131757676.setOnClickListener(null);
    view2131757676 = null;

    this.target = null;
  }
}
