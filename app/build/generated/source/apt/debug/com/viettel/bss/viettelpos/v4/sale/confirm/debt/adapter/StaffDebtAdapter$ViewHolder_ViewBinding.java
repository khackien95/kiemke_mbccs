// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.sale.confirm.debt.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StaffDebtAdapter$ViewHolder_ViewBinding<T extends StaffDebtAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public StaffDebtAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvDebtType = Utils.findRequiredViewAsType(source, R.id.tvDebtType, "field 'tvDebtType'", TextView.class);
    target.tvNeedConfirm = Utils.findRequiredViewAsType(source, R.id.tvNeedConfirm, "field 'tvNeedConfirm'", TextView.class);
    target.tvConfirmed = Utils.findRequiredViewAsType(source, R.id.tvConfirmed, "field 'tvConfirmed'", TextView.class);
    target.tvDifferent = Utils.findRequiredViewAsType(source, R.id.tvDifferent, "field 'tvDifferent'", TextView.class);
    target.tvReason = Utils.findRequiredViewAsType(source, R.id.tvReason, "field 'tvReason'", TextView.class);
    target.img = Utils.findRequiredView(source, R.id.imageView, "field 'img'");
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvDebtType = null;
    target.tvNeedConfirm = null;
    target.tvConfirmed = null;
    target.tvDifferent = null;
    target.tvReason = null;
    target.img = null;

    this.target = null;
  }
}
