// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.listnetworkinfor;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NetworkCollectAdapter$NetworkCollectHolder_ViewBinding<T extends NetworkCollectAdapter.NetworkCollectHolder> implements Unbinder {
  protected T target;

  @UiThread
  public NetworkCollectAdapter$NetworkCollectHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
    target.tvRequired = Utils.findRequiredViewAsType(source, R.id.tvRequired, "field 'tvRequired'", TextView.class);
    target.imgIcon = Utils.findRequiredViewAsType(source, R.id.imgIcon, "field 'imgIcon'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvName = null;
    target.tvRequired = null;
    target.imgIcon = null;

    this.target = null;
  }
}
