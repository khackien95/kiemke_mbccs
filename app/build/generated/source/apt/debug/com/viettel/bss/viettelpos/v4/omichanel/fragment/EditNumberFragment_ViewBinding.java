// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EditNumberFragment_ViewBinding<T extends EditNumberFragment> implements Unbinder {
  protected T target;

  @UiThread
  public EditNumberFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.listNumber = Utils.findRequiredViewAsType(source, R.id.listNumber, "field 'listNumber'", RecyclerView.class);
    target.editTypeSearch = Utils.findRequiredViewAsType(source, R.id.editTypeSearch, "field 'editTypeSearch'", EditText.class);
    target.btnSearch = Utils.findRequiredViewAsType(source, R.id.btnSearch, "field 'btnSearch'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.listNumber = null;
    target.editTypeSearch = null;
    target.btnSearch = null;

    this.target = null;
  }
}
