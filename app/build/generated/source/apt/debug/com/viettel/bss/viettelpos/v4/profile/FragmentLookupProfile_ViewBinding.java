// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.profile;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentLookupProfile_ViewBinding<T extends FragmentLookupProfile> implements Unbinder {
  protected T target;

  private View view2131755412;

  private View view2131755413;

  private View view2131755414;

  @UiThread
  public FragmentLookupProfile_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.edtIdNo = Utils.findRequiredViewAsType(source, R.id.edtIdNo, "field 'edtIdNo'", EditText.class);
    target.edtContractNo = Utils.findRequiredViewAsType(source, R.id.edtContractNo, "field 'edtContractNo'", EditText.class);
    target.edtIsdnOrAccount = Utils.findRequiredViewAsType(source, R.id.edtIsdnOrAccount, "field 'edtIsdnOrAccount'", EditText.class);
    view = Utils.findRequiredView(source, R.id.edtFromDate, "field 'edtFromDate' and method 'onClickFromDate'");
    target.edtFromDate = Utils.castView(view, R.id.edtFromDate, "field 'edtFromDate'", EditText.class);
    view2131755412 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickFromDate();
      }
    });
    view = Utils.findRequiredView(source, R.id.edtToDate, "field 'edtToDate' and method 'onClickToDate'");
    target.edtToDate = Utils.castView(view, R.id.edtToDate, "field 'edtToDate'", EditText.class);
    view2131755413 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickToDate();
      }
    });
    view = Utils.findRequiredView(source, R.id.btnSearch, "field 'btnSearch' and method 'lookupProfile'");
    target.btnSearch = Utils.castView(view, R.id.btnSearch, "field 'btnSearch'", Button.class);
    view2131755414 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.lookupProfile();
      }
    });
    target.recyclerViewTrans = Utils.findRequiredViewAsType(source, R.id.recyclerViewTrans, "field 'recyclerViewTrans'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtIdNo = null;
    target.edtContractNo = null;
    target.edtIsdnOrAccount = null;
    target.edtFromDate = null;
    target.edtToDate = null;
    target.btnSearch = null;
    target.recyclerViewTrans = null;

    view2131755412.setOnClickListener(null);
    view2131755412 = null;
    view2131755413.setOnClickListener(null);
    view2131755413 = null;
    view2131755414.setOnClickListener(null);
    view2131755414 = null;

    this.target = null;
  }
}
