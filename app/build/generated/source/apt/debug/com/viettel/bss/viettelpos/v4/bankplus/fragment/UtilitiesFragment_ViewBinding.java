// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.bankplus.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ListView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UtilitiesFragment_ViewBinding<T extends UtilitiesFragment> implements Unbinder {
  protected T target;

  @UiThread
  public UtilitiesFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.listView = Utils.findRequiredViewAsType(source, R.id.listView, "field 'listView'", ListView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.listView = null;

    this.target = null;
  }
}
