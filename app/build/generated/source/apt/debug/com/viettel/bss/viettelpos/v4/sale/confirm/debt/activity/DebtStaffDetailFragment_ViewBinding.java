// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.sale.confirm.debt.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.connecttionService.listener.ExpandableHeightListView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DebtStaffDetailFragment_ViewBinding<T extends DebtStaffDetailFragment> implements Unbinder {
  protected T target;

  private View view2131757653;

  private View view2131757656;

  @UiThread
  public DebtStaffDetailFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.lvDebt = Utils.findRequiredViewAsType(source, R.id.lvDebt, "field 'lvDebt'", ExpandableHeightListView.class);
    target.tvStartBalance = Utils.findRequiredViewAsType(source, R.id.tvStartBalance, "field 'tvStartBalance'", TextView.class);
    target.tvEndBalance = Utils.findRequiredViewAsType(source, R.id.tvEndBalance, "field 'tvEndBalance'", TextView.class);
    target.tvConfirm = Utils.findRequiredViewAsType(source, R.id.tvConfirm, "field 'tvConfirm'", TextView.class);
    target.tvDifferent = Utils.findRequiredViewAsType(source, R.id.tvDifferent, "field 'tvDifferent'", TextView.class);
    target.tvReason = Utils.findRequiredViewAsType(source, R.id.tvReason, "field 'tvReason'", TextView.class);
    target.lnDebtStaff = Utils.findRequiredView(source, R.id.lnDebtStaff, "field 'lnDebtStaff'");
    view = Utils.findRequiredView(source, R.id.lnDebtViettel, "field 'lnDebtViettel' and method 'onClickViettelDebt'");
    target.lnDebtViettel = view;
    view2131757653 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickViettelDebt();
      }
    });
    view = Utils.findRequiredView(source, R.id.btnConfirmDebt, "field 'btnConfirmDebt' and method 'showDialogConfirm'");
    target.btnConfirmDebt = Utils.castView(view, R.id.btnConfirmDebt, "field 'btnConfirmDebt'", Button.class);
    view2131757656 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showDialogConfirm();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.lvDebt = null;
    target.tvStartBalance = null;
    target.tvEndBalance = null;
    target.tvConfirm = null;
    target.tvDifferent = null;
    target.tvReason = null;
    target.lnDebtStaff = null;
    target.lnDebtViettel = null;
    target.btnConfirmDebt = null;

    view2131757653.setOnClickListener(null);
    view2131757653 = null;
    view2131757656.setOnClickListener(null);
    view2131757656 = null;

    this.target = null;
  }
}
