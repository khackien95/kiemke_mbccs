// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class IncomeStatementDetailsFragment_ViewBinding<T extends IncomeStatementDetailsFragment> implements Unbinder {
  protected T target;

  @UiThread
  public IncomeStatementDetailsFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.tvTitle, "field 'tvTitle'", TextView.class);
    target.tvValue = Utils.findRequiredViewAsType(source, R.id.tvValue, "field 'tvValue'", TextView.class);
    target.tvFormula = Utils.findRequiredViewAsType(source, R.id.tvFormula, "field 'tvFormula'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvTitle = null;
    target.tvValue = null;
    target.tvFormula = null;
    target.recyclerView = null;

    this.target = null;
  }
}
