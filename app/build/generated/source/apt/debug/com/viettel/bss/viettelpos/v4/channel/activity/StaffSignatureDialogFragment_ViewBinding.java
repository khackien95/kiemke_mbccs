// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.channel.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kyanogen.signatureview.SignatureView;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StaffSignatureDialogFragment_ViewBinding<T extends StaffSignatureDialogFragment> implements Unbinder {
  protected T target;

  @UiThread
  public StaffSignatureDialogFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.signatureView = Utils.findRequiredViewAsType(source, R.id.signature_view, "field 'signatureView'", SignatureView.class);
    target.clear = Utils.findRequiredViewAsType(source, R.id.clear, "field 'clear'", Button.class);
    target.save = Utils.findRequiredViewAsType(source, R.id.save, "field 'save'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.signatureView = null;
    target.clear = null;
    target.save = null;

    this.target = null;
  }
}
