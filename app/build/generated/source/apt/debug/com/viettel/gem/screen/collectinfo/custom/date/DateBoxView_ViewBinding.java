// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.date;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DateBoxView_ViewBinding<T extends DateBoxView> implements Unbinder {
  protected T target;

  @UiThread
  public DateBoxView_ViewBinding(T target, View source) {
    this.target = target;

    target.boxDate = Utils.findRequiredViewAsType(source, R.id.boxDate, "field 'boxDate'", LinearLayout.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.boxDate = null;
    target.tvName = null;

    this.target = null;
  }
}
