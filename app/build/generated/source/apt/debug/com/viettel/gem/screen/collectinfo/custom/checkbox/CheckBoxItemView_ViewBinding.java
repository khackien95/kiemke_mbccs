// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.checkbox;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CheckBoxItemView_ViewBinding<T extends CheckBoxItemView> implements Unbinder {
  protected T target;

  private View view2131756264;

  @UiThread
  public CheckBoxItemView_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.cboValue, "field 'cboValue' and method 'onCheckedChanged'");
    target.cboValue = Utils.castView(view, R.id.cboValue, "field 'cboValue'", CheckBox.class);
    view2131756264 = view;
    ((CompoundButton) view).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton p0, boolean p1) {
        target.onCheckedChanged(p1);
      }
    });
    target.edtInput = Utils.findRequiredViewAsType(source, R.id.edtInput, "field 'edtInput'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.cboValue = null;
    target.edtInput = null;

    ((CompoundButton) view2131756264).setOnCheckedChangeListener(null);
    view2131756264 = null;

    this.target = null;
  }
}
