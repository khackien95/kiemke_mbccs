// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.customer.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListComplainFragment_ViewBinding<T extends ListComplainFragment> implements Unbinder {
  protected T target;

  @UiThread
  public ListComplainFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.rvListComplain = Utils.findRequiredViewAsType(source, R.id.rvListComplain, "field 'rvListComplain'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.rvListComplain = null;

    this.target = null;
  }
}
