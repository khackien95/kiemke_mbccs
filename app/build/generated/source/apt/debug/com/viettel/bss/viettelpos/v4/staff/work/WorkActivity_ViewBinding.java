// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.staff.work;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WorkActivity_ViewBinding<T extends WorkActivity> implements Unbinder {
  protected T target;

  private View view2131755269;

  private View view2131755268;

  @UiThread
  public WorkActivity_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.mTVName = Utils.findRequiredViewAsType(source, R.id.tv_name, "field 'mTVName'", TextView.class);
    target.mTVAssign = Utils.findRequiredViewAsType(source, R.id.tv_assign, "field 'mTVAssign'", TextView.class);
    target.mTVTimeStart = Utils.findRequiredViewAsType(source, R.id.tv_time_from_date, "field 'mTVTimeStart'", TextView.class);
    target.mTVTimeEnd = Utils.findRequiredViewAsType(source, R.id.tv_time_to_date, "field 'mTVTimeEnd'", TextView.class);
    target.mTVContent = Utils.findRequiredViewAsType(source, R.id.tv_content, "field 'mTVContent'", TextView.class);
    target.mEdtReason = Utils.findRequiredViewAsType(source, R.id.edt_reason, "field 'mEdtReason'", EditText.class);
    target.mEdtSolution = Utils.findRequiredViewAsType(source, R.id.edt_solution, "field 'mEdtSolution'", EditText.class);
    target.mSpStatus = Utils.findRequiredViewAsType(source, R.id.sp_status, "field 'mSpStatus'", AppCompatSpinner.class);
    view = Utils.findRequiredView(source, R.id.btn_update, "field 'btn_update' and method 'onUpdate'");
    target.btn_update = Utils.castView(view, R.id.btn_update, "field 'btn_update'", Button.class);
    view2131755269 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onUpdate(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_close, "method 'onClose'");
    view2131755268 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClose();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mTVName = null;
    target.mTVAssign = null;
    target.mTVTimeStart = null;
    target.mTVTimeEnd = null;
    target.mTVContent = null;
    target.mEdtReason = null;
    target.mEdtSolution = null;
    target.mSpStatus = null;
    target.btn_update = null;

    view2131755269.setOnClickListener(null);
    view2131755269 = null;
    view2131755268.setOnClickListener(null);
    view2131755268 = null;

    this.target = null;
  }
}
