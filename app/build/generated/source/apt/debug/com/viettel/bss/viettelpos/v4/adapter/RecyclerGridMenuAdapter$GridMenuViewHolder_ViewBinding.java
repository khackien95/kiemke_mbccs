// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RecyclerGridMenuAdapter$GridMenuViewHolder_ViewBinding<T extends RecyclerGridMenuAdapter.GridMenuViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public RecyclerGridMenuAdapter$GridMenuViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvTitile = Utils.findRequiredViewAsType(source, R.id.tvTitle, "field 'tvTitile'", TextView.class);
    target.gridView = Utils.findRequiredViewAsType(source, R.id.gridView, "field 'gridView'", GridView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvTitile = null;
    target.gridView = null;

    this.target = null;
  }
}
