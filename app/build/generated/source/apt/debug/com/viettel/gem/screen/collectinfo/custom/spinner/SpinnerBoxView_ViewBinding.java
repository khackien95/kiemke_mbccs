// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.spinner;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SpinnerBoxView_ViewBinding<T extends SpinnerBoxView> implements Unbinder {
  protected T target;

  private View view2131756290;

  @UiThread
  public SpinnerBoxView_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
    view = Utils.findRequiredView(source, R.id.imvAdd, "field 'imvAdd' and method 'addRow'");
    target.imvAdd = Utils.castView(view, R.id.imvAdd, "field 'imvAdd'", ImageView.class);
    view2131756290 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.addRow();
      }
    });
    target.boxSpinner = Utils.findRequiredViewAsType(source, R.id.boxSpinner, "field 'boxSpinner'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvName = null;
    target.imvAdd = null;
    target.boxSpinner = null;

    view2131756290.setOnClickListener(null);
    view2131756290 = null;

    this.target = null;
  }
}
