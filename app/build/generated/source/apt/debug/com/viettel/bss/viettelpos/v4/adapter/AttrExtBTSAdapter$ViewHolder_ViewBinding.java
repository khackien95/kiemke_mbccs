// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AttrExtBTSAdapter$ViewHolder_ViewBinding<T extends AttrExtBTSAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public AttrExtBTSAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
    target.tvValue = Utils.findRequiredViewAsType(source, R.id.tvValue, "field 'tvValue'", TextView.class);
    target.tvValueInc = Utils.findRequiredViewAsType(source, R.id.tvValueInc, "field 'tvValueInc'", TextView.class);
    target.imgInc = Utils.findRequiredViewAsType(source, R.id.imgInc, "field 'imgInc'", AppCompatImageView.class);
    target.imgDec = Utils.findRequiredViewAsType(source, R.id.imgDec, "field 'imgDec'", AppCompatImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvName = null;
    target.tvValue = null;
    target.tvValueInc = null;
    target.imgInc = null;
    target.imgDec = null;

    this.target = null;
  }
}
