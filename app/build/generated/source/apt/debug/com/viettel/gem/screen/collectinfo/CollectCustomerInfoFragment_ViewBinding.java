// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CollectCustomerInfoFragment_ViewBinding<T extends CollectCustomerInfoFragment> implements Unbinder {
  protected T target;

  private View view2131755238;

  private View view2131755508;

  private View view2131756617;

  private View view2131756620;

  @UiThread
  public CollectCustomerInfoFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.back_iv, "field 'mBackIv' and method 'onClickView'");
    target.mBackIv = Utils.castView(view, R.id.back_iv, "field 'mBackIv'", ImageView.class);
    view2131755238 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickView(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.title_tv, "field 'mTitleTv' and method 'onClickView'");
    target.mTitleTv = Utils.castView(view, R.id.title_tv, "field 'mTitleTv'", TextView.class);
    view2131755508 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickView(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.action_iv, "field 'mActioniv' and method 'onClickView'");
    target.mActioniv = Utils.castView(view, R.id.action_iv, "field 'mActioniv'", ImageView.class);
    view2131756617 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickView(p0);
      }
    });
    target.svRoot = Utils.findRequiredViewAsType(source, R.id.svRoot, "field 'svRoot'", ScrollView.class);
    target.contentRoot = Utils.findRequiredViewAsType(source, R.id.contentRoot, "field 'contentRoot'", LinearLayout.class);
    target.rlTitle = Utils.findRequiredViewAsType(source, R.id.rlTitle, "field 'rlTitle'", RelativeLayout.class);
    view = Utils.findRequiredView(source, R.id.btnUpdate, "field 'btnUpdate' and method 'onClickView'");
    target.btnUpdate = Utils.castView(view, R.id.btnUpdate, "field 'btnUpdate'", Button.class);
    view2131756620 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickView(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mBackIv = null;
    target.mTitleTv = null;
    target.mActioniv = null;
    target.svRoot = null;
    target.contentRoot = null;
    target.rlTitle = null;
    target.btnUpdate = null;

    view2131755238.setOnClickListener(null);
    view2131755238 = null;
    view2131755508.setOnClickListener(null);
    view2131755508 = null;
    view2131756617.setOnClickListener(null);
    view2131756617 = null;
    view2131756620.setOnClickListener(null);
    view2131756620 = null;

    this.target = null;
  }
}
