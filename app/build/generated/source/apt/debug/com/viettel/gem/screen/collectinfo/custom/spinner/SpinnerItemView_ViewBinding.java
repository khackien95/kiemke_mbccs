// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.spinner;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SpinnerItemView_ViewBinding<T extends SpinnerItemView> implements Unbinder {
  protected T target;

  private View view2131756286;

  private View view2131756287;

  private View view2131756288;

  @UiThread
  public SpinnerItemView_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.spnName = Utils.findRequiredViewAsType(source, R.id.spnName, "field 'spnName'", Spinner.class);
    view = Utils.findRequiredView(source, R.id.imvDown, "field 'imvDown' and method 'onClickDown'");
    target.imvDown = Utils.castView(view, R.id.imvDown, "field 'imvDown'", ImageView.class);
    view2131756286 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickDown();
      }
    });
    view = Utils.findRequiredView(source, R.id.tvCount, "field 'tvCount' and method 'changeCount'");
    target.tvCount = Utils.castView(view, R.id.tvCount, "field 'tvCount'", TextView.class);
    view2131756287 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.changeCount();
      }
    });
    view = Utils.findRequiredView(source, R.id.imvUp, "field 'imvUp' and method 'onClickUp'");
    target.imvUp = Utils.castView(view, R.id.imvUp, "field 'imvUp'", ImageView.class);
    view2131756288 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickUp();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.spnName = null;
    target.imvDown = null;
    target.tvCount = null;
    target.imvUp = null;

    view2131756286.setOnClickListener(null);
    view2131756286 = null;
    view2131756287.setOnClickListener(null);
    view2131756287 = null;
    view2131756288.setOnClickListener(null);
    view2131756288 = null;

    this.target = null;
  }
}
