// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EditBundleFragment_ViewBinding<T extends EditBundleFragment> implements Unbinder {
  protected T target;

  @UiThread
  public EditBundleFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.llFilter = Utils.findRequiredViewAsType(source, R.id.llFilter, "field 'llFilter'", LinearLayout.class);
    target.spnObject = Utils.findRequiredViewAsType(source, R.id.spnObject, "field 'spnObject'", Spinner.class);
    target.recProductView = Utils.findRequiredViewAsType(source, R.id.recProductView, "field 'recProductView'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.llFilter = null;
    target.spnObject = null;
    target.recProductView = null;

    this.target = null;
  }
}
