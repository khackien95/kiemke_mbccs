// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.plan.screen;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CreatePlanFragment_ViewBinding<T extends CreatePlanFragment> implements Unbinder {
  protected T target;

  private View view2131756255;

  private View view2131756261;

  private View view2131756262;

  @UiThread
  public CreatePlanFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.edtAddress, "field 'mEdtAddress' and method 'onClickView'");
    target.mEdtAddress = Utils.castView(view, R.id.edtAddress, "field 'mEdtAddress'", EditText.class);
    view2131756255 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickView(p0);
      }
    });
    target.precinctSpin = Utils.findRequiredViewAsType(source, R.id.precinctSpin, "field 'precinctSpin'", Spinner.class);
    target.mVillageSpin = Utils.findRequiredViewAsType(source, R.id.villageSpin, "field 'mVillageSpin'", Spinner.class);
    target.edtIns = Utils.findRequiredViewAsType(source, R.id.edtIns, "field 'edtIns'", EditText.class);
    target.edtStreet = Utils.findRequiredViewAsType(source, R.id.edtStreet, "field 'edtStreet'", EditText.class);
    view = Utils.findRequiredView(source, R.id.edtDate, "field 'mEdtDate' and method 'onClickView'");
    target.mEdtDate = Utils.castView(view, R.id.edtDate, "field 'mEdtDate'", TextView.class);
    view2131756261 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickView(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btnCreatePlan, "field 'mBtnCreatePlan' and method 'onClickView'");
    target.mBtnCreatePlan = Utils.castView(view, R.id.btnCreatePlan, "field 'mBtnCreatePlan'", Button.class);
    view2131756262 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickView(p0);
      }
    });
    target.mTitleDateTv = Utils.findRequiredViewAsType(source, R.id.titleDateTv, "field 'mTitleDateTv'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mEdtAddress = null;
    target.precinctSpin = null;
    target.mVillageSpin = null;
    target.edtIns = null;
    target.edtStreet = null;
    target.mEdtDate = null;
    target.mBtnCreatePlan = null;
    target.mTitleDateTv = null;

    view2131756255.setOnClickListener(null);
    view2131756255 = null;
    view2131756261.setOnClickListener(null);
    view2131756261 = null;
    view2131756262.setOnClickListener(null);
    view2131756262 = null;

    this.target = null;
  }
}
