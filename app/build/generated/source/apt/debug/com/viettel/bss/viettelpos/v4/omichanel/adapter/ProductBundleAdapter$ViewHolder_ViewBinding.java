// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductBundleAdapter$ViewHolder_ViewBinding<T extends ProductBundleAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public ProductBundleAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.llContent = Utils.findRequiredViewAsType(source, R.id.llContent, "field 'llContent'", LinearLayout.class);
    target.btnChange = Utils.findRequiredViewAsType(source, R.id.btnChange, "field 'btnChange'", Button.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
    target.tvPrice = Utils.findRequiredViewAsType(source, R.id.tvPrice, "field 'tvPrice'", TextView.class);
    target.tvDescription = Utils.findRequiredViewAsType(source, R.id.tvDescription, "field 'tvDescription'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.llContent = null;
    target.btnChange = null;
    target.tvName = null;
    target.tvPrice = null;
    target.tvDescription = null;

    this.target = null;
  }
}
