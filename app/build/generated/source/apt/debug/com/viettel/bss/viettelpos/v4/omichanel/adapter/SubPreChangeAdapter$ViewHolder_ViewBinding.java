// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubPreChangeAdapter$ViewHolder_ViewBinding<T extends SubPreChangeAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public SubPreChangeAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.llTotalFee = Utils.findRequiredViewAsType(source, R.id.llTotalFee, "field 'llTotalFee'", RelativeLayout.class);
    target.llFeeTrans = Utils.findRequiredViewAsType(source, R.id.llFeeTrans, "field 'llFeeTrans'", RelativeLayout.class);
    target.cvDetail = Utils.findRequiredViewAsType(source, R.id.cvDetail, "field 'cvDetail'", CardView.class);
    target.llSub = Utils.findRequiredViewAsType(source, R.id.llSub, "field 'llSub'", LinearLayout.class);
    target.llCode = Utils.findRequiredViewAsType(source, R.id.llCode, "field 'llCode'", LinearLayout.class);
    target.llPageData = Utils.findRequiredViewAsType(source, R.id.llPageData, "field 'llPageData'", LinearLayout.class);
    target.llMonthAmount = Utils.findRequiredViewAsType(source, R.id.llMonthAmount, "field 'llMonthAmount'", LinearLayout.class);
    target.llFee = Utils.findRequiredViewAsType(source, R.id.llFee, "field 'llFee'", LinearLayout.class);
    target.tvTotalFrice = Utils.findRequiredViewAsType(source, R.id.tvTotalFee, "field 'tvTotalFrice'", TextView.class);
    target.tvSub = Utils.findRequiredViewAsType(source, R.id.tvSub, "field 'tvSub'", TextView.class);
    target.tvPackData = Utils.findRequiredViewAsType(source, R.id.tvPackData, "field 'tvPackData'", TextView.class);
    target.tvCDTOld = Utils.findRequiredViewAsType(source, R.id.tvCDTOld, "field 'tvCDTOld'", TextView.class);
    target.tvCDTNew = Utils.findRequiredViewAsType(source, R.id.tvCDTNew, "field 'tvCDTNew'", TextView.class);
    target.tvMonthPromotion = Utils.findRequiredViewAsType(source, R.id.tvMonthPromotion, "field 'tvMonthPromotion'", TextView.class);
    target.tvFee = Utils.findRequiredViewAsType(source, R.id.tvFee, "field 'tvFee'", TextView.class);
    target.tvFeeTrans = Utils.findRequiredViewAsType(source, R.id.tvFeeTrans, "field 'tvFeeTrans'", TextView.class);
    target.tvCode = Utils.findRequiredViewAsType(source, R.id.tvCode, "field 'tvCode'", TextView.class);
    target.imgDetail = Utils.findRequiredViewAsType(source, R.id.imgDetail, "field 'imgDetail'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.llTotalFee = null;
    target.llFeeTrans = null;
    target.cvDetail = null;
    target.llSub = null;
    target.llCode = null;
    target.llPageData = null;
    target.llMonthAmount = null;
    target.llFee = null;
    target.tvTotalFrice = null;
    target.tvSub = null;
    target.tvPackData = null;
    target.tvCDTOld = null;
    target.tvCDTNew = null;
    target.tvMonthPromotion = null;
    target.tvFee = null;
    target.tvFeeTrans = null;
    target.tvCode = null;
    target.imgDetail = null;

    this.target = null;
  }
}
