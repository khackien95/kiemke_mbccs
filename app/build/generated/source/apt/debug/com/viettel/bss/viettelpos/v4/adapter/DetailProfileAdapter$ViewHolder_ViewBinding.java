// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DetailProfileAdapter$ViewHolder_ViewBinding<T extends DetailProfileAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public DetailProfileAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvProfileName = Utils.findRequiredViewAsType(source, R.id.tvProfileName, "field 'tvProfileName'", TextView.class);
    target.tvCustomerName = Utils.findRequiredViewAsType(source, R.id.tvCustomerName, "field 'tvCustomerName'", TextView.class);
    target.tvDate = Utils.findRequiredViewAsType(source, R.id.tvDate, "field 'tvDate'", TextView.class);
    target.tvProfileStatus = Utils.findRequiredViewAsType(source, R.id.tvProfileStatus, "field 'tvProfileStatus'", TextView.class);
    target.tvOrder = Utils.findRequiredViewAsType(source, R.id.tvOrder, "field 'tvOrder'", TextView.class);
    target.imgPdf = Utils.findRequiredViewAsType(source, R.id.imgPdf, "field 'imgPdf'", ImageView.class);
    target.imgZip = Utils.findRequiredViewAsType(source, R.id.imgZip, "field 'imgZip'", ImageView.class);
    target.imgDoc = Utils.findRequiredViewAsType(source, R.id.imgDoc, "field 'imgDoc'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvProfileName = null;
    target.tvCustomerName = null;
    target.tvDate = null;
    target.tvProfileStatus = null;
    target.tvOrder = null;
    target.imgPdf = null;
    target.imgZip = null;
    target.imgDoc = null;

    this.target = null;
  }
}
