// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.infrastrucure.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.ListView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SeachNodeOnlineFragment_ViewBinding<T extends SeachNodeOnlineFragment> implements Unbinder {
  protected T target;

  @UiThread
  public SeachNodeOnlineFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.edtSearchNode = Utils.findRequiredViewAsType(source, R.id.edtSearchNode, "field 'edtSearchNode'", AppCompatEditText.class);
    target.lvNode = Utils.findRequiredViewAsType(source, R.id.lvNode, "field 'lvNode'", ListView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtSearchNode = null;
    target.lvNode = null;

    this.target = null;
  }
}
