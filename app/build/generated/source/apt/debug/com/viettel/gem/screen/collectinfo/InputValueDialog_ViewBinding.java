// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class InputValueDialog_ViewBinding<T extends InputValueDialog> implements Unbinder {
  protected T target;

  private View view2131755472;

  private View view2131755371;

  @UiThread
  public InputValueDialog_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.edtValue = Utils.findRequiredViewAsType(source, R.id.edtValue, "field 'edtValue'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btnOk, "method 'onClickOK'");
    view2131755472 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickOK();
      }
    });
    view = Utils.findRequiredView(source, R.id.btnCancel, "method 'onClickCancel'");
    view2131755371 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickCancel();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtValue = null;

    view2131755472.setOnClickListener(null);
    view2131755472 = null;
    view2131755371.setOnClickListener(null);
    view2131755371 = null;

    this.target = null;
  }
}
