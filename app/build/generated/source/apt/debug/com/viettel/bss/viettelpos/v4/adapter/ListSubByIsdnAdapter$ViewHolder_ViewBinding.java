// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListSubByIsdnAdapter$ViewHolder_ViewBinding<T extends ListSubByIsdnAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public ListSubByIsdnAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvIsdn = Utils.findRequiredViewAsType(source, R.id.tvIsdn, "field 'tvIsdn'", TextView.class);
    target.tvService = Utils.findRequiredViewAsType(source, R.id.tvService, "field 'tvService'", TextView.class);
    target.tvIsdnType = Utils.findRequiredViewAsType(source, R.id.tvIsdnType, "field 'tvIsdnType'", TextView.class);
    target.tvStatus = Utils.findRequiredViewAsType(source, R.id.tvStatus, "field 'tvStatus'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvIsdn = null;
    target.tvService = null;
    target.tvIsdnType = null;
    target.tvStatus = null;

    this.target = null;
  }
}
