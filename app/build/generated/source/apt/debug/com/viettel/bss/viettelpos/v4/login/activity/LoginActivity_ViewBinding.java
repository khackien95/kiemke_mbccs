// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.login.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActivity_ViewBinding<T extends LoginActivity> implements Unbinder {
  protected T target;

  @UiThread
  public LoginActivity_ViewBinding(T target, View source) {
    this.target = target;

    target.btnLoginPublic = Utils.findRequiredViewAsType(source, R.id.btnLoginPublic, "field 'btnLoginPublic'", Button.class);
    target.imgSwitchNetwork = Utils.findRequiredViewAsType(source, R.id.imgSwitchNetwork, "field 'imgSwitchNetwork'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.btnLoginPublic = null;
    target.imgSwitchNetwork = null;

    this.target = null;
  }
}
