// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderOmniAdapter$ViewHolder_ViewBinding<T extends OrderOmniAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public OrderOmniAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.cardView = Utils.findRequiredViewAsType(source, R.id.cardView, "field 'cardView'", CardView.class);
    target.tvCheckIn = Utils.findRequiredViewAsType(source, R.id.tvCheckIn, "field 'tvCheckIn'", TextView.class);
    target.tvOrderCode = Utils.findRequiredViewAsType(source, R.id.tvOrderCode, "field 'tvOrderCode'", TextView.class);
    target.tvOrderTarget = Utils.findRequiredViewAsType(source, R.id.tvOrderTarget, "field 'tvOrderTarget'", TextView.class);
    target.tvIsdn = Utils.findRequiredViewAsType(source, R.id.tvIsdn, "field 'tvIsdn'", TextView.class);
    target.tvRecipientPhone = Utils.findRequiredViewAsType(source, R.id.tvRecipientPhone, "field 'tvRecipientPhone'", TextView.class);
    target.tvRecipientName = Utils.findRequiredViewAsType(source, R.id.tvRecipientName, "field 'tvRecipientName'", TextView.class);
    target.tvAddress = Utils.findRequiredViewAsType(source, R.id.tvAddress, "field 'tvAddress'", TextView.class);
    target.tvCreateDate = Utils.findRequiredViewAsType(source, R.id.tvCreateDate, "field 'tvCreateDate'", TextView.class);
    target.tvOrderTypeDesc = Utils.findRequiredViewAsType(source, R.id.tvOrderTypeDesc, "field 'tvOrderTypeDesc'", TextView.class);
    target.tvDeadlineStatus = Utils.findRequiredViewAsType(source, R.id.tvDeadlineStatus, "field 'tvDeadlineStatus'", TextView.class);
    target.linIsdn = Utils.findRequiredViewAsType(source, R.id.linIsdn, "field 'linIsdn'", LinearLayout.class);
    target.linOrderTarget = Utils.findRequiredViewAsType(source, R.id.linOrderTarget, "field 'linOrderTarget'", LinearLayout.class);
    target.linRecipientPhone = Utils.findRequiredViewAsType(source, R.id.linRecipientPhone, "field 'linRecipientPhone'", LinearLayout.class);
    target.linRecipientName = Utils.findRequiredViewAsType(source, R.id.linRecipientName, "field 'linRecipientName'", LinearLayout.class);
    target.linAddress = Utils.findRequiredViewAsType(source, R.id.linAddress, "field 'linAddress'", LinearLayout.class);
    target.linCreateDate = Utils.findRequiredViewAsType(source, R.id.linCreateDate, "field 'linCreateDate'", LinearLayout.class);
    target.linOrderTypeDesc = Utils.findRequiredViewAsType(source, R.id.linOrderTypeDesc, "field 'linOrderTypeDesc'", LinearLayout.class);
    target.linIdNo = Utils.findRequiredViewAsType(source, R.id.linIdNo, "field 'linIdNo'", LinearLayout.class);
    target.linDeadlineStatus = Utils.findRequiredViewAsType(source, R.id.linDeadlineStatus, "field 'linDeadlineStatus'", LinearLayout.class);
    target.linOrderCode = Utils.findRequiredViewAsType(source, R.id.linOrderCode, "field 'linOrderCode'", LinearLayout.class);
    target.linCurrentState = Utils.findRequiredViewAsType(source, R.id.linCurrentState, "field 'linCurrentState'", LinearLayout.class);
    target.tvDestroy = Utils.findRequiredViewAsType(source, R.id.tvDestroy, "field 'tvDestroy'", TextView.class);
    target.cbccheckIdNo = Utils.findRequiredViewAsType(source, R.id.cbccheckIdNo, "field 'cbccheckIdNo'", CheckBox.class);
    target.tvIdNo = Utils.findRequiredViewAsType(source, R.id.tvIdNo, "field 'tvIdNo'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.cardView = null;
    target.tvCheckIn = null;
    target.tvOrderCode = null;
    target.tvOrderTarget = null;
    target.tvIsdn = null;
    target.tvRecipientPhone = null;
    target.tvRecipientName = null;
    target.tvAddress = null;
    target.tvCreateDate = null;
    target.tvOrderTypeDesc = null;
    target.tvDeadlineStatus = null;
    target.linIsdn = null;
    target.linOrderTarget = null;
    target.linRecipientPhone = null;
    target.linRecipientName = null;
    target.linAddress = null;
    target.linCreateDate = null;
    target.linOrderTypeDesc = null;
    target.linIdNo = null;
    target.linDeadlineStatus = null;
    target.linOrderCode = null;
    target.linCurrentState = null;
    target.tvDestroy = null;
    target.cbccheckIdNo = null;
    target.tvIdNo = null;

    this.target = null;
  }
}
