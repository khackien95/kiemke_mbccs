// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.gem.screen.collectinfo.custom.GroupBoxView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CollectCustomerInfoAdapter$CollectCustomerInfoViewHolder_ViewBinding<T extends CollectCustomerInfoAdapter.CollectCustomerInfoViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public CollectCustomerInfoAdapter$CollectCustomerInfoViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.groupBoxView = Utils.findRequiredViewAsType(source, R.id.groupBoxView, "field 'groupBoxView'", GroupBoxView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.groupBoxView = null;

    this.target = null;
  }
}
