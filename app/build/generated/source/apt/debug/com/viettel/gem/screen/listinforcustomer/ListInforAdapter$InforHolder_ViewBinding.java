// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.listinforcustomer;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListInforAdapter$InforHolder_ViewBinding<T extends ListInforAdapter.InforHolder> implements Unbinder {
  protected T target;

  @UiThread
  public ListInforAdapter$InforHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.mNameTv = Utils.findRequiredViewAsType(source, R.id.name_tv, "field 'mNameTv'", TextView.class);
    target.mPhoneTv = Utils.findRequiredViewAsType(source, R.id.phone_tv, "field 'mPhoneTv'", TextView.class);
    target.mDescTv = Utils.findRequiredViewAsType(source, R.id.desc_tv, "field 'mDescTv'", TextView.class);
    target.mInforIv = Utils.findRequiredViewAsType(source, R.id.infor_iv, "field 'mInforIv'", ImageView.class);
    target.mInforTv = Utils.findRequiredViewAsType(source, R.id.infor_tv, "field 'mInforTv'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mNameTv = null;
    target.mPhoneTv = null;
    target.mDescTv = null;
    target.mInforIv = null;
    target.mInforTv = null;

    this.target = null;
  }
}
