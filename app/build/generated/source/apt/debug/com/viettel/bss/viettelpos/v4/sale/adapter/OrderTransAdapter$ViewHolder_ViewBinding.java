// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.sale.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderTransAdapter$ViewHolder_ViewBinding<T extends OrderTransAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public OrderTransAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.cardView = Utils.findRequiredViewAsType(source, R.id.cardView, "field 'cardView'", CardView.class);
    target.tvOrderCode = Utils.findRequiredViewAsType(source, R.id.tvOrderCode, "field 'tvOrderCode'", TextView.class);
    target.tvStatus = Utils.findRequiredViewAsType(source, R.id.tvStatus, "field 'tvStatus'", TextView.class);
    target.tvCreateDate = Utils.findRequiredViewAsType(source, R.id.tvCreateDate, "field 'tvCreateDate'", TextView.class);
    target.tvCreateName = Utils.findRequiredViewAsType(source, R.id.tvCreateName, "field 'tvCreateName'", TextView.class);
    target.tvAcceptName = Utils.findRequiredViewAsType(source, R.id.tvAcceptName, "field 'tvAcceptName'", TextView.class);
    target.tvDestroy = Utils.findRequiredViewAsType(source, R.id.tvDestroy, "field 'tvDestroy'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.cardView = null;
    target.tvOrderCode = null;
    target.tvStatus = null;
    target.tvCreateDate = null;
    target.tvCreateName = null;
    target.tvAcceptName = null;
    target.tvDestroy = null;

    this.target = null;
  }
}
