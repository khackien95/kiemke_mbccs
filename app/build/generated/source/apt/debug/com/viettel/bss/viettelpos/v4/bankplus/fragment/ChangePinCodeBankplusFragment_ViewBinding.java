// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.bankplus.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChangePinCodeBankplusFragment_ViewBinding<T extends ChangePinCodeBankplusFragment> implements Unbinder {
  protected T target;

  @UiThread
  public ChangePinCodeBankplusFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.edtPinOld = Utils.findRequiredViewAsType(source, R.id.edtPinOld, "field 'edtPinOld'", EditText.class);
    target.edtNewPin = Utils.findRequiredViewAsType(source, R.id.edtNewPin, "field 'edtNewPin'", EditText.class);
    target.edtConfirmPin = Utils.findRequiredViewAsType(source, R.id.edtConfirmPin, "field 'edtConfirmPin'", EditText.class);
    target.btnChangePin = Utils.findRequiredViewAsType(source, R.id.btnChangePin, "field 'btnChangePin'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtPinOld = null;
    target.edtNewPin = null;
    target.edtConfirmPin = null;
    target.btnChangePin = null;

    this.target = null;
  }
}
