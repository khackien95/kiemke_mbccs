// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.advisory.screen;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SearchAdvisoryFragment_ViewBinding<T extends SearchAdvisoryFragment> implements Unbinder {
  protected T target;

  @UiThread
  public SearchAdvisoryFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.searchButton = Utils.findRequiredViewAsType(source, R.id.searchButton, "field 'searchButton'", Button.class);
    target.edtNumberValue = Utils.findRequiredViewAsType(source, R.id.edtNumberValue, "field 'edtNumberValue'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.searchButton = null;
    target.edtNumberValue = null;

    this.target = null;
  }
}
