// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.plan.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PlanListAdapter$SalePlansHolder_ViewBinding<T extends PlanListAdapter.SalePlansHolder> implements Unbinder {
  protected T target;

  @UiThread
  public PlanListAdapter$SalePlansHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvDate = Utils.findRequiredViewAsType(source, R.id.tvDate, "field 'tvDate'", TextView.class);
    target.tvAddress = Utils.findRequiredViewAsType(source, R.id.tvAddress, "field 'tvAddress'", TextView.class);
    target.tvHaTang = Utils.findRequiredViewAsType(source, R.id.tvHaTang, "field 'tvHaTang'", TextView.class);
    target.imvDelete = Utils.findRequiredViewAsType(source, R.id.imvDelete, "field 'imvDelete'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvDate = null;
    target.tvAddress = null;
    target.tvHaTang = null;
    target.imvDelete = null;

    this.target = null;
  }
}
