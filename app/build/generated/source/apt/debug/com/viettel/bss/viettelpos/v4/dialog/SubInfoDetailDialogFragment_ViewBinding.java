// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.dialog;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubInfoDetailDialogFragment_ViewBinding<T extends SubInfoDetailDialogFragment> implements Unbinder {
  protected T target;

  @UiThread
  public SubInfoDetailDialogFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.lv_accountList = Utils.findRequiredViewAsType(source, R.id.lv_accountList, "field 'lv_accountList'", ListView.class);
    target.imgViewClose = Utils.findRequiredViewAsType(source, R.id.imgViewClose, "field 'imgViewClose'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.lv_accountList = null;
    target.imgViewClose = null;

    this.target = null;
  }
}
