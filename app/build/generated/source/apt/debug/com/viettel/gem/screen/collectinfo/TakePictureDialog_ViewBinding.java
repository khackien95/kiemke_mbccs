// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TakePictureDialog_ViewBinding<T extends TakePictureDialog> implements Unbinder {
  protected T target;

  private View view2131756552;

  private View view2131756551;

  @UiThread
  public TakePictureDialog_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.from_camera, "method 'fromCam'");
    view2131756552 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.fromCam();
      }
    });
    view = Utils.findRequiredView(source, R.id.from_galary, "method 'fromGal'");
    view2131756551 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.fromGal();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (this.target == null) throw new IllegalStateException("Bindings already cleared.");

    view2131756552.setOnClickListener(null);
    view2131756552 = null;
    view2131756551.setOnClickListener(null);
    view2131756551 = null;

    this.target = null;
  }
}
