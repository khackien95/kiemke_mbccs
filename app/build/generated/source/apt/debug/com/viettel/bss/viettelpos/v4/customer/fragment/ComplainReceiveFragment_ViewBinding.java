// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.customer.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.connecttionService.listener.ExpandableHeightListView;
import com.viettel.bss.viettelpos.v4.customview.CustomAutoCompleteTextView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ComplainReceiveFragment_ViewBinding<T extends ComplainReceiveFragment> implements Unbinder {
  protected T target;

  private View view2131756640;

  private View view2131756642;

  private View view2131756643;

  private View view2131756648;

  private View view2131756649;

  private View view2131756650;

  private View view2131756656;

  private View view2131756658;

  private View view2131756628;

  private View view2131756635;

  private View view2131756644;

  private View view2131756652;

  private View view2131756626;

  @UiThread
  public ComplainReceiveFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.edtIdNo = Utils.findRequiredViewAsType(source, R.id.edtIdNo, "field 'edtIdNo'", EditText.class);
    target.edtIsdnComplain = Utils.findRequiredViewAsType(source, R.id.edtIsdnComplain, "field 'edtIsdnComplain'", EditText.class);
    target.lnComplain = Utils.findRequiredViewAsType(source, R.id.lnComplain, "field 'lnComplain'", LinearLayout.class);
    target.spnPriorityLevel = Utils.findRequiredViewAsType(source, R.id.spnPriorityLevel, "field 'spnPriorityLevel'", Spinner.class);
    view = Utils.findRequiredView(source, R.id.spnGroupComplaint, "field 'spnGroupComplaint' and method 'spnGroupComplaintSelect'");
    target.spnGroupComplaint = Utils.castView(view, R.id.spnGroupComplaint, "field 'spnGroupComplaint'", Spinner.class);
    view2131756640 = view;
    ((AdapterView<?>) view).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> p0, View p1, int p2, long p3) {
        target.spnGroupComplaintSelect();
      }

      @Override
      public void onNothingSelected(AdapterView<?> p0) {
      }
    });
    view = Utils.findRequiredView(source, R.id.spnTheLoai, "field 'spnTheLoai' and method 'spnTheLoaiSelect'");
    target.spnTheLoai = Utils.castView(view, R.id.spnTheLoai, "field 'spnTheLoai'", Spinner.class);
    view2131756642 = view;
    ((AdapterView<?>) view).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> p0, View p1, int p2, long p3) {
        target.spnTheLoaiSelect();
      }

      @Override
      public void onNothingSelected(AdapterView<?> p0) {
      }
    });
    view = Utils.findRequiredView(source, R.id.spnType, "field 'spnType' and method 'spnTypeSelect'");
    target.spnType = Utils.castView(view, R.id.spnType, "field 'spnType'", Spinner.class);
    view2131756643 = view;
    ((AdapterView<?>) view).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> p0, View p1, int p2, long p3) {
        target.spnTypeSelect();
      }

      @Override
      public void onNothingSelected(AdapterView<?> p0) {
      }
    });
    view = Utils.findRequiredView(source, R.id.edtProvince, "field 'edtProvince' and method 'edtProvinceOnClick'");
    target.edtProvince = Utils.castView(view, R.id.edtProvince, "field 'edtProvince'", EditText.class);
    view2131756648 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.edtProvinceOnClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.edtDistrict, "field 'edtDistrict' and method 'edtDistrictOnClick'");
    target.edtDistrict = Utils.castView(view, R.id.edtDistrict, "field 'edtDistrict'", EditText.class);
    view2131756649 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.edtDistrictOnClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.edtPrecint, "field 'edtPrecint' and method 'edtPrecintOnClick'");
    target.edtPrecint = Utils.castView(view, R.id.edtPrecint, "field 'edtPrecint'", EditText.class);
    view2131756650 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.edtPrecintOnClick();
      }
    });
    target.edtDateAppoint = Utils.findRequiredViewAsType(source, R.id.edtDateAppoint, "field 'edtDateAppoint'", EditText.class);
    target.lvFileAttack = Utils.findRequiredViewAsType(source, R.id.lvFileAttack, "field 'lvFileAttack'", ExpandableHeightListView.class);
    view = Utils.findRequiredView(source, R.id.lnAddFileAttack, "field 'lnAddFileAttack' and method 'lnAddFileAttackOnClick'");
    target.lnAddFileAttack = Utils.castView(view, R.id.lnAddFileAttack, "field 'lnAddFileAttack'", LinearLayout.class);
    view2131756656 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.lnAddFileAttackOnClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.btnComplain, "field 'btnComplain' and method 'btnComplainOnClick'");
    target.btnComplain = Utils.castView(view, R.id.btnComplain, "field 'btnComplain'", Button.class);
    view2131756658 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.btnComplainOnClick();
      }
    });
    target.edtProblemContent = Utils.findRequiredViewAsType(source, R.id.edtProblemContent, "field 'edtProblemContent'", EditText.class);
    target.edtAddress = Utils.findRequiredViewAsType(source, R.id.edtAddress, "field 'edtAddress'", EditText.class);
    target.edtIsdnContact = Utils.findRequiredViewAsType(source, R.id.edtIsdnContact, "field 'edtIsdnContact'", EditText.class);
    target.edtEmail = Utils.findRequiredViewAsType(source, R.id.edtEmail, "field 'edtEmail'", EditText.class);
    target.edtNote = Utils.findRequiredViewAsType(source, R.id.edtNote, "field 'edtNote'", EditText.class);
    target.edtCustComplain = Utils.findRequiredViewAsType(source, R.id.edtCustComplain, "field 'edtCustComplain'", EditText.class);
    target.expanInfoCustComplain = Utils.findRequiredViewAsType(source, R.id.expanInfoCustComplain, "field 'expanInfoCustComplain'", ExpandableLinearLayout.class);
    target.expanInfoComplain = Utils.findRequiredViewAsType(source, R.id.expanInfoComplain, "field 'expanInfoComplain'", ExpandableLinearLayout.class);
    target.expanInfoProcess = Utils.findRequiredViewAsType(source, R.id.expanInfoProcess, "field 'expanInfoProcess'", ExpandableLinearLayout.class);
    target.expanInfoAddition = Utils.findRequiredViewAsType(source, R.id.expanInfoAddition, "field 'expanInfoAddition'", ExpandableLinearLayout.class);
    view = Utils.findRequiredView(source, R.id.lnInfoCustComplain, "field 'lnInfoCustComplain' and method 'lnInfoCustComplainOnClick'");
    target.lnInfoCustComplain = Utils.castView(view, R.id.lnInfoCustComplain, "field 'lnInfoCustComplain'", LinearLayout.class);
    view2131756628 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.lnInfoCustComplainOnClick();
      }
    });
    target.acImgViewInfoCustComplain = Utils.findRequiredViewAsType(source, R.id.acImgViewInfoCustComplain, "field 'acImgViewInfoCustComplain'", AppCompatImageView.class);
    view = Utils.findRequiredView(source, R.id.lnInfoComplain, "field 'lnInfoComplain' and method 'lnInfoComplainOnClick'");
    target.lnInfoComplain = Utils.castView(view, R.id.lnInfoComplain, "field 'lnInfoComplain'", LinearLayout.class);
    view2131756635 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.lnInfoComplainOnClick();
      }
    });
    target.acImgViewInfoComplain = Utils.findRequiredViewAsType(source, R.id.acImgViewInfoComplain, "field 'acImgViewInfoComplain'", AppCompatImageView.class);
    view = Utils.findRequiredView(source, R.id.lnInfoProcess, "field 'lnInfoProcess' and method 'lnInfoProcessOnClick'");
    target.lnInfoProcess = Utils.castView(view, R.id.lnInfoProcess, "field 'lnInfoProcess'", LinearLayout.class);
    view2131756644 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.lnInfoProcessOnClick();
      }
    });
    target.acImgViewInfoProcess = Utils.findRequiredViewAsType(source, R.id.acImgViewInfoProcess, "field 'acImgViewInfoProcess'", AppCompatImageView.class);
    view = Utils.findRequiredView(source, R.id.lnInfoAddition, "field 'lnInfoAddition' and method 'lnInfoAdditionOnClick'");
    target.lnInfoAddition = Utils.castView(view, R.id.lnInfoAddition, "field 'lnInfoAddition'", LinearLayout.class);
    view2131756652 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.lnInfoAdditionOnClick();
      }
    });
    target.acImgViewInfoAddition = Utils.findRequiredViewAsType(source, R.id.acImgViewInfoAddition, "field 'acImgViewInfoAddition'", AppCompatImageView.class);
    target.actvGroupComplaint = Utils.findRequiredViewAsType(source, R.id.actvGroupComplaint, "field 'actvGroupComplaint'", CustomAutoCompleteTextView.class);
    view = Utils.findRequiredView(source, R.id.imgViewSearch, "method 'searchSubscriberComplain'");
    view2131756626 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.searchSubscriberComplain();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtIdNo = null;
    target.edtIsdnComplain = null;
    target.lnComplain = null;
    target.spnPriorityLevel = null;
    target.spnGroupComplaint = null;
    target.spnTheLoai = null;
    target.spnType = null;
    target.edtProvince = null;
    target.edtDistrict = null;
    target.edtPrecint = null;
    target.edtDateAppoint = null;
    target.lvFileAttack = null;
    target.lnAddFileAttack = null;
    target.btnComplain = null;
    target.edtProblemContent = null;
    target.edtAddress = null;
    target.edtIsdnContact = null;
    target.edtEmail = null;
    target.edtNote = null;
    target.edtCustComplain = null;
    target.expanInfoCustComplain = null;
    target.expanInfoComplain = null;
    target.expanInfoProcess = null;
    target.expanInfoAddition = null;
    target.lnInfoCustComplain = null;
    target.acImgViewInfoCustComplain = null;
    target.lnInfoComplain = null;
    target.acImgViewInfoComplain = null;
    target.lnInfoProcess = null;
    target.acImgViewInfoProcess = null;
    target.lnInfoAddition = null;
    target.acImgViewInfoAddition = null;
    target.actvGroupComplaint = null;

    ((AdapterView<?>) view2131756640).setOnItemSelectedListener(null);
    view2131756640 = null;
    ((AdapterView<?>) view2131756642).setOnItemSelectedListener(null);
    view2131756642 = null;
    ((AdapterView<?>) view2131756643).setOnItemSelectedListener(null);
    view2131756643 = null;
    view2131756648.setOnClickListener(null);
    view2131756648 = null;
    view2131756649.setOnClickListener(null);
    view2131756649 = null;
    view2131756650.setOnClickListener(null);
    view2131756650 = null;
    view2131756656.setOnClickListener(null);
    view2131756656 = null;
    view2131756658.setOnClickListener(null);
    view2131756658 = null;
    view2131756628.setOnClickListener(null);
    view2131756628 = null;
    view2131756635.setOnClickListener(null);
    view2131756635 = null;
    view2131756644.setOnClickListener(null);
    view2131756644 = null;
    view2131756652.setOnClickListener(null);
    view2131756652 = null;
    view2131756626.setOnClickListener(null);
    view2131756626 = null;

    this.target = null;
  }
}
