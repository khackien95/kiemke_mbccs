// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.stockinpect.view;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StockInpectWithSerialActivity_ViewBinding<T extends StockInpectWithSerialActivity> implements Unbinder {
  protected T target;

  @UiThread
  public StockInpectWithSerialActivity_ViewBinding(T target, View source) {
    this.target = target;

    target.lnNumSerial = Utils.findRequiredViewAsType(source, R.id.lnNumSerial, "field 'lnNumSerial'", LinearLayout.class);
    target.txtserial = Utils.findRequiredViewAsType(source, R.id.txtserial, "field 'txtserial'", EditText.class);
    target.btnbar = Utils.findRequiredViewAsType(source, R.id.btnbar, "field 'btnbar'", Button.class);
    target.lnFromSerial = Utils.findRequiredViewAsType(source, R.id.lnFromSerial, "field 'lnFromSerial'", LinearLayout.class);
    target.txtSerialFrom = Utils.findRequiredViewAsType(source, R.id.txtSerialFrom, "field 'txtSerialFrom'", EditText.class);
    target.btnbarFrom = Utils.findRequiredViewAsType(source, R.id.btnbarFrom, "field 'btnbarFrom'", Button.class);
    target.lnToSerial = Utils.findRequiredViewAsType(source, R.id.lnToSerial, "field 'lnToSerial'", LinearLayout.class);
    target.txtSerialTo = Utils.findRequiredViewAsType(source, R.id.txtSerialTo, "field 'txtSerialTo'", EditText.class);
    target.btnbarTo = Utils.findRequiredViewAsType(source, R.id.btnbarTo, "field 'btnbarTo'", Button.class);
    target.btnAddSerial = Utils.findRequiredViewAsType(source, R.id.btnAddSerial, "field 'btnAddSerial'", Button.class);
    target.quantiy = Utils.findRequiredViewAsType(source, R.id.quantity, "field 'quantiy'", TextView.class);
    target.recListSerial = Utils.findRequiredViewAsType(source, R.id.recListSerial, "field 'recListSerial'", RecyclerView.class);
    target.confirm_inspect_serial = Utils.findRequiredViewAsType(source, R.id.confirm_inspect_serial, "field 'confirm_inspect_serial'", CheckBox.class);
    target.btnInspect = Utils.findRequiredViewAsType(source, R.id.btnInspect, "field 'btnInspect'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.lnNumSerial = null;
    target.txtserial = null;
    target.btnbar = null;
    target.lnFromSerial = null;
    target.txtSerialFrom = null;
    target.btnbarFrom = null;
    target.lnToSerial = null;
    target.txtSerialTo = null;
    target.btnbarTo = null;
    target.btnAddSerial = null;
    target.quantiy = null;
    target.recListSerial = null;
    target.confirm_inspect_serial = null;
    target.btnInspect = null;

    this.target = null;
  }
}
