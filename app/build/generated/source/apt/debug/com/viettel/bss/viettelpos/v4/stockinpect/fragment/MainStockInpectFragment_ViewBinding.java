// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.stockinpect.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainStockInpectFragment_ViewBinding<T extends MainStockInpectFragment> implements Unbinder {
  protected T target;

  @UiThread
  public MainStockInpectFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.rbNumber = Utils.findRequiredViewAsType(source, R.id.rbNumber, "field 'rbNumber'", RadioButton.class);
    target.rbSerial = Utils.findRequiredViewAsType(source, R.id.rbSerial, "field 'rbSerial'", RadioButton.class);
    target.btnCheck = Utils.findRequiredViewAsType(source, R.id.btnCheck, "field 'btnCheck'", Button.class);
    target.spStatusInpect = Utils.findRequiredViewAsType(source, R.id.spStatusInpect, "field 'spStatusInpect'", Spinner.class);
    target.spStock = Utils.findRequiredViewAsType(source, R.id.spStock, "field 'spStock'", Spinner.class);
    target.spStateStock = Utils.findRequiredViewAsType(source, R.id.spStateStock, "field 'spStateStock'", Spinner.class);
    target.spTypeStock = Utils.findRequiredViewAsType(source, R.id.spTypeStock, "field 'spTypeStock'", Spinner.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.rbNumber = null;
    target.rbSerial = null;
    target.btnCheck = null;
    target.spStatusInpect = null;
    target.spStock = null;
    target.spStateStock = null;
    target.spTypeStock = null;

    this.target = null;
  }
}
