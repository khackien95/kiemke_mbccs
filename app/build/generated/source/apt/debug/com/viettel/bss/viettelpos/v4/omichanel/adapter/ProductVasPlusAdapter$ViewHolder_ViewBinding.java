// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductVasPlusAdapter$ViewHolder_ViewBinding<T extends ProductVasPlusAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public ProductVasPlusAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.llContent = Utils.findRequiredViewAsType(source, R.id.llContent, "field 'llContent'", LinearLayout.class);
    target.btnDetail = Utils.findRequiredViewAsType(source, R.id.btnDetail, "field 'btnDetail'", Button.class);
    target.tvCode = Utils.findRequiredViewAsType(source, R.id.tvCode, "field 'tvCode'", TextView.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
    target.tvPrice = Utils.findRequiredViewAsType(source, R.id.tvPrice, "field 'tvPrice'", TextView.class);
    target.cbSelectState = Utils.findRequiredViewAsType(source, R.id.cbSelectState, "field 'cbSelectState'", CheckBox.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.llContent = null;
    target.btnDetail = null;
    target.tvCode = null;
    target.tvName = null;
    target.tvPrice = null;
    target.cbSelectState = null;

    this.target = null;
  }
}
