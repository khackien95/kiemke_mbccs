// Generated code from Butter Knife. Do not modify!
package com.viettel.bccs2.viettelpos.v2.connecttionMobile.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ActivitySelectCustomer_ViewBinding<T extends ActivitySelectCustomer> implements Unbinder {
  protected T target;

  @UiThread
  public ActivitySelectCustomer_ViewBinding(T target, View source) {
    this.target = target;

    target.edtloaikh = Utils.findRequiredViewAsType(source, R.id.edtloaikh, "field 'edtloaikh'", EditText.class);
    target.spinner_type_giay_to = Utils.findRequiredViewAsType(source, R.id.spinner_type_giay_to, "field 'spinner_type_giay_to'", Spinner.class);
    target.edit_socmnd = Utils.findRequiredViewAsType(source, R.id.edit_socmnd, "field 'edit_socmnd'", EditText.class);
    target.btnkiemtra = Utils.findRequiredViewAsType(source, R.id.btnkiemtra, "field 'btnkiemtra'", Button.class);
    target.btnedit = Utils.findRequiredViewAsType(source, R.id.btnedit, "field 'btnedit'", Button.class);
    target.edit_tenKH = Utils.findRequiredViewAsType(source, R.id.edit_tenKH, "field 'edit_tenKH'", EditText.class);
    target.edit_ngaysinh = Utils.findRequiredViewAsType(source, R.id.edit_ngaysinh, "field 'edit_ngaysinh'", EditText.class);
    target.spinner_gioitinh = Utils.findRequiredViewAsType(source, R.id.spinner_gioitinh, "field 'spinner_gioitinh'", Spinner.class);
    target.edit_ngaycap = Utils.findRequiredViewAsType(source, R.id.edit_ngaycap, "field 'edit_ngaycap'", EditText.class);
    target.lnngayhethan = Utils.findRequiredViewAsType(source, R.id.lnngayhethan, "field 'lnngayhethan'", LinearLayout.class);
    target.edit_ngayhethan = Utils.findRequiredViewAsType(source, R.id.editngayhethan, "field 'edit_ngayhethan'", EditText.class);
    target.edit_noicap = Utils.findRequiredViewAsType(source, R.id.edit_noicap, "field 'edit_noicap'", EditText.class);
    target.spinner_quoctichpr = Utils.findRequiredViewAsType(source, R.id.spinner_quoctichpr, "field 'spinner_quoctichpr'", Spinner.class);
    target.lnAddressCusPr = Utils.findRequiredViewAsType(source, R.id.lnAddressCusPr, "field 'lnAddressCusPr'", LinearLayout.class);
    target.edtdiachilapdat = Utils.findRequiredViewAsType(source, R.id.edtdiachilapdat, "field 'edtdiachilapdat'", EditText.class);
    target.btncancel = Utils.findRequiredViewAsType(source, R.id.btncancel, "field 'btncancel'", Button.class);
    target.btnthemmoi = Utils.findRequiredViewAsType(source, R.id.btnthemmoi, "field 'btnthemmoi'", Button.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.prbTypePaperPR = Utils.findRequiredViewAsType(source, R.id.prbTypePaper, "field 'prbTypePaperPR'", ProgressBar.class);
    target.btnRefreshTypePR = Utils.findRequiredViewAsType(source, R.id.btnRefreshTypePaper, "field 'btnRefreshTypePR'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtloaikh = null;
    target.spinner_type_giay_to = null;
    target.edit_socmnd = null;
    target.btnkiemtra = null;
    target.btnedit = null;
    target.edit_tenKH = null;
    target.edit_ngaysinh = null;
    target.spinner_gioitinh = null;
    target.edit_ngaycap = null;
    target.lnngayhethan = null;
    target.edit_ngayhethan = null;
    target.edit_noicap = null;
    target.spinner_quoctichpr = null;
    target.lnAddressCusPr = null;
    target.edtdiachilapdat = null;
    target.btncancel = null;
    target.btnthemmoi = null;
    target.toolbar = null;
    target.prbTypePaperPR = null;
    target.btnRefreshTypePR = null;

    this.target = null;
  }
}
