// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.plan.screen;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.github.clans.fab.FloatingActionMenu;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SalePlanFragment_ViewBinding<T extends SalePlanFragment> implements Unbinder {
  protected T target;

  private View view2131755412;

  private View view2131755413;

  private View view2131755414;

  @UiThread
  public SalePlanFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.edtFromDate, "field 'edtFromDate' and method 'clickFromDate'");
    target.edtFromDate = Utils.castView(view, R.id.edtFromDate, "field 'edtFromDate'", EditText.class);
    view2131755412 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.clickFromDate();
      }
    });
    view = Utils.findRequiredView(source, R.id.edtToDate, "field 'edtToDate' and method 'clickToDate'");
    target.edtToDate = Utils.castView(view, R.id.edtToDate, "field 'edtToDate'", EditText.class);
    view2131755413 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.clickToDate();
      }
    });
    target.rcvListSalePlan = Utils.findRequiredViewAsType(source, R.id.listSalePlan, "field 'rcvListSalePlan'", SuperRecyclerView.class);
    target.fabActionMenu = Utils.findRequiredViewAsType(source, R.id.fabActionMenu, "field 'fabActionMenu'", FloatingActionMenu.class);
    view = Utils.findRequiredView(source, R.id.btnSearch, "method 'search'");
    view2131755414 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.search();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.edtFromDate = null;
    target.edtToDate = null;
    target.rcvListSalePlan = null;
    target.fabActionMenu = null;

    view2131755412.setOnClickListener(null);
    view2131755412 = null;
    view2131755413.setOnClickListener(null);
    view2131755413 = null;
    view2131755414.setOnClickListener(null);
    view2131755414 = null;

    this.target = null;
  }
}
