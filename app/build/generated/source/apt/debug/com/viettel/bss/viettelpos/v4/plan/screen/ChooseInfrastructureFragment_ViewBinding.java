// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.plan.screen;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ExpandableListView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChooseInfrastructureFragment_ViewBinding<T extends ChooseInfrastructureFragment> implements Unbinder {
  protected T target;

  @UiThread
  public ChooseInfrastructureFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.listBts = Utils.findRequiredViewAsType(source, R.id.listBts, "field 'listBts'", ExpandableListView.class);
    target.mFlooatSave = Utils.findRequiredViewAsType(source, R.id.float_save, "field 'mFlooatSave'", FloatingActionButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.listBts = null;
    target.mFlooatSave = null;

    this.target = null;
  }
}
