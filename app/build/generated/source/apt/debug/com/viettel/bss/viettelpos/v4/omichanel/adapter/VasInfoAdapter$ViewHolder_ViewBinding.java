// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class VasInfoAdapter$ViewHolder_ViewBinding<T extends VasInfoAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public VasInfoAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvVasName = Utils.findRequiredViewAsType(source, R.id.tvVasName, "field 'tvVasName'", TextView.class);
    target.tvVasPrice = Utils.findRequiredViewAsType(source, R.id.tvVasPrice, "field 'tvVasPrice'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvVasName = null;
    target.tvVasPrice = null;

    this.target = null;
  }
}
