// Generated code from Butter Knife. Do not modify!
package com.viettel.bccs2.viettelpos.v2.connecttionMobile.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FragmentChooseCusGroupBCCS_ViewBinding<T extends FragmentChooseCusGroupBCCS> implements Unbinder {
  protected T target;

  @UiThread
  public FragmentChooseCusGroupBCCS_ViewBinding(T target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtinfo = Utils.findRequiredViewAsType(source, R.id.txtinfo, "field 'txtinfo'", TextView.class);
    target.btnRefresh = Utils.findRequiredViewAsType(source, R.id.btnRefreshStreetBlock, "field 'btnRefresh'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.toolbar = null;
    target.txtinfo = null;
    target.btnRefresh = null;

    this.target = null;
  }
}
