// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.omichanel.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TaskResultOptionAdapter$ViewHolder_ViewBinding<T extends TaskResultOptionAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public TaskResultOptionAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.llContent = Utils.findRequiredViewAsType(source, R.id.llContent, "field 'llContent'", LinearLayout.class);
    target.rbOptionSelect = Utils.findRequiredViewAsType(source, R.id.rbOptionSelect, "field 'rbOptionSelect'", RadioButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.llContent = null;
    target.rbOptionSelect = null;

    this.target = null;
  }
}
