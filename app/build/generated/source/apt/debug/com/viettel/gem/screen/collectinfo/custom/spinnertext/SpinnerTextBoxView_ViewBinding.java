// Generated code from Butter Knife. Do not modify!
package com.viettel.gem.screen.collectinfo.custom.spinnertext;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SpinnerTextBoxView_ViewBinding<T extends SpinnerTextBoxView> implements Unbinder {
  protected T target;

  private View view2131756290;

  private View view2131756292;

  @UiThread
  public SpinnerTextBoxView_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
    target.boxSpinner = Utils.findRequiredViewAsType(source, R.id.boxSpinner, "field 'boxSpinner'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.imvAdd, "method 'addRow'");
    view2131756290 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.addRow();
      }
    });
    view = Utils.findRequiredView(source, R.id.tvAdd, "method 'addRow'");
    view2131756292 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.addRow();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvName = null;
    target.boxSpinner = null;

    view2131756290.setOnClickListener(null);
    view2131756290 = null;
    view2131756292.setOnClickListener(null);
    view2131756292 = null;

    this.target = null;
  }
}
