// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.bankplus.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RecyclerViewTransactionAdapter$ViewHolder_ViewBinding<T extends RecyclerViewTransactionAdapter.ViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public RecyclerViewTransactionAdapter$ViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.txtIdTrans = Utils.findRequiredViewAsType(source, R.id.txtIdTrans, "field 'txtIdTrans'", TextView.class);
    target.txtMoney = Utils.findRequiredViewAsType(source, R.id.txtMoney, "field 'txtMoney'", TextView.class);
    target.txtPhone = Utils.findRequiredViewAsType(source, R.id.txtPhone, "field 'txtPhone'", TextView.class);
    target.txtAddress = Utils.findRequiredViewAsType(source, R.id.txtAddress, "field 'txtAddress'", TextView.class);
    target.txtExpire = Utils.findRequiredViewAsType(source, R.id.txtExpire, "field 'txtExpire'", TextView.class);
    target.txtStatus = Utils.findRequiredViewAsType(source, R.id.txStatus, "field 'txtStatus'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.txtIdTrans = null;
    target.txtMoney = null;
    target.txtPhone = null;
    target.txtAddress = null;
    target.txtExpire = null;
    target.txtStatus = null;

    this.target = null;
  }
}
