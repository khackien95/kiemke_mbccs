// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.staff.work;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WorkListActivity_ViewBinding<T extends WorkListActivity> implements Unbinder {
  protected T target;

  private View view2131755283;

  private View view2131755285;

  private View view2131755168;

  @UiThread
  public WorkListActivity_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.mRVList = Utils.findRequiredViewAsType(source, R.id.rv_list, "field 'mRVList'", RecyclerView.class);
    target.mTVFromDate = Utils.findRequiredViewAsType(source, R.id.tv_from_date, "field 'mTVFromDate'", TextView.class);
    target.mTVToDate = Utils.findRequiredViewAsType(source, R.id.tv_to_date, "field 'mTVToDate'", TextView.class);
    target.mSpStatus = Utils.findRequiredViewAsType(source, R.id.sp_status, "field 'mSpStatus'", AppCompatSpinner.class);
    target.lnResult = Utils.findRequiredViewAsType(source, R.id.lnResult, "field 'lnResult'", LinearLayout.class);
    target.edtWoCode = Utils.findRequiredViewAsType(source, R.id.edtWoCode, "field 'edtWoCode'", EditText.class);
    view = Utils.findRequiredView(source, R.id.ll_from_date, "method 'clickFromDate'");
    view2131755283 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.clickFromDate(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ll_to_date, "method 'clickToDate'");
    view2131755285 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.clickToDate(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_search, "method 'onSearch'");
    view2131755168 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onSearch(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mRVList = null;
    target.mTVFromDate = null;
    target.mTVToDate = null;
    target.mSpStatus = null;
    target.lnResult = null;
    target.edtWoCode = null;

    view2131755283.setOnClickListener(null);
    view2131755283 = null;
    view2131755285.setOnClickListener(null);
    view2131755285 = null;
    view2131755168.setOnClickListener(null);
    view2131755168 = null;

    this.target = null;
  }
}
