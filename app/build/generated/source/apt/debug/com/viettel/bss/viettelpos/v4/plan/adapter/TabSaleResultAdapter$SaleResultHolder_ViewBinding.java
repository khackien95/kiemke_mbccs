// Generated code from Butter Knife. Do not modify!
package com.viettel.bss.viettelpos.v4.plan.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.viettel.bss.viettelpos.v4.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TabSaleResultAdapter$SaleResultHolder_ViewBinding<T extends TabSaleResultAdapter.SaleResultHolder> implements Unbinder {
  protected T target;

  @UiThread
  public TabSaleResultAdapter$SaleResultHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.tvIndex = Utils.findRequiredViewAsType(source, R.id.tvIndex, "field 'tvIndex'", TextView.class);
    target.tvAddress = Utils.findRequiredViewAsType(source, R.id.tvAddress, "field 'tvAddress'", TextView.class);
    target.tvPlanDate = Utils.findRequiredViewAsType(source, R.id.tvPlanDate, "field 'tvPlanDate'", TextView.class);
    target.tvPoinCode = Utils.findRequiredViewAsType(source, R.id.tvPoinCode, "field 'tvPoinCode'", TextView.class);
    target.tvPoinName = Utils.findRequiredViewAsType(source, R.id.tvPoinName, "field 'tvPoinName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.tvIndex = null;
    target.tvAddress = null;
    target.tvPlanDate = null;
    target.tvPoinCode = null;
    target.tvPoinName = null;

    this.target = null;
  }
}
